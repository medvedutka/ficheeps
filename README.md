# FicheEPS
Un logiciel de gestion de fiche de suivie pluriannuelle pour l'EPS.

## Description
Le Logiciciel FicheEPS permet la saisie de fiche de suivie pluriannuelle pour l'ensemble d'une base élève.

## Installation et configuration

### Utilisation de l'installateur sous Windows
Un installateur pour les sytème Windows est fournis. Vous n'avez qu'à le télécharger puis l'éxécuter pour installer FicheEPS sur votre ordinateur.

### Installation sur un serveur (Linux/Windows)
FicheEPS est une application Web s'éxécutant sur un environement de type 'Servlet Container' comme Tomcat.
L'archive war pré-compilé est disponible en téléchargement; Vous n'avez qu'à la déployer.


## Fichier de Configuration
Un unique fichier de configuration permet de spécifier toutes les informations. Vous pourrez préciser:

* Le repertoire contenant toutes les données (base de données, sauvegardes, photos)
* Le mot de passe de l'utilisateur 'admin'
* Le mot de passe de la base de donnée

## Developpement
* Utilise le mode "super dev" de GWT
* Ouvrir dans Netbeans
* Lancer dans Netbeans
* On accède à l'application dans le navigateur
* Depuis une ligne de commande, lancer "mvn gwt:run-codeserver"

* Comment cr�er un installateur ? *

1) S'assurer que:
   - FicheEPS.war, dans web.xml, contient la d�finition de la variable 'ficheepsConfigurationFile' avec comme valeur ${ficheepsConfigurationFile}.
   - Lee fichier ficheEPS.config contient le tag 'FICHEEPS_DATAFOLDER' � la ligne:
     dataFolder=FICHEEPS_DATAFOLDER

2) Placez dans le repertoire "dist\" tous les composants suivants:
     dist\FicheEPS.exe -> FicheEPS._xe copi� et renomm�
     dist\FicheEPS.war -> L'archive compil� de FicheEPS
     dist\lib\jre -> le jre d�compr�ss�
     dist\lib\res -> Le repertoire "res\" copi�
     dist\FicheEPS\ficheEPS.config -> Le fichier template "ficheEPS.config" copi�
     dist\FicheEPS\apache-tomcat -> tomcat d�compr�ss�     


3) Utilisez InnoSetup avec 'FicheEPS_setup.iss' pour g�n�rer l'installateur.


* Description des fichiers *

dist\
	Un repertoire contenant tous les composant � empaqueter dans l'installateur

dist-setup\
	Le repertoire contenant l'installateur g�n�r�

build.xml
	Script 'ant' permettant la cr�ation semi-automatique de dist\

FicheEPS_setup.iss:
	Script InnoSetup permettant la g�n�ration de l'installateur

res\
	Differents icones utilis� par l'exe

FicheEPS.ahk
	Script Autohotkey permettant de cr�er une icone dans la barre des taches incluant un menu contextuel pour ouvrir le navigateur ou arreter FicheEPS. Il peut etre compil� avec la commande:
	       Ahk2Exe.exe /in FicheEPS.ahk /out FicheEPS.exe /icon res\favicon.ico

ficheEPS.config:
	Mod�le de fichier de configuration

FicheEPS._xe:
	L'�x�cutable pr�-compil� g�n�r� par Autohotkey � partir du script "FicheEPS.ahk"
	Il est n�cessaire de le renomer en .exe

;Autohotkey script to create windows executable displaying systray icon to run/open/stop FicheEPS (tomcat)
;Assume the folder structure:
;
;FicheEPS.exe
;    /FicheEPS/
;    /lib/
;        res/
;        jre/
;        config.ini
;
;     apache-tomcat/

#Persistent
#SingleInstance ignore


readProperties()
{
    Loop, read, %A_WorkingDir%\lib\config.ini
    {
	Line := A_LoopReadLine
	Pos := RegExMatch(Line, "^\s*#")
	;MsgBox, Pos %Pos%
	If Pos = 0
  	{
	   Pos := InStr(Line, "=")
           If Pos > 1
           {
		Key := Trim( SubStr(Line, 1, Pos-1) )
		Value := Trim( SubStr(Line, Pos+1) )
	        ;Tokens := StrSplit(Line, "=")
	        ;Key := Tokens[1]
	        ;Value := Tokens[2]
		;MsgBox, %Key%  %Value%
	        EnvSet, %Key%, %Value%
            }
        }
    }
    ;EnvGet, javahome, JAVA_HOME
    ;EnvGet, catalinahome, CATALINA_HOME
    ;EnvGet, fcf, ficheepsConfigurationFile
    ;MsgBox, %fcf% %javahome% %catalinahome%
}


Menu, Tray, NoStandard
Menu Tray, Icon, lib\res\favicon.ico
Menu, Tray, Tip, FicheEPS
Menu, Tray, Add, D�marrer FicheEPS, StartFicheEPS
Menu, Tray, Icon, D�marrer FicheEPS, lib\res\start3.ico
Gosub, StartFicheEPS
Gosub, StartBrowser
;Menu, Tray, Click, 1 ;Remove this line to require double click
Return

StartFicheEPS:
EnvSet, JAVA_HOME, %A_WorkingDir%\lib\jre
EnvSet, CATALINA_HOME, %A_WorkingDir%\lib\apache-tomcat
EnvSet, ficheepsConfigurationFile, %A_WorkingDir%\FicheEPS\ficheEPS.config
readProperties()
EnvGet, catalinaHome, CATALINA_HOME
RunWait %catalinaHome%\bin\startup.bat,,Hide
Menu, Tray, DeleteAll
Menu, Tray, Add, Ouvrir le navigateur, StartBrowser
Menu, Tray, Icon, Ouvrir le navigateur, lib\res\browser2.ico
Menu, Tray, Add
Menu, Tray, Add, Arreter FicheEPS, StopFicheEPS
Menu, Tray, Icon, Arreter FicheEPS, lib\res\stop3.ico
Return

StopFicheEPS:
EnvSet, JAVA_HOME, %A_WorkingDir%\lib\jre
EnvSet, CATALINA_HOME, %A_WorkingDir%\lib\apache-tomcat
readProperties()
EnvGet, catalinaHome, CATALINA_HOME
RunWait %catalinaHome%\bin\shutdown.bat,,Hide
Menu, Tray, DeleteAll
Menu, Tray, Add, D�marrer FicheEPS, StartFicheEPS
Menu, Tray, Icon, D�marrer FicheEPS, lib\res\start3.ico
ExitApp 0
Return

StartBrowser:
EnvSet, port, 8080
readProperties()
;EnvGet, javahome, JAVA_HOME
;EnvGet, catalinaHome, CATALINA_HOME
;EnvGet, fcf, ficheepsConfigurationFile
;MsgBox, %fcf% %javahome% %catalinaHome%
Run, http://localhost:%port%/FicheEPS
Return

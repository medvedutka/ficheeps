Create WAR:
  
  Using Eclipse:
    right click the project
    choose Google→GWT Compile

    when compilation has finished, the console will say i.e.

        Linking into /home/janus/bpworkspace/gwtwerkstatt2/war/gwtwerkstatt2

        Link succeeded

        Compilation succeeded -- 28.623s

    open a terminal and navigate to the directory
    create the WAR: jar cv * > /tmp/myGWTproject.war
    you can now launch it with jetty-runner or similar: java -jar jetty-runner-8.1.7.v20120910.jar /tmp/myGWTproject.war

or:
    In Eclipse on the main panel click on "Deploy module on aplication server" (It's next to blue google button).
    Chose war file name and location where to store it
    Click ok

or:
http://blog.elitecoderz.net/en/gwt-and-tomcat-create-war-using-eclipse-to-deploy-war-on-tomcat/2009/12/




/var/lib/openshift/ab4f89ae7736439e954b05d7f3067064/app-root/data
package ficheeps.server;

import java.util.List;
import java.util.logging.Level;

import ficheeps.model.Eleve;
import ficheeps.model.Groupe;
import ficheeps.model.Settings;
import ficheeps.server.actions.EleveActions;
import ficheeps.server.actions.GroupeActions;
import ficheeps.server.actions.ModelActions;
import ficheeps.server.actions.SettingsActions;
import ficheeps.server.utils.AESSerializer;
import java.io.File;
import java.io.InputStream;
import java.util.logging.Logger;
import org.prevayler.Prevayler;
import org.prevayler.PrevaylerFactory;
import org.prevayler.Query;
import org.prevayler.Transaction;
import org.prevayler.TransactionWithQuery;
import org.prevayler.foundation.serialization.GZIPSerializer;
import org.prevayler.foundation.serialization.Serializer;
import org.prevayler.foundation.serialization.XStreamSerializer;

public class DBAccess {



    private static final Object lock = new Object();
    private static DBAccess SINGLETON = null;

    public static DBAccess Get() {
        if(SINGLETON == null) {
            synchronized(lock) {
                if(SINGLETON == null) {
                    try {
                        SINGLETON = new DBAccess();
                    }
                    catch(Exception e) {
                        java.util.logging.Logger.getLogger("Database").log(Level.SEVERE,"Cannot access db",e);
                    }
                }
            }
        }
        return SINGLETON;
    }
    
    private Prevayler<Model> prevayler;
    private DBAccess() throws Exception {
        PrevaylerFactory<Model> factory = new PrevaylerFactory<Model>();
        factory.configurePrevalenceDirectory(new File(Configuration.Get().getDatabaseFolder()).getAbsolutePath());
        factory.configureJournalDiskSync(false);
        Serializer snapshotSerializer = new GZIPSerializer(new XStreamSerializer("UTF-8"));
        Serializer journalSerializer = new GZIPSerializer(new XStreamSerializer("UTF-8"));
        if(Configuration.Get().isThereADBPwd()) {
            byte[] key = AESSerializer.StringToKey(Configuration.Get().getDBPwdHash());
            journalSerializer = new AESSerializer(journalSerializer, key);
            snapshotSerializer = new AESSerializer(snapshotSerializer, key);
        }
        
        factory.configureJournalSerializer("FicheEPSjournal",journalSerializer);
        factory.configureSnapshotSerializer("FicheEPSsnapshot", snapshotSerializer);
        factory.configurePrevalentSystem(new Model());
        prevayler = factory.create();
    }



	public static void DropDB() {
		//TODO
	}


	public void doAction(Transaction<Model> action) {
            try {
                prevayler.execute(action);
            } catch (Exception ex) {
                Logger.getLogger(DBAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        public <T> T doActionWithResult(TransactionWithQuery<Model, T> action) {
            try {
                return prevayler.execute(action);
            } catch (Exception ex) {
                Logger.getLogger(DBAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
      
        public <T> T doQuery(Query<Model, T> query) {
            try {
                return prevayler.execute(query);
            } catch (Exception ex) {
                Logger.getLogger(DBAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
        
        public Settings getSettingsSingleton() {
            return doQuery(new SettingsActions.GetSingleton());
        }
        

        
    public List<String> getAllClasses() {
        return doQuery(new ModelActions.GetAllClassesNames());
    }
    
    public List<String> getApsaForSuggestion() {
	return doQuery(new ModelActions.GetApsaForSuggestion());  	
    }

    /**
     * @return groups with only simple attributes set (no list of eleves)
     */
    public List<Groupe> getAllGroupesLight() {
        return doQuery(new GroupeActions.GetAllGroupesLight());
    }
	
    //TODO: verify we request orid!
    public List<String> getAllGroupsNameForEleve(String eleveOrid) {
        return doQuery(new ModelActions.GetAllGroupsNameForEleve(eleveOrid));
    }

    public void deleteAllGroupes() {
       doAction(new ModelActions.DeleteAllGroups());
    }

    public List<Eleve> getElevesFromClasse(String classe) {
        return doQuery(new ModelActions.GetElevesFromClasse(classe));  
    }
    
    public List<Eleve> findEleveBestMatch(String classe, String nom, String prenom) {
        return doQuery(new EleveActions.FindElevesBestMatch(classe, nom, prenom));
    }
	
    public void unregisterAllElevesFromClasses() {
        doAction(new ModelActions.UnregisterAllElevesFromClasses());
    }

    public void deleteAllElevesWithoutClasses() {
        doAction(new EleveActions.DeleteAllElevesWithoutClasses());
    }
	
    public List<Groupe> retrieveGroupe(String groupeName) {
        return doQuery(new GroupeActions.GetGroupesByName(groupeName));
    }
	
    
    
    public void importDB_v_old(InputStream is) throws Exception {
        //TODO: prevent concurent modification
        //TODO: cleanup model
        prevayler.prevalentSystem().importFormFile_v_old(is);
        createDBSnapshot();
    }
    
    
    public File createDBSnapshot() throws Exception {
        return prevayler.takeSnapshot();
    }
    
    public static void CreateDummyDB() {
        DBAccess db = Get();
        db.doAction(new ModelActions.CreateDummyDB());
    }

   
}

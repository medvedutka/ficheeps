package ficheeps.server;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;
import java.util.logging.Level;

import javax.naming.InitialContext;

public class Configuration extends Properties {


        public String getGoogleApiKey() {
            return this.getProperty("googleApiKey");
        }
    
	public String getDatabaseFolder() {
		return getDataFolder() + File.separator + "db";
	}
	
	public File getElevePhotosFolder() {
		File f = new File(getDataFolder(),"photos");
		f.mkdirs();
		return f;
	}
	
	public File getDataFolder() {
		File f = new File(this.getProperty("dataFolder"));
		f.mkdirs();
		return f;
	}
	
	public File getDBBackupFolder() {
		File f = new File(getDataFolder(),"dbbackup");
		f.mkdirs();
		return f;
	}
	
	public File getLogFolder() {
		File f = new File(getDataFolder(),"log");
		f.mkdirs();
		return f;
	}

	public File getDocumentsFolder() {
		File f = new File(getDataFolder(),"documents");
		f.mkdirs();
		return f;
	}

	public File getDocumentSuggestionFinDeSecondeTemplate() {
		File f = new File(getDocumentsFolder(),"ModeleSuggestionFinDeSeconde.pdf");
		return f;
	}

	public String getAdminPwdHash() {
		return this.getProperty("adminPwd");
	}
        
        public boolean isThereADBPwd() {
            String p = getDBPwdHash();
            return p!= null && !p.isEmpty();
	}
        public String getDBPwdHash() {
            String p =this.getProperty("dbPwd");
            if(p != null) {
                p = p.trim();
            }
            return p;
	}
	
	private static long lastModifiedTimeStamp = Long.MIN_VALUE;
	private static Configuration singleton = null;
	
	public synchronized static Configuration Get() {
		try {
			InitialContext ctx=new InitialContext();
			//java:comp/env/mySpecialValue
        	String filePath=(String)ctx.lookup("java:comp/env/ficheepsConfigurationFile");
        	File file = new File(filePath);
        	long lastModified = file.lastModified();
        	if(singleton == null || lastModified>lastModifiedTimeStamp) {
        		lastModifiedTimeStamp = lastModified;
        		singleton = new Configuration();
        		singleton.load(new FileReader(file));	
        	}
		}
        catch (Exception e) {
        	singleton = null;
        	lastModifiedTimeStamp = Long.MIN_VALUE;
        	java.util.logging.Logger.getAnonymousLogger().log(Level.SEVERE, "Impossible de lire la configuration: " +e.getMessage(), e);
		}
		return singleton;
	}
}

package ficheeps.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;

import javax.servlet.http.HttpSession;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.ServerServiceException;
import ficheeps.model.Apsa;
import ficheeps.model.BackupFile;
import ficheeps.model.CSVLine;
import ficheeps.model.Commentaire;
import ficheeps.model.Eleve;
import ficheeps.model.EleveDiff;
import ficheeps.model.EmailsDiff;
import ficheeps.model.EmailsDiffImportResult;
import ficheeps.model.EmailsImportOptions;
import ficheeps.model.Groupe;
import ficheeps.model.ImportOptions;
import ficheeps.model.ListPrettyMail;
import ficheeps.model.ListeLibre;
import ficheeps.model.ListeLibreItem;
import ficheeps.model.ListeLibreLight;
import ficheeps.model.Settings;
import ficheeps.model.StatisticsFilter;
import ficheeps.model.Tuple;
import ficheeps.model.User;
import ficheeps.server.actions.EleveActions;
import ficheeps.server.actions.GroupeActions;
import ficheeps.server.actions.ListeLibreActions;
import ficheeps.server.actions.ModelActions;
import ficheeps.server.actions.SettingsActions;
import ficheeps.server.actions.UserActions;
import ficheeps.server.utils.AESSerializer;
import ficheeps.server.utils.ClassGroupInfosImport;
import ficheeps.server.utils.EmailImport;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;

public class FicheEPSServiceImpl extends RemoteServiceServlet implements FicheEPSService {

    @Override
    public Eleve setEleveNumen(String eleveOrid, String newNumen) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        
        //Check if 'newNumen' is free
        //TODO: the whole thing should be atomic (including photo renaming)
        List<Eleve> elevesTest = db.doQuery(new EleveActions.GetByNumen(newNumen));
        if(!elevesTest.isEmpty()) {
            if(elevesTest.size() == 1) {
                Eleve eTest = db.doQuery(new EleveActions.GetById(eleveOrid));
                if(eTest == elevesTest.get(0)) {
                    return eTest;
                }
            }
            throw new ServerServiceException("Le NUMEN '"+newNumen+"'est déja attribué à au moins un autre élève (Les NUMEN doivent êtres uniques!)");
        }
        
        String oldNumen = db.doQuery(new EleveActions.GetById(eleveOrid)).getId();
        Eleve result = db.doActionWithResult(new EleveActions.Set(eleveOrid).numen(newNumen));

        try {
            // must rename resources: photos 
            changeResourcesName(oldNumen, newNumen);
        } catch (Exception e) {
            throw new ServerServiceException(e.getMessage());
        }

        return result;
    }
   

    /**
     * Rename all resources (photos ...)
     */
    private void changeResourcesName(String oldNumen, String newNumen) throws Exception {
        File oldFile = new File(Configuration.Get().getElevePhotosFolder(), oldNumen + ".jpg");
        if (oldFile.exists()) {
            File newFile = new File(Configuration.Get().getElevePhotosFolder(), newNumen + ".jpg");
            if (!oldFile.renameTo(newFile)) {
                throw new Exception("Impossible de renomer la resource '" + oldFile.getAbsolutePath() + "'");
            }
        }
    }

    
    @Override
    public Eleve createEleveLike(Eleve e) {
        DBAccess db = DBAccess.Get();
        Eleve eleve = db.doActionWithResult(new EleveActions.Create());
        return db.doActionWithResult(new EleveActions.UpdateFirstLevel(eleve.getOrid(), e));
    }
    
    @Override
    public Eleve updateEleveFirstLevel(Eleve e) {
        DBAccess db = DBAccess.Get();
        Eleve result = db.doActionWithResult(new EleveActions.UpdateFirstLevel(e.getOrid(),e));
        return result;
    }
    
    @Override
    public Eleve updateEleveDeep(Eleve e) {
        DBAccess db = DBAccess.Get();
        Eleve result = db.doActionWithResult(new EleveActions.UpdateDeep(e.getOrid(),e));
        return result;
    }

    @Override
    public String setEleveEmails(String eleveOrid, String emails) throws ServerServiceException {
        try {
            String validatedMails = Eleve.cleanAndValidateMailList(emails);
            
            DBAccess db = DBAccess.Get();
            Eleve eleve = db.doActionWithResult(new EleveActions.Set(eleveOrid).emails(validatedMails));
            
            return eleve.getEmails();
        }
        catch(Exception e) {
            throw new ServerServiceException(e.getMessage());
        }
    }
    
    @Override
    public ListPrettyMail retrieveAllMails(List<String> classes, List<String> groupes, List<String> listeLibresIds) throws ServerServiceException {
        ListPrettyMail result = new ListPrettyMail();

        Set<String> prettyMailsMailling = new HashSet<String>();
        Set<String> mailsList = new HashSet<String>();
        if(classes != null) {
            DBAccess db = DBAccess.Get();
            for(String classe : classes) {
                List<Eleve> eleves = db.getElevesFromClasse(classe);
                for(Eleve e : eleves) {
                    for(String prettyMail : e.getPrettyEmailsAsList()) {
                        prettyMailsMailling.add(prettyMail);
                    }
                    for(String mail: e.getEmailsAsList()) {
                        mailsList.add(e.getNom()+ "\t" + e.getPrenom()+ "\t" + mail);
                    }
                }
            }
        }
        if(groupes != null) {
            for(String groupName : groupes) {
                DBAccess db = DBAccess.Get();
                List<Groupe> groupesObjects = db.doQuery(new GroupeActions.GetGroupesByName(groupName));
                if(groupesObjects.size() == 1) {
                    Groupe groupe = groupesObjects.get(0);
                    if(groupe.getEleves() != null) {
                        for(Eleve e : groupe.getEleves()) {
                            for(String prettyMail : e.getPrettyEmailsAsList()) {
                                prettyMailsMailling.add(prettyMail);
                            }
                            for(String mail: e.getEmailsAsList()) {
                                mailsList.add(e.getNom()+ "\t" + e.getNom() + "\t" + mail);
                            }                        
                        }                    
                    }
                }
            }
        }        
        if(listeLibresIds != null) {
            for(String llid : listeLibresIds) {
                DBAccess db = DBAccess.Get();
                ListeLibre listeLibre = db.doQuery(new ListeLibreActions.GetListeLibreById(llid));
                for(ListeLibreItem item : listeLibre.getItems()) {
                    Eleve e = item.getEleve();
                    if(e != null) {
                        for(String prettyMail : e.getPrettyEmailsAsList()) {
                            prettyMailsMailling.add(prettyMail);
                        }
                        for(String mail: e.getEmailsAsList()) {
                            mailsList.add(e.getNom()+ "\t" + e.getNom() + "\t" + mail);
                        }                        
                    }
                }
            }
        }
        
        
        StringBuilder maillingResult = new StringBuilder();
        {
            List<String> listMail = new LinkedList<String>(prettyMailsMailling);
            Collections.sort(listMail);
            Iterator iter = listMail.iterator();
            while(iter.hasNext()) {
                maillingResult.append(iter.next());
                if(iter.hasNext()) {
                    maillingResult.append(",");
                }
            }
        }
        
        StringBuilder listMailResult = new StringBuilder();
        {
            List<String> listMail = new LinkedList<String>(mailsList);
            Collections.sort(listMail);
            Iterator iter = listMail.iterator();
            while(iter.hasNext()) {
                listMailResult.append(iter.next());
                if(iter.hasNext()) {
                    listMailResult.append("\n");
                }
            }
        }        
        
        result.setMaillingPrettyMails(maillingResult.toString());
        result.setListPrettyMails(listMailResult.toString());
        
        return result;
    }
    
    
    
    @Override
    public EmailsDiffImportResult tryEmailsImport(EmailsImportOptions importOptions) throws ServerServiceException {
        try {
            EmailImport emailImport = new EmailImport(importOptions);
            emailImport.process();
            EmailsDiffImportResult result = new EmailsDiffImportResult(emailImport.getAllEmailsDiff(),emailImport.getInfoMessage(),emailImport.getErrorMessages());
            return result;
        }
        catch(Exception e) {
            java.util.logging.Logger.getAnonymousLogger().log(Level.WARNING,"Erreur lors de l'import de mails", e);
            throw new ServerServiceException(e.getMessage());
        }
    }
    
    @Override
    public String commitEmailsImport(EmailsImportOptions importOptions,List<EmailsDiff> led) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        for(EmailsDiff ed : led) {
            Eleve eleve = retrieveUniqEleveByNumen(ed.getEleveId());
            eleve = db.doActionWithResult(new EleveActions.Set(eleve.getOrid()).emails(ed.getNewMails()));
        }
        return "" + led.size() + " élèves modifiés.";
    }

    private Eleve retrieveUniqEleveByNumen(String numen) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        List<Eleve> list = db.doQuery(new EleveActions.GetByNumen(numen));

        if (list.size() > 1) {
            throw new ServerServiceException("Au moins deux élèves possèdent le même identifiant: veuillez corriger ce problème avant de pouvoir continuer");
        } else if (list.isEmpty()) {
            throw new ServerServiceException("Aucun élèves avec l'identifiant '" + numen + "' n'a été trouvé dans la base");
        }

        Eleve eleve = list.get(0);
        return eleve;
    }
    
    
    @Override
    public Tuple<List<Eleve>, Integer> getListElevesRange(int startOffset, int count, String filter, String sortBy, boolean sortAscending, boolean includeAllEleves) {
        DBAccess db = DBAccess.Get();
        filter = ("" + filter).toLowerCase();
        EleveActions.Search search = new EleveActions.Search(filter, sortBy, sortAscending, includeAllEleves, startOffset, count);
        List<Eleve> list = db.doQuery(search);
        Integer totalCount = search.getQueryResultTotalCount();
        
        return new Tuple<List<Eleve>, Integer>(list, totalCount);
    }
    

    @Override
    public List<String> getClasses() {
        DBAccess db = DBAccess.Get();
        List<String> result = db.getAllClasses();
        if (result != null) {
            Collections.sort(result, new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    if (o1 == o2) {
                        return 0;
                    } else if (o1 == null) {
                        return -1;
                    } else if (o2 == null) {

                        return 1;
                    } else if (o1.isEmpty() && o2.isEmpty()) {
                        return 0;
                    } else if (o1.isEmpty()) {
                        return -1;
                    } else if (o2.isEmpty()) {
                        return 1;
                    }

                    o1 = o1.toLowerCase();
                    o2 = o2.toLowerCase();

                    char c1 = o1.charAt(0);
                    char c2 = o2.charAt(0);

                    if (c1 == c2) {
                        return o1.compareTo(o2);
                    }

                    int w1 = levelWeight(c1);
                    int w2 = levelWeight(c2);
                    if (w1 == -1 || w2 == -1) {
                        return o1.compareTo(o2);
                    }

                    return w1 - w2;
                }
            });
        }
        return result;
    }

    private int levelWeight(char level) {
        switch (level) {
            case '6':
                return 1;
            case '5':
                return 2;
            case '4':
                return 3;
            case '3':
                return 4;
            case '2':
                return 5;
            case '1':
                return 6;
            case 't':
                return 7;
        }
        return -1;
    }

    @Override
    public List<Eleve> getElevesFromClasse(String classe) {
        DBAccess db = DBAccess.Get();
        List<Eleve> eleves = db.getElevesFromClasse(classe);
        return eleves;
    }

    @Override
    public void createDummyDB() {
        DBAccess.CreateDummyDB();
    }

    @Override
    public void dropDB() {
        DBAccess.DropDB();
    }
    
    @Override
    public void createDBSnapshot() throws ServerServiceException {
        try {
            DBAccess.Get().createDBSnapshot();
        }
        catch(Exception e) {
            throw new ServerServiceException(e.getMessage());
        }
    }

    @Override
    public String tryLogin() {
        HttpSession session = getThreadLocalRequest().getSession(false);
        if (session != null) {
            return (String) session.getAttribute("username");
        }
        return null;
    }


    @Override
    public String logout() {
        HttpSession session = getThreadLocalRequest().getSession(false);
        String msg = "";
        if (session != null) {
            session.invalidate();
            msg = "ok";
        } else {
            msg = "Strange, no session attached";
        }
        SchedulerInitializerListener.ScheduleLogoutAutomaticBackup();
        return msg;
    }

    @Override
    public List<User> retrieveAllUsers() {
        DBAccess db = DBAccess.Get();
        return db.doQuery(new UserActions.GetAllUsers());
    }

    @Override
    public User createUser() throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        User user = db.doActionWithResult(new UserActions.Create());
        //user = db.doActionWithResult(new UserActions.Set(user.getOrid()).local(local).fullName("nom complet").login("login").pwd("" + System.currentTimeMillis()));
        return user;
    }
    
    @Override
    public User updateUserFirstLevel(User u) {
        DBAccess db = DBAccess.Get();
        return db.doActionWithResult(new UserActions.UpdateFirstLevel(u.getOrid(),u));
    }

    @Override
    public void deleteUser(User u) {
        DBAccess db = DBAccess.Get();
        db.doAction(new UserActions.Delete(u.getOrid()));
    }

    @Override
    public void deleteAllUsers() throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        db.doAction(new UserActions.DeleteAll());
    }
    
    @Override
    public User getCurrentSessionUser() {
        HttpSession session = getThreadLocalRequest().getSession(false);
        if (session != null) {
            return (User) session.getAttribute("user");
        }
        return null;
    }

    @Override
    public void deleteEleve(Eleve e) {
        File photoFile = new File(Configuration.Get().getElevePhotosFolder(), e.getId() + ".jpg");
        if (photoFile.exists()) {
            photoFile.delete();
        }

        DBAccess db = DBAccess.Get();
        db.doAction(new EleveActions.Delete(e.getOrid()));
    }

    @Override
    public List<BackupFile> getAllBackupFiles() {
        List<BackupFile> result = new LinkedList<BackupFile>();
        File backupFolder = Configuration.Get().getDBBackupFolder();
        File[] allFiles = backupFolder.listFiles();
        for (File f : allFiles) {
            BackupFile bf = ParseBackupFileName(f);
            if (bf != null) {
                result.add(bf);
            }
        }
        return result;
    }

    private static BackupFile ParseBackupFileName(File f) {
        String fileName = f.getName();
        int index = fileName.indexOf('-');
        if (index == -1) {
            return null;
        }
        String backupName = fileName.substring(0, index);
        if (backupName.isEmpty()) {
            return null;
        }
        int extensionIndex = fileName.lastIndexOf('.');
        if (extensionIndex == -1
                || (index > extensionIndex)) {
            return null;
        }
        String date = fileName.substring(index + 1, extensionIndex);
        BackupFile backupFile = new BackupFile();
        backupFile.setDate(date);
        backupFile.setFileName(fileName);
        backupFile.setName(backupName);
        backupFile.setSize(f.length());
        return backupFile;
    }

    @Override
    public void deleteBackupFile(BackupFile backupFile) {
        File backupFolder = Configuration.Get().getDBBackupFolder();
        File file = new File(backupFolder, backupFile.getFileName());
        if (file.exists()) {
            file.delete();
        }
    }

    @Override
    public BackupFile createBackup() {
        String name = "sauvegarde";
        File backupFile = CreateBackup(name);
        return ParseBackupFileName(backupFile);
    }

    public static File CreateBackup(String name) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(new Date());

        String fileName = name + "-" + date + ".zip";
        File backupFolder = Configuration.Get().getDBBackupFolder();
        File backupFile = new File(backupFolder, fileName);
        int i = 2;
        while (backupFile.exists()) {
            fileName = name + i + "-" + date + ".zip";
            backupFile = new File(backupFolder, fileName);
            i++;
        }

        try (FileOutputStream os = new FileOutputStream(backupFile)) {
            if(Configuration.Get().isThereADBPwd()) {
                AESSerializer aesCipher = new AESSerializer(null, AESSerializer.StringToKey(Configuration.Get().getDBPwdHash()));
                CipherOutputStream cos = aesCipher.getCipherOutpuStream(os);
                DBAccess.Get().doQuery(new ModelActions.ExportModel_v_old(cos));
                cos.close();
            }
            else {
               DBAccess.Get().doQuery(new ModelActions.ExportModel_v_old(os));
            }
        } catch (Exception e) {
            java.util.logging.Logger.getAnonymousLogger().log(Level.SEVERE, "Impossible de créer la sauvegarde: " + e.getMessage(), e);
            //TODO: throw exption
            return null;
        }

        return backupFile;
    }

    @Override
    public String restoreBackup(BackupFile backupFile) {
        //TODO: must cleanup db I guess ...
        File backupFolder = Configuration.Get().getDBBackupFolder();
        File file = new File(backupFolder, backupFile.getFileName());
        if (file.exists()) {
            try(FileInputStream is = new FileInputStream(file);) {
                if(Configuration.Get().isThereADBPwd()) {
                    AESSerializer aesCipher = new AESSerializer(null, AESSerializer.StringToKey(Configuration.Get().getDBPwdHash()));
                    CipherInputStream cis = aesCipher.getCipherInputStream(is);
                    DBAccess.Get().importDB_v_old(cis);
                    cis.close();
                }
                else {
                   DBAccess.Get().importDB_v_old(is);
                }
                return "ok";
            } catch (Exception e) {
                java.util.logging.Logger.getAnonymousLogger().log(Level.SEVERE, "Impossible de restaurer la sauvegarde: " + e.getMessage(), e);
                return null;
            }
        } else {
            return "Fichier de sauvegarde '" + backupFile.getFileName() + "' non trouvé.";
        }
    }

    @Override
    public ImportOptions initImportFromUploadedFile(String token) throws ServerServiceException {
        return ElevesImport.CreateImportOptionsFrom(token);
    }

    @Override
    public List<CSVLine> verifyImportOptions(ImportOptions importOptions) throws ServerServiceException {
        try {
            ElevesImport eleveImport = new ElevesImport(importOptions);
            return eleveImport.readAsExample(10);
        } catch (Exception e) {
            java.util.logging.Logger.getAnonymousLogger().log(Level.SEVERE, "Erreur lors de la vérification des options d'import", e);
            throw new ServerServiceException(e.getMessage());
        }
    }

    @Override
    public List<EleveDiff> tryImport(ImportOptions importOptions) throws ServerServiceException {
        try {
            ElevesImport eleveImport = new ElevesImport(importOptions);
            return eleveImport.diff();
        } catch (Exception e) {
            java.util.logging.Logger.getAnonymousLogger().log(Level.SEVERE, "Erreur lors de l'import d'essais", e);
            throw new ServerServiceException(e.getMessage());
        }
    }

    @Override
    public String commitImport(ImportOptions importOptions, List<EleveDiff> led) throws ServerServiceException {
        try {
            ElevesImport eleveImport = new ElevesImport(importOptions);
            return eleveImport.commit(led);
        } catch (Exception e) {
            java.util.logging.Logger.getAnonymousLogger().log(Level.SEVERE, "Erreur lors de l'impor", e);
            throw new ServerServiceException(e.getMessage());
        }
    }

    @Override
    public List<Groupe> getGroupesLight() {
        DBAccess db = DBAccess.Get();
        List<Groupe> result = db.getAllGroupesLight();
        if (result != null) {
            Collections.sort(result, new Comparator<Groupe>() {
                @Override
                public int compare(Groupe o1, Groupe o2) {
                    if (o1 == o2) {
                        return 0;
                    } else if (o1 == null) {
                        return -1;
                    } else if (o2 == null) {
                        return 1;
                    } else if (o1.getNom() == null || o1.getNom().isEmpty()) {
                        return -1;
                    } else if (o2.getNom() == null || o2.getNom().isEmpty()) {
                        return 1;
                    }
                    return o1.getNom().compareToIgnoreCase(o2.getNom());
                }
            });
        }
        return result;
    }

    @Override
    public Groupe updateGroupeFirstLevel(Groupe g) {
        DBAccess db = DBAccess.Get();
        Groupe result = db.doActionWithResult(new GroupeActions.UpdateFirstLevel(g.getOrid(),g));
        return result;
    }
    
    @Override
    public Groupe createGroupe() throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        Groupe result = db.doActionWithResult(new GroupeActions.Create());
        return result;
    }

    @Override
    public void deleteGroupe(String groupeOrid) {
        DBAccess db = DBAccess.Get();
        db.doAction(new GroupeActions.Delete(groupeOrid));
    }

    @Override
    public Groupe getGroupeByOrid(String groupeOrid) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        return db.doQuery(new GroupeActions.GetGroupeByOrid(groupeOrid));
    }

    @Override
    public void addEleveToGroupe(String groupeOrid, String eleveOrid) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        Groupe groupe = db.doActionWithResult(new GroupeActions.AddEleve(groupeOrid, eleveOrid));
    }

    @Override
    public void removeEleveFromGroupe(String groupeOrid, String eleveOrid) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        Groupe groupe = db.doActionWithResult(new GroupeActions.RemoveEleve(groupeOrid, eleveOrid));
    }

    @Override
    public List<String> getAllGroupsForEleve(String eleveOrid) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        return db.getAllGroupsNameForEleve(eleveOrid);
    }
        
    @Override
    public void deleteAllGroupes() {
        DBAccess db = DBAccess.Get();
        db.deleteAllGroupes();
    }

    @Override
    public void unregisterAllElevesFromClasses() {
        DBAccess db = DBAccess.Get();
        db.unregisterAllElevesFromClasses();
    }

    @Override
    public void deleteAllElevesWithoutClasses() {
        DBAccess db = DBAccess.Get();
        db.deleteAllElevesWithoutClasses();
    }

    public String deleteAllEleveWithoutClasseBornBefore(String dateYYYYMMDD) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        return db.doActionWithResult(new EleveActions.DeleteAllElevesWithoutClassesBornBefore(dateYYYYMMDD));
    }
    
    @Override
    public List<String> getApsaForSuggestion() {
        DBAccess db = DBAccess.Get();
        return db.getApsaForSuggestion();
    }

    @Override
    public boolean isTemplateSuggestionFinDeSeondeSet() {
        File file = Configuration.Get().getDocumentSuggestionFinDeSecondeTemplate();
        if (!file.exists()) {
            return false;
        }
        if (file.length() < 100) {
            return false;
        }
        return true;
    }

    @Override
    public void deleteTemplateFinDeSeconde() {
        File file = Configuration.Get().getDocumentSuggestionFinDeSecondeTemplate();
        if (file.exists()) {
            file.delete();
        }
    }

    @Override
    public List<String> getApsaSuggestion(Commentaire.Niveau niveau, String annee, String classOrGroupeName, boolean isClassElseGroupe) throws ServerServiceException {
        Set<String> result = new HashSet<String>();
        if (isClassElseGroupe) {
            List<Eleve> eleves = this.getElevesFromClasse(classOrGroupeName);
            if (eleves != null) {
                for (Eleve eleve : eleves) {
                    Commentaire commentaire = eleve.getCommentaireWithNiveau(niveau);
                    if (commentaire.getApsas() != null && ficheeps.server.utils.Utils.StringEquals(annee, commentaire.getAnneeScolaire())) {
                        for (Apsa apsa : commentaire.getApsas()) {
                            result.add(apsa.getActivite());
                        }

                    }
                }
            }
        } else {
            DBAccess db = DBAccess.Get();
            List<Groupe> groupesObjects = db.doQuery(new GroupeActions.GetGroupesByName(classOrGroupeName));
            if(groupesObjects.size() == 1) {
                Groupe groupe = groupesObjects.get(0);
                if (groupe.getEleves() != null) {
                    for (Eleve eleve : groupe.getEleves()) {
                        Commentaire commentaire = eleve.getCommentaireWithNiveau(niveau);
                        if (commentaire.getApsas() != null && ficheeps.server.utils.Utils.StringEquals(annee, commentaire.getAnneeScolaire())) {
                            for (Apsa apsa : commentaire.getApsas()) {
                                result.add(apsa.getActivite());
                            }
                        }
                    }
                }
            }
        }
        return new LinkedList<String>(result);
    }

    @Override
    public String wizardCreateCommentaireForGroupClass(Commentaire commentaireLike, String classOrGroupeName, boolean isClassElseGroupe) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        db.doActionWithResult(new EleveActions.WizardCreateCommentaireForGroupClass(commentaireLike, classOrGroupeName, isClassElseGroupe));
        return null;
    }


    /**
     * Offset in minutes
     */
    @Override
    public int getServerTimezoneOffset() {
        final Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        int offset = (cal.get(Calendar.ZONE_OFFSET) + cal.get(Calendar.DST_OFFSET)) / 1000;
        offset = offset / 60; //minutes
        return offset;
    }

    /*@Override
    public String dateRA12() throws ServerServiceException {
        StringBuilder result = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm:ss.S");
        int nbModifs = 0;
        OrientDBAccess db = OrientDBAccess.Get();
        List<Eleve> eleves = db.retrieveAll(Eleve.class);
        for (Eleve eleve : eleves) {
            Date date = eleve.getDateDeNaissance();
            if (date != null) {
                Date oldDate = date;
                date = Utils.DateRA12(date);
                eleve.setDateDeNaissance(date);
                db.saveEleve(eleve, true);
                nbModifs++;
                result.append(eleve.getNom()).append("\n").append(eleve.getPrenom()).append(" ").append(sdf.format(oldDate)).append(" -> ").append(sdf.format(date));
            }
        }
        result.append("\n").append(nbModifs).append(" changements réalisés. (sur ").append(eleves.size()).append(" élèves)\n");

        return result.toString();
    }*/
    
    
    
    /*Settings Management*/
    @Override
    public Settings getSettings() throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        Settings settings = db.getSettingsSingleton();
        if(settings == null) {
            throw new ServerServiceException("Cannot retrieve 'Settings' object.");
        }
        return settings;
    }
    @Override
    public boolean setConstraintApsaLabels(boolean mode) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        Settings settings = db.doActionWithResult(new SettingsActions.Set().constraintApsaLabel(mode));
        return settings.isConstraintApsaLabel();
    }
    @Override
    public List<String> addApsaLabel(String newLabel) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        Settings settings = db.doActionWithResult(new SettingsActions.ApsaLabels().add(newLabel));
        return settings.getApsaLabels();
    }
    @Override
    public List<String> deleteApsaLabel(String label) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        Settings settings = db.doActionWithResult(new SettingsActions.ApsaLabels().remove(label));
        return settings.getApsaLabels();
    }
    
    
 

    @Override
    public Collection<ListeLibreLight> getAllListeLibreLight() throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        return db.doQuery(new ListeLibreActions.GetAllListeLibreLight());
    }

    @Override
    public ListeLibre getListeLibreById(String id) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        return db.doQuery(new ListeLibreActions.GetListeLibreById(id));
    }

    @Override
    public ListeLibre createListeLibre() throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        return db.doActionWithResult(new ListeLibreActions.Create());
    }

    @Override
    public void deleteListeLibre(String id) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        db.doAction(new ListeLibreActions.Delete(id));
    }

    @Override
    public void setListesLibreName(String id, String name) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        db.doAction(new ListeLibreActions.Set(id).name(name));
    }

    @Override
    public void setListesLibreDescription(String id, String description) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        db.doAction(new ListeLibreActions.Set(id).description(description));
    }

    @Override
    public void setListesLibreItemProfName(String llid, String itemId, String profName) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        db.doAction(new ListeLibreActions.ItemSet(llid, itemId).profName(profName));
    }

    @Override
    public void setListesLibreItemComment(String llid, String itemId, String comment) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        db.doAction(new ListeLibreActions.ItemSet(llid, itemId).comment(comment));
    }

    @Override
    public ListeLibreItem createListeLibreItemFromEleve(String llid, String eleveId) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        ListeLibreItem item = db.doActionWithResult(new ListeLibreActions.AddNewItemFromEleve(llid, eleveId));
        if(item == null) {
            throw new ServerServiceException("Impossible de rajouter l'élève; Est-il déja présent dans la liste ?");
        }
        
        User user = getCurrentSessionUser();
        if(user != null) {
            String currentProfName = user.getFullName();
            db.doAction(new ListeLibreActions.ItemSet(llid, item.getId()).profName(currentProfName));
        }
 
        return item;
    }

    @Override
    public void deleteListeLibreItem(String llid, String itemId) {
        DBAccess db = DBAccess.Get();
        db.doAction(new ListeLibreActions.DeleteItem(llid, itemId));
    }

    @Override
    public void deleteAllListesLibre() throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        db.doAction(new ListeLibreActions.DeleteAll());
    }

    
    @Override
    public StatisticsFilter getFullStatisticsFilter() throws ServerServiceException {
       DBAccess db = DBAccess.Get();
       return db.doQuery(new ModelActions.GetFullStatisticsFilter());
    }

    
    @Override
    public Map<String,String> getInfosClassesGroupes() throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        return db.doQuery(new SettingsActions.InfosClassesGroupes.Get());
    }
    @Override
    public void putInfosClassesGroupes(String classOrGroupName, String info) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        db.doAction(new SettingsActions.InfosClassesGroupes.Put(classOrGroupName,info));
    }
    @Override
    public void removeInfosClassesGroupes(String classOrGroupName) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        db.doAction(new SettingsActions.InfosClassesGroupes.Remove(classOrGroupName));
    }

    @Override
    public List<Tuple<String, String>> tryInfosClassesGroupesImport(String fileUpoaded) throws ServerServiceException {
        try {
            File uploadedFile = new File(fileUpoaded);
            ClassGroupInfosImport cgii = new ClassGroupInfosImport(uploadedFile);
            cgii.process();
            return cgii.getAllTuplesToImport();
        }
        catch(Exception e) {
            throw new ServerServiceException(e.getMessage());
        }
    }

    @Override
    public String commitInfosClassesGroupesImport(List<Tuple<String, String>> infos) throws ServerServiceException {
        DBAccess db = DBAccess.Get();
        db.doAction(new SettingsActions.InfosClassesGroupes.PutAll(infos));
        return "ok";
    }
}

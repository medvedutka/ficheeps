package ficheeps.server;

import ficheeps.server.utils.AESSerializer;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.crypto.CipherInputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class DownloadBackupServlet extends HttpServlet {

	public DownloadBackupServlet() {
		super();
	}

	private static final int DEFAULT_BUFFER_SIZE = 10240;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	       String backupFile = request.getParameter("backupFile");

	        String contentType = null;
	        long fileLenght = 0;
	        InputStream inputStream = null;
	        String fileName = null;
	        
	        File file = new File(Configuration.Get().getDBBackupFolder(),backupFile);
	        if (!file.exists()) {
	        	 response.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
		         return;
	        }
	        else {
	        	fileName = file.getName();
		        fileLenght = file.length();
		        inputStream = new FileInputStream(file);
	        }

        	contentType = getServletContext().getMimeType(fileName);
	        // If content type is unknown, then set the default value.
	        // For all content types, see: http://www.w3schools.com/media/media_mimeref.asp
	        // To add new content types, add new mime-mapping entry in web.xml.
	        if (contentType == null) {
	            contentType = "application/octet-stream";
	        }
	        
	        // Init servlet response.
	        response.reset();
	        response.setBufferSize(DEFAULT_BUFFER_SIZE);
	        response.setContentType(contentType);
	        response.setHeader("Content-Length", String.valueOf(fileLenght));
	        response.setHeader("Content-Disposition","attachment; filename=\"" + fileName +"\"");

	        // Prepare streams.
	        BufferedInputStream input = null;
	        BufferedOutputStream output = null;

	        try {
	            // Open streams.
	            input = new BufferedInputStream(inputStream, DEFAULT_BUFFER_SIZE);
	            output = new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE);

	            // Write file contents to response.
	            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
	            int length;
	            while ((length = input.read(buffer)) > 0) {
	                output.write(buffer, 0, length);
	            }
	        } finally {
	            // Gently close streams.
	            close(output);
	            close(input);
	        }
		
	}
	
	   private static void close(Closeable resource) {
	        if (resource != null) {
	            try {
	                resource.close();
	            } catch (IOException e) {
	                // Do your thing with the exception. Print it, log it or mail it.
	                e.printStackTrace();
	            }
	        }
	    }

	   
	
		private long FILE_SIZE_LIMIT = 2000 * 1024 * 1024; // 2000 MiB
		
		@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	       String importFile = req.getParameter("importFile");
	       String password = req.getParameter("pwd"); 
	       if (importFile == null || !"true".equals(importFile)) {
	        	resp.sendError(HttpServletResponse.SC_BAD_REQUEST); 
	            return;
	        }
			
		    try {
		      DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
		      ServletFileUpload fileUpload = new ServletFileUpload(fileItemFactory);
		      fileUpload.setSizeMax(FILE_SIZE_LIMIT);

		      List<FileItem> items = fileUpload.parseRequest(req);

		      for (FileItem item : items) {
		        if (item.isFormField()) {
		        	doGet(req,resp);
		        	return;
		        } else {
		        }

		        if (!item.isFormField()) {
		          if (item.getSize() > FILE_SIZE_LIMIT) {
		            resp.sendError(HttpServletResponse.SC_REQUEST_ENTITY_TOO_LARGE,"File size exceeds limit");
		            return;
		          }
		          preventCache(resp);
		          
	        	  InputStream is = item.getInputStream();
	        	   if(password != null && !password.isEmpty()) {
                                AESSerializer aesCipher = new AESSerializer(null, AESSerializer.StringToKey(password));
                                CipherInputStream cis = aesCipher.getCipherInputStream(is);
                                DBAccess.Get().importDB_v_old(cis);
                            }
                            else {
                               DBAccess.Get().importDB_v_old(is);
                            }
	        	  is.close();	          
		          
		          resp.getWriter().print("ok"); //TODO: return msg
		        }
		      }
		    } catch (Exception e) {
		      throw new ServletException(e);
		    }
		  }
		
		
		private void preventCache(HttpServletResponse response) {
			// Set standard HTTP/1.1 no-cache headers.
			response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
			// Set standard HTTP/1.0 no-cache header.
			response.setHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", 0);

			response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
			response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
			response.setDateHeader("Expires", 0); // Proxies.
		}
}

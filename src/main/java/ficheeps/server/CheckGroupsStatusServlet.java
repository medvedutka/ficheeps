package ficheeps.server;

import java.io.Closeable;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ficheeps.server.GroupeStatusAnalyser.SmallEleve;
import ficheeps.server.GroupeStatusAnalyser.SmallGroupe;

public class CheckGroupsStatusServlet extends HttpServlet {

	public CheckGroupsStatusServlet() {
		super();
	}
	
	// * Auncun élèves de la classe ccc n'appartient à un groupe.
	// * Tous les élèves de la classe ccc appartiennent à un groupe.
	// * X élèves de la classe ccc n'appartiennent a aucun groupe (Jhon Doe, Fileas Fog ...)
	// * plusieurs groupes ont le même nom !! 
	// * Le groupe ggg n'a pas le champs "professeur renseigné"
	// * Le groupe ggg n'a pas le champs "description" renseigné"
	// * L'élève eeeee appartiens à plusieurs groupes: 
	// * Le groupe ggg ne contient moins de 15 élèves
	// + le groupe ggg contient x élèves

	
	/*	
	public Map<String,Integer> getGroupeSize() {
	public String getClassesDontTousLesElevesSontDansDesGroupes() {
	public String getClasseDontAucunElevesDansUnGroupe() {
	public Map<String,List<SmallEleve>> classeDontQuelquesElevesNonAffecteAUnGroupe() {
	public String getGroupeAvecLeMemeNom() {
	public String getGroupeSansProfesseur() {
	public String getGroupeSansDescription() {
	public Set<SmallEleve> getElevesDansPlusieursGroupes()*/
	
	private String openSpanGood = "<span style='color:#0B610B'>";
	private String openSpanBad = "<span style='color:#8A0808'>";
	private String closeSpan = "</span>";
	
	
	private void writeGood(PrintWriter writer, String s) {
		writer.write("\n" + openSpanGood + s + closeSpan + "\n");
	}
	private void writeBad(PrintWriter writer, String s) {
		writer.write("\n" + openSpanBad + s + closeSpan + "\n");
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		preventCache(response);

		PrintWriter writer = response.getWriter();

		try {
			GroupeStatusAnalyser gsa = new GroupeStatusAnalyser();
			gsa.check();

			writer.write("<html><head>");
			writer.write("\n<title>Vérification des groupes</title>\n");
			writer.write("</head><body>");
			writer.write("\n<center><h1>Vérification des groupes</h1></center>\n");
			writer.write("");
			
			
			writer.write("\n<h2>Vérifications simples:</h2>\n");

			if(gsa.getGroupeAvecLeMemeNom().isEmpty()) {
				writeGood(writer, "Tous les groupes ont bien un nom différent.");
			}
			else {
				writeBad(writer,"Les groupes suivant possèdent le méme nom:<br/>");
				writeTable10(writer,  gsa.getGroupeAvecLeMemeNom(), Check.Bad);
				writer.write("</br> ! Veuillez corriger cela: Tous les groupes doivent avoir un nom différent !");
			}
			writer.write("\n<br/><br/>\n");

			if(gsa.getGroupeSansProfesseur().isEmpty()) {
				writeGood(writer,"Tous les groupes ont bien leurs champs 'professeur' renseigné.");
			}
			else {
				writeBad(writer,"Les groupes suivant n'ont pas leurs champs 'professeur' renseignés:<br/>");
				writeTable10(writer,  gsa.getGroupeSansProfesseur(), Check.Bad);
			}
			writer.write("\n<br/><br/>\n");
			
			if(gsa.getGroupeSansDescription().isEmpty()) {
				writeGood(writer,"Tous les groupes ont bien leurs champs 'description' renseignés.");
			}
			else {
				writeBad(writer,"Les groupes suivant n'ont pas leurs champs 'description' renseigné:<br/>");
				writeTable10(writer,  gsa.getGroupeSansDescription(), Check.Bad);
			}
			writer.write("\n<br/><br/>\n");
			
			
			
			writer.write("\n<h2>Informations groupes/classes:</h2>\n");
			
			if(!gsa.getElevesWithoutClasseButInGroupes().isEmpty()) {
				writeBad(writer,"Les élèves suivants sont affectés à un groupe, mais ne sont pas affecté à une classe (élèves sortis de l'établissement?):");
				writer.write("\n<ul>");
				for(SmallEleve se : gsa.getElevesWithoutClasseButInGroupes()) {
					writer.write("\n<li>" + se.nom + ": " + se.groupes + "</li>");
				}
				writer.write("\n</ul>");
				writer.write("\n<br/><br/>\n");
			}			

			
			
			writer.write("\n<h2>Informations groupes/classes (groupes EPS seulement):</h2>\n");
			
			if(gsa.getClasseDontAucunElevesDansUnGroupeEPS().isEmpty()) {
			}
			else {
				Collections.sort(gsa.getClasseDontAucunElevesDansUnGroupeEPS());
				writer.write("Les classes suivantes n'ont aucun élèves dans un groupe EPS:<br/>");
				writeTable10(writer,  gsa.getClasseDontAucunElevesDansUnGroupeEPS(), Check.Neutral);
				writer.write("\n<br/><br/>\n");
			}
			if(gsa.getClassesDontTousLesElevesSontDansDesGroupesEPS().isEmpty()) {
			}
			else {
				Collections.sort(gsa.getClassesDontTousLesElevesSontDansDesGroupesEPS());
				writeGood(writer,"Tous les élèves des classes suivantes sont affecté à un groupe EPS:<br/>");
				writeTable10(writer,  gsa.getClassesDontTousLesElevesSontDansDesGroupesEPS(), Check.Good);
				writer.write("\n<br/><br/>\n");
			}
			
			
			if(!gsa.getClasseDontQuelquesElevesNonAffecteAUnGroupeEPS().isEmpty()) {
				writeBad(writer,"Les élèves suivants ne sont pas affectés à un groupe EPS (alors qu'au moins un de leurs camarade de classes est déja affecté à un groupe EPS):");
				writer.write("\n<ul>");
				Iterator<Map.Entry<String, List<SmallEleve>>> iter = gsa.getClasseDontQuelquesElevesNonAffecteAUnGroupeEPS().entrySet().iterator();
				while(iter.hasNext()) {
					Map.Entry<String, List<SmallEleve>> entry = iter.next();
					String classe = entry.getKey();
					for(SmallEleve se : entry.getValue()) {
						writer.write("\n<li>" + classe + " " + se.nom + "</li>");
					}
				}
				writer.write("\n</ul>");
				writer.write("\n<br/><br/>\n");
			}
			
			
			
			writer.write("\n<h2>Informations groupes/eléves:</h2>\n");
			
			if(!gsa.getElevesDansPlusieursGroupesEPS().isEmpty()) {
				writeBad(writer,"Les élèves suivant sont affectés à plusieurs groupes EPS: ");
				writer.write("\n<ul>");
				for(SmallEleve se : gsa.getElevesDansPlusieursGroupesEPS()) {
					writer.write("\n<li>" + se.classe + " " + se.nom + ": " + se.groupes + "</li>");
				}
				writer.write("\n</ul>");
				writer.write("\n<br/><br/>\n");
			}
		
			/*TODO
			if(!gsa.getElevesDansPlusieursGroupesAS().isEmpty()) {
				writeBad(writer,"Les élèves suivant sont affectés à plusieurs groupes A.S.: ");
				writer.write("\n<ul>");
				for(SmallEleve se : gsa.getElevesDansPlusieursGroupesAS()) {
					writer.write("\n<li>" + se.classe + " " + se.nom + ": " + se.groupes + "</li>");
				}
				writer.write("\n</ul>");
				writer.write("\n<br/><br/>\n");
			}*/			
			
			if(!gsa.getGroupeSize().isEmpty()) {
				writer.write("<h3>Nombre d'élèves par groupe:</h3>");
				writer.write("\n<table border='1'>");
				writer.write("\n<tr><th>Nb d'élèves</th><th>Nom du groupe</th><th>Type de groupe</th><th>Nom du professeur</th><th>Description</th></tr>");
				//TODO add table header
				sortSmallGroupList(gsa.getGroupeSize());
				Iterator<SmallGroupe> iter = gsa.getGroupeSize().iterator();
				while(iter.hasNext()) {
					SmallGroupe smallGroupe = iter.next();
					//TODO escape
					writer.write("\n<tr><td align='center'>" + smallGroupe.nbEleves + "</td>");
					writer.write("<td>" + smallGroupe.nom + "</td>");
					writer.write("<td>" + smallGroupe.type + "</td>");
					writer.write("<td align='center'>" + smallGroupe.nomProfesseur + "</td>");
					writer.write("<td align='center'>" + smallGroupe.description + "</td></tr>");
				}
				writer.write("\n</table>");
			}
			writer.write("\n<br/>");
			
			writer.write("</body></html>");
			writer.flush();
		} catch (Exception e) {
			throw new ServletException(e);
		} finally {
			// Gently close streams.
			close(writer);
		}
	}
	
	
    
    
    public void sortSmallGroupList(List<SmallGroupe> list) {
    	Collections.sort(list,new Comparator<SmallGroupe>() {
			@Override
			public int compare(SmallGroupe o1, SmallGroupe o2) {
				String s1 = "" + o1.type + "#   \t   #" + o1.nom;
				String s2 = "" + o2.type + "#   \t   #" + o2.nom;
				return s1.compareTo(s2);
			}
		});
    }
	
	
    public void sortSmallEleveList(List<SmallEleve> list) {
    	Collections.sort(list,new Comparator<SmallEleve>() {
			@Override
			public int compare(SmallEleve o1, SmallEleve o2) {
				String s1 = "" + o1.classe + "#   \t   #" + o1.nom;
				String s2 = "" + o2.classe + "#   \t   #" + o2.nom;
				return s1.compareTo(s2);
			}
		});
    }
    
	
	private static enum Check {
		Good,Bad,Neutral
	}
	
	private void writeTable10(PrintWriter writer, List<String> items, Check check) {
		writer.write("\n<table border='1' width='100%'>");
		int i = 0;
		for(String item : items) {
			if(i == 0) {
				writer.write("\n<tr>");
			}
			writer.write("<td align='center' width='10%'>");
			switch (check) {
			case Good:
				writeGood(writer, item);
				break;
			case Bad:
				writeBad(writer, item);
				break;
			default:
				writer.write(item);
				break;
			}
			writer.write("</td>");
			i++;
			if(i == 10) {
				writer.write("</tr>");
				i = 0;
			}
		}
		if(i!=0) {
			for(int j = i;j<=10;j++) {
				writer.write("<td></td>");
			}
			writer.write("</tr>");
		}
		writer.write("</table>");
	}
	
	
	private boolean isStringNullOrEmpty(String s) {
		return s == null || s.trim().isEmpty();
	}

	private static void close(Closeable resource) {
		if (resource != null) {
			try {
				resource.close();
			} catch (IOException e) {
				// Do your thing with the exception. Print it, log it or mail
				// it.
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

	private void preventCache(HttpServletResponse response) {
		// Set standard HTTP/1.1 no-cache headers.
		response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
		// Set standard HTTP/1.0 no-cache header.
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);

		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP
																					// 1.1.
		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		response.setDateHeader("Expires", 0); // Proxies.
	}
}

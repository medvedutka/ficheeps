package ficheeps.server;

import com.google.gson.Gson;
import ficheeps.model.ListeLibre;
import ficheeps.model.StatisticsFilter;
import ficheeps.server.actions.Action;
import ficheeps.server.actions.ListeLibreActions;
import ficheeps.server.utils.Utils;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

public class ExportServlet extends HttpServlet {

    public ExportServlet() {
        super();
    }

    private static final int DEFAULT_BUFFER_SIZE = 10240;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null || action.isEmpty()) {
            throw new ServletException("Parameter 'action' is mandatory");
        }

        if ("exportGroup".equalsIgnoreCase(action)) {
            exportGroup(request, response);
        } 
        else if ("exportStats".equalsIgnoreCase(action)) {
            exportStats(request, response);
        }
        else if("exportListeLibre".equalsIgnoreCase(action)) {
            exportListeLibre(request, response);
        }
    }

    protected void exportStats(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String filterOptionStr = request.getParameter("filterObject");
        if(filterOptionStr == null) {
            throw new ServletException("Parameter 'filterObject' is mandatory");
        }
        
        Gson gson = new Gson();
        StatisticsFilter statisticsFilter = gson.fromJson(filterOptionStr, StatisticsFilter.class);
        if(statisticsFilter == null) {
            throw new ServletException("Impossible de retrouver les filtres d'export des statistiques.");
        }
        
        String contentType = null;
        String fileName = "StatsFicheEPS.xlsx";

        contentType = getServletContext().getMimeType(fileName);
        // If content type is unknown, then set the default value.
        // For all content types, see: http://www.w3schools.com/media/media_mimeref.asp
        // To add new content types, add new mime-mapping entry in web.xml.
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        response.reset();
        response.setBufferSize(DEFAULT_BUFFER_SIZE);
        response.setContentType(contentType);
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        preventCache(response);

        
        Workbook workbook = null;
        try(BufferedOutputStream output = new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE)) {
            ExcelExport excelExport = new ExcelExport();
            //workbook = new XSSFWorkbook();
            workbook = new SXSSFWorkbook();
            ((SXSSFWorkbook)workbook).setCompressTempFiles(true);
            excelExport.exportAllStatsAsXLSX(workbook,statisticsFilter);
            workbook.write(output);
        } catch (Exception e) {
            throw new ServletException(e);
        }
        finally {
            if(workbook != null && workbook instanceof SXSSFWorkbook) {
                ((SXSSFWorkbook)workbook).dispose();
            }
        }
        
    }
    
    

    protected void exportGroup(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String contentType = null;
        String fileName = "GroupesEPS.xls";

        contentType = getServletContext().getMimeType(fileName);
        // If content type is unknown, then set the default value.
        // For all content types, see: http://www.w3schools.com/media/media_mimeref.asp
        // To add new content types, add new mime-mapping entry in web.xml.
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        response.reset();
        response.setBufferSize(DEFAULT_BUFFER_SIZE);
        response.setContentType(contentType);
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        preventCache(response);

        // Prepare streams.
        BufferedOutputStream output = null;
        try {
            output = new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE);
            ExcelExport excelExport = new ExcelExport();
            excelExport.exportAllGroups(output);

        } catch (Exception e) {
            throw new ServletException(e);
        } finally {
            // Gently close streams.
            close(output);
        }
    }

    
    protected void exportListeLibre(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String listeLibreId = request.getParameter("listeLibreId");
        if (listeLibreId == null || listeLibreId.isEmpty()) {
            throw new ServletException("Parameter 'listeLibreId' is mandatory");
        }
                
        DBAccess db = DBAccess.Get();
        ListeLibre listeLibre = db.doQuery(new ListeLibreActions.GetListeLibreById(listeLibreId));
        
        if(listeLibre == null) {
            throw new ServletException("Parameter 'listeLibreId' is mandatory");
        }
        
        String contentType = null;
        String fileName = "Liste_" + Utils.sanitizeFileName(listeLibre.getName()) + ".xlsx";

        contentType = getServletContext().getMimeType(fileName);
        // If content type is unknown, then set the default value.
        // For all content types, see: http://www.w3schools.com/media/media_mimeref.asp
        // To add new content types, add new mime-mapping entry in web.xml.
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        response.reset();
        response.setBufferSize(DEFAULT_BUFFER_SIZE);
        response.setContentType(contentType);
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        preventCache(response);

        // Prepare streams.
        
        try(BufferedOutputStream output = new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE);) {
            final ExcelExport excelExport = new ExcelExport();
            String resultLog = db.doQuery(new Action.Query<String>() {
                @Override
                protected String doAction(Model model) throws Exception {
                    ListeLibre listeLibre = model.getListeLibreById(listeLibreId);
                    excelExport.exportListeLibreAsXLSX(listeLibre, output);
                    return "";
                }
            });

        } catch (Exception e) {
            throw new ServletException(e);
        }

    }

    
    
    private static void close(Closeable resource) {
        if (resource != null) {
            try {
                resource.close();
            } catch (IOException e) {
                // Do your thing with the exception. Print it, log it or mail it.
                e.printStackTrace();
            }
        }
    }

    private long FILE_SIZE_LIMIT = 2000 * 1024 * 1024; // 2000 MiB

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    private void preventCache(HttpServletResponse response) {
        // Set standard HTTP/1.1 no-cache headers.
        response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
        // Set standard HTTP/1.0 no-cache header.
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);

        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.
    }
}

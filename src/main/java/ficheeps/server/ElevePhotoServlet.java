package ficheeps.server;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.Level;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.IOUtils;

import ficheeps.model.Eleve;
import ficheeps.model.Groupe;
import ficheeps.server.actions.EleveActions;
import ficheeps.server.pdf.FicheElevePDFGenerator;
import ficheeps.server.pdf.PDFExportOptions;
import java.util.Date;
import org.prevayler.Query;

public class ElevePhotoServlet extends HttpServlet {

	public ElevePhotoServlet() {
		super();
	}

	private static final int DEFAULT_BUFFER_SIZE = 10240;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String numen = request.getParameter("id");
		String classe = request.getParameter("classe");
		String groupe = request.getParameter("groupe");
                String all = request.getParameter("all");

		// Check if file is actually supplied to the request URI.
		if (numen == null && classe == null && groupe == null && all == null) {
			// Do your thing if the file is not supplied to the request URI.
			// Throw an exception, or send 404, or show default/warning page, or
			// just ignore it.
			response.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
			return;
		}

		String fiche = request.getParameter("fiche");
		if ("pdf".equals(fiche)) {
			if (classe != null) {
				fichePDFClasse(classe, request, response);
			} else if (groupe != null) {
				fichePDFGroupe(groupe, request, response);
			} else if(numen != null) {
				fichePDFEleve(numen, request, response);
			}
                        else {
                            fichePDFAll(request, response);
                        }
			return;
		}

		/*
		 * InitialContext ctx=new InitialContext(); URL
		 * url=(URL)ctx.lookup("url/properties"); Properties p=new Properties();
		 * p.load(url.openStream());
		 */

		String contentType = null;
		long fileLenght = 0;
		InputStream inputStream = null;
		String fileName = null;

		File file = new File(Configuration.Get().getElevePhotosFolder(), numen + ".jpg");
		if (!file.exists()) {
			// must return a default one: ficheeps.client.photo_64x64.png
			inputStream = this.getClass().getResourceAsStream("/ficheeps/client/photo_64x64.png");
			fileLenght = 4197;
			fileName = "photo_64x64.png";
		} else {
			fileName = file.getName();
			fileLenght = file.length();
			inputStream = new FileInputStream(file);
		}

		contentType = getServletContext().getMimeType(fileName);
		// If content type is unknown, then set the default value.
		// For all content types, see:
		// http://www.w3schools.com/media/media_mimeref.asp
		// To add new content types, add new mime-mapping entry in web.xml.
		if (contentType == null) {
			contentType = "application/octet-stream";
		}

		// Init servlet response.
		response.reset();
		response.setBufferSize(DEFAULT_BUFFER_SIZE);
		response.setContentType(contentType);
		response.setHeader("Content-Length", String.valueOf(fileLenght));
		preventCache(response);

		// Prepare streams.
		BufferedInputStream input = null;
		BufferedOutputStream output = null;

		try {
			// Open streams.
			input = new BufferedInputStream(inputStream, DEFAULT_BUFFER_SIZE);
			output = new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE);

			// Write file contents to response.
			byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
			int length;
			while ((length = input.read(buffer)) > 0) {
				output.write(buffer, 0, length);
			}
		} finally {
			// Gently close streams.
			close(output);
			close(input);
		}

	}

	private static void close(Closeable resource) {
		if (resource != null) {
			try {
				resource.close();
			} catch (IOException e) {
				// Do your thing with the exception. Print it, log it or mail
				// it.
				e.printStackTrace();
			}
		}
	}

	private long FILE_SIZE_LIMIT = 40 * 1024 * 1024; // 20 MiB
	private int SCALED_IMG_SIZE = 64 * 4;

	private FileItem getUploadedFile(HttpServletRequest req,HttpServletResponse resp) throws FileUploadException, IOException {
		DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
		ServletFileUpload fileUpload = new ServletFileUpload(fileItemFactory);
		fileUpload.setSizeMax(FILE_SIZE_LIMIT);
		List<FileItem> items = fileUpload.parseRequest(req);
		for (FileItem item : items) {
			if (!item.isFormField()) {
				if (item.getSize() > FILE_SIZE_LIMIT) {
					resp.sendError(HttpServletResponse.SC_REQUEST_ENTITY_TOO_LARGE, "File size exceeds limit");
					return null;
				}
				return item;
			}
		}
		resp.sendError(HttpServletResponse.SC_NO_CONTENT,"Expecting file upload");
		return null;
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String numen = req.getParameter("id");
		String classe = req.getParameter("classe");
		String groupe = req.getParameter("groupe");
                String all = req.getParameter("all");
		String suggestionFinDeSecondeTemplate = req.getParameter("suggestionFinDeSecondeTemplate");

		// Check if file is actually supplied to the request URI.
		if (numen == null && classe == null && groupe == null && all == null && suggestionFinDeSecondeTemplate == null) {
			// Do your thing if the file is not supplied to the request URI.
			// Throw an exception, or send 404, or show default/warning page, or
			// just ignore it.
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST); // TODO.
			return;
		}

		String fiche = req.getParameter("fiche");
		if ("pdf".equals(fiche)) {
			if (classe != null) {
                            fichePDFClasse(classe, req, resp);
			} else if (groupe != null) {
                            fichePDFGroupe(groupe, req, resp);
			} else if (numen != null) {
                            fichePDFEleve(numen, req, resp);
			} else if(all != null) {
                            fichePDFAll(req, resp);
                        }
                        
			return;
		}
		
		
		try {
			
			if(suggestionFinDeSecondeTemplate != null) {
				if ("upload".equals(suggestionFinDeSecondeTemplate)) {
					uploadSuggestionFinDeSecondeTemplate(req, resp);
					return;
				}
				resp.sendError(HttpServletResponse.SC_BAD_REQUEST,"Mauvais paramètres pour 'suggestionFinDeSecondeTemplate'.");
				return;
			}
			
			FileItem fileItem = getUploadedFile(req,resp);
			if(fileItem == null) {
				//Error response set in 'getUploadedFile'
				return;
			}

			File file = new File(Configuration.Get().getElevePhotosFolder(), numen + ".jpg");
			InputStream is = fileItem.getInputStream();
			BufferedImage img = ImageIO.read(is);
			int width = img.getWidth();
			int height = img.getHeight();
			if (width > SCALED_IMG_SIZE && height > SCALED_IMG_SIZE) {
				if (width > height) {
					width = -1;
					height = SCALED_IMG_SIZE;
				} else {
					width = SCALED_IMG_SIZE;
					height = -1;
				}

			}
			Image scaledImg = img.getScaledInstance(SCALED_IMG_SIZE, -1, Image.SCALE_SMOOTH);
			img = new BufferedImage(scaledImg.getWidth(null), scaledImg.getHeight(null), BufferedImage.TYPE_INT_RGB);
			img.getGraphics().drawImage(scaledImg, 0, 0, null);

			try {
				FileOutputStream os = new FileOutputStream(file);
				ImageIO.write(img, "jpeg", os);
				is.close();
				os.close();
			} catch (Exception e) {
				if (file.exists()) {
					file.delete();
				}
				throw e;
			}

			if (!fileItem.isInMemory()) {
				fileItem.delete();
			}

			resp.getWriter().print("ok");
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
	
	
	
	private void uploadSuggestionFinDeSecondeTemplate(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		FileItem fileItem = getUploadedFile(req,resp);
		if(fileItem == null) {
			throw new Exception("Un fichier pdf est nécessaire.");
		}

		InputStream is = null;
		OutputStream os = null;
		try {
			File file = Configuration.Get().getDocumentSuggestionFinDeSecondeTemplate();
			file.delete();
			is = fileItem.getInputStream();
			os = new FileOutputStream(file);
			IOUtils.copy(is, os);
		}
		catch(Exception e) {
			throw e;
		}
		finally {
			if(is != null) {
				is.close();
			}
			if(os != null) {
				os.close();
			}
			
			if (!fileItem.isInMemory()) {
				fileItem.delete();
			}
		}
		
		resp.getWriter().print("ok");
	}
	
	
	
	private PDFExportOptions readPDFExportOptionsParameters(HttpServletRequest request,HttpServletResponse response) throws FileUploadException, IOException {
		PDFExportOptions option = new PDFExportOptions();

		FileItem fileItem = null;
		if(request.getMethod().toLowerCase().equals("post")) {
			fileItem = getUploadedFile(request,response);
			if(fileItem != null) {			
				IOUtils.copy(fileItem.getInputStream(), option.getPdfSuggestionFinDeSeconde());
				if(fileItem.isInMemory()) {
					fileItem.delete();
				}
			}
		}
		
		String param = request.getParameter("CommentsInLevelOrder");
		option.setCommentsInLevelOrder((param == null ? false : Boolean.parseBoolean(param)));
		param = request.getParameter("ShowSuiviMedical");
		option.setShowSuiviMedical((param == null ? false : Boolean.parseBoolean(param)));
		param = request.getParameter("ExportFicheSuggestionFinSeconde");
		option.setExportFicheSuggestionFinSeconde((param == null ? false : Boolean.parseBoolean(param)));
		
		param = request.getParameter("FSFDSExportCartoucheSeconde");
		option.setFicheSuggestionFinSecondeExportCartoucheSeconde((param == null ? false : Boolean.parseBoolean(param)));
		param = request.getParameter("FSFSExportCartoucheSecondeSiNonVide");
		option.setFicheSuggestionFinSecondeExportCartoucheSecondeIfNotEmpty((param == null ? false : Boolean.parseBoolean(param)));
		
		param = request.getParameter("FSFDSExportSuggestionFinDeSeconde");
		option.setFicheSuggestionFinSecondeExportSuggestionFinDeSeconde((param == null ? false : Boolean.parseBoolean(param)));
		param = request.getParameter("FSFSExportSuggestionFinDeSecondeSiNonVide");
		option.setFicheSuggestionFinSecondeExportSuggestionFinDeSecondeIfNotEmpty((param == null ? false : Boolean.parseBoolean(param)));

		
		param = request.getParameter("FicheSuggestionFinSecondeUseDefaultTemplate");
		boolean useDefaultTemplate = param == null ? false : Boolean.parseBoolean(param);
		//Do we need to use the default template set by admin ?
		if(option.isExportFicheSuggestionFinSeconde() && useDefaultTemplate) {
			option.getPdfSuggestionFinDeSeconde().reset();
			File suggestionFinDesSecondeTemplate = Configuration.Get().getDocumentSuggestionFinDeSecondeTemplate();
			FileInputStream is = new FileInputStream(suggestionFinDesSecondeTemplate);
			IOUtils.copy(is, option.getPdfSuggestionFinDeSeconde());
			is.close();
		}
		
		return option;
	}
	
        private void exportAsPDF(final List<Eleve> list,final PDFExportOptions pdfOptions,String fileName,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            response.reset();
            preventCache(response);
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            final OutputStream os = response.getOutputStream();
            DBAccess db = DBAccess.Get();
            db.doQuery(new Query<Model, Void>() {
                @Override
                public Void query(Model p, Date date) throws Exception {
                    FicheElevePDFGenerator.GenerateDocument(os, list, pdfOptions);
                    return null;
                }
            });
        }
        
	private void fichePDFClasse(String classe, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DBAccess db = DBAccess.Get();
		final List<Eleve> list = db.getElevesFromClasse(classe);

		if (list == null || list.isEmpty()) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
			return;
		}

		try {
			final PDFExportOptions pdfOptions = readPDFExportOptionsParameters(request,response);
			if(pdfOptions ==  null) {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST); 
				return;
			}
			
                        exportAsPDF(list, pdfOptions, "FicheEPS_" + classe + ".pdf", request, response);
		} catch (Exception e) {
			java.util.logging.Logger.getAnonymousLogger().log(Level.SEVERE, "Error exorting fiche for Classe", e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

	private void fichePDFGroupe(String groupe, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DBAccess db = DBAccess.Get();
		final List<Groupe> list = db.retrieveGroupe(groupe);

		if (list == null || list.isEmpty()) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
			return;
		}
		if (list.size() > 2) {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					"Plusieurs groupes existent avec le meme nom");
			return;
		}

		try {
			final PDFExportOptions pdfOptions = readPDFExportOptionsParameters(request,response);
			if(pdfOptions ==  null) {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST);
				return;
			}
			
                        exportAsPDF(list.get(0).getEleves(), pdfOptions, "FicheEPS_" + groupe + ".pdf", request, response);
 		} catch (Exception e) {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

        
	private void fichePDFAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DBAccess db = DBAccess.Get();
		final List<Eleve> list = db.doQuery(new EleveActions.GetAll());

		if (list == null || list.isEmpty()) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
			return;
		}

		try {
			final PDFExportOptions pdfOptions = readPDFExportOptionsParameters(request,response);
			if(pdfOptions ==  null) {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST);
				return;
			}
			
                        exportAsPDF(list, pdfOptions, "FicheEPS_tout.pdf", request, response);
 		} catch (Exception e) {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}
        
        
	private void fichePDFEleve(String id, HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException {
		DBAccess db = DBAccess.Get();
		final List<Eleve> list = db.doQuery(new EleveActions.GetByNumen(id));

		if (list == null || list.isEmpty()) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
			return;
		}

		try {
			final PDFExportOptions pdfOptions = readPDFExportOptionsParameters(request,response);
			if(pdfOptions ==  null) {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST);
				return;
			}
			
			String fileDownloadName = "FicheEPS_";
			if (list.size() == 1) {
				fileDownloadName += list.get(0).getNom() + "_" + list.get(0).getPrenom();
			} else {
				fileDownloadName += id;
			}
			fileDownloadName = fileDownloadName.replace('"', '_').replace(' ', '_').replace('\n', '_').replace('\r', '_');
			fileDownloadName += ".pdf";
         
                        exportAsPDF(list, pdfOptions, fileDownloadName, request, response);
		} catch (Exception e) {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

	private void preventCache(HttpServletResponse response) {
		// Set standard HTTP/1.1 no-cache headers.
		response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
		// Set standard HTTP/1.0 no-cache header.
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);

		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP
																					// 1.1.
		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		response.setDateHeader("Expires", 0); // Proxies.
	}

}

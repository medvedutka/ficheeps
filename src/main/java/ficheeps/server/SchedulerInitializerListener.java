package ficheeps.server;

import java.io.File;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.CronScheduleBuilder;
import org.quartz.DateBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;



/**
 * <p>
 * A ServletContextListner that can be used to initialize Quartz.
 * </p>
 *
 * <p>
 * You'll want to add something like this to your WEB-INF/web.xml file:
 *
 * <pre>
 *     &lt;context-param&gt;
 *         &lt;param-name&gt;quartz:config-file&lt;/param-name&gt;
 *         &lt;param-value&gt;/some/path/my_quartz.properties&lt;/param-value&gt;
 *     &lt;/context-param&gt;
 *     &lt;context-param&gt;
 *         &lt;param-name&gt;quartz:shutdown-on-unload&lt;/param-name&gt;
 *         &lt;param-value&gt;true&lt;/param-value&gt;
 *     &lt;/context-param&gt;
 *     &lt;context-param&gt;
 *         &lt;param-name&gt;quartz:wait-on-shutdown&lt;/param-name&gt;
 *         &lt;param-value&gt;true&lt;/param-value&gt;
 *     &lt;/context-param&gt;
 *     &lt;context-param&gt;
 *         &lt;param-name&gt;quartz:start-on-load&lt;/param-name&gt;
 *         &lt;param-value&gt;true&lt;/param-value&gt;
 *     &lt;/context-param&gt;
 *     
 *     &lt;listener&gt;
 *         &lt;listener-class&gt;
 *             org.quartz.ee.servlet.QuartzInitializerListener
 *         &lt;/listener-class&gt;
 *     &lt;/listener&gt;
 * </pre>
 *
 * </p>
 * <p>
 * The init parameter 'quartz:config-file' can be used to specify the path (and
 * filename) of your Quartz properties file. If you leave out this parameter,
 * the default ("quartz.properties") will be used.
 * </p>
 *
 * <p>
 * The init parameter 'quartz:shutdown-on-unload' can be used to specify whether you
 * want scheduler.shutdown() called when the listener is unloaded (usually when
 * the application server is being shutdown). Possible values are "true" or
 * "false". The default is "true".
 * </p>
 *
 * <p>
 * The init parameter 'quartz:wait-on-shutdown' has effect when 
 * 'quartz:shutdown-on-unload' is specified "true", and indicates whether you
 * want scheduler.shutdown(true) called when the listener is unloaded (usually when
 * the application server is being shutdown).  Passing "true" to the shutdown() call
 * causes the scheduler to wait for existing jobs to complete. Possible values are 
 * "true" or "false". The default is "false".
 * </p>
 * 
 * <p>
 * The init parameter 'quartz:start-on-load' can be used to specify whether
 * you want the scheduler.start() method called when the listener is first loaded.
 * If set to false, your application will need to call the start() method before
 * the scheduler begins to run and process jobs. Possible values are "true" or
 * "false". The default is "true", which means the scheduler is started.
 * </p>
 *
 * A StdSchedulerFactory instance is stored into the ServletContext. You can gain access
 * to the factory from a ServletContext instance like this:
 * <br>
 * <pre>
 * StdSchedulerFactory factory = (StdSchedulerFactory) ctx
 *                .getAttribute(QuartzInitializerListener.QUARTZ_FACTORY_KEY);</pre>
 * <p>
 * The init parameter 'quartz:servlet-context-factory-key' can be used to override the
 * name under which the StdSchedulerFactory is stored into the ServletContext, in 
 * which case you will want to use this name rather than 
 * <code>QuartzInitializerListener.QUARTZ_FACTORY_KEY</code> in the above example.
 * </p>
 *
 * <p>
 * The init parameter 'quartz:scheduler-context-servlet-context-key' if set, the 
 * ServletContext will be stored in the SchedulerContext under the given key
 * name (and will therefore be available to jobs during execution). 
 * </p>
 *
 * <p>
 * The init parameter 'quartz:start-delay-seconds' can be used to specify the amount
 * of time to wait after initializing the scheduler before scheduler.start()
 * is called.
 * </p>
 *
 * Once you have the factory instance, you can retrieve the Scheduler instance by calling
 * <code>getScheduler()</code> on the factory.
 *
 * @author James House
 * @author Chuck Cavaness
 * @author John Petrocik
 * @author JC
 */
public class SchedulerInitializerListener implements ServletContextListener {

	
	public static void ScheduleDailyAutomaticBackup() {
		try {
			 Trigger trigger = TriggerBuilder.newTrigger()
				      .withIdentity("dailyBackupTrigger","FicheEPSTriggerGroup")
				      .startNow()
				      .withSchedule(CronScheduleBuilder.dailyAtHourAndMinute(2, 45)) // execute job daily at 3:30            
				      .build();
			 JobDetail dailyBackupJob = JobBuilder.newJob(AutomaticBackupJob.class)
				      .withIdentity("dailyBackupJob","FicheEPSTriggerGroup")
				      .build();
			 scheduler.scheduleJob(dailyBackupJob, trigger);
		}
		catch(Exception e) {
			log.log(Level.SEVERE,"Cannot create daily backup schedule: " + e.getMessage());
		}
	}
        
        
        public static void ScheduleDailyAutomaticSnapshot() {
		try {
			 Trigger trigger = TriggerBuilder.newTrigger()
				      .withIdentity("dailySnapshotTrigger","FicheEPSTriggerGroup")
				      .startNow()
				      .withSchedule(CronScheduleBuilder.dailyAtHourAndMinute(3, 15)) // execute job daily at 3:30            
				      .build();
			 JobDetail dailySnapshotJob = JobBuilder.newJob(AutomaticSnapshotJob.class)
				      .withIdentity("dailySnapshotJob","FicheEPSTriggerGroup")
				      .build();
			 scheduler.scheduleJob(dailySnapshotJob, trigger);
		}
		catch(Exception e) {
			log.log(Level.SEVERE,"Cannot create daily snapshot schedule: " + e.getMessage());
		}
	}
        

	public static void ScheduleLogoutAutomaticBackup() {
		try {
			 scheduler.unscheduleJob(TriggerKey.triggerKey("afterLogoutBackupTrigger","FicheEPSTriggerGroup"));
			 JobDetail logoutBackupJob = JobBuilder.newJob(LogoutBackupJob.class)
				      .withIdentity("logoutBackupJob","FicheEPSTriggerGroup")
				      .build();
			 Trigger trigger = TriggerBuilder.newTrigger()
				      .withIdentity("afterLogoutBackupTrigger","FicheEPSTriggerGroup")
				      .startAt(DateBuilder.futureDate(5, DateBuilder.IntervalUnit.MINUTE))
				      .build();

			 scheduler.scheduleJob(logoutBackupJob,trigger);
		}
		catch(Exception e) {
			log.log(Level.SEVERE,"Cannor create after logout backup schedule: " + e.getMessage());
		}
	}
	
	
	public static class AutomaticBackupJob implements Job {
            public AutomaticBackupJob() {}

            @Override
            public void execute(JobExecutionContext arg0) throws JobExecutionException {
                try {
                    File file = FicheEPSServiceImpl.CreateBackup("automatique");
                    if(! file.exists()) {
                        throw new Exception("Error during automatic backup (backup file don't exist after backup)");
                    }
                }
                catch(Exception e) {
                    log.log(Level.SEVERE,"Error executing Automatic Backup Job: " + e.getMessage());
                }
            }
	}
        
        
        public static class AutomaticSnapshotJob implements Job {
            public AutomaticSnapshotJob() {}

            @Override
            public void execute(JobExecutionContext arg0) throws JobExecutionException {
                try {
                    DBAccess db = DBAccess.Get();
                    File file = db.createDBSnapshot();
                    if(! file.exists()) {
                        throw new Exception("Error during automatic snapshot (snapshot file don't exist after snapshot)");
                    }
                }
                catch(Exception e) {
                    log.log(Level.SEVERE,"Error executing Automatic Snapshot Job: " + e.getMessage());
                }
            }
	}
        
        
        public static class LogoutBackupJob implements Job {
            public LogoutBackupJob() {}

            @Override
            public void execute(JobExecutionContext arg0) throws JobExecutionException {
                try {
                    File file = FicheEPSServiceImpl.CreateBackup("automatique");
                    if(! file.exists()) {
                        throw new Exception("Error during logout automatic backup (backup file don't exist after backup)");
                    }
                }
                catch(Exception e) {
                    log.log(Level.SEVERE,"Error executing Logout Backup Job: " + e.getMessage());
                }
            }
	}
	
	
    public static final String QUARTZ_FACTORY_KEY = "org.quartz.impl.StdSchedulerFactory.KEY";

    private static StdSchedulerFactory factory;
    
    private boolean performShutdown = true;
    private boolean waitOnShutdown = false;

    private static Scheduler scheduler = null;

    private static final Logger log = Logger.getLogger("SchedulerInitializerListener");
    


    public void contextInitialized(ServletContextEvent sce) {

        log.info("Quartz Initializer Servlet loaded, initializing Scheduler...");

        ServletContext servletContext = sce.getServletContext();
        try {
            String shutdownPref = servletContext.getInitParameter("quartz:shutdown-on-unload");
            if(shutdownPref == null)
                shutdownPref = servletContext.getInitParameter("shutdown-on-unload");
            if (shutdownPref != null) {
                performShutdown = Boolean.valueOf(shutdownPref).booleanValue();
            }
            String shutdownWaitPref = servletContext.getInitParameter("quartz:wait-on-shutdown");
            if (shutdownPref != null) {
                waitOnShutdown = Boolean.valueOf(shutdownWaitPref).booleanValue();
            }

            createSchedulerFactory();

            // Always want to get the scheduler, even if it isn't starting, 
            // to make sure it is both initialized and registered.
            scheduler = factory.getScheduler();

            // Should the Scheduler being started now or later
            String startOnLoad = servletContext.getInitParameter("quartz:start-on-load");
            if(startOnLoad == null)
                startOnLoad = servletContext.getInitParameter("start-scheduler-on-load");

            int startDelay = 0;
            String startDelayS = servletContext.getInitParameter("quartz:start-delay-seconds");
            if(startDelayS == null)
                startDelayS = servletContext.getInitParameter("start-delay-seconds");
            try {
                if(startDelayS != null && startDelayS.trim().length() > 0)
                    startDelay = Integer.parseInt(startDelayS);
            } catch(Exception e) {
                log.log(Level.SEVERE,"Cannot parse value of 'start-delay-seconds' to an integer: " + startDelayS + ", defaulting to 5 seconds.");
                startDelay = 5;
            }

            /*
             * If the "quartz:start-on-load" init-parameter is not specified,
             * the scheduler will be started. This is to maintain backwards
             * compatability.
             */
            if (startOnLoad == null || (Boolean.valueOf(startOnLoad).booleanValue())) {
                if(startDelay <= 0) {
                    // Start now
                    scheduler.start();
                    log.info("Scheduler has been started...");
                }
                else {
                    // Start delayed
                    scheduler.startDelayed(startDelay);
                    log.info("Scheduler will start in " + startDelay + " seconds.");
                }
            } else {
                log.info("Scheduler has not been started. Use scheduler.start()");
            }

            String factoryKey = servletContext.getInitParameter("quartz:servlet-context-factory-key");
            if(factoryKey == null)
                factoryKey = servletContext.getInitParameter("servlet-context-factory-key");
            if (factoryKey == null) {
                factoryKey = QUARTZ_FACTORY_KEY;
            }

            log.info("Storing the Quartz Scheduler Factory in the servlet context at key: "
                    + factoryKey);
            servletContext.setAttribute(factoryKey, factory);
            
            
            String servletCtxtKey = servletContext.getInitParameter("quartz:scheduler-context-servlet-context-key");
            if(servletCtxtKey == null)
                servletCtxtKey = servletContext.getInitParameter("scheduler-context-servlet-context-key");
            if (servletCtxtKey != null) {
                log.info("Storing the ServletContext in the scheduler context at key: "
                        + servletCtxtKey);
                scheduler.getContext().put(servletCtxtKey, servletContext);
            }

            
            ScheduleDailyAutomaticBackup();
            ScheduleDailyAutomaticSnapshot();
            
        } catch (Exception e) {
        	log.log(Level.SEVERE,"Quartz Scheduler failed to initialize: " + e.toString());
            e.printStackTrace();
        }
    }

    protected void createSchedulerFactory() throws SchedulerException {
        try {
        	Properties props = new Properties();
        	props.load(this.getClass().getResourceAsStream("/ficheeps/server/utils/quartzScheduler.properties"));
        	factory = new StdSchedulerFactory(props);
        }
        catch(Exception e) {
        	throw new SchedulerException("Cannnot instanciate SchedulerFactory", e);
        }
    }

    public void contextDestroyed(ServletContextEvent sce) {
      
        if (!performShutdown) {
            return;
        }

        try {
            if (scheduler != null) {
                scheduler.shutdown(waitOnShutdown);
            }
        } catch (Exception e) {
        	log.log(Level.SEVERE,"Quartz Scheduler failed to shutdown cleanly: " + e.toString());
            e.printStackTrace();
        }

        log.info("Quartz Scheduler successful shutdown.");
    }


}

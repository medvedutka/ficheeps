package ficheeps.server;

import java.util.logging.Level;

import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;

import com.google.gson.JsonElement;

import ficheeps.model.User;
import ficheeps.server.actions.UserActions;

public class Administration {
	
	
	public User isLocalAllowed(String localLogin, String pwdSha1) {
		if("admin".equals(localLogin)) {
			if((""+pwdSha1).equals(Configuration.Get().getAdminPwdHash())) {
				User admin = new User();
				admin.setAdmin(true);
				admin.setFullName("admin");
				admin.setLogin("admin");
				admin.setLocal(true);
				return admin;
			}
		}
		
		DBAccess db = DBAccess.Get();
                return db.doQuery(new UserActions.LocalLogin(localLogin, pwdSha1));
	}

	public User isEmailLoginAllowed(String emailLogin) {
		DBAccess db = DBAccess.Get();
                return db.doQuery(new UserActions.EmailLogin(emailLogin));
	}
	
	
	
	public String googleAuthentication(String tokenAuth) {
		try {
			String url = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token="+tokenAuth;
			Content response = Request.Get(url).execute().returnContent();
			
			com.google.gson.JsonParser parser = new com.google.gson.JsonParser();
			JsonElement json = parser.parse(response.asString());
			if(json.isJsonObject()) {
				JsonElement nameJson =json.getAsJsonObject().get("name");
				String name = nameJson.getAsString();
				return name;
			}
		} 
		catch (Exception e) {
			java.util.logging.Logger.getAnonymousLogger().log(Level.SEVERE, "Impossible de retrouver les infos du compte google: " + e.getMessage(), e);
		}
	
		return null;
	}
	
}

package ficheeps.server;

import ficheeps.model.Apsa;
import ficheeps.model.Commentaire;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import ficheeps.model.Eleve;
import ficheeps.model.Groupe;
import ficheeps.model.GroupeType;
import ficheeps.model.ListeLibre;
import ficheeps.model.ListeLibreItem;
import ficheeps.model.StatisticsFilter;
import ficheeps.server.actions.EleveActions;
import ficheeps.server.utils.Utils;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class ExcelExport {

    java.util.logging.Logger LOG = java.util.logging.Logger.getLogger("ExcelExport");
    
    
    public ExcelExport() {
    }

    public void exportAllGroups(OutputStream outputStream) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("Groupes EPS");

        int rowIndex = 0;
        HSSFRow row = sheet.createRow(rowIndex);
        Cell cell = null;

        int cellIndex = 0;
        row.createCell(cellIndex++).setCellValue("NUMEN");
        row.createCell(cellIndex++).setCellValue("Nom");
        row.createCell(cellIndex++).setCellValue("Prenom");
        row.createCell(cellIndex++).setCellValue("Date de naissance");
        row.createCell(cellIndex++).setCellValue("Classe");
        row.createCell(cellIndex++).setCellValue("Groupe");
        row.createCell(cellIndex++).setCellValue("Type");
        row.createCell(cellIndex++).setCellValue("Professeur du groupe");
        row.createCell(cellIndex++).setCellValue("Description du groupe");

        DBAccess db = DBAccess.Get();
        List<Groupe> groupsLight = db.getAllGroupesLight();

        for (Groupe groupeLight : groupsLight) {
            String groupeName = groupeLight.getNom();
            List<Groupe> groups = db.retrieveGroupe(groupeName);
            for (Groupe groupe : groups) {
                if (groupe.getEleves() != null) {
                    for (Eleve eleve : groupe.getEleves()) {
                        rowIndex++;
                        cellIndex = 0;
                        row = sheet.createRow(rowIndex);
                        row.createCell(cellIndex++).setCellValue("" + eleve.getId());
                        row.createCell(cellIndex++).setCellValue("" + eleve.getNom());
                        row.createCell(cellIndex++).setCellValue("" + eleve.getPrenom());
                        row.createCell(cellIndex++).setCellValue("" + sdf.format(eleve.getDateDeNaissance()));
                        row.createCell(cellIndex++).setCellValue("" + eleve.getClasse());
                        row.createCell(cellIndex++).setCellValue("" + groupeName);
                        row.createCell(cellIndex++).setCellValue(GroupeType.AS.equals(groupe.getGroupeType()) ? "AS" : "EPS");
                        row.createCell(cellIndex++).setCellValue("" + groupe.getNomProfesseur());
                        row.createCell(cellIndex++).setCellValue("" + groupe.getDescription());
                    }
                }
            }
        }

        wb.write(outputStream);
    }


    
    public String exportAllStatsAsXLSX(Workbook workook, StatisticsFilter statisticsFilter) throws Exception {
        StringBuilder logResult = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Sheet sheet = workook.createSheet("FicheEPS-long");

        int maxNumberOfApsa = 0;
        
        int rowIndex = 0;
        Row row = sheet.createRow(rowIndex++);
        Cell cell = null;

        int cellIndex = 0;
        row.createCell(cellIndex++).setCellValue("NUMEN");
        row.createCell(cellIndex++).setCellValue("NOM");
        row.createCell(cellIndex++).setCellValue("PRENOM");
        row.createCell(cellIndex++).setCellValue("DATE DE NAISSANCE");
        row.createCell(cellIndex++).setCellValue("CLASSE ACTUELLE");
        row.createCell(cellIndex++).setCellValue("NIVEAU");
        row.createCell(cellIndex++).setCellValue("ANNEE");
        row.createCell(cellIndex++).setCellValue("A.S.");
        row.createCell(cellIndex++).setCellValue("PROFESSEUR");
        row.createCell(cellIndex++).setCellValue("APSA");
        row.createCell(cellIndex++).setCellValue("NOTE");
        
        DBAccess db = DBAccess.Get();
        List<Eleve> eleves = db.doQuery(new EleveActions.GetAll());

        for (Eleve eleve : eleves) {
            try {
                String id = "" + eleve.getId();
                String nom = "" + eleve.getNom();
                String prenom = "" + eleve.getPrenom();
                String dateDeNaissance = "" + sdf.format(eleve.getDateDeNaissance());
                String classe = "" + eleve.getClasse();

                if(eleve.getCommentaires() != null) {
                    for(Commentaire commentaire : eleve.getCommentaires()) {
                        //int oldMax = maxNumberOfApsa;
                        maxNumberOfApsa = Math.max(maxNumberOfApsa, commentaire.getApsas() == null ? 0 : commentaire.getApsas().size());
                        if(!statisticsFilter.isCommentaireOkToExport(commentaire)) {
                            continue;
                        }                            
                        /*if(oldMax != maxNumberOfApsa) {
                            LOG.log(Level.SEVERE,"nbApsa: " + maxNumberOfApsa + " : " + eleve.getId() + " " + eleve.getNom() + " " + eleve.getPrenom() + " " + eleve.getClasse()+ " " + commentaire.getAnneeScolaire() + " " + commentaire.getNiveau());
                        }*/
                        String niveau = "" + commentaire.getNiveau();
                        String anneeScolaire = "" + commentaire.getAnneeScolaire();
                        if(commentaire.getApsas() != null) {
                            for(Apsa apsa : commentaire.getApsas()) {
                                if(!statisticsFilter.isAPSAOkToExport(apsa)) {
                                    continue;
                                }
                                row = sheet.createRow(rowIndex++);
                                cellIndex = 0;                            
                                row.createCell(cellIndex++).setCellValue(id);
                                row.createCell(cellIndex++).setCellValue(nom);
                                row.createCell(cellIndex++).setCellValue(prenom);
                                row.createCell(cellIndex++).setCellValue(dateDeNaissance);
                                row.createCell(cellIndex++).setCellValue(classe);
                                row.createCell(cellIndex++).setCellValue(niveau);
                                row.createCell(cellIndex++).setCellValue(anneeScolaire);
                                row.createCell(cellIndex++).setCellValue(commentaire.isAssociationSportive() ? "oui" : "non");
                                row.createCell(cellIndex++).setCellValue(commentaire.getProfesseur() == null ? "" : commentaire.getProfesseur());
                                row.createCell(cellIndex++).setCellValue(apsa.getActivite() == null ? "" : apsa.getActivite());
                                Double note = NoteToDouble(apsa.getResultat());
                                if(note != null) {
                                    row.createCell(cellIndex++).setCellValue(note.doubleValue());
                                }
                                else {
                                    row.createCell(cellIndex++).setCellValue(apsa.getResultat() == null ? "" : apsa.getResultat());
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception e) {
                logResult.append("Erreur d'export sur l'élève '")
                        .append(eleve.getId())
                        .append(" ").append(eleve.getNom())
                        .append(" ").append(eleve.getPrenom())
                        .append("': ").append(e.getMessage());
                LOG.log(Level.SEVERE,"Erreur d'export sur un élève ", e);
            }
        }

        
        Map<String,StatsAnneeNiveau> allStatsAnneeNiveau = new HashMap<String, StatsAnneeNiveau>();
        
        sheet = workook.createSheet("FicheEPS-short");
        rowIndex = 0;
        row = sheet.createRow(rowIndex++);
        cellIndex = 0;
        row.createCell(cellIndex++).setCellValue("NUMEN");
        row.createCell(cellIndex++).setCellValue("NOM");
        row.createCell(cellIndex++).setCellValue("PRENOM");
        row.createCell(cellIndex++).setCellValue("DATE DE NAISSANCE");
        row.createCell(cellIndex++).setCellValue("CLASSE ACTUELLE");
        row.createCell(cellIndex++).setCellValue("NIVEAU");
        row.createCell(cellIndex++).setCellValue("ANNEE");
        row.createCell(cellIndex++).setCellValue("A.S.");
        row.createCell(cellIndex++).setCellValue("PROFESSEUR");
        for(int i=1;i<=maxNumberOfApsa;i++) {
            row.createCell(cellIndex++).setCellValue("APSA"+i);
            row.createCell(cellIndex++).setCellValue("NOTE"+i);
        }
        int cellMoyenneIndex = cellIndex;
        row.createCell(cellIndex++).setCellValue("MOYENNE");
        
        
        for (Eleve eleve : eleves) {
            try {
                if(eleve.getCommentaires() != null) {
                    for(Commentaire commentaire : eleve.getCommentaires()) {
                        if(!statisticsFilter.isCommentaireOkToExport(commentaire)) {
                            continue;
                        }
                        String anneeScolaire = "" + commentaire.getAnneeScolaire();
                        String niveau = "" + commentaire.getNiveau();
                        
                        if(!allStatsAnneeNiveau.containsKey(anneeScolaire)) {
                            StatsAnneeNiveau san = new StatsAnneeNiveau();
                            san.annee = anneeScolaire;
                            allStatsAnneeNiveau.put(anneeScolaire,san);
                        }
                        if(!allStatsAnneeNiveau.get(anneeScolaire).statsNiveau.containsKey(niveau)) {
                            StatsEleves sn = new StatsEleves();
                            allStatsAnneeNiveau.get(anneeScolaire).statsNiveau.put(niveau,sn);
                        }
                        StatsEleves statsEleves = allStatsAnneeNiveau.get(anneeScolaire).statsNiveau.get(niveau);
                        statsEleves.nbEleves++;
                                
                        cellIndex = 0;
                        row = sheet.createRow(rowIndex++);
                        row.createCell(cellIndex++).setCellValue("" + eleve.getId());
                        row.createCell(cellIndex++).setCellValue("" + eleve.getNom());
                        row.createCell(cellIndex++).setCellValue("" + eleve.getPrenom());
                        row.createCell(cellIndex++).setCellValue("" + sdf.format(eleve.getDateDeNaissance()));
                        row.createCell(cellIndex++).setCellValue("" + eleve.getClasse());
                        row.createCell(cellIndex++).setCellValue(niveau);
                        row.createCell(cellIndex++).setCellValue(anneeScolaire);
                        row.createCell(cellIndex++).setCellValue(commentaire.isAssociationSportive() ? "oui" : "non");
                        row.createCell(cellIndex++).setCellValue(commentaire.getProfesseur() == null ? "" : commentaire.getProfesseur());
                        if(commentaire.getApsas() != null) {
                            double sum = 0;
                            int nbValue = 0;
                            for(Apsa apsa : commentaire.getApsas()) {
                                if(!statisticsFilter.isAPSAOkToExport(apsa)) {
                                    continue;
                                }                                
                                Double note = NoteToDouble(apsa.getResultat());
                                if(note != null) {
                                    sum = sum + note.doubleValue();
                                    nbValue++;
                                }
                                row.createCell(cellIndex++).setCellValue(apsa.getActivite() == null ? "" : apsa.getActivite());
                                if(note != null) {
                                    row.createCell(cellIndex++).setCellValue(note.doubleValue());
                                }
                                else {
                                    row.createCell(cellIndex++).setCellValue(apsa.getResultat() == null ? "" : apsa.getResultat());
                                }
                            }
                            if(nbValue != 0) {
                                double moyene = sum/(double)nbValue;
                                cell = row.createCell(cellMoyenneIndex);
                                cell.setCellValue(Math.round( moyene * 100.0 ) / 100.0);
                                statsEleves.nbElevesAvecNote++;
                                statsEleves.sum = statsEleves.sum + moyene;
                            }
                        }
                    }
                }
            }
            catch(Exception e) {
                logResult.append("Erreur d'export sur l'élève '")
                        .append(eleve.getId())
                        .append(" ").append(eleve.getNom())
                        .append(" ").append(eleve.getPrenom())
                        .append("': ").append(e.getMessage());
                LOG.log(Level.SEVERE,"Erreur d'export sur un élève ", e);
            }            
        }
        
        
        
        
        sheet = workook.createSheet("FicheEPS-stats");
        rowIndex = 0;
        row = sheet.createRow(rowIndex++);
        cellIndex = 0;
        row.createCell(cellIndex++).setCellValue("ANNEE");
        row.createCell(cellIndex++).setCellValue("NIVEAU");
        row.createCell(cellIndex++).setCellValue("NBELEVESTOTAL");
        row.createCell(cellIndex++).setCellValue("NBELEVESAVECNOTE");
        row.createCell(cellIndex++).setCellValue("MOYENNE");
        for(StatsAnneeNiveau san : allStatsAnneeNiveau.values()) {
            for(Map.Entry<String,StatsEleves> entrySe : san.statsNiveau.entrySet()) {
                row = sheet.createRow(rowIndex++);
                cellIndex = 0;
                row.createCell(cellIndex++).setCellValue(san.annee);
                row.createCell(cellIndex++).setCellValue(entrySe.getKey());
                StatsEleves se = entrySe.getValue();
                row.createCell(cellIndex++).setCellValue(se.nbEleves);
                row.createCell(cellIndex++).setCellValue(se.nbElevesAvecNote);
                double moyene = se.sum / se.nbElevesAvecNote;
                row.createCell(cellIndex++).setCellValue(Math.round( moyene * 100.0 ) / 100.0);
            }
        }
        
        
        
        
        sheet = workook.createSheet("FicheEPS-groupes");
        rowIndex = 0;
        row = sheet.createRow(rowIndex++);
        cellIndex = 0;
        row.createCell(cellIndex++).setCellValue("GROUPE");
        row.createCell(cellIndex++).setCellValue("G-PROFESSEUR");
        row.createCell(cellIndex++).setCellValue("G-DESCRIPTION");
        row.createCell(cellIndex++).setCellValue("G-TYPE");
        row.createCell(cellIndex++).setCellValue("ID");
        row.createCell(cellIndex++).setCellValue("NOM");
        row.createCell(cellIndex++).setCellValue("PRENOM");
        row.createCell(cellIndex++).setCellValue("DATEDENAISSANCE");
        row.createCell(cellIndex++).setCellValue("ANNEE");        
        row.createCell(cellIndex++).setCellValue("NIVEAU");
        row.createCell(cellIndex++).setCellValue("A.S.");
        row.createCell(cellIndex++).setCellValue("PROFESSEUR");
        row.createCell(cellIndex++).setCellValue("APSA");
        row.createCell(cellIndex++).setCellValue("NOTE");
        List<Groupe> groupsLight = db.getAllGroupesLight();
        for (Groupe groupeLight : groupsLight) {
            String groupeName = groupeLight.getNom() == null ? "" : groupeLight.getNom();
            List<Groupe> groups = db.retrieveGroupe(groupeName);
            if(groups != null) {
                for (Groupe groupe : groups) {
                    if (groupe != null && groupe.getEleves() != null) {
                        for (Eleve eleve : groupe.getEleves()) {
                            if(eleve.getCommentaires() != null) {
                                for(Commentaire c : eleve.getCommentaires()) {
                                    if(!statisticsFilter.isCommentaireOkToExport(c)) {
                                        continue;
                                    }                                    
                                    if(c.getApsas() != null) {
                                        for(Apsa apsa : c.getApsas()) {
                                            if(!statisticsFilter.isAPSAOkToExport(apsa)) {
                                                continue;
                                            }                                            
                                            row = sheet.createRow(rowIndex++);
                                            cellIndex = 0;                      
                                            row.createCell(cellIndex++).setCellValue(groupeName);
                                            row.createCell(cellIndex++).setCellValue(groupe.getNomProfesseur() == null ? "" : groupe.getNomProfesseur());
                                            row.createCell(cellIndex++).setCellValue(groupe.getDescription() == null ? "" : groupe.getDescription());
                                            row.createCell(cellIndex++).setCellValue(groupe.getGroupeType() == null ? "" : ""+groupe.getGroupeType());
                                            
                                            row.createCell(cellIndex++).setCellValue(eleve.getId() == null ? "" : eleve.getId());
                                            row.createCell(cellIndex++).setCellValue(eleve.getNom() == null ? "" : eleve.getNom());
                                            row.createCell(cellIndex++).setCellValue(eleve.getPrenom() == null ? "" : eleve.getPrenom());
                                            row.createCell(cellIndex++).setCellValue(eleve.getDateDeNaissance() == null ? "" : sdf.format(eleve.getDateDeNaissance()));
                                            
                                            row.createCell(cellIndex++).setCellValue(c.getAnneeScolaire() == null ? "" : c.getAnneeScolaire());
                                            row.createCell(cellIndex++).setCellValue(c.getNiveau() == null ? "" : ""+ c.getNiveau());
                                            row.createCell(cellIndex++).setCellValue(c.isAssociationSportive() ? "oui" : "non");
                                            row.createCell(cellIndex++).setCellValue(c.getProfesseur() == null ? "" : c.getProfesseur());

                                            row.createCell(cellIndex++).setCellValue(apsa.getActivite() == null ? "" : apsa.getActivite());
                                            Double note = NoteToDouble(apsa.getResultat());
                                            if(note != null) {
                                                row.createCell(cellIndex++).setCellValue(note.doubleValue());
                                            }
                                            else {
                                                row.createCell(cellIndex++).setCellValue(apsa.getResultat() == null ? "" : apsa.getResultat());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return logResult.toString();
    }

 
    
    private Double NoteToDouble(String note) {
        if(note == null) {
            return null;
        }
        
        Double result = null;
        try {
            note = note.replace(',', '.');
            result = Double.valueOf(note);        
        }
        catch(Exception e) {/*discard*/}
        
        return result;
    }
    
    
    
    private static class StatsAnneeNiveau {
        String annee;
        Map<String, StatsEleves> statsNiveau = new HashMap<String, StatsEleves>();
    }
    private static class StatsEleves {
        int nbEleves;
        int nbElevesAvecNote;
        double sum;
    }
    
    
    
   public String exportListeLibreAsXLSX(ListeLibre listeLibre,OutputStream outputStream) throws Exception {
        StringBuilder logResult = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Workbook wb = new XSSFWorkbook();
        Sheet sheet = wb.createSheet(Utils.sanitizeFileName(listeLibre.getName()));
        
        int rowIndex = 0;
        Row row = sheet.createRow(rowIndex++);
        Cell cell = null;

        int cellIndex = 0;
        row.createCell(cellIndex++).setCellValue("NUMEN");
        row.createCell(cellIndex++).setCellValue("NOM");
        row.createCell(cellIndex++).setCellValue("PRENOM");
        row.createCell(cellIndex++).setCellValue("DATE DE NAISSANCE");
        row.createCell(cellIndex++).setCellValue("CLASSE");
        row.createCell(cellIndex++).setCellValue("PROFESSEUR");
        row.createCell(cellIndex++).setCellValue("COMMENTAIRE");
        
        for (ListeLibreItem item : listeLibre.getItems()) {
            try {
                String id = "";
                String nom = "";
                String prenom = "";
                String dateDeNaissance = "";
                String classe = "";

                if(item.getEleve() != null) {
                    Eleve eleve = item.getEleve();
                    id = "" + eleve.getId();
                    nom = "" + eleve.getNom();
                    prenom = "" + eleve.getPrenom();
                    dateDeNaissance = "" + sdf.format(eleve.getDateDeNaissance());
                    classe = "" + eleve.getClasse();
                }
                
                String profName = "" + item.getProfName();
                String comment = "" + item.getComment();
                
                row = sheet.createRow(rowIndex++);
                cellIndex = 0;
                row.createCell(cellIndex++).setCellValue(id);
                row.createCell(cellIndex++).setCellValue(nom);
                row.createCell(cellIndex++).setCellValue(prenom);
                row.createCell(cellIndex++).setCellValue(dateDeNaissance);
                row.createCell(cellIndex++).setCellValue(classe);
                row.createCell(cellIndex++).setCellValue(profName);
                row.createCell(cellIndex++).setCellValue(comment);
            }
            catch(Exception e) {
                logResult.append("Erreur d'export de l'item: '")
                        .append(item.getId())
                        .append(" ").append(item.getEleve().getId())
                        .append(" ").append(item.getEleve().getNom())
                        .append(" ").append(item.getEleve().getPrenom())
                        .append("': ").append(e.getMessage());
                LOG.log(Level.SEVERE,"Erreur d'export sur un item ", e);
            }
        }
        
        
        sheet = wb.createSheet("Description");
        rowIndex = 0;
        row = sheet.createRow(rowIndex++);
        cellIndex = 0;
        row.createCell(cellIndex++).setCellValue("" + listeLibre.getName());
        row = sheet.createRow(rowIndex++);
        cellIndex = 0;
        row.createCell(cellIndex++).setCellValue("" + listeLibre.getDescription());
        
        
        wb.write(outputStream);
        
        return logResult.toString();
    }

}

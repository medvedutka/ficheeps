/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficheeps.server.utils;

import ficheeps.model.Eleve;
import ficheeps.model.EmailsDiff;
import ficheeps.model.EmailsImportOptions;
import ficheeps.server.DBAccess;
import ficheeps.server.actions.EleveActions;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author cjalady
 */
public class EmailImport {
    
    private final StringBuilder errorMessages = new StringBuilder();
    private final StringBuilder infoMessages = new StringBuilder();
    private final Map<String,EmailsDiff> emailsDiff = new HashMap<String, EmailsDiff>();
    private final EmailsImportOptions emailsImportOptions;
    
    public EmailImport(EmailsImportOptions emailsImportOptions) {
        this.emailsImportOptions = emailsImportOptions;
    }
    
    private static class EmailToImport {
        EmailToImport(String eleveId,String eleveClasse,String eleveNom,String elevePrenom,String mail) {
            this.eleveId = eleveId;
            this.eleveClasse = eleveClasse;
            this.eleveNom = eleveNom;
            this.elevePrenom = elevePrenom;
            this.mail = mail;
        }
        String eleveId;
        String eleveClasse;
        String eleveNom;
        String elevePrenom;
        String mail;

        public String toDescString() {
            StringBuilder result = new StringBuilder();
            if(!Utils.StringNullOrWhiteSpace(eleveClasse)) {
                result.append(" ").append(eleveClasse);
            }
            if(!Utils.StringNullOrWhiteSpace(eleveNom)) {
                result.append(" ").append(eleveNom);
            }
            if(!Utils.StringNullOrWhiteSpace(elevePrenom)) {
                result.append(" ").append(elevePrenom);
            } 
            result.append(" ").append(mail);
            return result.toString();
        }
    }
    
    public void process()  throws Exception{
        tryImport();
        mergeAll();
    }
    
    public List<EmailsDiff> getAllEmailsDiff() {
        List<EmailsDiff> result = new ArrayList<EmailsDiff>(emailsDiff.size());
        result.addAll(emailsDiff.values());
        return result;
    }
    
    public String getErrorMessages() {
        return errorMessages.toString();
    }
    public String getInfoMessage() {
        return infoMessages.toString();
    }
    
    
    private  void tryImport() throws Exception {
        Workbook workbook = null;
        if(emailsImportOptions.isFileXLSX()) {
            workbook = new XSSFWorkbook(getUploadedFileInputStream());
        }
        else {
            workbook = new HSSFWorkbook(getUploadedFileInputStream());
        } 
            
        Sheet sheet = workbook.getSheetAt(0);
        int rowIndex = 0;
        /*if (importOptions.isSkipFirstLine()) {
                rowIndex++;
        }*/
        Row row = null;
        while ((row = sheet.getRow(rowIndex)) != null) {
            String numen = getCellAsStringOrEmpty(row.getCell(0));
            String classe = getCellAsStringOrEmpty(row.getCell(1));
            String nom = getCellAsStringOrEmpty(row.getCell(2));
            String prenom = getCellAsStringOrEmpty(row.getCell(3));
            String email = getCellAsStringOrEmpty(row.getCell(4));
            if(!Utils.StringNullOrWhiteSpace(email)) {
                EmailToImport emailToImport = new EmailToImport(numen, classe, nom, prenom, email);
                lookupAnddiff(emailToImport);
            }
            else {
                errorMessages.append("Ligne ").append(rowIndex).append(": aucun mail n'est renseigné.\n");
            }
            rowIndex++;
        }
    }    
    
    private String getCellAsStringOrEmpty(Cell cell) {
        if(cell == null) {
            return "";
        }
        return Utils.StringToTrimedOrEmpty(cell.getStringCellValue());
    }
    
    
    private void mergeAll() {
        for(EmailsDiff ed : emailsDiff.values()) {
            String[] oldMail = ed.getOriginalMails() == null ? new String[0] : ed.getOriginalMails().split(",");
            String[] mailToImport = ed.getMailToImport() == null ? new String[0] : ed.getMailToImport().split(",");
            for(String s: mailToImport) {
                if(Utils.ArrayContains(oldMail, s)) {
                    ed.appendKeepedMail(s);
                }
                else {
                    ed.appendAddedMail(s);
                }
            }
            
            
            // In mode replace, we must remove all mails not added again
            if(emailsImportOptions.isModeReplace()) {
                List<String> mails = new LinkedList<String>(Arrays.asList(oldMail));
                if(ed.getKeepedMails() != null) {
                    mails.removeAll(ed.getKeepedMails());
                }
                ed.setRemovedMails(mails);
            }
            
            
            // Compute new mails: keepedMails + addedMails
            StringBuilder sb = new StringBuilder();
            Iterator<String> iter;
            if(ed.getKeepedMails() != null) {
                iter = ed.getKeepedMails().iterator();
                while(iter.hasNext()) {
                    sb.append(iter.next());
                    if(iter.hasNext()) {
                        sb.append(",");
                    }
                }
            }
            if(ed.getAddedMails() != null) {
                iter = ed.getAddedMails().iterator();
                while(iter.hasNext()) {
                    if(sb.length()>0) {
                        sb.append(",");
                    }                    
                    sb.append(iter.next());
                }
            }
            ed.setNewMails(sb.toString());
        }
    }
    
    private void lookupAnddiff(EmailToImport emailToImport) {
        Eleve eleve = null;
        List<Eleve> eleves = null;
        DBAccess db = DBAccess.Get();
        if(!emailToImport.eleveId.isEmpty()) {
            eleves = db.doQuery(new EleveActions.GetByNumen(emailToImport.eleveId));
        }
        else {
            eleves = db.findEleveBestMatch(emailToImport.eleveClasse,emailToImport.eleveNom,emailToImport.elevePrenom);
        }
        
        if(eleves.size() != 1) {
            errorMessages.append(emailToImport.toDescString());
            if(eleves.size()>1) {
                errorMessages.append(": trop de correspondance ! (");
                errorMessages.append(eleves.size());
                errorMessages.append(").\n");
            }
            else {
                errorMessages.append(": aucune correspondance !\n");               
            }
            return;
        }

        eleve = eleves.get(0);
        List<String> emails = eleve.getEmailsAsList();
        if(emails.contains(emailToImport.mail)) {
            infoMessages.append(emailToImport.toDescString());
            infoMessages.append(": existe déjà.\n");
            return;//discard !
        }
        
        if(!emailsDiff.containsKey(eleve.getId())) {
            EmailsDiff ed = new EmailsDiff();
            ed.setEleveId(eleve.getId());
            ed.setEleveName(eleve.getPrettyNomPrenom());
            ed.setEleveClasse(eleve.getClasse());
            ed.setOriginalMails(eleve.getEmails());
            emailsDiff.put(eleve.getId(), ed);
        }
        
        EmailsDiff ed = emailsDiff.get(eleve.getId());
        ed.appendMailToImport(emailToImport.mail);
    }
    
    private InputStream getUploadedFileInputStream() throws Exception {
        return new FileInputStream(new File(emailsImportOptions.getFileUploadToken()));
    }
    
    
}

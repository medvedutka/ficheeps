package ficheeps.server.utils;

import java.io.File;
import java.io.FileInputStream;
import javax.crypto.*;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.prevayler.foundation.serialization.Serializer;

/**
 * Fully copied and modified from org.prevayler.foundation.serialization.DESSerializer
 */
public class AESSerializer implements Serializer {

  private final ThreadLocal _ciphers = new ThreadLocal() {
    @Override
    protected Object initialValue() {
      try {
        return Cipher.getInstance("AES/CBC/PKCS5Padding");
      } catch (GeneralSecurityException e) {
        throw new RuntimeException(e);
      }
    }
  };

  private final Serializer _delegate;
  private final SecretKey _key;
  private final IvParameterSpec ivSpec;
  
  /**
   * @param key : a 128 bits key (16 bytes)
   * @throws java.security.GeneralSecurityException
   */
  public AESSerializer(Serializer delegate, byte[] key) throws GeneralSecurityException {
    _delegate = delegate;
    if (key == null || key.length != 16) {
      throw new IllegalArgumentException("Key must be 16 bytes (128 bits)");
    }
    _key = new SecretKeySpec(key, "AES");
    
    // build the initialization vector. 
    byte[] iv = { 9, 99, 1, -54, 15, -102, -97, 74, 111, -121, 46, 26, 21, 67, 68, 12 };
    ivSpec = new IvParameterSpec(iv);
  }

  @Override
  public void writeObject(OutputStream stream, Object object) throws Exception {
    Cipher cipher = getCipher();
    cipher.init(Cipher.ENCRYPT_MODE, _key,ivSpec);
      try (CipherOutputStream encrypt = new CipherOutputStream(stream, cipher)) {
          _delegate.writeObject(encrypt, object);
      }
  }

  @Override
  public Object readObject(InputStream stream) throws Exception {
    Cipher cipher = getCipher();
    cipher.init(Cipher.DECRYPT_MODE, _key,ivSpec);
    CipherInputStream decrypt = new CipherInputStream(stream, cipher);
    return _delegate.readObject(decrypt);
  }

  
  public CipherInputStream getCipherInputStream(InputStream stream) throws Exception {
    Cipher cipher = getCipher();
    cipher.init(Cipher.DECRYPT_MODE, _key,ivSpec);
    CipherInputStream decrypt = new CipherInputStream(stream, cipher);
    return decrypt;
  }
  
  public CipherOutputStream getCipherOutpuStream(OutputStream stream) throws Exception {
    Cipher cipher = getCipher();
    cipher.init(Cipher.ENCRYPT_MODE, _key,ivSpec);
    CipherOutputStream encrypt = new CipherOutputStream(stream, cipher);
    return encrypt;
  }
  
  
  private Cipher getCipher() throws GeneralSecurityException {
    try {
      return (Cipher) _ciphers.get();
    } catch (RuntimeException e) {
      if (e.getCause() instanceof GeneralSecurityException) {
        throw (GeneralSecurityException) e.getCause();
      } else {
        throw e;
      }
    }
  }

  public static byte[] StringToKey(String s) {
    byte[] key = s.getBytes(Charset.forName("UTF-8"));
    if(key.length > 16) {
        key = Arrays.copyOf(key, 16);
    }      
    return key;
  }
  
  
  
    public static void DecryptFile(File inputFile, File outputFile, String pwd) throws Exception {
        AESSerializer aesCipher = new AESSerializer(null,AESSerializer.StringToKey(pwd));
        try(FileInputStream fis = new FileInputStream(inputFile);CipherInputStream cis = aesCipher.getCipherInputStream(fis);) {
            Files.copy(cis, outputFile.toPath());
        }
    }
  
}   


package ficheeps.server.utils;

import ficheeps.model.Tuple;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;


public class ClassGroupInfosImport {
    
    private final StringBuilder errorMessages = new StringBuilder();
    private final StringBuilder infoMessages = new StringBuilder();
    private final Map<String,Tuple<String,String>> tuples = new HashMap<String, Tuple<String,String>>();
    private final File excelFile;
    
    public ClassGroupInfosImport(File excelFile) {
        this.excelFile = excelFile;
    }
    
   
    
    public void process()  throws Exception{
        tryImport();
    }
    
    public List<Tuple<String,String>> getAllTuplesToImport() {
        List<Tuple<String,String>> result = new ArrayList<Tuple<String,String>>(tuples.size());
        result.addAll(tuples.values());
        return result;
    }
    
    public String getErrorMessages() {
        return errorMessages.toString();
    }
    public String getInfoMessage() {
        return infoMessages.toString();
    }
    
    
    private  void tryImport() throws Exception {
        Workbook workbook = WorkbookFactory.create(excelFile);
        Sheet sheet = workbook.getSheetAt(0);
        int rowIndex = 0;
        Row row = null;
        while ((row = sheet.getRow(rowIndex)) != null) {
            String nomClassOrGroup = getCellAsStringOrEmpty(row.getCell(0));
            String infos = getCellAsStringOrEmpty(row.getCell(1));
            Tuple<String,String> t = new Tuple<String,String>(nomClassOrGroup, infos);
            tuples.put(nomClassOrGroup, t);
            rowIndex++;
        }
    }    
    
    
    final private DataFormatter dataFormatter = new DataFormatter();
    private String getCellAsStringOrEmpty(Cell cell) {
        if(cell == null) {
            return "";
        }
        return Utils.StringToTrimedOrEmpty(dataFormatter.formatCellValue(cell));
    }
    
}

package ficheeps.server.utils;

import com.googlecode.gwt.crypto.bouncycastle.digests.SHA1Digest;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Utils {

    public static String Sha1(String pwd) {
        try {
            //String passwordString = "happa3";
            SHA1Digest sha1 = new SHA1Digest();
            byte[] bytes;
            byte[] result = new byte[sha1.getDigestSize()];
            bytes = pwd.getBytes();
            sha1.update(bytes, 0, bytes.length);
            int val = sha1.doFinal(result, 0);
            //assertEquals("fe7f3cffd8a5f0512a5f1120f1369f48cd6f47c2", byteArrayToHexString(result));
            return byteArrayToHexString(result);
        } catch (ArrayIndexOutOfBoundsException e) {
            //fail(e.toString());
        } catch (Exception e) {
            //fail(e.toString());
        }
        return null;
    }

    private static String byteArrayToHexString(byte[] b) throws Exception {
        String result = "";
        for (int i = 0; i < b.length; i++) {
            result
                    += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }

    public static Date DateRA12(Date date) {
        if (date == null) {
            return null;
        }
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * Escape an html string. Escaping data received from the client helps to
     * prevent cross-site script vulnerabilities.
     *
     * @param html the html string to escape
     * @return the escaped string
     */
    public String EscapeHtml(String html) {
        if (html == null) {
            return null;
        }
        return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;")
                .replaceAll(">", "&gt;");
    }


    private static boolean DateEquals(Date d1,Date d2) {
        if(d1 == d2) {
            return true;
        }
        else if(d1 == null) {
            return false;
        }   
        return d1.equals(d2);
    }

    public static boolean StringEquals(String s1,String s2) {
        if(s1 == s2) {
            return true;
        }
        if(s1 == null || s2 == null) {
            return false;
        }
        return s1.equals(s2);
    }
    
    public static boolean StringNullOrWhiteSpace(String s) {
        if(s == null) {
            return true;
        }
        return s.trim().length() == 0;
    }
    
    /**
     * Return the string s as en empty string if null or to the trimed lower cased version
     * @param s
     * @return 
     */
    public static String StringToTrimedLowerCaseOrEmpty(String s) {
        if(s == null) {
            return "";
        }
        return s.trim().toLowerCase();
    }
    
    /**
     * Return the string s as en empty string if null or to the trimed lower cased version
     * @param s
     * @return 
     */
    public static String StringToTrimedOrEmpty(String s) {
        if(s == null) {
            return "";
        }
        return s.trim();
    }    
    
    /**
     * Seach in an array if it contains a value
     * @param <T>
     * @param array
     * @param v
     * @return 
     */
    public static <T> boolean ArrayContains(final T[] array, final T v) {
        if (v == null) {
            for (final T e : array)
                if (e == null)
                    return true;
        } else {
            for (final T e : array)
                if (e == v || v.equals(e))
                    return true;
        }

        return false;
    }

    
    public static void UpdateIfDifferent(Object target,Object source, String property) throws Exception {
        String getter = "get" + property.substring(0,1).toUpperCase() + property.substring(1);
        Method getMethod;
        
        Class classe = source.getClass();
        getMethod = classe.getMethod(getter);

        Class propertyType = getMethod.getReturnType();
        
        Object pValueSource = getMethod.invoke(source);
        Object pValueTarget = getMethod.invoke(target);
        
        boolean equals = false;
        if(pValueSource == pValueTarget) {
            equals = true;
        }
        else if(pValueSource == null || pValueTarget == null) {
            equals = false;
        }
        else {
            equals = pValueSource.equals(pValueTarget);
        }
        
        if(!equals) {
            String setter = "set" + property.substring(0,1).toUpperCase() + property.substring(1);
            Method setMethod = classe.getMethod(setter, propertyType);
            setMethod.invoke(target, pValueSource);
        }
    }
    
    
    public static String sanitizeFileName( String name ) {
        return name.replaceAll( "[\u0001-\u001f<>:\"/\\\\|?*\u007f]+", "" ).trim();
    }
}

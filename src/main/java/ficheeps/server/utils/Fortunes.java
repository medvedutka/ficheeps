/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ficheeps.server.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cjalady
 */
public class Fortunes {
 
    private static String[] LINES = null;
    
    public static String GetRandomFortune() {
        if(LINES ==  null) {
            CreateCache();
        }
        return LINES[ (int)(Math.floor(Math.random() * ((double)LINES.length)))];
    }
    
    
    private static synchronized void CreateCache() {
        if(LINES ==  null) {
        	Scanner scanner = null;
            try {
                List<String> lines = new LinkedList<String>();
            	scanner =  new Scanner(Fortunes.class.getResourceAsStream("/ficheeps/server/utils/sport.fortunes"), "UTF-8");
                while (scanner.hasNextLine()){
                	String line = scanner.nextLine();
                	if(line != null) {
                		line = line.trim();
                		if(!line.isEmpty()) {
                			lines.add(line);
                		}
                	}
                }  
                LINES = lines.toArray(new String[lines.size()]);
            }
            catch(Exception e) {

                Logger.getAnonymousLogger().log(Level.SEVERE,"Error caching fortunes",e);
                LINES = new String[]{"erreur"};
            }
            finally {
            	if(scanner != null) {
            		scanner.close();
            	}
            }
        }
    }
        
}

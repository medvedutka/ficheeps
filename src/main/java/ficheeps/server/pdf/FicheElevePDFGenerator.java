package ficheeps.server.pdf;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.io.IOUtils;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.CompactHtmlSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.TextMarginFinder;
import com.itextpdf.tool.xml.ElementHandler;
import com.itextpdf.tool.xml.Pipeline;
import com.itextpdf.tool.xml.Writable;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CSSFileWrapper;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.CssFileProcessor;
import com.itextpdf.tool.xml.css.CssFilesImpl;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.exceptions.RuntimeWorkerException;
import com.itextpdf.tool.xml.html.CssAppliers;
import com.itextpdf.tool.xml.html.CssAppliersImpl;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.WritableElement;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.ElementHandlerPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;

import ficheeps.model.Apsa;
import ficheeps.model.Commentaire;
import ficheeps.model.Eleve;
import ficheeps.model.Commentaire.Niveau;
import ficheeps.server.Configuration;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class FicheElevePDFGenerator {

	public static void GenerateDocument(OutputStream os, java.util.List<Eleve> eleves, PDFExportOptions options)
			throws Exception {

		
		Collections.sort(eleves, new Comparator<Eleve>() {
			@Override
			public int compare(Eleve e1, Eleve e2) {
				if((e1.getNom() ==  null) && (e2.getNom() == null)) {
					return 0;
				}
				else if(e1.getNom() ==  null) {
					return -1;
				}
				else if(e2.getNom() == null) {
					return 1;
				}
				
				int c =  e1.getNom().compareTo(e2.getNom());
				
				if(c == 0) {
					if((e1.getPrenom() ==  null) && (e2.getPrenom() == null)) {
						return c;
					}
					else if(e1.getPrenom() ==  null) {
						return -1;
					}
					else if(e2.getPrenom() == null) {
						return 1;
					}			
					c =  e1.getPrenom().compareTo(e2.getPrenom());
				}
				
				return c;
			}
		});
		
		if (options.isExportFicheSuggestionFinSeconde()) {
                        float margin = com.itextpdf.text.Utilities.millimetersToPoints(5.6f);
			Document document = new Document(PageSize.A4,margin,margin,margin,margin);
			PdfCopy copy = new PdfCopy(document, os);
			document.open();

			ByteArrayOutputStream pdfBuffer = new ByteArrayOutputStream();
			Iterator<Eleve> iter = eleves.iterator();
			while (iter.hasNext()) {
				pdfBuffer.reset();
				PdfReader pdfOriginal = new PdfReader(options.getPdfSuggestionFinDeSeconde().toByteArray());
				PdfStamper pdfStamper = new PdfStamper(pdfOriginal, pdfBuffer);
				Eleve eleve = iter.next();
				FicheElevePDFGenerator fepg = new FicheElevePDFGenerator(options, eleve);
				fepg.generateFicheSuggestionFinDeSeconde(pdfStamper, document);
				pdfStamper.close();

				PdfReader reader = new PdfReader(pdfBuffer.toByteArray());
				int n = reader.getNumberOfPages();
				for (int page = 1; page <= n; page++) {
					copy.addPage(copy.getImportedPage(reader, page));
				}
				copy.freeReader(reader);
				reader.close();

				// if(iter.hasNext()) {
				// TODO document.newPage();
				// }
			}

			document.close();

		} else {
                        float margin = com.itextpdf.text.Utilities.millimetersToPoints(5.6f);
			Document document = new Document(PageSize.A4,margin,margin,margin,margin);
			PdfWriter pdfWriter = PdfWriter.getInstance(document, os);
			document.open();

			Iterator<Eleve> iter = eleves.iterator();
			while (iter.hasNext()) {
				Eleve eleve = iter.next();
				FicheElevePDFGenerator fepg = new FicheElevePDFGenerator(options, eleve);
				fepg.generateFicheEleve(pdfWriter, document);
				if (iter.hasNext()) {
					document.newPage();
				}
			}

			document.close();
		}
	}

	private PDFExportOptions options;
	private Eleve eleve;

	public FicheElevePDFGenerator(PDFExportOptions options, Eleve eleve) {
		this.options = options;
		this.eleve = eleve;
	}

	private ColumnText columnText;

	private void commitOrBreak(Document doc, Element e) throws Exception {
		float pos = columnText.getYLine();
		columnText.addElement(e);
		if (ColumnText.hasMoreText(columnText.go(true))) {
			doc.newPage();
			columnText.setText(new Phrase());
			columnText.addElement(e);
			columnText.setYLine(doc.top());
			columnText.go();
		} else {
			columnText.addElement(e);
			columnText.setYLine(pos);
			columnText.go(false);
		}
	}

	
	
	private static boolean IsRichTextEmpty(String richText) {
		if(richText == null || richText.trim().isEmpty()) {
			return true;
		}
		richText = richText.trim();
		if("<br>".equals(richText)) {
			return true;
		}
		return false;
	}
	
	public void generateFicheSuggestionFinDeSeconde(PdfStamper pdfStamper, Document doc) throws Exception {
		float left = doc.left();
		float right = doc.right();
		float top = doc.top();
		float bottom = doc.bottom();
		float halfHeight = (bottom - top) / 2f;
		bottom = top + halfHeight;

		PdfContentByte content = pdfStamper.getOverContent(1);
		columnText = new ColumnText(content);
		columnText.setSimpleColumn(left, top, right, bottom);
		// columnText.setSimpleColumn(doc.left(),doc.top(),doc.right(),doc.bottom());

		// addMetaData(doc); //TODO
		// addTitlePage(doc);
		Paragraph header = createHeader();
		// header.setSpacingAfter(2f);
		// commitOrBreak(doc,header);

		//Retrieve commentaire for level 2sd:
		Paragraph commentaireSecondeParagraph = null;
		if(options.isFicheSuggestionFinSecondeExportCartoucheSeconde() || options.isFicheSuggestionFinSecondeExportCartoucheSecondeIfNotEmpty()) {
			Commentaire commentaireSeconde = null;
			if(eleve.getCommentaires() != null) {
				for(Commentaire c : eleve.getCommentaires()) {
					if(c.getNiveau() == Niveau.Seconde) {
						commentaireSeconde = c;
						if(options.isFicheSuggestionFinSecondeExportCartoucheSecondeIfNotEmpty()
						&& IsRichTextEmpty(c.getAppreciation())) {
							break;
						}
						commentaireSecondeParagraph = createCartouche(commentaireSeconde,false);
						commentaireSecondeParagraph.setSpacingBefore(15f);
						break;
					}
				}
			}		
		}
		Paragraph suggestionParagraph = null;
		if(options.isFicheSuggestionFinSecondeExportSuggestionFinDeSeconde()
		|| (options.isFicheSuggestionFinSecondeExportSuggestionFinDeSecondeIfNotEmpty()
			&& !IsRichTextEmpty(eleve.getSuggestionApresSeconde()))) {
			suggestionParagraph = createSuggestionFinDeSeconde();
			suggestionParagraph.setSpacingBefore(15f);
		}
		

		float pos = columnText.getYLine();
		columnText.addElement(header);
		if(commentaireSecondeParagraph != null) {columnText.addElement(commentaireSecondeParagraph);}
		if(suggestionParagraph != null) {columnText.addElement(suggestionParagraph);}

		if (ColumnText.hasMoreText(columnText.go(true))) {
			// TODO: try to resize somehow !
			columnText.setText(new Phrase());
			columnText.addElement(header);
			if(commentaireSecondeParagraph != null) {columnText.addElement(commentaireSecondeParagraph);}
			if(suggestionParagraph != null) {columnText.addElement(suggestionParagraph);}
			columnText.setYLine(pos);
			columnText.go();
		} else {
			columnText.addElement(header);
			if(commentaireSecondeParagraph != null) {columnText.addElement(commentaireSecondeParagraph);}
			if(suggestionParagraph != null) {columnText.addElement(suggestionParagraph);}
			columnText.setYLine(pos);
			columnText.go(false);
		}

	}

	public void generateFicheEleve(PdfWriter pdfWriter, Document doc) throws Exception {
		columnText = new ColumnText(pdfWriter.getDirectContent());
		columnText.setSimpleColumn(doc.left(), doc.top(), doc.right(), doc.bottom());

		addMetaData(doc);
		// addTitlePage(doc);
		Paragraph header = createHeader();
		header.setSpacingAfter(2f);
		commitOrBreak(doc, header);

		if (eleve.getCommentaires() != null) {
			// commentsInLevelOrder
			List<Commentaire> comments = new LinkedList<Commentaire>();
			comments.addAll(eleve.getCommentaires());
			Collections.sort(comments, new Commentaire.NiveauComparator(options.isCommentsInLevelOrder()));
			for (Commentaire c : comments) {
				Paragraph commentParagraph = createCartouche(c,true);
				commentParagraph.setSpacingBefore(15f);
				commitOrBreak(doc, commentParagraph);
			}
		}

		if (options.isShowSuiviMedical()) {
			Paragraph suiviMedical = createSuiviMedical();
			suiviMedical.setSpacingBefore(15f);
			commitOrBreak(doc, suiviMedical);
		}

	}

	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	private Paragraph createHeader() throws Exception {
		Paragraph paragraph = new Paragraph();
		paragraph.setKeepTogether(true);

		PdfPTable table = new PdfPTable(3);

		table.setSpacingBefore(0);
		table.setSpacingAfter(0);
		table.setWidthPercentage(100);

		// t.setBorderColor(BaseColor.GRAY);
		// t.setPadding(4);
		// t.setSpacing(4);
		// t.setBorderWidth(1);

		Font defaultFont = new Font(FontFamily.TIMES_ROMAN, (float) 12.);
		Font font = new Font(defaultFont);
		font.setStyle(Font.BOLD);
		font.setSize(font.getSize() * ((float) 1.8));
		Phrase phrase = new Phrase(eleve.getNom().toUpperCase() + " " + eleve.getPrenom(), font);
		PdfPCell c1 = zeroBorderCell(new PdfPCell(phrase));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(c1);

		font = new Font(defaultFont);
		// font.setStyle(Font.BOLD);
		font.setSize(font.getSize() * ((float) 1.3));
		phrase = new Phrase("Né(e) le: " + simpleDateFormat.format(eleve.getDateDeNaissance()), font);
		c1 = zeroBorderCell(new PdfPCell(phrase));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(c1);

		Image photo = null;
		try {
			File file = new File(Configuration.Get().getElevePhotosFolder(), eleve.getId() + ".jpg");
			if (file.exists()) {
				photo = Image.getInstance(file.getAbsolutePath());
			}
		} catch (Exception e) {
			java.util.logging.Logger.getAnonymousLogger().log(Level.SEVERE,
					"Impossible de lire la photo d'élève: " + e.getMessage(), e);
		}

		if (photo == null) {
			// TODO: must be a box and not a fake photo
			photo = Image.getInstance(IOUtils.toByteArray(this.getClass().getResourceAsStream(
					"/ficheeps/client/photo_64x64.png")));
		}

		photo.scaleToFit(60f, 60f);
		// float xPos = doc.getPageSize().getRight() - (photo.getWidth() +
		// doc.leftMargin());
		// float yPos = doc.getPageSize().getTop() - (photo.getHeight() +
		// doc.topMargin());
		// photo.setAbsolutePosition(xPos,yPos);
		// photo.scalePercent(25);
		// doc.add(photo);
		c1 = zeroBorderCell(new PdfPCell(photo));
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setVerticalAlignment(Element.ALIGN_TOP);
		table.addCell(c1);

		paragraph.add(table);

		return paragraph;
	}

	private Paragraph createCartouche(Commentaire commentaire, boolean exportAutre) throws Exception {
		Paragraph paragraph = new Paragraph();
		paragraph.setKeepTogether(true);

		{
			PdfPTable table = new PdfPTable(6);
			table.setKeepTogether(true);
			table.setSpacingBefore(0);
			table.setSpacingAfter(0);
			table.setWidthPercentage(100);

			Font font = new Font(FontFamily.TIMES_ROMAN, (float) 14.);
			font.setStyle(Font.BOLD);
			Phrase phrase = new Phrase("" + commentaire.getNiveau(), font);
			PdfPCell cell = zeroBorderCell(new PdfPCell(phrase));
			cell.setPaddingBottom(5f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cell);

			cell = zeroBorderCell(new PdfPCell(new Phrase("Année scolaire " + commentaire.getAnneeScolaire())));
			cell.setColspan(2);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cell);

			cell = zeroBorderCell(new PdfPCell(new Phrase("Professeur: " + commentaire.getProfesseur())));
			cell.setColspan(3);
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cell);

			paragraph.add(table);
		}

		{
			PdfPTable table = new PdfPTable(16);
			table.setKeepTogether(true);
			table.setSpacingBefore(0);
			table.setSpacingAfter(0);
			table.setWidthPercentage(100);

			PdfPCell cell = new PdfPCell(new Phrase("A.P.S.A"));
			cell.setColspan(3);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cell);

			int nbApsaPerLine = 3;
			cell = zeroMarginCell(new PdfPCell());
			PdfPTable layoutTable = new PdfPTable(nbApsaPerLine);
			layoutTable.setKeepTogether(true);
			layoutTable.setSpacingBefore(0);
			layoutTable.setSpacingAfter(0);
			layoutTable.setWidthPercentage(100);
			List<Apsa> apsas = new LinkedList<Apsa>();
			if (commentaire.getApsas() != null) {
				apsas.addAll(commentaire.getApsas());
				Collections.reverse(apsas); // don't get but needed to get the
											// same order as displayed!
			}
			// complete the layoutTable to insert the right number of cells:
			int nbApsaAdditional = 0;
			int mod = apsas.size() % nbApsaPerLine;
			if(mod > 0) {
				nbApsaAdditional = nbApsaPerLine - mod;
			}
			for (int i = 0; i < nbApsaAdditional; i++) {
				Apsa apsa = new Apsa();
				apsa.setActivite(" ");
				apsa.setResultat(" ");
				apsas.add(apsa);
			}
			for (Apsa apsa : apsas) {
				PdfPTable apsaTable = new PdfPTable(4);
				apsaTable.setKeepTogether(true);
				apsaTable.setSpacingBefore(0);
				apsaTable.setSpacingAfter(0);

				PdfPCell cellApsa = zeroMarginCell(new PdfPCell(new Phrase( (apsa.getActivite() == null) ? "" : ("" + apsa.getActivite()))), true, false);
				cellApsa.setColspan(3);
				cellApsa.setHorizontalAlignment(Element.ALIGN_CENTER);
				cellApsa.setVerticalAlignment(Element.ALIGN_MIDDLE);
				apsaTable.addCell(cellApsa);
				cellApsa = zeroMarginCell(new PdfPCell(new Phrase((apsa.getResultat() == null) ? "" : ("" + apsa.getResultat()))), true, true);
				cellApsa.setNoWrap(true);
				cellApsa.setHorizontalAlignment(Element.ALIGN_CENTER);
				cellApsa.setVerticalAlignment(Element.ALIGN_MIDDLE);
				apsaTable.addCell(cellApsa);

				layoutTable.addCell(apsaTable);
			}
			cell.setColspan(12);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.addElement(layoutTable);
			table.addCell(cell);

			// A.S.
			cell = new PdfPCell();
			{
				PdfPTable asTable = new PdfPTable(1);
				asTable.setKeepTogether(true);
				asTable.setSpacingBefore(0);
				asTable.setSpacingAfter(0);
				PdfPCell subCell = zeroMarginCell(new PdfPCell(new Phrase("A.S.")));
				asTable.addCell(subCell);
				if (commentaire.isAssociationSportive()) {
					Image checkImg = Image.getInstance(IOUtils.toByteArray(this.getClass().getResourceAsStream(
							"/ficheeps/client/checkBlack_32x32.png")));
					asTable.addCell(zeroMarginCell(new PdfPCell(checkImg,true)));
				}
				cell.addElement(asTable);
			}
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);

			// Apreciations / Bilan
			cell = new PdfPCell(new Phrase("Appréciations/ 	bilan:"));
			cell.setColspan(3);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cell);

			cell = new PdfPCell();
			cell.setExtraParagraphSpace(0);
			cell.setPaddingTop(0);
			cell.setPaddingBottom(5);
			{
				Paragraph p = new Paragraph();
				java.util.List<Element> elements = RichTextToPDFElements(commentaire.getAppreciation());
				p.addAll(elements);
				cell.addElement(p);

				if (elements.size() == 0) {
					for (int i = 0; i < 3; i++) {
						cell.addElement(new Paragraph(" "));
					}
				}
			}
			cell.setColspan(exportAutre ? 7 : (7 + 6));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cell);

			if(exportAutre) {
				// Autre
				cell = new PdfPCell(new Phrase("Autre:"));
				cell.setColspan(2);
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				table.addCell(cell);
	
				cell = new PdfPCell();
				cell.setExtraParagraphSpace(0);
				cell.setPaddingTop(0);
				cell.setPaddingBottom(5);
				{
					Paragraph p = new Paragraph();
					java.util.List<Element> elements = RichTextToPDFElements(commentaire.getAutre());
					p.addAll(elements);
					cell.addElement(p);
					if (elements.size() == 0) {
						for (int i = 0; i < 3; i++) {
							cell.addElement(new Paragraph(" "));
						}
					}
				}
				cell.setColspan(4);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				table.addCell(cell);
			}
			
			paragraph.add(table);
		}

		return paragraph;
	}

	private Paragraph createSuiviMedical() throws Exception {
		Paragraph paragraph = new Paragraph();
		paragraph.setKeepTogether(true);

		{
			PdfPTable table = new PdfPTable(1);
			table.setSpacingBefore(0);
			table.setSpacingAfter(0);
			table.setWidthPercentage(100);

			PdfPCell cell = zeroBorderCell(new PdfPCell());
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			{
				PdfPTable subtable = new PdfPTable(1);
				subtable.setWidthPercentage(20);
				table.setSpacingBefore(0);
				table.setSpacingAfter(0);
				PdfPCell subCell = new PdfPCell(new Phrase("Suivi médical"));
				subCell.setExtraParagraphSpace(0);
				subCell.setPaddingTop(2);
				subCell.setPaddingBottom(5);
				subCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				subCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				subtable.addCell(subCell);
				cell.addElement(subtable);
			}
			table.addCell(cell);

			cell = new PdfPCell();
			Paragraph p = new Paragraph();
			java.util.List<Element> elements = RichTextToPDFElements(eleve.getSuiviMedical());
			p.addAll(elements);
			cell.addElement(p);
			if (elements.size() == 0) {
				for (int i = 0; i < 3; i++) {
					cell.addElement(new Paragraph(" "));
				}
			}
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setExtraParagraphSpace(0);
			cell.setPaddingTop(0);
			cell.setPaddingBottom(5);
			table.addCell(cell);

			paragraph.add(table);
		}
		return paragraph;
	}

	private Paragraph createSuggestionFinDeSeconde() throws Exception {
		Paragraph paragraph = new Paragraph();
		paragraph.setKeepTogether(true);

		{
			PdfPTable table = new PdfPTable(1);
			table.setSpacingBefore(0);
			table.setSpacingAfter(0);
			table.setWidthPercentage(100);

			PdfPCell cell = zeroBorderCell(new PdfPCell());
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			{
				PdfPTable subtable = new PdfPTable(1);
				subtable.setWidthPercentage(20);
				table.setSpacingBefore(0);
				table.setSpacingAfter(0);
				PdfPCell subCell = new PdfPCell(new Phrase("Suggestion fin de seconde:"));
				subCell.setExtraParagraphSpace(0);
				subCell.setPaddingTop(2);
				subCell.setPaddingBottom(5);
				subCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				subCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				subtable.addCell(subCell);
				cell.addElement(subtable);
			}
			table.addCell(cell);

			cell = new PdfPCell();
			Paragraph p = new Paragraph();
			java.util.List<Element> elements = RichTextToPDFElements(eleve.getSuggestionApresSeconde());
			p.addAll(elements);
			cell.addElement(p);
			if (elements.size() == 0) {
				for (int i = 0; i < 3; i++) {
					cell.addElement(new Paragraph(" "));
				}
			}
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setExtraParagraphSpace(0);
			cell.setPaddingTop(0);
			cell.setPaddingBottom(5);
			table.addCell(cell);

			paragraph.add(table);
		}
		return paragraph;
	}

	private static java.util.List<Element> RichTextToPDFElements(String richText) {
		if (richText == null) {
			richText = "";
		}
		richText = richText.replace("<div>", "").replace("</div>", "").replace("<div/>","").replace("<div />","");
		richText = richText.replace("<font color=\"", "<span style=\"color:");
		richText = richText.replace("</font>", "</span>");
		richText = "<p>" + richText + "</p>";

		final java.util.List<Element> result = new LinkedList<Element>();
		try {

			HtmlCleaner cleaner = new HtmlCleaner();
			CleanerProperties props = cleaner.getProperties();
			props.setOmitComments(true);
			props.setOmitDoctypeDeclaration(true);
			props.setOmitDoctypeDeclaration(true);
			props.setOmitXmlDeclaration(true);
			props.setOmitHtmlEnvelope(true);
			TagNode node = cleaner.clean(richText);

			richText = new CompactHtmlSerializer(props).getAsString(node);

			/*
			 * ElementHandlerPipeline ehp = new ElementHandlerPipeline(new
			 * ElementHandler() { public void add(final Writable w) { if (w
			 * instanceof WritableElement) {
			 * result.addAll(((WritableElement)w).elements()); } } },null);
			 * HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
			 * htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
			 * CSSResolver cssResolver =
			 * XMLWorkerHelper.getInstance().getDefaultCssResolver(true);
			 * Pipeline<?> pipeline = new
			 * com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline
			 * (cssResolver, new HtmlPipeline(htmlContext,ehp)); XMLWorker
			 * worker = new XMLWorker(pipeline, true); XMLParser p = new
			 * XMLParser(worker); p.parse(new StringReader(richText));
			 */

			XMLWorkerHelper xmlWorker = XMLWorkerHelper.getInstance();
			//xmlWorker.parseXHtml(new ElementHandler() {
                        myPparseXHtml(new ElementHandler() {
				public void add(final Writable w) {
					if (w instanceof WritableElement) {
						result.addAll(((WritableElement) w).elements());
					}
				}
			}, new StringReader(richText));
		} catch (Exception e) {
			java.util.logging.Logger.getAnonymousLogger().log(Level.SEVERE,
					"Impossible de convertir le texte enrichis en PDF: " + e.getMessage(), e);
			result.add(new Paragraph(richText));
		}
		return result;
	}

        private static void myPparseXHtml(final ElementHandler d, final Reader in) throws IOException {
            CssFilesImpl cssFiles = new CssFilesImpl();
            cssFiles.add(myGetCSS(XMLWorkerHelper.class.getResourceAsStream("/default.css")));
            StyleAttrCSSResolver cssResolver = new StyleAttrCSSResolver(cssFiles);
            XMLWorkerFontProvider fontProvider = new XMLWorkerFontProvider(XMLWorkerFontProvider.DONTLOOKFORFONTS);
            CssAppliers cssAppliers = new CssAppliersImpl(fontProvider);
            HtmlPipelineContext hpc = new HtmlPipelineContext(cssAppliers);
            hpc.setAcceptUnknown(true).autoBookmark(true).setTagFactory(Tags.getHtmlTagProcessorFactory());
            Pipeline<?> pipeline = new CssResolverPipeline(cssResolver, new HtmlPipeline(hpc, new ElementHandlerPipeline(d,null)));
            XMLWorker worker = new XMLWorker(pipeline, true);
            XMLParser p = new XMLParser();
            p.addListener(worker);
            p.parse(in);
	}
        
        private static synchronized CssFile myGetCSS(InputStream in) {
            CssFile cssFile = null;
            if (null != in) {
                final CssFileProcessor cssFileProcessor = new CssFileProcessor();
                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                try {
                    char[] buffer = new char[8192];
                    int length;
                    while ((length = br.read(buffer)) > 0) {
                        for(int i = 0 ; i < length; i++) {
                            cssFileProcessor.process(buffer[i]);
                        }
                    }
                    cssFile = new CSSFileWrapper(cssFileProcessor.getCss(), true);
                } catch (final IOException e) { throw new RuntimeWorkerException(e); }
                finally
                { try { in.close(); } catch (final IOException e) { throw new RuntimeWorkerException(e); } }
            }
            return cssFile;
        }
        
        
	private PdfPCell zeroMarginCell(PdfPCell cell, boolean left, boolean right) {
		// cell.setBorder(0);
		cell.setBorderWidthTop(0);
		cell.setBorderWidthBottom(0);
		if (left) {
			cell.setBorderWidthLeft(0);
		}
		if (right) {
			cell.setBorderWidthRight(0);
		}
		// cell.setExtraParagraphSpace(0);
		// cell.setIndent(0);
		// cell.setPadding(0);
		return cell;
	}

	private PdfPCell zeroBorderCell(PdfPCell cell) {
		cell.setBorderWidth(0);
                /*cell.setBorderWidth(1);
                cell.setBorderColor(BaseColor.RED);*/
		return cell;
	}

	private PdfPCell zeroMarginCell(PdfPCell cell) {
		cell.setBorder(0);
		cell.setBorderWidth(0);
		cell.setExtraParagraphSpace(0);
		cell.setIndent(0);
		cell.setPadding(0);
		return cell;
	}

	private void addMetaData(Document document) {
		document.addTitle("My first PDF");
		document.addSubject("Using iText");
		document.addKeywords("Java, PDF, iText");
		document.addAuthor("Lars Vogel");
		document.addCreator("Lars Vogel");
	}

	public static void main(String[] args) {
		try {
			// showMarginRectangle("/home/thecat/test.pdf", "/tmp/foo.pdf");
			// if(true) return;

			// File outputFile = new File("/tmp/foo.pdf");
			// Document document = new Document(PageSize.A4);
			// PdfWriter pdfWriter = PdfWriter.getInstance(document, new
			// FileOutputStream(outputFile));
			// document.open();

			List<Eleve> eleves = new LinkedList<Eleve>();
                        for(int ei = 0;ei<100;ei++) {
                            Eleve e = new Eleve();
                            e.setId("0945876458");
                            e.setNom("De Maupertuis");
                            e.setPrenom("Armand");
                            e.setSuiviMedical("");
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(new Date());
                            cal.add(Calendar.YEAR, -18);
                            e.setDateDeNaissance(cal.getTime());
                            e.setClasse("1PRO-COMPTA SEC");

                            for (int j = 0; j < 12; j++) {
                                    Commentaire c = new Commentaire();
                                    c.setAnneeScolaire(j + " -2011/2012");
                                    c.setAppreciation("");
                                    c.setAutre("");
                                    c.setNiveau(Niveau.Cinquieme);
                                    c.setProfesseur("Issangrin de Valaloupos");
                                    c.setAssociationSportive(true);

                                    Apsa apsa = new Apsa();
                                    apsa.setActivite("Natation");
                                    apsa.setResultat("12.5");
                                    c.addApsa(apsa);

                                    apsa = new Apsa();
                                    apsa.setActivite("Equitation");
                                    apsa.setResultat("11.5");
                                    c.addApsa(apsa);

                                    apsa = new Apsa();
                                    apsa.setActivite("Relais 3x100m");
                                    apsa.setResultat("12.5");
                                    c.addApsa(apsa);

                                    apsa = new Apsa();
                                    apsa.setActivite("Atlethisme");
                                    apsa.setResultat("12.5");
                                    c.addApsa(apsa);

                                    c.setAppreciation(j
                                                    + " Le ptit chat est mort le petit chat est mort le petit chat est mor le petit chat est mort le petit chat est mort le petit chat est mort le petit chat est mort");
                                    e.addCommentaire(c);
                            }
                            eleves.add(e);
                        }
			// FicheElevePDFGenerator fepg = new
			// FicheElevePDFGenerator(PDFExportOptions.AllOptions(),e);
			// fepg.generateFicheEleve(pdfWriter,document);

			PDFExportOptions options = new PDFExportOptions();
			options.setCommentsInLevelOrder(true);
			options.setExportFicheSuggestionFinSeconde(false);
			options.setShowSuiviMedical(true);
			//IOUtils.copy(new FileInputStream("/home/thecat/test.pdf"), options.getPdfSuggestionFinDeSeconde());
			//GenerateDocument(new FileOutputStream("/tmp/foo.pdf"), eleves, options);
                        GenerateDocument(new FileOutputStream("c:\\Temp\\tmp\\foo.pdf"), eleves, options);

			// document.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void showMarginRectangle(String src, String dest) throws IOException, DocumentException {
		PdfReader reader = new PdfReader(src);
		PdfReaderContentParser parser = new PdfReaderContentParser(reader);
		PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
		TextMarginFinder finder;
		for (int i = 1; i <= reader.getNumberOfPages(); i++) {
			finder = parser.processContent(i, new TextMarginFinder());
			PdfContentByte cb = stamper.getOverContent(i);
			cb.rectangle(finder.getLlx(), finder.getLly(), finder.getWidth(), finder.getHeight());
			cb.stroke();
		}
		stamper.close();
		reader.close();
	}

	private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
	private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
	private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
	private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);

	private static void addEmptyLine(Paragraph paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}

}

package ficheeps.server.pdf;

import java.io.ByteArrayOutputStream;

public class PDFExportOptions {
	private boolean showSuiviMedical;
	private boolean commentsInLevelOrder; // Sixieme, Cinquieme ...
	private boolean exportFicheSuggestionFinSeconde;
	
	private boolean ficheSuggestionFinSecondeExportCartoucheSeconde = false;
	private boolean ficheSuggestionFinSecondeExportCartoucheSecondeIfNotEmpty = false;
	private boolean ficheSuggestionFinSecondeExportSuggestionFinDeSeconde = false;
	private boolean ficheSuggestionFinSecondeExportSuggestionFinDeSecondeIfNotEmpty = false;
	
	private ByteArrayOutputStream pdfSuggestionFinDeSeconde = new ByteArrayOutputStream();
	
	
	public static PDFExportOptions AllOptions() {
		PDFExportOptions o = new PDFExportOptions();
		o.setShowSuiviMedical(true);
		o.setCommentsInLevelOrder(true);
		o.setExportFicheSuggestionFinSeconde(false);
		
		o.setFicheSuggestionFinSecondeExportCartoucheSeconde(true);
		o.setFicheSuggestionFinSecondeExportCartoucheSecondeIfNotEmpty(false);
		o.setFicheSuggestionFinSecondeExportSuggestionFinDeSeconde(true);
		o.setFicheSuggestionFinSecondeExportSuggestionFinDeSecondeIfNotEmpty(false);
		
		return o;
	}

	public boolean isExportFicheSuggestionFinSeconde() {
		return exportFicheSuggestionFinSeconde;
	}

	public void setExportFicheSuggestionFinSeconde(boolean exportFicheSuggestionFinSeconde) {
		this.exportFicheSuggestionFinSeconde = exportFicheSuggestionFinSeconde;
	}

	public boolean isShowSuiviMedical() {
		return showSuiviMedical;
	}

	public void setShowSuiviMedical(boolean showSuiviMedical) {
		this.showSuiviMedical = showSuiviMedical;
	}

	public boolean isCommentsInLevelOrder() {
		return commentsInLevelOrder;
	}

	public void setCommentsInLevelOrder(boolean commentsInLevelOrder) {
		this.commentsInLevelOrder = commentsInLevelOrder;
	}

	public ByteArrayOutputStream getPdfSuggestionFinDeSeconde() {
		return pdfSuggestionFinDeSeconde;
	}

	public void setPdfSuggestionFinDeSeconde(ByteArrayOutputStream pdfSuggestionFinDeSeconde) {
		this.pdfSuggestionFinDeSeconde = pdfSuggestionFinDeSeconde;
	}

	public boolean isFicheSuggestionFinSecondeExportCartoucheSeconde() {
		return ficheSuggestionFinSecondeExportCartoucheSeconde;
	}

	public void setFicheSuggestionFinSecondeExportCartoucheSeconde(boolean ficheSuggestionFinSecondeExportCartoucheSeconde) {
		this.ficheSuggestionFinSecondeExportCartoucheSeconde = ficheSuggestionFinSecondeExportCartoucheSeconde;
	}

	public boolean isFicheSuggestionFinSecondeExportCartoucheSecondeIfNotEmpty() {
		return ficheSuggestionFinSecondeExportCartoucheSecondeIfNotEmpty;
	}

	public void setFicheSuggestionFinSecondeExportCartoucheSecondeIfNotEmpty(
			boolean ficheSuggestionFinSecondeExportCartoucheSecondeIfNotEmpty) {
		this.ficheSuggestionFinSecondeExportCartoucheSecondeIfNotEmpty = ficheSuggestionFinSecondeExportCartoucheSecondeIfNotEmpty;
	}

	public boolean isFicheSuggestionFinSecondeExportSuggestionFinDeSeconde() {
		return ficheSuggestionFinSecondeExportSuggestionFinDeSeconde;
	}

	public void setFicheSuggestionFinSecondeExportSuggestionFinDeSeconde(
			boolean ficheSuggestionFinSecondeExportSuggestionFinDeSeconde) {
		this.ficheSuggestionFinSecondeExportSuggestionFinDeSeconde = ficheSuggestionFinSecondeExportSuggestionFinDeSeconde;
	}

	public boolean isFicheSuggestionFinSecondeExportSuggestionFinDeSecondeIfNotEmpty() {
		return ficheSuggestionFinSecondeExportSuggestionFinDeSecondeIfNotEmpty;
	}

	public void setFicheSuggestionFinSecondeExportSuggestionFinDeSecondeIfNotEmpty(
			boolean ficheSuggestionFinSecondeExportSuggestionFinDeSecondeIfNotEmpty) {
		this.ficheSuggestionFinSecondeExportSuggestionFinDeSecondeIfNotEmpty = ficheSuggestionFinSecondeExportSuggestionFinDeSecondeIfNotEmpty;
	}
	
	
	
}
package ficheeps.server;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import ficheeps.model.Eleve;
import ficheeps.model.Groupe;
import ficheeps.model.GroupeType;

public class GroupeStatusAnalyser {

	// Doit répondre:
	// + Auncun élèves de la classe ccc n'appartient à un groupe.
	// + Tous les élèves de la classe ccc appartiennent à un groupe.
	// + X élèves de la classe ccc n'appartiennent a aucun groupe (Jhon Doe, Fileas Fog ...)
	// + plusieurs groupes ont le même nom !! 
	// + Le groupe ggg n'a pas le champs "professeur renseigné"
	// + Le groupe ggg n'a pas le champs "description" renseigné"
	// + L'élève eeeee appartiens à plusieurs groupes: 
	// + Le groupe ggg ne contient moins de 15 élèves
	// + le groupe ggg contient x élèves
	

	Map<String, SmallEleve> mapEleveInGroupesEPS = new HashMap<String, GroupeStatusAnalyser.SmallEleve>();
	Set<SmallEleve> elevesDansPlusieursGroupesEPS = new HashSet<GroupeStatusAnalyser.SmallEleve>();
	List<SmallGroupe> groupeSize = new LinkedList<SmallGroupe>();
	List<String> groupesWithoutProfesseurs = new LinkedList<String>();
	List<String> groupesWithoutDescription = new LinkedList<String>();
	List<String> groupesAvecLeMemeNom = new LinkedList<String>();
	Set<String> classesDontElevesInGroupeEPS = new HashSet<String>();
	List<String> classesWithoutAnyElevesInAnyGroupesEPS = new LinkedList<String>();
	Map<String,List<SmallEleve>> elevesNotInGroupesEPSButCamaradeDeClasse = new HashMap<String, List<SmallEleve>>();
	List<String> classesDontTousLesElevesDansGroupeEPS = new LinkedList<String>();
	List<SmallEleve> elevesWithoutClasseButInGroupes = new LinkedList<SmallEleve>();
	
	
	public List<SmallGroupe> getGroupeSize() {
		return groupeSize;
	}
	public List<String> getClassesDontTousLesElevesSontDansDesGroupesEPS() {
		return classesDontTousLesElevesDansGroupeEPS;
	}
	public List<String> getClasseDontAucunElevesDansUnGroupeEPS() {
		return classesWithoutAnyElevesInAnyGroupesEPS;
	}
	public Map<String,List<SmallEleve>> getClasseDontQuelquesElevesNonAffecteAUnGroupeEPS() {
		return elevesNotInGroupesEPSButCamaradeDeClasse;
	}
	public List<String> getGroupeAvecLeMemeNom() {
		return groupesAvecLeMemeNom;
	}
	public List<String> getGroupeSansProfesseur() {
		return groupesWithoutProfesseurs;
	}
	public List<String> getGroupeSansDescription() {
		return groupesWithoutDescription;
	}
	public Set<SmallEleve> getElevesDansPlusieursGroupesEPS() {
		return elevesDansPlusieursGroupesEPS;
	}
	public List<SmallEleve> getElevesWithoutClasseButInGroupes() {
		return elevesWithoutClasseButInGroupes;
	}
	
	
	public GroupeStatusAnalyser() {
	}
	
	private StringBuilder message = new StringBuilder();
	
	
	public void check() {
		checkGroupesStatus(); // must be done first to populate 'mapEleveInGroupes';
		checkClasses();
	}
	
	
    private void checkGroupesStatus() {
    	try {
	    	DBAccess db = DBAccess.Get();
	    	List<Groupe> groupsLight = db.getAllGroupesLight();
	    	HashSet<String> duplicateGroupeNamesChecker = new HashSet<String>();
	    	
	    	for(Groupe groupeLight : groupsLight) {
				String groupeName = groupeLight.getNom();
				List<Groupe> groups = db.retrieveGroupe(groupeName);
				for(Groupe groupe : groups) {
					if(groupe.getDescription() == null || groupe.getDescription().trim().isEmpty()) {
						groupesWithoutDescription.add(groupeName);
					}
					
					if(groupe.getNomProfesseur() == null || groupe.getNomProfesseur().trim().isEmpty()) {
						groupesWithoutProfesseurs.add(groupeName);
					}
					
					if(duplicateGroupeNamesChecker.contains(groupeName)) {
						groupesAvecLeMemeNom.add(groupeName);
					}
					duplicateGroupeNamesChecker.add(groupeName);
					
					SmallGroupe smallGroupe = new SmallGroupe();
					smallGroupe.nom = groupe.getNom();
					smallGroupe.nomProfesseur = groupe.getNomProfesseur();
					smallGroupe.description = groupe.getDescription();
					smallGroupe.nbEleves = (groupe.getEleves() == null) ? 0 : groupe.getEleves().size();
					smallGroupe.type = (GroupeType.AS.equals(groupe.getGroupeType())) ? "AS" : "EPS";
					groupeSize.add(smallGroupe);
		    		
					boolean groupEPS = GroupeType.EPS.equals(groupeLight.getGroupeType());
					
					
					if(groupe.getEleves() != null) {
						
						for(Eleve eleve : groupe.getEleves()) {
							if(eleve.getClasse() == null || eleve.getClasse().trim().isEmpty()) {
								SmallEleve se = new SmallEleve();
								se.id = "" + eleve.getId();
								se.nom = "" + eleve.getNom() + " " + eleve.getPrenom();
								se.classe = "" + eleve.getClasse();
								se.groupes = groupeName;
								elevesWithoutClasseButInGroupes.add(se);
							}
						}
						
						if(groupEPS) {	
							for(Eleve eleve : groupe.getEleves()) {
								if(!mapEleveInGroupesEPS.containsKey(eleve.getId())) {
									SmallEleve se = new SmallEleve();
									se.id = "" + eleve.getId();
									se.nom = "" + eleve.getNom() + " " + eleve.getPrenom();
									se.classe = "" + eleve.getClasse();
									se.groupes = groupeName;
									mapEleveInGroupesEPS.put(se.id, se);
								}
								else {
									SmallEleve se = mapEleveInGroupesEPS.get(eleve.getId());
									se.groupes = se.groupes + ", " + groupeName;
									elevesDansPlusieursGroupesEPS.add(se);
								}
								
								if(eleve.getClasse() != null && !eleve.getClasse().trim().isEmpty()) {
									classesDontElevesInGroupeEPS.add(eleve.getClasse());
								}
							}
						}
					}
				}
			}
		}
    	catch(Exception e) {
    		java.util.logging.Logger.getAnonymousLogger().log(Level.SEVERE, "Erreur lors de l'analyse des groupes", e);
    		message.append("\n\n Erreur lors de l'analyse des groupes: " + e.getMessage());
    	}	
    }
    
    
    private void checkClasses() {
    	try {
	    	DBAccess db = DBAccess.Get();
	    	List<String> classes = db.getAllClasses();
	    	for(String classe : classes) {
	    		boolean tousLesElevesDansDesGroupes = true;
	    		if(classesDontElevesInGroupeEPS.contains(classe)) {
	    			List<Eleve> eleves = db.getElevesFromClasse(classe);
	    			for(Eleve eleve : eleves) {
	    				if(!mapEleveInGroupesEPS.containsKey(eleve.getId())) {
	    					// eleve sans groupe dont ses cmarades de classes appartiennent eux à un groupe
	    					if(!elevesNotInGroupesEPSButCamaradeDeClasse.containsKey(classe)) {
	    						elevesNotInGroupesEPSButCamaradeDeClasse.put(classe,new LinkedList<GroupeStatusAnalyser.SmallEleve>());
	    					}
	    					SmallEleve se = new SmallEleve();
	    					se.id = eleve.getId();
	    					se.nom = eleve.getNom() + " " + eleve.getPrenom();
	    					se.classe = eleve.getClasse();
	    					se.groupes = "";
	    					elevesNotInGroupesEPSButCamaradeDeClasse.get(classe).add(se);
	    					tousLesElevesDansDesGroupes = false;
	    				}
	    			}
	    			if(tousLesElevesDansDesGroupes) {
	    				classesDontTousLesElevesDansGroupeEPS.add(classe);
	    			}
	    		}
	    		else {
	    			classesWithoutAnyElevesInAnyGroupesEPS.add(classe);
	    		}
	    	}
		}
    	catch(Exception e) {
    		java.util.logging.Logger.getAnonymousLogger().log(Level.SEVERE, "Erreur lors de l'analyse des groupes -analyse des classes-", e);
    		message.append("\n\n Erreur lors de l'analyse des groupes -analyse des classes-: " + e.getMessage());
    	}
    }
    

    
    public static class SmallEleve {
    	String id;
    	String classe;
    	String nom;
    	String groupes;
    }
    
    public static class SmallGroupe {
    	String nom;
    	String nomProfesseur;
    	String description;
    	String type;
    	int nbEleves;
    }

}

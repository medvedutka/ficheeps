package ficheeps.server;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import ficheeps.model.Eleve;
import ficheeps.model.Groupe;
import ficheeps.model.ListeLibre;
import ficheeps.model.ListeLibreItem;
import ficheeps.model.Settings;
import ficheeps.model.User;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.io.IOUtils;


public class Model implements Serializable {

    
    public Model() {
    }
    
    
    private LinkedList<ListeLibre> listesLibres = new LinkedList<ListeLibre>();
    public Collection<ListeLibre> getAllListesLibres() {
        if(listesLibres == null) {
            listesLibres = new LinkedList<ListeLibre>();
        }
        return listesLibres;
    }
    public ListeLibre createListeLibre() {
        if(listesLibres == null) {
            listesLibres = new LinkedList<ListeLibre>();
        }
        ListeLibre ll = new ListeLibre();
        ll.setId(nextListeLibreId());
        ll.setName("Nouvelle liste");
        listesLibres.add(ll);
        return ll;
    }
    public ListeLibre getListeLibreById(String id) {
        for(ListeLibre ll : listesLibres) {
            if(id.equals(ll.getId())) {
                return ll;
            }
        }
        return null;
    }
    public void deleteListeLibre(String id) {
        Iterator<ListeLibre> iter = listesLibres.iterator();
        while(iter.hasNext()) {
            if(id.equals(iter.next().getId())) {
                iter.remove();
            }
        }
    }
    public void deleteAllListeLibre() {
        listesLibres.clear();
    }
    
    private long listeLibreId = 0;
    private String nextListeLibreId() {
        String id = null;
        boolean checkAgain = true;
        while(checkAgain) {
            id = "#ListeLibre-" + (listeLibreId++);
            checkAgain = false;
            for(ListeLibre ll : listesLibres) {
                if(id.equals(ll.getId())) {
                    checkAgain = true;
                }
            }
        }
        return id;
    }
    
    
    private List<Groupe> groups = new LinkedList<Groupe>();
    public List<Groupe> getGroups() {
        return groups;
    }
    
    public void deleteAllGroups() {
        groups = new LinkedList<Groupe>();
    }
    
    public void deleteGroupe(String groupeOrid) {
        Iterator<Groupe> iter = groups.iterator();
        while(iter.hasNext()) {
            Groupe g = iter.next();
            if(groupeOrid.equals(g.getOrid())) {
                iter.remove();
            }
        }
    }
    
    public Groupe getGroupeByOrid(String orid) {
        for(Groupe g : groups) {
            if(orid.equals(g.getOrid())) {
                return g;
            }
        }
        return null;
    }
    
    public List<Groupe> getGroupByName(String name) {
        if(name == null) {
            name = "";
        }
        
        List<Groupe> result = new LinkedList<Groupe>();
        for(Groupe g : groups) {
            if(name.equals(g.getNom())) {
                result.add(g);
            }
        }
        return result;
    }
    public Groupe getUniqGroupByName(String name) throws Exception {
        List<Groupe> groups = getGroupByName(name);
        if(groups.size() == 1) {
            return groups.get(0);
        }
        throw new Exception("");
    }
    
    public Groupe createGroupe() {
        Groupe groupe = new Groupe();
        groupe.setOrid(nextGroupeOrid());
        groups.add(groupe);
        return groupe;
    }
    
    private long groupeOrid = 0;
    private String nextGroupeOrid() {
        String orid = null;
        boolean checkAgain = true;
        while(checkAgain) {
            orid = "#Groupe-" + (groupeOrid++);
            checkAgain = false;
            for(Groupe groupe : groups) {
                if(orid.equals(groupe.getOrid())) {
                    checkAgain = true;
                }
            }
        }
        return orid;
    }

    
    private Map<String,Eleve> eleves = new HashMap<String,Eleve>();

    public Eleve createEleve() {
        Eleve eleve = new Eleve();
        eleve.setOrid(nextEleveOrid());
        eleves.put(eleve.getOrid(), eleve);
        return eleve;
    }
    
    public Collection<Eleve> getAllEleves() {
        return eleves.values();
    }
    
    public Eleve getEleveByOrid(String orid) {
        if(eleves.containsKey(orid)) {
            return eleves.get(orid);
        }
        return null;
    }
    
    public List<Eleve> getElevesByNumen(String numen) {
        if(numen == null) {numen = "";}
        List<Eleve> result = new LinkedList<Eleve>();
        for(Eleve eleve : eleves.values()) {
            if(numen.equals(eleve.getId())) {
                result.add(eleve);
            }
        }
        return result;
    }
    
    public List<Eleve> getElevesFromClasse(String classe) {
        if(classe == null) {classe = "";}
        List<Eleve> result = new LinkedList<Eleve>();
        for(Eleve eleve : eleves.values()) {
            if(classe.equals(eleve.getClasse())) {
                result.add(eleve);
            }
        }
        return result;
    }
    
    public void deleteEleve(String eleveOrid) {
        Iterator<Map.Entry<String,Eleve>> iter = eleves.entrySet().iterator();
        while(iter.hasNext()) {
            Eleve eleve = iter.next().getValue();
            if(eleveOrid.equals(eleve.getOrid())) {
                iter.remove();
                break;
            }
        }
        
        //Must delete eleve from Group and from ListeLibre
        for(Groupe group : getGroups()) {
            group.removeEleve(eleveOrid);
        }
        for(ListeLibre ll : getAllListesLibres()) {
            ll.deleteItemsWithEleve(eleveOrid);
        }
    }
    
    public void deleteAllElevesWithoutClasses() {
        Iterator<Map.Entry<String,Eleve>> iter = eleves.entrySet().iterator();
        while(iter.hasNext()) {
            Eleve eleve = iter.next().getValue();
            if(eleve.getClasse() == null || eleve.getClasse().isEmpty()) {
                iter.remove();
            }
        }
    }

    public String deleteAllElevesWithoutClassesBornBefore(Date date) {
        int nbDeletion = 0;
        Iterator<Map.Entry<String,Eleve>> iter = eleves.entrySet().iterator();
        while(iter.hasNext()) {
            Eleve eleve = iter.next().getValue();
            if(eleve.getClasse() == null || eleve.getClasse().isEmpty()) {
                if(eleve.getDateDeNaissance() != null && eleve.getDateDeNaissance().before(date)) {
                    iter.remove();
                    nbDeletion++;
                }
            }
        }
        return "" + nbDeletion + " élèves supprimés.";
    }
    
    private long eleveOrid = 0;
    private String nextEleveOrid() {
        String orid = null;
        boolean checkAgain = true;
        while(checkAgain) {
            orid = "#Eleve-" + (eleveOrid++);
            checkAgain = eleves.containsKey(orid);
        }
        return orid;
    }
   
    
    
    
    private List<User> users = new LinkedList<User>();
    public List<User> getAllUsers() {
        return users;
    }
    
    public User getUserByOrid(String userOrid) {
        for(User user : users) {
            if(userOrid.equals(user.getOrid())) {
                return user;
            }
        }
        return null;
    }
    
    public User createUser() {
        User user = new User();
        user.setOrid(nextUserOrid());
        users.add(user);
        return user;
    }
    
    public void deleteUser(String userOrid) {
        Iterator<User> iter = users.iterator();
        while(iter.hasNext()) {
            if(userOrid.equals(iter.next().getOrid())) {
                iter.remove();
            }
        }
    }
    
    public void deleteAllUsers() {
        users.clear();
    }
    
    private long userOrid = 0;
    private String nextUserOrid() {
        String orid = null;
        boolean checkAgain = true;
        while(checkAgain) {
            orid = "#User-" + (userOrid++);
            checkAgain = false;
            for(User user : users) {
                if(orid.equals(user.getOrid())) {
                    checkAgain = true;
                }
            }
        }
        return orid;
    }
    
 
    private Settings settings = new Settings();
    public Settings getSettings() {
        return settings;
    }
    
    
    
    public void importFormFile_v_old(InputStream inputStream) throws Exception {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        IOUtils.copy(inputStream, buffer);
        ByteArrayInputStream isBuffer = new ByteArrayInputStream(buffer.toByteArray());

        ZipEntry ze = null;
        Gson gson = new Gson();
        ZipInputStream zis = null;

        isBuffer.reset();
        zis = new ZipInputStream(isBuffer);
        while(((ze = zis.getNextEntry()) != null) && (!"Eleve".equals(ze.getName()))) {/*DoNothig!*/}
        if(ze != null) {
            JsonReader reader = new JsonReader(new InputStreamReader(zis, "UTF-8"));
            reader.beginArray();
            while (reader.hasNext()) {
                Eleve eleve = gson.fromJson(reader, Eleve.class);
                eleve.setOrid(nextEleveOrid());
                eleves.put(eleve.getOrid(), eleve);
            }
            reader.endArray();
            reader.close();
        }

        isBuffer.reset();
        zis = new ZipInputStream(isBuffer);
        while(((ze = zis.getNextEntry()) != null) && (!"User".equals(ze.getName()))) {/*DoNothig!*/}
        if(ze != null) {
            JsonReader reader = new JsonReader(new InputStreamReader(zis, "UTF-8"));
            reader.beginArray();
            while (reader.hasNext()) {
                User user = gson.fromJson(reader, User.class);
                user.setOrid(nextUserOrid());
                users.add(user);
            }
            reader.endArray();
            reader.close();
        }

        isBuffer.reset();
        zis = new ZipInputStream(isBuffer);
        while(((ze = zis.getNextEntry()) != null) && (!"Groupe".equals(ze.getName()))) {/*DoNothing!*/}
        if(ze != null) {
            JsonReader reader = new JsonReader(new InputStreamReader(zis, "UTF-8"));
            reader.beginArray();
            while (reader.hasNext()) {
                Groupe groupe = gson.fromJson(reader, Groupe.class);
                groupe.setOrid(nextGroupeOrid());
                List<Eleve> eleves = new LinkedList<Eleve>();
                for(Eleve eleve : groupe.getEleves()) {
                    List<Eleve> list = getElevesByNumen(eleve.getId());
                    if (list.size() > 1) {
                        throw new Exception("Impossible de créer le groupe: Au moins deux élèves possèdent le même identifiant " + eleve.getId());
                    } else if (list.size() == 0) {
                        throw new Exception("Impossible de créer le groupe: Aucun élèves avec l'identifiant '" + eleve.getId() + "' n'a été trouvé dans la base");
                    }
                    eleves.add(list.get(0));
                }
                groupe.setEleves(eleves);
                groups.add(groupe);
            }
            reader.endArray();
            reader.close();
        }

        
        {//ListeLibre begin
            isBuffer.reset();
            zis = new ZipInputStream(isBuffer);
            while(((ze = zis.getNextEntry()) != null) && (!"ListeLibre".equals(ze.getName()))) {/*DoNothing!*/}
            if(ze != null) {
                JsonReader reader = new JsonReader(new InputStreamReader(zis, "UTF-8"));
                reader.beginArray();
                while (reader.hasNext()) {
                    ListeLibre listeLibre = gson.fromJson(reader, ListeLibre.class);
                    listeLibre.setId(nextListeLibreId());
                    if(listeLibre.getItems() != null) {
                        Iterator<ListeLibreItem> iter = listeLibre.getItems().iterator();
                        while(iter.hasNext()) {
                            ListeLibreItem item = iter.next();
                            Eleve eleve = item.getEleve();
                            if(eleve != null) {
                                List<Eleve> list = getElevesByNumen(eleve.getId());
                                if (list.size() > 1) {
                                    iter.remove();
                                    java.util.logging.Logger.getLogger("Model.importFormFile_v_old").log(Level.SEVERE,"Erreur lors de la création de la ListeLibre: Au moins deux élèves possèdent le même identifiant " + eleve.getId());
                                } 
                                else if (list.isEmpty()) {
                                    iter.remove();
                                    java.util.logging.Logger.getLogger("Model.importFormFile_v_old").log(Level.SEVERE,"Erreur lors de la création de la ListeLibre: Aucun élèves avec l'identifiant '" + eleve.getId() + "' n'a été trouvé dans la base");
                                }
                                else {
                                    item.setEleve(list.get(0));
                                }
                            }
                            else {
                                iter.remove();
                            }
                        }
                    }
                    listesLibres.add(listeLibre);
                }
                reader.endArray();
                reader.close();
            }
        }//ListeLibre end

        isBuffer.reset();
        zis = new ZipInputStream(isBuffer);
        while(((ze = zis.getNextEntry()) != null) && (!"Settings".equals(ze.getName()))) {/*DoNothig!*/}
        if(ze != null) {
            JsonReader reader = new JsonReader(new InputStreamReader(zis, "UTF-8"));
            reader.beginArray();
            while (reader.hasNext()) {
                settings = gson.fromJson(reader, Settings.class);
            }
            reader.endArray();
            reader.close();
        }                

    }
    
    
    
}

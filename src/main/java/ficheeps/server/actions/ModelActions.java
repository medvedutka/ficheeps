package ficheeps.server.actions;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;
import ficheeps.model.Apsa;
import ficheeps.model.Commentaire;
import ficheeps.model.Eleve;
import ficheeps.model.Groupe;
import ficheeps.model.ListeLibre;
import ficheeps.model.Settings;
import ficheeps.model.StatisticsFilter;
import ficheeps.model.User;
import ficheeps.server.Model;
import ficheeps.server.DBAccess;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.prevayler.Query;
import org.prevayler.Transaction;


public class ModelActions {
    
    private ModelActions() {
    }
    
    public static class GetAllClassesNames implements Query<Model, List<String>> {
        @Override
        public List<String> query(Model model, Date date) throws Exception {
            Set<String> set = new HashSet<String>();
            for(Eleve e : model.getAllEleves()) {
                if(e.getClasse() != null) {
                    set.add(e.getClasse());
                }
            }
            return new ArrayList<String>(set);
        }
        
    }
    
    
    public static class GetApsaForSuggestion implements Query<Model, List<String>> {
        @Override
        public List<String> query(Model model, Date date) throws Exception {
            Set<String> set = new HashSet<String>();
            for(Eleve e : model.getAllEleves()) {
                if(e.getCommentaires() != null) {
                    for(Commentaire c : e.getCommentaires()) {
                        if(c.getApsas() != null) {
                            for(Apsa a : c.getApsas()) {
                                if(a.getActivite() != null) {
                                    set.add(a.getActivite());
                                }
                            }
                        }
                    }
                }
            }
            return new ArrayList<String>(set);
        }   
    }
    
    
    public static class GetElevesFromClasse implements Query<Model, List<Eleve>> {
        private String className = "";
        public void setClassName(String cn) {
            this.className = cn;
        }
        public String getClassName() {
            return this.className;
        }
        
        public GetElevesFromClasse() {
        }
        public GetElevesFromClasse(String classe) {
            this.className = classe;
        }

        @Override
        public List<Eleve> query(Model model, Date date) throws Exception {
            if(this.className == null) {
                this.className = "";
            }
            List<Eleve> result = new LinkedList<Eleve>();
            for(Eleve e : model.getAllEleves()) {
                if(className.equals(e.getClasse())) {
                    result.add(e);
                }
            }
            return result;
        }
    }
    
    public static class GetAllGroupsNameForEleve implements Query<Model, List<String>> {
        
        private String eleveOrid;
        public String getEleveOrid() {
            return eleveOrid;
        }
        public void setEleveOrid(String eleveOrid) {
            this.eleveOrid = eleveOrid;
        }
        
        public GetAllGroupsNameForEleve() {
        }
        public GetAllGroupsNameForEleve(String eleveOrid) {
            this.eleveOrid = eleveOrid;
        }
        
        @Override
        public List<String> query(Model model, Date date) throws Exception {
            if(this.eleveOrid == null) {
                this.eleveOrid = "";
            }
            Set<String> resultSet = new HashSet<String>();
            Eleve eleve = model.getEleveByOrid(eleveOrid);
            for(Groupe g : model.getGroups()) {
                if(g.getEleves().contains(eleve)) {
                    resultSet.add(g.getNom());
                }
            }
            return new ArrayList<String>(resultSet);
        }
    }
    
    
    public static class DeleteAllGroups implements Transaction<Model> {
        public DeleteAllGroups() {
        }

        @Override
        public void executeOn(Model model, Date date) {
            model.deleteAllGroups();
        }
    }
    
    
    public static class UnregisterAllElevesFromClasses implements Transaction<Model> {
        public UnregisterAllElevesFromClasses() {
        }
        
        @Override
        public void executeOn(Model model, Date date) {
            for(Eleve eleve : model.getAllEleves()) {
                eleve.setClasse("");
            }
        }
    }
    
    
    public static class CreateDummyDB implements Transaction<Model> {

        public CreateDummyDB() {
        }
        
        @Override
        public void executeOn(Model model, Date date) {
            Scanner sc = new Scanner(DBAccess.class.getResourceAsStream("/ficheeps/server/noms.txt"));
            List<String> lines = new LinkedList<String>();
            while (sc.hasNextLine()) {
                lines.add(sc.nextLine());
            }
            String[] noms = lines.toArray(new String[0]);
            lines.clear();
            sc = new Scanner(DBAccess.class.getResourceAsStream("/ficheeps/server/prenoms.txt"));
            while (sc.hasNextLine()) {
                lines.add(sc.nextLine());
            }
            String[] prenoms = lines.toArray(new String[0]);
            lines.clear();
            sc = new Scanner(DBAccess.class.getResourceAsStream("/ficheeps/server/sportifs.txt"));
            while (sc.hasNextLine()) {
                lines.add(sc.nextLine());
            }
            String[] sportifs = lines.toArray(new String[0]);
            sc = new Scanner(DBAccess.class.getResourceAsStream("/ficheeps/server/fortunes.txt"));
            sc.useDelimiter("\\%");
            while (sc.hasNext()) {
                lines.add(sc.next().trim());
            }
            String[] fortunes = lines.toArray(new String[0]);
            lines.clear();
            sc = new Scanner(DBAccess.class.getResourceAsStream("/ficheeps/server/sports.txt"));
            sc.useDelimiter("\\n");
            while (sc.hasNext()) {
                lines.add(sc.next().trim());
            }
            String[] sports = lines.toArray(new String[0]);

            String[] classesPrefix = { "1", "2", "T", "6", "5", "4", "3", "1STG", "TSTG", "2PRO", "1PRO", "TPRO",
                            "2PRO-COMPTA", "1PRO-COMPTA", "TPRO-COMPTA" };
            String[] anneeScolaires = { "2001/2002", "2002/2003", "2003/2004", "2004/2005", "2005/2006", "2006/2007",
                            "2007/2008", "2008/2009", "2009/2010", "2010/2011", "2011/2012", "2012/2013" };

            int id = 3500;

            for (String cp : classesPrefix) {
                for (int cn = 1; cn < 15; cn++) {
                    for (int i = 0; i < 25; i++) {
                        Eleve e = model.createEleve();
                        e.setId(String.format("%07d", id++));
                        e.setNom(noms[(int) (Math.random() * ((double) noms.length))]);
                        e.setPrenom(prenoms[(int) (Math.random() * ((double) prenoms.length))]);
                        e.setSuiviMedical(fortunes[(int) (Math.random() * ((double) fortunes.length))]);
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(new Date());
                        cal.add(Calendar.YEAR, -18);
                        cal.add(GregorianCalendar.DATE, (int) (Math.random() * (7. * 365)));
                        e.setDateDeNaissance(cal.getTime());
                        e.setClasse(String.format("%s%02d", cp, cn));

                        int maxComments = (int) (Math.random() * 5.0) + 1;
                        for (int j = 0; j < maxComments; j++) {
                                Commentaire c = new Commentaire();
                                c.setAnneeScolaire(anneeScolaires[(int) (Math.random() * ((double) anneeScolaires.length))]);
                                c.setAppreciation(fortunes[(int) (Math.random() * ((double) fortunes.length))]);
                                c.setAutre(fortunes[(int) (Math.random() * ((double) fortunes.length))]);
                                c.setNiveau(Commentaire.Niveau.values()[(int) (Math.random() * ((double) Commentaire.Niveau.values().length))]);
                                c.setProfesseur(sportifs[(int) (Math.random() * ((double) sportifs.length))]);
                                c.setAssociationSportive(Math.random() > 0.5);
                                int nbApsa = 3 + (int)(Math.random() * 10.0);	
                                for(int iApsa = 0;iApsa<nbApsa;iApsa++) {
                                        Apsa apsa = new Apsa();
                                        apsa.setActivite(sports[(int) (Math.random() * ((double) sports.length))]);
                                        apsa.setResultat(((int)(Math.random() * 20.0)) +"," + ((int)(Math.random() * 9.0)));
                                        c.addApsa(apsa);
                                }
                                e.addCommentaire(c);
                        }
                    }
                }
            }
        }
    }
            
    
    
    public static class ExportModel_v_old implements Query<Model,String> {
        
        private OutputStream outputStream;
        public OutputStream getOutputStream() {
            return outputStream;
        }
        public void setOutputStream(OutputStream outputStream) {
            this.outputStream = outputStream;
        }

        private ExportModel_v_old() {
        }
        
        public ExportModel_v_old(OutputStream outputStream) {
            this.outputStream = outputStream;
        }
        
        @Override
        public String query(Model model, Date date) throws Exception {
            Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                @Override
                public boolean shouldSkipField(FieldAttributes arg0) {
                        if("orid".equals(arg0.getName())) {
                                return true;
                        }
                        return false;
                }
                @Override
                public boolean shouldSkipClass(Class<?> arg0) {
                        return false;
                }
            }).create();
            ZipOutputStream zos = new ZipOutputStream(outputStream);

            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            JsonWriter writer = null;

            buffer.reset();
            writer = new JsonWriter(new OutputStreamWriter(buffer, "UTF-8"));
            writer.beginArray();
            for(Eleve eleve : model.getAllEleves()) {
                gson.toJson(eleve, Eleve.class, writer);
            }
            writer.endArray();
            writer.flush();
            writer.close();

            zos.putNextEntry(new ZipEntry("Eleve"));
            buffer.writeTo(zos);
            zos.closeEntry();


            buffer.reset();
            writer = new JsonWriter(new OutputStreamWriter(buffer, "UTF-8"));
            writer.beginArray();
            for(User user: model.getAllUsers()) {
                gson.toJson(user, User.class, writer);
            }
            writer.endArray();
            writer.flush();
            writer.close();

            zos.putNextEntry(new ZipEntry("User"));
            buffer.writeTo(zos);
            zos.closeEntry();
            
            {
                buffer.reset();
                writer = new JsonWriter(new OutputStreamWriter(buffer, "UTF-8"));			
                writer.beginArray();
                Gson gson4Groupe = new GsonBuilder()
                    .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes arg0) {
                        if(arg0.getDeclaredClass() == Eleve.class) {
                            if("id".equals(arg0.getName())) {
                                return false;
                            }
                            return true;
                        }
                        return false;
                    }
                    @Override
                    public boolean shouldSkipClass(Class<?> arg0) {
                            return false;
                    }
                }).create();
                for(Groupe groupe : model.getGroups()) {
                    gson4Groupe.toJson(groupe, Groupe.class, writer);
                }
                writer.endArray();
                writer.flush();
                writer.close();

                zos.putNextEntry(new ZipEntry("Groupe"));
                buffer.writeTo(zos);
                zos.closeEntry();
            }

            
            {//ListeLibre begin
                buffer.reset();
                writer = new JsonWriter(new OutputStreamWriter(buffer, "UTF-8"));			
                writer.beginArray();
                Gson gson4ListeLibre = new GsonBuilder()
                    .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes arg0) {
                        if(arg0.getDeclaringClass() == Eleve.class) {
                            if("id".equals(arg0.getName())) {
                                return false;
                            }
                            return true;
                        }
                        return false;
                    }
                    @Override
                    public boolean shouldSkipClass(Class<?> arg0) {
                        return false;
                    }
                }).create();
                for(ListeLibre listeLibre : model.getAllListesLibres()) {
                    gson4ListeLibre.toJson(listeLibre, ListeLibre.class, writer);
                }
                writer.endArray();
                writer.flush();
                writer.close();

                zos.putNextEntry(new ZipEntry("ListeLibre"));
                buffer.writeTo(zos);
                zos.closeEntry();
            }//ListeLibre end

            
            buffer.reset();
            writer = new JsonWriter(new OutputStreamWriter(buffer, "UTF-8"));			
            writer.beginArray();
            gson.toJson(model.getSettings(), Settings.class, writer);
            writer.endArray();
            writer.flush();
            writer.close();

            zos.putNextEntry(new ZipEntry("Settings"));
            buffer.writeTo(zos);
            zos.closeEntry();                        
            
            zos.close();
            return "Good"; //TODO
	}        
    }

    
     public static class GetFullStatisticsFilter implements Query<Model, StatisticsFilter> {

        public GetFullStatisticsFilter() {
        }
        
        @Override
        public StatisticsFilter query(Model model, Date date) throws Exception {
            StatisticsFilter statFilter = new StatisticsFilter();
            
            Set<String> annees = new HashSet<>();
            Set<String> apsas = new HashSet<>();
            for(Eleve eleve : model.getAllEleves()) {
                if(eleve.getCommentaires() != null) {
                    for(Commentaire commentaire : eleve.getCommentaires()) {
                        String annee = commentaire.getAnneeScolaire();
                        if(annee != null) {
                            annees.add(annee);
                        }
                        if(commentaire.getApsas() != null) {
                            for(Apsa apsa : commentaire.getApsas()) {
                                String label = apsa.getActivite();
                                if(label != null) {
                                    apsas.add(label);
                                }
                            }
                        }
                    }
                }
            }
            
            statFilter.getAnnees().clear();
            statFilter.getAnnees().addAll(annees);
            Collections.sort(statFilter.getAnnees());
            statFilter.getApsas().clear();
            statFilter.getApsas().addAll(apsas);
            Collections.sort(statFilter.getApsas(),String.CASE_INSENSITIVE_ORDER);
            statFilter.getNiveaux().clear();
            for(Commentaire.Niveau n : Commentaire.Niveau.values()) {
                statFilter.getNiveaux().add(n.name());
            }
            
            return statFilter;
        }
    }
    
            


}

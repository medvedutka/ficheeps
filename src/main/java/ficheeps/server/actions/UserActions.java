package ficheeps.server.actions;

import ficheeps.model.User;
import ficheeps.server.Model;
import ficheeps.server.utils.Utils;
import java.util.Date;
import java.util.List;
import org.prevayler.Query;
import org.prevayler.Transaction;
import org.prevayler.TransactionWithQuery;


public class UserActions {

    public static class Create implements TransactionWithQuery<Model, User>  {
        public Create() {
        }

        @Override
        public User executeAndQuery(Model model, Date date) throws Exception {
            return model.createUser();
        }
    }
    
    public static class Set implements TransactionWithQuery<Model, User>  {
        private String userId;
        private String _login = null;
        private String _pwd = null;
        private Boolean _local = null;
        private String _fullName = null;
        private Boolean _admin = null;
        
        private Set() {}
        public Set(String userId) {
            this.userId = userId;
        }
        
        public Set login(String _login) {
            this._login = _login;
            return this;
        }
        public Set pwd(String _pwd) {
            this._pwd = _pwd;
            return this;
        }
        public Set fullName(String _fullName) {
            this._fullName = _fullName;
            return this;
        }
        public Set admin(Boolean _admin) {
            this._admin = _admin;
            return this;
        }
        public Set local(Boolean _local) {
            this._local = _local;
            return this;
        }        
        
        @Override
        public User executeAndQuery(Model model, Date date) throws Exception {
            User user = model.getUserByOrid(userId);
            if(user != null) {
                if(_login != null) {
                    user.setLogin(_login);
                }
                if(_pwd != null) {
                    user.setPwd(_pwd);
                }
                if(_fullName != null) {
                    user.setFullName(_fullName);
                }
                if(_local != null) {
                    user.setLocal(_local);
                }
                if(_admin != null) {
                    user.setAdmin(_admin);
                }
            }
            return user;
        }
    }    
    
    
    public static class UpdateFirstLevel implements TransactionWithQuery<Model, User>  {
        private String userOrid;
        private User withUser;
        
        private UpdateFirstLevel(){}
        public UpdateFirstLevel(String userOrid, User withUser) {
            this.userOrid = userOrid;
            this.withUser = withUser;
        }
        
        @Override
        public User executeAndQuery(Model model, Date date) throws Exception {
            User user = model.getUserByOrid(userOrid);
            if(user != null) {
                user.setLocal(withUser.isLocal());
                Utils.UpdateIfDifferent(user, withUser, "login");
                Utils.UpdateIfDifferent(user, withUser, "pwd");
                Utils.UpdateIfDifferent(user, withUser, "fullName");
                user.setAdmin(withUser.isAdmin());
                return user;
            }
            return null;
        }
    }
    
    public static class GetAllUsers implements Query<Model, List<User>> {
    
        public GetAllUsers() {
        }

        @Override
        public List<User> query(Model model, Date date) throws Exception {
            return model.getAllUsers();
        }
    }
    
    public static class Delete implements Transaction<Model> {
        private String userOrid;
        
        private Delete() {
        }
        public Delete(String userOrid) {
            this.userOrid = userOrid;
        }

        @Override
        public void executeOn(Model model, Date date) {
            model.deleteUser(userOrid);
        }
    }
    
    public static class DeleteAll implements Transaction<Model> {
        public DeleteAll() {
        }

        @Override
        public void executeOn(Model model, Date date) {
            model.deleteAllUsers();
        }
    }
    
    public static class LocalLogin implements Query<Model, User> {
        private String localLogin;
        private String pwdSha1;
        
        private LocalLogin() {}
        public LocalLogin(String localLogin, String pwdSha1) {
            this.localLogin = localLogin;
            this.pwdSha1 = pwdSha1;
        }

        @Override
        public User query(Model model, Date date) throws Exception {
            for(User user : model.getAllUsers()) {
                if(user.isLocal() && localLogin.equals(user.getLogin()) && pwdSha1.equals(user.getPwd())) {
                    return user;
                }
            }
            return null;
        }
    }

    public static class EmailLogin implements Query<Model, User> {
        private String emailLogin;
        
        private EmailLogin() {}
        public EmailLogin(String emailLogin) {
            this.emailLogin = emailLogin;
        }

        @Override
        public User query(Model model, Date date) throws Exception {
            for(User user : model.getAllUsers()) {
                if(!user.isLocal() && emailLogin.equalsIgnoreCase(user.getLogin())) {
                    return user;
                }
            }
            return null;
        }
    }


}

package ficheeps.server.actions;

import ficheeps.model.Eleve;
import ficheeps.model.Groupe;
import ficheeps.server.Model;
import ficheeps.server.utils.Utils;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.prevayler.Query;
import org.prevayler.Transaction;
import org.prevayler.TransactionWithQuery;

public class GroupeActions {

    private GroupeActions() {}
            
    
    public static class Create implements TransactionWithQuery<Model, Groupe>{
        @Override
	public Groupe executeAndQuery(Model model, Date date){
            return model.createGroupe();
	}
    }
    
    public static class Delete implements Transaction<Model> {
        private String groupeOrid;
        
        private Delete() {
        }
        public Delete(String groupeOrid) {
            this.groupeOrid = groupeOrid;
        }

        @Override
        public void executeOn(Model model, Date date) {
            model.deleteGroupe(groupeOrid);
        }
    }
    
    public static class AddEleve implements TransactionWithQuery<Model, Groupe>{
        private String groupOrid;
        private String eleveOrid;
        
        private AddEleve() {}
        public AddEleve(String groupOrid,String eleveOrid) {
            this.groupOrid = groupOrid;
            this.eleveOrid = eleveOrid;
        }

        @Override
        public Groupe executeAndQuery(Model model, Date date) throws Exception {
            Groupe groupe = model.getGroupeByOrid(groupOrid);
            Eleve eleve = model.getEleveByOrid(eleveOrid);
            groupe.addEleve(eleve);
            return groupe;
        }
    }
    
    public static class RemoveEleve implements TransactionWithQuery<Model, Groupe>{
        private String groupOrid;
        private String eleveOrid;
        
        private RemoveEleve() {}
        public RemoveEleve(String groupOrid,String eleveOrid) {
            this.groupOrid = groupOrid;
            this.eleveOrid = eleveOrid;
        }

        @Override
        public Groupe executeAndQuery(Model model, Date date) throws Exception {
            Groupe groupe = model.getGroupeByOrid(groupOrid);
            Eleve eleve = model.getEleveByOrid(eleveOrid);
            groupe.removeEleve(eleve);
            return groupe;
        }
    }
    
     
    public static class UpdateFirstLevel implements TransactionWithQuery<Model, Groupe>  {
        private String groupeOrid;
        private Groupe withGroupe;
        
        private UpdateFirstLevel(){}
        public UpdateFirstLevel(String groupeOrid, Groupe withGroupe) {
            this.groupeOrid = groupeOrid;
            this.withGroupe = withGroupe;
        }
        
        @Override
        public Groupe executeAndQuery(Model model, Date date) throws Exception {
            Groupe groupe = model.getGroupeByOrid(groupeOrid);
            if(groupe != null) {
                Utils.UpdateIfDifferent(groupe, withGroupe, "nomProfesseur");
                Utils.UpdateIfDifferent(groupe, withGroupe, "nom");
                Utils.UpdateIfDifferent(groupe, withGroupe, "description");
                Utils.UpdateIfDifferent(groupe, withGroupe, "groupeType");
                return groupe;
            }
            return null;
        }
    }
    
    public static class GetGroupeByOrid implements Query<Model, Groupe>{
        private String groupeOrid;
        
        private GetGroupeByOrid() {}        
        public GetGroupeByOrid(String groupeOrid) {
            this.groupeOrid = groupeOrid;
        }
        
        @Override
	public Groupe query(Model model, Date date){
            return model.getGroupeByOrid(groupeOrid);
	}
    }
    
    public static class GetGroupesByName implements Query<Model, List<Groupe>>{
        
        private String name;
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }

        
        private GetGroupesByName() {
        }
        
        public GetGroupesByName(String name) {
            this.name = name;
        }
        
        @Override
	public List<Groupe> query(Model model, Date date){
            return model.getGroupByName(name);
	}
    }
    
    
    public static class GetAllGroupesLight implements Query<Model, List<Groupe>> {

        public GetAllGroupesLight() {
        }
        
        @Override
        public List<Groupe> query(Model model, Date date) throws Exception {
            List<Groupe> result = new LinkedList<Groupe>();
            for(Groupe g : model.getGroups()) {
                result.add(g.createGroupeLight());
            }
            return result;
        }
        
    }
   
    
}

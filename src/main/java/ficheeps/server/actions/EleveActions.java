package ficheeps.server.actions;

import ficheeps.model.Apsa;
import ficheeps.model.Commentaire;
import ficheeps.model.Eleve;
import ficheeps.model.Groupe;
import ficheeps.server.Model;
import ficheeps.server.utils.BeanComparator;
import ficheeps.server.utils.Utils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.functors.AndPredicate;
import org.prevayler.Query;
import org.prevayler.Transaction;
import org.prevayler.TransactionWithQuery;


public class EleveActions {
    
    private EleveActions() {}
    
    
    public static class Create implements TransactionWithQuery<Model,Eleve> {
        @Override
        public Eleve executeAndQuery(Model model, Date date) throws Exception {
            return model.createEleve();
        }
    }
    
    
    public static class GetById implements Query<Model,Eleve> {
        private String orid;
        
        private GetById() {
        }
        public GetById(String orid) {
            this.orid = orid;
        }

        @Override
        public Eleve query(Model model, Date date) throws Exception {
            return model.getEleveByOrid(orid);
        }
    }
    
    public static class GetAll implements Query<Model, List<Eleve>> {
        public GetAll() {
        }

        @Override
        public List<Eleve> query(Model model, Date date) throws Exception {
            return new ArrayList<Eleve>(model.getAllEleves());
        }
    }
    
    public static class UpdateFirstLevel implements TransactionWithQuery<Model,Eleve> {
        private String eleveOrid;
        private Eleve eleve;
        
        private UpdateFirstLevel() {
        }
        
        public UpdateFirstLevel(String eleveOrid,Eleve eleve) {
            this.eleveOrid = eleveOrid;
            this.eleve = eleve;
        }
        
        @Override
        public Eleve executeAndQuery(Model model, Date date) throws Exception {
            Eleve dbEleve = model.getEleveByOrid(eleveOrid);
            Utils.UpdateIfDifferent(dbEleve, eleve, "id");
            Utils.UpdateIfDifferent(dbEleve, eleve, "nom");
            Utils.UpdateIfDifferent(dbEleve, eleve, "prenom");
            Utils.UpdateIfDifferent(dbEleve, eleve, "dateDeNaissance");
            Utils.UpdateIfDifferent(dbEleve, eleve, "classe");
            Utils.UpdateIfDifferent(dbEleve, eleve, "suggestionApresSeconde");
            Utils.UpdateIfDifferent(dbEleve, eleve, "suiviMedical");
            Utils.UpdateIfDifferent(dbEleve, eleve, "emails");
            return dbEleve;
        }
    }
    
    public static class UpdateDeep implements TransactionWithQuery<Model,Eleve> {
        private String eleveOrid;
        private Eleve eleve;
        
        private UpdateDeep() {
        }
        
        public UpdateDeep(String eleveOrid,Eleve eleve) {
            this.eleveOrid = eleveOrid;
            this.eleve = eleve;
        }
        
        @Override
        public Eleve executeAndQuery(Model model, Date date) throws Exception {
            UpdateFirstLevel updateFirstLevel = new UpdateFirstLevel(eleveOrid,eleve);
            updateFirstLevel.executeAndQuery(model, date);
            
            Eleve dbEleve = model.getEleveByOrid(eleveOrid);
            dbEleve.setCommentaires(eleve.getCommentaires());
            return dbEleve;
        }
    }
    
    public static class Set implements TransactionWithQuery<Model,Eleve> {
        private String eleveOrid;
        
        private String _numen = null;
	private String _nom = null;
	private String _prenom = null;
	private Date _dateDeNaissance = null;
	private String _classe = null;
	private String _suiviMedical = null;
	private String _suggestionApresSeconde = null;
        private String _emails = null;
        
        private Set() {
        }
        public Set(String eleveOrid) {
            this.eleveOrid = eleveOrid;
        }
        
        public Set numen(String numen) {
            this._numen = numen;
            return this;
        }
        public Set nom(String nom) {
            this._nom = nom;
            return this;
        }
        public Set prenom(String prenom) {
            this._prenom = prenom;
            return this;
        }
        public Set dateDeNaissance(Date dateDeNaissance) {
            this._dateDeNaissance = dateDeNaissance;
            return this;
        }
        public Set classe(String classe) {
            this._classe = classe;
            return this;
        }
        public Set suiviMedical(String suiviMedical) {
            this._suiviMedical = suiviMedical;
            return this;
        }
        public Set suggestionApresSeconde(String suggestionApresSeconde) {
            this._suggestionApresSeconde = suggestionApresSeconde;
            return this;
        }
        public Set emails(String emails) {
            this._emails = emails;
            return this;
        }
        
        @Override
        public Eleve executeAndQuery(Model model, Date date) throws Exception {
            Eleve result =  model.getEleveByOrid(eleveOrid);
            if(_nom != null) {
                result.setNom(_nom);
            }
            if(_numen != null) {
                result.setId(_numen);
            }
            if(_prenom != null) {
                result.setPrenom(_prenom);
            }
            if(_suggestionApresSeconde != null) {
                result.setSuggestionApresSeconde(_suggestionApresSeconde);
            }
            if(_dateDeNaissance != null) {
                result.setDateDeNaissance(Utils.DateRA12(_dateDeNaissance));
            }
            if(_suiviMedical != null) {
                result.setSuiviMedical(_suiviMedical);
            }
            if(_emails != null) {
                result.setEmails(_emails);
            }
            if(_classe != null) {
                result.setClasse(_classe);
            }
            return result; 
        }
    }
    
    
    public static class DeleteAllElevesWithoutClassesBornBefore implements TransactionWithQuery<Model,String> {
        private String dateYYYYMMDD = null;
        
        public DeleteAllElevesWithoutClassesBornBefore() {
        }
        public DeleteAllElevesWithoutClassesBornBefore(String dateYYYYMMDD) {
            this.dateYYYYMMDD = dateYYYYMMDD;
        }
        
        @Override
        public String executeAndQuery(Model model, Date dateExecution) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                Date date = sdf.parse(dateYYYYMMDD);
                date.setHours(0);
                date.setMinutes(0);
                date.setSeconds(0);
                return model.deleteAllElevesWithoutClassesBornBefore(date);
            }
            catch(Exception e) {
                return "Date non reconnue, aucun élève supprimé.";
            }
        }
    }
    
    
    public static class DeleteAllElevesWithoutClasses implements Transaction<Model> {
        
        public DeleteAllElevesWithoutClasses() {
        }

        @Override
        public void executeOn(Model model, Date date) {
            model.deleteAllElevesWithoutClasses();
        }
    }
    
    public static class Delete implements Transaction<Model> {
        private String eleveOrid;
        
        private Delete() {
        }
        public Delete(String eleveOrid) {
            this.eleveOrid = eleveOrid;
        }

        @Override
        public void executeOn(Model model, Date date) {
            model.deleteEleve(eleveOrid);
        }
        
    }
 
    
    
    public static class FindElevesBestMatch implements Query<Model, List<Eleve>> {
        
        private String classe;
        public String getClasse() {
            return classe;
        }
        public void setClasse(String classe) {
            this.classe = classe;
        }

        private String nom;
        public String getNom() {
            return nom;
        }
        public void setNom(String nom) {
            this.nom = nom;
        }

        private String prenom;
        public String getPrenom() {
            return prenom;
        }
        public void setPrenom(String prenom) {
            this.prenom = prenom;
        }

        private FindElevesBestMatch() {
        }
        public FindElevesBestMatch(String classe,String nom,String prenom) {
            this.classe = classe;
            this.nom = nom;
            this.prenom = prenom;
        }
        
        @Override
        public List<Eleve> query(Model model, Date date) throws Exception {
            List<Eleve> eleves = new LinkedList<Eleve>();
                    
            nom = Utils.StringToTrimedLowerCaseOrEmpty(nom);
            prenom = Utils.StringToTrimedLowerCaseOrEmpty(prenom);

            if(!Utils.StringNullOrWhiteSpace(classe)) {
                eleves.addAll(model.getElevesFromClasse(classe));
            }
            else {
                eleves.addAll(model.getAllEleves());
                if(!Utils.StringNullOrWhiteSpace(nom)) { 
                    nom = nom.trim().toLowerCase();
                    Iterator<Eleve> iter = eleves.iterator();
                    while(iter.hasNext()) {
                        Eleve eleve = iter.next();
                        String eleveNom = (""+eleve.getNom()).toLowerCase();
                        if(!eleveNom.contains(nom)) {
                            iter.remove();
                        }
                    }
                }
                if(!Utils.StringNullOrWhiteSpace(prenom)) {
                    prenom = prenom.trim().toLowerCase();
                    Iterator<Eleve> iter = eleves.iterator();
                    while(iter.hasNext()) {
                        Eleve eleve = iter.next();
                        String elevePrenom = (""+eleve.getPrenom()).toLowerCase();
                        if(!elevePrenom.contains(nom)) {
                            iter.remove();
                        }
                    }
                }
            } 

            //Refine on 'nom':
            List<Eleve> eleveWidthGoodNom = new LinkedList(eleves);
            Iterator<Eleve> iter = eleveWidthGoodNom.iterator();
            while(iter.hasNext()) {
                Eleve eleve = iter.next();
                String en = Utils.StringToTrimedLowerCaseOrEmpty(eleve.getNom());
                if(!en.equals(nom)) {
                    iter.remove();
                }
            }
            if(eleveWidthGoodNom.isEmpty()) {//We try to be less strict using 'contains'
                eleveWidthGoodNom = new LinkedList(eleves);
                iter = eleveWidthGoodNom.iterator();
                while(iter.hasNext()) {
                    Eleve eleve = iter.next();
                    String en = Utils.StringToTrimedLowerCaseOrEmpty(eleve.getNom());
                    if(!en.contains(nom)) {
                        iter.remove();
                    }
                }
            }

            if(eleveWidthGoodNom.isEmpty()) { //Ok back to original list for prenom refinement
                eleveWidthGoodNom = new LinkedList(eleves);
            }

            //Refine on 'prenom':
            List<Eleve> eleveWidthGoodPrenom = new LinkedList(eleveWidthGoodNom);
            iter = eleveWidthGoodPrenom.iterator();
            while(iter.hasNext()) {
                Eleve eleve = iter.next();
                String ep = Utils.StringToTrimedLowerCaseOrEmpty(eleve.getPrenom());
                if(!ep.equals(prenom)) {
                    iter.remove();
                }
            }
            if(eleveWidthGoodPrenom.isEmpty()) { // We try to be less strict using now 'contains'
                eleveWidthGoodPrenom = new LinkedList(eleveWidthGoodNom);
                iter = eleveWidthGoodPrenom.iterator();
                while(iter.hasNext()) {
                    Eleve eleve = iter.next();
                    String ep = Utils.StringToTrimedLowerCaseOrEmpty(eleve.getPrenom());
                    if(!ep.contains(prenom)) {
                        iter.remove();
                    }
                }
            }

            if(eleveWidthGoodPrenom.isEmpty()) { // ok back to original
                eleveWidthGoodPrenom = new LinkedList(eleveWidthGoodNom);
            }

            return eleveWidthGoodPrenom;
        }
        
    }
        

    
    
    public static class Search implements Query<Model,List<Eleve>> {
        private int startOffset;
        private int count;
        private String filter;
        private String sortBy;
        private boolean sortAscending;
	private boolean includeEleveWithoutClass;	
        
        private transient int queryResultTotalCount;
        public int getQueryResultTotalCount() {
            return queryResultTotalCount;
        }
        
        private Search() {
        }

        public Search(String filter,String sortBy,boolean sortAscending,boolean includeEleveWithoutClass,int startOffset,int count) {
            this.startOffset = startOffset;
            this.count = count;
            this.filter = filter;
            this.sortAscending = sortAscending;
            this.sortBy = sortBy;
            this.includeEleveWithoutClass = includeEleveWithoutClass;
        }
                                
        @Override
        public List<Eleve> query(Model model, Date date) throws Exception {
            List<Eleve> result = new ArrayList<Eleve>(model.getAllEleves());
            
            if (sortBy != null && !sortBy.isEmpty()) {
                String methodName = "get" + sortBy.substring(0,1).toUpperCase() + sortBy.substring(1);
                Collections.sort(result, new BeanComparator(Eleve.class, methodName, sortAscending, true));
            }
            
            Predicate predicate = null;
            
            if (filter != null && !filter.isEmpty()) {
                //"id", "nom", "prenom", "classe"
                final String filterNormalized = filter.toLowerCase();
                predicate = new Predicate() {
                    @Override
                    public boolean evaluate(Object o) {
                        if(o instanceof Eleve) {
                            Eleve eleve = (Eleve)o;
                            String id = eleve.getId() == null ? "" : eleve.getId().toLowerCase();
                            if(id.contains(filterNormalized)) {
                                return true;
                            }
                            String nom = eleve.getNom() == null ? "" : eleve.getNom().toLowerCase();
                            if(nom.contains(filterNormalized)) {
                                return true;
                            }
                            String prenom = eleve.getPrenom() == null ? "" : eleve.getPrenom().toLowerCase();
                            if(prenom.contains(filterNormalized)) {
                                return true;
                            }
                            String classe = eleve.getClasse() == null ? "" : eleve.getClasse().toLowerCase();
                            if(classe.contains(filterNormalized)) {
                                return true;
                            }
                        }
                        return false;
                    }
                };    
            }
            
            if(!includeEleveWithoutClass) {
                Predicate p2 = new Predicate() {
                    @Override
                    public boolean evaluate(Object o) {
                        if(o instanceof Eleve) {
                            Eleve eleve = (Eleve)o;
                            if(eleve.getClasse() == null || eleve.getClasse().trim().isEmpty()) {
                                return false;
                            }
                            return true;
                        }
                        return false;
                    }
                };
                if(predicate != null) {
                    predicate = new AndPredicate(p2, predicate);
                }
                else {
                    predicate = p2;
                }
            }
            
            if(predicate != null) {
                CollectionUtils.filter(result, predicate);
            }

            queryResultTotalCount = result.size();
            
            result = new ArrayList<Eleve>(result.subList(startOffset,Math.min(startOffset + count,result.size())));
            return result;
        }
        
    }
    
    
    public static class GetByNumen implements Query<Model, List<Eleve>> {
        private String numen;
        
        private GetByNumen() {
        }
        public GetByNumen(String numen) {
            this.numen = numen;
        }

        @Override
        public List<Eleve> query(Model model, Date date) throws Exception {
            List<Eleve> result = new LinkedList<Eleve>();
            for(Eleve eleve : model.getAllEleves()) {
                if(numen.equals(eleve.getId())) {
                    result.add(eleve);
                }
            }
            return result;
        }
        
        
    }
    
    
    
    
    
    public static class WizardCreateCommentaireForGroupClass implements TransactionWithQuery<Model,Eleve> {
        private Commentaire commentaireLike;
        private String classOrGroupeName;
        private boolean isClassElseGroupe;
        
        private WizardCreateCommentaireForGroupClass(){}
        public WizardCreateCommentaireForGroupClass(Commentaire commentaireLike, String classOrGroupeName, boolean isClassElseGroupe) {
            this.commentaireLike = commentaireLike;
            this.classOrGroupeName = classOrGroupeName;
            this.isClassElseGroupe = isClassElseGroupe;
        }
    
        
        @Override
        public Eleve executeAndQuery(Model model, Date date) throws Exception {
            List<Eleve> eleves = null;
            if (isClassElseGroupe) {
                eleves = model.getElevesFromClasse(classOrGroupeName);
            } 
            else {
                Groupe groupe = model.getUniqGroupByName(classOrGroupeName);
                eleves = groupe.getEleves();
            }

            if (eleves != null) {
                for (Eleve eleve : eleves) {
                    //On recherche un commentaire de meme niveau/meme annee
                    Commentaire commentaire = null;
                    if(eleve.getCommentaires() != null) {
                        for(Commentaire c : eleve.getCommentaires()) {
                            if(c.getNiveau() == commentaireLike.getNiveau() && ficheeps.server.utils.Utils.StringEquals(c.getAnneeScolaire(), commentaireLike.getAnneeScolaire())) {
                                commentaire = c;
                                break;
                            }
                        }
                    }

                    boolean checkIfApsaAlreadyExist = true;

                    if(commentaire == null) {
                        checkIfApsaAlreadyExist = false;
                        commentaire = new Commentaire();
                        commentaire.setNiveau(commentaireLike.getNiveau());
                        commentaire.setAnneeScolaire(commentaireLike.getAnneeScolaire());
                        commentaire.setProfesseur(commentaireLike.getProfesseur());
                        commentaire.setDateCreation(new Date());
                    }

                    if(commentaireLike.getApsas() != null) {
                        for(Apsa apsa : commentaireLike.getApsas()) {
                            if(checkIfApsaAlreadyExist && commentaire.getApsaForActivite(apsa.getActivite()) != null) {
                                continue;
                            }

                            if(!ficheeps.server.utils.Utils.StringNullOrWhiteSpace(apsa.getActivite())) {
                                Apsa newApsa = new Apsa();
                                newApsa.setActivite(apsa.getActivite());
                                commentaire.addApsa(newApsa);
                            }
                        }
                    }

                    eleve.addCommentaire(commentaire);
                }
            }        

            return null;//TODO
        }
    }


    
}

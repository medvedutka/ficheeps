package ficheeps.server.actions;

import ficheeps.model.Eleve;
import ficheeps.model.ListeLibre;
import ficheeps.model.ListeLibreItem;
import ficheeps.model.ListeLibreLight;
import ficheeps.server.Model;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class ListeLibreActions {

    private ListeLibreActions() {}
            
    
    public static class Create extends Action.WithResult<ListeLibre> {
        public Create() {
        }
        
        @Override
	protected ListeLibre doAction(Model model){
            return model.createListeLibre();
	}
    }
    
    public static class Delete extends Action {
        private String listeLibreId;
        
        private Delete() {
        }
        public Delete(String listeLibreId) {
            this.listeLibreId = listeLibreId;
        }

        @Override
        protected void doAction(Model model) {
            model.deleteListeLibre(listeLibreId);
        }
    }
    
    public static class DeleteAll extends Action {
        public DeleteAll() {
        }

        @Override
        protected void doAction(Model model) {
            model.deleteAllListeLibre();
        }
    }
         
    public static class Set extends Action {
        private String listeLibreId;
        public Set() {
        }
        public Set(String listeLibreId) {
            this.listeLibreId = listeLibreId;
        }
        
        private String _name;
        public Set name(String name) {
            this._name = name;
            return this;
        }
        private String _description;
        public Set description(String description) {
            this._description = description;
            return this;
        }
        
        @Override
        protected void doAction(Model model) {
            ListeLibre ll = model.getListeLibreById(listeLibreId);
            if(_name != null) {
                ll.setName(_name);
            }
            if(_description != null) {
                ll.setDescription(_description);
            }
        }
    }
    
    public static class AddNewItemFromEleve extends Action.WithResult<ListeLibreItem>{
        private String listeLibreId;
        private String eleveId;
        public AddNewItemFromEleve() {
        }
        public AddNewItemFromEleve(String listeLibreId,String eleveId) {
            this.listeLibreId = listeLibreId;
            this.eleveId = eleveId;
        }
        
        @Override
	protected ListeLibreItem doAction(Model model) {
            ListeLibre ll = model.getListeLibreById(listeLibreId);
            Eleve eleve = model.getEleveByOrid(eleveId);
            //check if eleve already in ListeLibre:
            if(ll.isEleveAlreadyPresent(eleve)) {
                return null;
            }
            ListeLibreItem item = ll.newItem();
            item.setEleve(eleve);
            return item;
	}
    }
    
    public static class GetListeLibreById extends Action.Query<ListeLibre> {
        private String listeLibreId;
        
        private GetListeLibreById() {}        
        public GetListeLibreById(String listeLibreId) {
            this.listeLibreId = listeLibreId;
        }
        
        @Override
	protected ListeLibre doAction(Model model) throws Exception {
            return model.getListeLibreById(listeLibreId);
	}
    }
    
   
    public static class ItemSet extends Action {
        private String listeLibreId;
        private String itemId;
        public ItemSet() {
        }
        public ItemSet(String listeLibreId,String itemId) {
            this.listeLibreId = listeLibreId;
            this.itemId = itemId;
        }
        
        private String _profName;
        public ItemSet profName(String profName) {
            this._profName = profName;
            return this;
        }
        private String _comment;
        public ItemSet comment(String comment) {
            this._comment = comment;
            return this;
        }
        
        @Override
        public void doAction(Model model) {
            ListeLibre ll = model.getListeLibreById(listeLibreId);
            ListeLibreItem item = ll.getItemById(itemId);
            if(_profName != null) {
                item.setProfName(_profName);;
            }
            if(_comment != null) {
                item.setComment(_comment);
            }
        }
    }
   
    
    public static class DeleteItem extends Action {
        private String listeLibreId;
        private String itemId;
        public DeleteItem() {
        }
        public DeleteItem(String listeLibreId,String itemId) {
            this.listeLibreId = listeLibreId;
            this.itemId = itemId;
        }
        
        @Override
        protected void doAction(Model model) {
            ListeLibre ll = model.getListeLibreById(listeLibreId);
            ll.deleteItem(itemId);
        }
    }

    
    public static class GetAllListeLibreLight extends Action.Query<Collection<ListeLibreLight>> {

        public GetAllListeLibreLight() {
        }
        
        @Override
        protected Collection<ListeLibreLight> doAction(Model model) throws Exception {
            List<ListeLibreLight> result = new LinkedList<ListeLibreLight>();
            for(ListeLibre ll : model.getAllListesLibres()) {
                result.add(ll.createLight());
            }
            return result;
        }
        
    }
   
    
}

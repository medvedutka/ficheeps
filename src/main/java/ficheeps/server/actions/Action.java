package ficheeps.server.actions;

import ficheeps.server.Model;
import java.io.Serializable;
import java.util.Date;
import org.prevayler.Transaction;
import org.prevayler.TransactionWithQuery;

public abstract  class Action implements Serializable,Transaction<Model> {

    @Override
    public void executeOn(Model model, Date date) {
        doAction(model);
    }
    
    protected abstract void doAction(Model model);
    
    
    
    public static abstract  class WithResult<T> implements Serializable,TransactionWithQuery<Model, T> {
        @Override
	public T executeAndQuery(Model model, Date date){
            return doAction(model);
	}

        protected abstract T doAction(Model model);
        
    }
    
    
    public static abstract class Query<T> implements Serializable,org.prevayler.Query<Model, T> {
        @Override
        public T query(Model model, Date date) throws Exception {
            return doAction(model);
        }

        protected abstract T doAction(Model model) throws Exception;
    }
}

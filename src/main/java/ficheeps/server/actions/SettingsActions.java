package ficheeps.server.actions;


import ficheeps.model.Settings;
import ficheeps.model.Tuple;
import ficheeps.server.Model;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.prevayler.Query;
import org.prevayler.Transaction;
import org.prevayler.TransactionWithQuery;


public class SettingsActions {

    private  SettingsActions() {}
    
    
    public static class GetSingleton implements Query<Model, Settings> {
        public GetSingleton() {
        }
        @Override
        public  Settings query(Model model, Date date){
           return model.getSettings();
        }
        
    }
    
    public static class Set implements TransactionWithQuery<Model, Settings> {
        private Boolean _constraintApsaLabel = null;
        
        public Set() {
        }
        
        public Set constraintApsaLabel(boolean constraintApsaLabel) {
            _constraintApsaLabel= constraintApsaLabel;
            return this;
        }
        
        @Override
        public Settings executeAndQuery(Model model, Date date) throws Exception {
            if(_constraintApsaLabel != null) {
                model.getSettings().setConstraintApsaLabel(_constraintApsaLabel);
            }
            return model.getSettings();
        }
    }
    
    public static class ApsaLabels implements TransactionWithQuery<Model, Settings> {
        private List<String> apsaLabelToAdd = new LinkedList<String>();
        private List<String> apsaLabelToRemove = new LinkedList<String>();
        
        public ApsaLabels() {
        }
        
        public ApsaLabels add(String apsaLabel) {
            apsaLabelToAdd.add(apsaLabel);
            return this;
        }
        public ApsaLabels remove(String apsaLabel) {
            apsaLabelToRemove.add(apsaLabel);
            return this;
        }
        
        @Override
        public Settings executeAndQuery(Model model, Date date) throws Exception {
            if(model.getSettings().getApsaLabels() == null) {
                model.getSettings().setApsaLabels(new LinkedList<String>());
            }
            model.getSettings().getApsaLabels().addAll(apsaLabelToAdd);
            model.getSettings().getApsaLabels().removeAll(apsaLabelToRemove);
            Collections.sort(model.getSettings().getApsaLabels());
            
            return model.getSettings();
        }
    }
    
    
    public static class InfosClassesGroupes {
        public InfosClassesGroupes() {}
        
        public static class Get implements Query<Model, Map<String,String>> {
            public Get() {}
            @Override
            public Map<String,String> query(Model model, Date date) throws Exception {
                if(model.getSettings().getInfosClassesGroupes() == null) {
                    return new HashMap<String,String>();
                }
                return model.getSettings().getInfosClassesGroupes();
            }
        }
        
        public static class Put implements Transaction<Model> {
            private String classOrGoupName;
            private String info;
            private Put() {}
            public Put(String classOrGoupName,String info) {
                this.classOrGoupName = classOrGoupName;
                this.info = info;
            }

            @Override
            public void executeOn(Model model, Date date) {
                model.getSettings().putInfosClassesGroupes(classOrGoupName, info);
            }
        }
        
        public static class PutAll implements Transaction<Model> {
            private List<Tuple<String,String>> nameInfos;
            private PutAll() {}
            public PutAll(List<Tuple<String,String>> nameInfos) {
                this.nameInfos = nameInfos;
            }

            @Override
            public void executeOn(Model model, Date date) {
                if(nameInfos != null) {
                    for(Tuple<String,String> t : nameInfos) {
                        model.getSettings().putInfosClassesGroupes(t.getFirst(), t.getSecond());
                    }
                }
            }
        }
        
        public static class Remove implements Transaction<Model> {
            private String classOrGoupName = "";
            private Remove() {}
            public Remove(String classOrGoupName) {
                this.classOrGoupName = classOrGoupName;
            }

            @Override
            public void executeOn(Model model, Date date) {
                model.getSettings().removeInfosClassesGroupes(classOrGoupName);
            }
        }        

    }
}

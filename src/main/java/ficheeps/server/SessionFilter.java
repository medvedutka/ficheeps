package ficheeps.server;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SessionFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if(request instanceof HttpServletRequest) {
			HttpSession session = ((HttpServletRequest)request).getSession(false);
			if (session != null) {
				//String username = (String)session.getAttribute("username");		
				//good to go
			}
			else {
				if(response instanceof HttpServletResponse) {
					((HttpServletResponse)response).sendRedirect("/login");
					return;
				}
			}
        }
        chain.doFilter(request, response);
    }

    public void destroy() {
    }

}
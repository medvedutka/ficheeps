package ficheeps.server;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import ficheeps.model.Eleve;


public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		CreateDummyCSVFile(new FileWriter(new File("/home/thecat/dummyCSVFile.csv")));
	}
	
	public static void CreateDummyCSVFile(Writer writer) throws Exception {
		Scanner sc = new Scanner(Test.class.getResourceAsStream("/ficheeps/server/noms.txt"));
		List<String> lines = new LinkedList<String>();
		while (sc.hasNextLine()) {
		  lines.add(sc.nextLine());
		}
		String[] noms = lines.toArray(new String[0]);
		lines.clear();
		sc = new Scanner(Test.class.getResourceAsStream("/ficheeps/server/prenoms.txt"));
		while (sc.hasNextLine()) {
		  lines.add(sc.nextLine());
		}		
		String[] prenoms = lines.toArray(new String[0]);
		lines.clear();
		sc = new Scanner(Test.class.getResourceAsStream("/ficheeps/server/sportifs.txt"));
		while (sc.hasNextLine()) {
		  lines.add(sc.nextLine());
		}		
		String[] sportifs = lines.toArray(new String[0]);
		sc = new Scanner(Test.class.getResourceAsStream("/ficheeps/server/fortunes.txt"));
		sc.useDelimiter("\\%");
		while (sc.hasNext()) {
		  lines.add(sc.next().trim());
		}				
		String[] fortunes = lines.toArray(new String[0]);
		
		String[] classesPrefix = {"1","2","T","6","5","4","3","1STG","TSTG","2PRO","1PRO","TPRO","2PRO-COMPTA","1PRO-COMPTA","TPRO-COMPTA"};

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		int id = 3500;
		
		for(String cp : classesPrefix) {
			for(int cn=1;cn <15;cn++) {
				for(int i = 0;i< 25;i++) {
					Eleve e = new Eleve();
					e.setId(String.format("%07d",id++));
					e.setNom(noms[(int)(Math.random() * ((double)noms.length))]);
					e.setPrenom(prenoms[(int)(Math.random() * ((double)prenoms.length))]);
					e.setSuiviMedical(fortunes[(int)(Math.random() * ((double)fortunes.length))]);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new Date());
					cal.add(Calendar.YEAR,-18);
					cal.add(GregorianCalendar.DATE, (int)(Math.random() * (7. * 365)));
					e.setDateDeNaissance(cal.getTime());
					e.setClasse(String.format("%s%02d", cp,cn));
					
					char separator ='|';
					writer.append(e.getId()+separator+e.getNom()+separator+e.getPrenom()+separator);
					writer.append(dateFormat.format(e.getDateDeNaissance())+separator);
					writer.append(e.getClasse());
					writer.write("\n");
				}
			}
		}
	}

}

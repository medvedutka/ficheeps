package ficheeps.server.auth;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.nio.charset.Charset;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ficheeps.model.User;
import ficheeps.server.Administration;
import ficheeps.server.utils.Fortunes;



public class LoginServlet extends HttpServlet {

    static final String ATTR_LOGIN = "login";
    static final String ATTR_PWD = "pwd";


    @Override
    public void init() throws ServletException {
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String login = request.getParameter(ATTR_LOGIN);
        String pwd = request.getParameter(ATTR_PWD);
        
        if (login==null || pwd==null ||
        	pwd.isEmpty() || login.isEmpty()) {
            response.setContentType("text/html; charset=UTF-8");
            preventCache(response);
            showLoginPage(response.getWriter());
            return;
        }
        else {
        	Administration administration = new Administration();
        	//TODO: move hashing client side
        	pwd = ficheeps.server.utils.Utils.Sha1(pwd);
        	User user = administration.isLocalAllowed(login, pwd);
        	if(user != null) {
        		HttpSession session =  request.getSession(true);
        		session.setAttribute("user",user);
        		response.sendRedirect("FicheEPS.html");
        	}
        	else {
        		response.sendRedirect("login");
        	}
        }
    }

    void showLoginPage(PrintWriter pw) {
    	try {
	    	InputStream is = LoginServlet.class.getResourceAsStream("/ficheeps/server/auth/login.html");
	    	Reader isr = new InputStreamReader(is, Charset.forName("UTF-8"));
	    	StringBuilder stringBuilder = new StringBuilder();
	    	char[] buffer = new char[2048];
	    	int r = 0;
	    	while((r = isr.read(buffer)) != -1) {
	    		stringBuilder.append(buffer,0 , r);
	    	}
	    	is.close();
	    	isr.close();
	    	String s= stringBuilder.toString();
	    	s = s.replace("#fortunes!#", Fortunes.GetRandomFortune());
	    	//s = s.replace("GWTgetModuleBaseURL$", GWT.getModuleBaseURL());
	    	pw.write(s);
    	}
    	catch(Exception e) {
    		//discard
    	}
    }

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
    
	private void preventCache(HttpServletResponse response) {
		// Set standard HTTP/1.1 no-cache headers.
		response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
		// Set standard HTTP/1.0 no-cache header.
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);

		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		response.setDateHeader("Expires", 0); // Proxies.
	}
    
}

package ficheeps.server.auth;

import com.google.api.client.googleapis.auth.oauth2.GoogleBrowserClientRequestUrl;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import ficheeps.model.User;
import ficheeps.server.Administration;
import ficheeps.server.Configuration;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;


public class OAuth2Servlet extends HttpServlet {

    //public static String GOOOGLE_FICHEEPS_API_KEY = "1041835746802-vfqceu5vjl8jblcv330v9s46gjmp23r6.apps.googleusercontent.com";
    
    @Override
    public void init() throws ServletException {
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("ficheEPSAction");
        if ("login".equals(action)) {
             String url = new GoogleBrowserClientRequestUrl(
                    Configuration.Get().getGoogleApiKey(),
                    request.getRequestURL().toString(), 
                    Arrays.asList(
                            "https://www.googleapis.com/auth/userinfo.email",
                            "https://www.googleapis.com/auth/userinfo.profile")).setState("stateProfile").build();
            response.sendRedirect(url);
            return;
        }
        else if ("processToken".equals(action)) {
            String accessToken = request.getParameter("access_token");
            if(accessToken == null) {
                Logger.getAnonymousLogger().log(Level.WARNING, "Error on login: processToken is null.");  
                response.sendRedirect("login");
                return;
            }
            
            //https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=1/fFBGRNJru1FQd44AzqT3Zg
            HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
            HttpRequestFactory requestFactory = HTTP_TRANSPORT.createRequestFactory();
            HttpRequest httpRequest = requestFactory.buildGetRequest(new GenericUrl("https://www.googleapis.com/oauth2/v2/tokeninfo?access_token=" + accessToken));
            HttpResponse httpResponse = httpRequest.execute();
            
            String responseStr = httpResponse.parseAsString();
            Type typeOfHashMap = new TypeToken<Map<String, String>>() { }.getType();
            Gson gson = new Gson();
            Map<String, String> mapResult = gson.fromJson(responseStr,typeOfHashMap);
            if(!mapResult.containsKey("audience")) {
                Logger.getAnonymousLogger().log(Level.WARNING, "Error on login: token verification error: 'audience' field not present.");  
                response.sendRedirect("login");
                return;
            }
            if(! Configuration.Get().getGoogleApiKey().equals(mapResult.get("audience"))) {
                Logger.getAnonymousLogger().log(Level.WARNING, "Error on login: token verification error: 'audience' field not equal to ficheeps api key.");  
                response.sendRedirect("login");
                return;
            }

            httpRequest = requestFactory.buildGetRequest(new GenericUrl("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + accessToken));
            httpResponse = httpRequest.execute();
            responseStr = httpResponse.parseAsString();
            gson = new Gson();
            mapResult = gson.fromJson(responseStr,typeOfHashMap);
            
            if(!mapResult.containsKey("email")) {
                Logger.getAnonymousLogger().log(Level.WARNING, "Error on login: Cannot retrieve email from account.");  
                response.sendRedirect("login");
                return;
            }
            
            String email = mapResult.get("email");
            
            String fullName = email;
            if(mapResult.containsKey("name")) {
               fullName = mapResult.get("name");
            }
            
            Administration administration = new Administration();
            User user = administration.isEmailLoginAllowed(email); 
            if(user != null) {
                HttpSession session =  request.getSession(true);
                user.setFullName(fullName);
                session.setAttribute("user", user);
                String url = "FicheEPS.html";
                response.sendRedirect(url);
                Logger.getAnonymousLogger().log(Level.INFO, "User logged in: " + fullName + "<" + email+">");            
                return;
            }

            Logger.getAnonymousLogger().log(Level.WARNING, "Error on login: user '" + email + "' is not allowed.");  
            response.sendRedirect("login");
            return;
        }
        else {
            StringBuilder js = new StringBuilder();
            js.append("<script type=\"text/javascript\">"); 
            js.append("var queryString = location.hash.substring(1);"); 
            js.append("window.location.replace(\"oauth2?ficheEPSAction=processToken&\" + queryString);"); 
            js.append("</script>"); 
            
            response.setContentType("text/html; charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.write(js.toString());
            return;
        }
    }

    
    
    private String createAndRegisterAntiForgeryStateToken(HttpServletRequest request) {
        // Create a state token to prevent request forgery.
        // Store it in the session for later validation.
        String state = new BigInteger(130, new SecureRandom()).toString(32);
        request.getSession().setAttribute("state", state);
        return state;
    }
}

package ficheeps.server;

import ficheeps.server.utils.AESSerializer;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import javax.crypto.CipherInputStream;


public class FicheEPSCmd {

    
    private static String USAGE = "ficheeps.server.FicheEPSCmd <action> <params>"
            + "\naction:"
            + "\n\t-decryptdbfile <file>  [-pwd <pwd>|-pwdhash <hashpwd>]"
            + "\n\t-sha1 <pwd>";

    public static void main(String[] args) {
        String action = args[0];
        if("-decryptdbfile".equals(action)) {
            DecryptDBFile(args);
        }
        else if("-sha1".equals(action)) {
            Sha1(args);
        }
        else {
            System.out.println(USAGE);
        }
    }
    
    
    private static void Sha1(String[] args) {
        String pwd = args[1];
        if(pwd == null || pwd.isEmpty()) {
            System.err.println("Password is required '" +args[1]+"'.");
            System.exit(1);
        }
        System.out.println(ficheeps.server.utils.Utils.Sha1(pwd));
    }
    
    private static void DecryptDBFile(String[] args) {
        String fileName = args[1];
        File file = new File(fileName);
        if(!file.exists()) {
            System.err.println("Error: file not found: '" + fileName+"'.");
            System.exit(1);
        }
        
        boolean pwdTypeHash = false;
        if("-pwd".equals(args[2])) {
            pwdTypeHash = false;
        }
        else if("-pwdhash".equals(args[2])) {
            pwdTypeHash = true;
        }
        else {
            System.err.println("Unknown param '" +args[2]+"'.");
            System.exit(1);
        }
        
        String pwd = args[3];
        if(pwd == null || pwd.isEmpty()) {
            System.err.println("Password is required '" +args[2]+"'.");
            System.exit(1);
        }
        
        if(!pwdTypeHash) {
            pwd = ficheeps.server.utils.Utils.Sha1(pwd);
        }
        
        File output = new File(file.getParent(),file.getName()+".decrypted");
        
        try (InputStream fis = new FileInputStream(file)){    
            AESSerializer aesCipher = new AESSerializer(null, AESSerializer.StringToKey(pwd));
            CipherInputStream cis = aesCipher.getCipherInputStream(fis);
            Files.copy(cis, output.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    
}

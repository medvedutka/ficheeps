package ficheeps.server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ResourcesServlet extends HttpServlet {

	public ResourcesServlet() {
		super();
	}

	private static final int DEFAULT_BUFFER_SIZE = 10240;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	       String resourcePath = request.getParameter("path");

	        if (resourcePath == null) {
	            response.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
	            return;
	        }

	        String contentType = null;
	        InputStream inputStream = this.getClass().getResourceAsStream(resourcePath);

        	contentType = getServletContext().getMimeType(resourcePath);
	        if (contentType == null) {
	            contentType = "application/octet-stream";
	        }
	        
	        response.reset();
	        response.setBufferSize(DEFAULT_BUFFER_SIZE);
	        response.setContentType(contentType);
	        preventCache(response);
	        
	        BufferedInputStream input = null;
	        BufferedOutputStream output = null;

	        try {
	            input = new BufferedInputStream(inputStream, DEFAULT_BUFFER_SIZE);
	            output = new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE);

	            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
	            int length;
	            while ((length = input.read(buffer)) > 0) {
	                output.write(buffer, 0, length);
	            }
	        } finally {
	            close(output);
	            close(input);
	        }
		
	}
	
	   private static void close(Closeable resource) {
	        if (resource != null) {
	            try {
	                resource.close();
	            } catch (IOException e) {
	                // Do your thing with the exception. Print it, log it or mail it.
	                e.printStackTrace();
	            }
	        }
	    }

	   
	
		private long FILE_SIZE_LIMIT = 2000 * 1024 * 1024; // 2000 MiB
		
		@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	       doGet(req,resp);
		}
		

		
		private void preventCache(HttpServletResponse response) {
			// Set standard HTTP/1.1 no-cache headers.
			response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
			// Set standard HTTP/1.0 no-cache header.
			response.setHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", 0);

			response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
			response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
			response.setDateHeader("Expires", 0); // Proxies.
		}
}

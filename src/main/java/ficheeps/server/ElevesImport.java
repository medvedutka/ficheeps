/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ficheeps.server;

import ficheeps.model.CSVLine;
import ficheeps.model.Eleve;
import ficheeps.model.EleveDiff;
import ficheeps.model.ImportOptions;
import ficheeps.server.actions.EleveActions;
import ficheeps.server.utils.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.supercsv.cellprocessor.ParseDate;
import org.supercsv.cellprocessor.Trim;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.constraint.StrNotNullOrEmpty;
import org.supercsv.cellprocessor.constraint.UniqueHashCode;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.exception.SuperCsvConstraintViolationException;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;

public class ElevesImport {

	private interface CSVLineProcessor {
		boolean process(CSVLine csvLine);
	}

	private interface FileImporterReader {
		void read(CSVLineProcessor processor) throws Exception;
	}

	public ElevesImport(ImportOptions io) {
		importOptions = io;
	}

	private ImportOptions importOptions;

	private FileImporterReader getReader() {
		if (importOptions.isFileExcel()) {
			return xlsReader;
		} else {
			return csvReader;
		}
	}

	public List<CSVLine> readAsExample(final int nbLineToRead) throws Exception {
		final List<CSVLine> result = new LinkedList<CSVLine>();

		getReader().read(new CSVLineProcessor() {
			int nbLineRead = 0;

			@Override
			public boolean process(CSVLine csvLine) {
				result.add(csvLine);
				nbLineRead++;
				if (nbLineRead >= nbLineToRead) {
					return false;
				}
				return true;
			}
		});

		return result;
	}

	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	
	private FileImporterReader xlsReader = new FileImporterReader() {
		public void read(CSVLineProcessor processor) throws Exception {
			org.apache.poi.ss.usermodel.Workbook workbook = null;
                        try{
                            workbook = new HSSFWorkbook(getUploadedFileInputStream()); // Try to load as .xls format
                        }
                        catch(Exception e) {
                            workbook = new XSSFWorkbook(getUploadedFileInputStream()); // Try to load as .xlsx format
                        }
			org.apache.poi.ss.usermodel.Sheet sheet = workbook.getSheetAt(0);

			int rowIndex = 0;
			if (importOptions.isSkipFirstLine()) {
				rowIndex++;
			}

			org.apache.poi.ss.usermodel.Row row = null;
			while ((row = sheet.getRow(rowIndex)) != null) {
				String numen = row.getCell(0).getStringCellValue().trim();
                                if(!Utils.StringNullOrWhiteSpace(numen)) { // Discard lines with empty numen
                                    String nom = getCellAsStringOrEmpty(row.getCell(1)).replace(",", " ").replace(";", " ");
                                    String prenom = getCellAsStringOrEmpty(row.getCell(2)).replace(","," ").replace(";", " ");
                                    Date dateDeNaissance = null;
                                    try {
                                            dateDeNaissance = row.getCell(3).getDateCellValue();
                                    } catch (Exception e) {
                                            //discard
                                    }
                                    if (dateDeNaissance == null) {
                                            String str = row.getCell(3).getStringCellValue().trim();
                                            if(Utils.StringNullOrWhiteSpace(str)) {
                                                throw new Exception("Impossible de lire la date de naissance ligne " + (rowIndex+1) + " ("+numen+"|"+nom+"|"+prenom+")");
                                            }
                                            dateDeNaissance = dateFormat.parse(str);
                                    }

                                    String classe = getCellAsStringOrEmpty(row.getCell(4));

                                    if (!processor.process(new CSVLine(numen, nom, prenom, dateDeNaissance, classe))) {
                                            break;
                                    }
                                }

				rowIndex++;
			}

		}
	};

            
        final private DataFormatter dataFormatter = new DataFormatter();
        private String getCellAsStringOrEmpty(Cell cell) {
            if(cell == null) {
                return "";
            }
            return Utils.StringToTrimedOrEmpty(dataFormatter.formatCellValue(cell));
        }
        
	private FileImporterReader csvReader = new FileImporterReader() {
		public void read(CSVLineProcessor processor) throws Exception {
			ICsvListReader listReader = null;
			try {
				CsvPreference prefs = new CsvPreference.Builder('"', (int) importOptions.getDelimiter(), "\r\n")
						.build();
				listReader = new CsvListReader(getUploadedFileReader(), prefs);

				if (importOptions.isSkipFirstLine()) {
					listReader.getHeader(true); // skip the header (can't be used with CsvListReader)
				}

				final CellProcessor[] processors = getProcessors();
				List<Object> csvLine;
				while ((csvLine = listReader.read(processors)) != null) {
					if (csvLine.size() != 5) {
						throw new Exception("Le nombre de champs lu est différent de 5");
					}

					String numen = (String) csvLine.get(0);
                                        if(!Utils.StringNullOrWhiteSpace(numen)) { // Discard lines with empty numen
                                            String nom = Utils.StringToTrimedOrEmpty((String) csvLine.get(1)).replace(",", " ").replace(";", " ");
                                            String prenom = Utils.StringToTrimedOrEmpty((String) csvLine.get(2)).replace(",", " ").replace(";", " ");
                                            Date dateDeNaissance = (Date) csvLine.get(3);
                                            String classe = Utils.StringToTrimedOrEmpty((String) csvLine.get(4));

                                            if (!processor.process(new CSVLine(numen, nom, prenom, dateDeNaissance, classe))) {
                                                    break;
                                            }
                                        }
				}
			} catch (SuperCsvConstraintViolationException ve) {
				String msg = "";
				msg += "Problème lors de la lecture de la ligne " + ve.getCsvContext().getLineNumber();
				msg += "\n" + ve.getMessage();
				if (ve.getCsvContext().getRowSource() != null) {
					msg += "\n";
					for (Object o : ve.getCsvContext().getRowSource()) {
						msg += "" + o + "|";
					}
				} else {
					msg += "\nLa ligne est-elle vide ?";
				}
				throw new Exception(msg);
			} finally {
				if (listReader != null) {
					listReader.close();
				}
			}
		}
	};

	public List<EleveDiff> diff() throws Exception {
		final List<EleveDiff> result = new LinkedList<EleveDiff>();
		getReader().read(new CSVLineProcessor() {
			@Override
			public boolean process(CSVLine csvLine) {
				Eleve currentEleve = retrieveEleve(csvLine.getId());
				EleveDiff ed = null;

				if (currentEleve == null) {
					ed = new EleveDiff(csvLine.getId(), csvLine.getNom(), csvLine.getPrenom(), csvLine
							.getDateDeNaissance(), csvLine.getClasse());
				} else {
					ed = EleveDiff.Diff(currentEleve, csvLine.getNom(), csvLine.getPrenom(),
							csvLine.getDateDeNaissance(), csvLine.getClasse());
					if(ed != null) {
						ed.setId(csvLine.getId());
					}
				}

				if (ed != null) {
					result.add(ed);
				}
				return true;
			}
		});
		return result;
	}

	
	public String commit(List<EleveDiff> elevesdDiff) {
		StringBuilder result = new StringBuilder();
		int nbUpdate = 0;
		int nbCreate = 0;
		for (EleveDiff ed : elevesdDiff) {
			if (ed.getEleve() == null) {
				// Create new one
                                DBAccess db = DBAccess.Get();
				Eleve eleve = db.doActionWithResult(new EleveActions.Create());
                                db.doActionWithResult(
                                        new EleveActions.Set(eleve.getOrid())
                                                .numen(ed.getId())
                                                .classe(ed.getNewClasse())
                                                .dateDeNaissance(ed.getNewDateDeNaissance())
                                                .nom(ed.getNewNom())
                                                .prenom(ed.getNewPrenom())
                                );
				nbCreate++;
			} else {
				// Update
				DBAccess db = DBAccess.Get();
				Eleve eleve = retrieveEleve(ed.getId());
                                db.doActionWithResult(
                                        new EleveActions.Set(eleve.getOrid())
                                        .classe(ed.getNewClasse())
                                        .dateDeNaissance(ed.getNewDateDeNaissance())
                                        .nom(ed.getNewNom())
                                        .prenom(ed.getNewPrenom())
                                );
				nbUpdate++;
			}
		}
		result.append("Nombre d'élève créé: " + nbCreate);
		result.append("\nNombre d'élève mis à jours: " + nbUpdate);
		return result.toString();
	}
	
        

	private Eleve retrieveEleve(String id) {
		DBAccess db = DBAccess.Get();
		List<Eleve> list = db.doQuery(new EleveActions.GetByNumen(id));

		if (list.size() > 1) {
			// TODO: must throw an exception !
		} else if (list.size() == 0) {
			return null;
		}
		
		Eleve eleve = list.get(0);
		return eleve;
	}

	private Reader getUploadedFileReader() throws Exception {
		String fileFormat = importOptions.getFileFormat();
		if(fileFormat == null || fileFormat.isEmpty()) {
			fileFormat = Charset.defaultCharset().name();
		}
		return GetUploadedFileReader(importOptions.getFileRessourceID(),fileFormat);
	}

	private InputStream getUploadedFileInputStream() throws Exception {
		return new FileInputStream(new File(importOptions.getFileRessourceID()));
	}

	/*
	 * Exemple CSV: numen;nom;prenom;dateDeNaissance;classe
	 */
	private static CellProcessor[] getProcessors() {
		final CellProcessor[] processors = new CellProcessor[] {
				new NotNull(new Trim(new StrNotNullOrEmpty(new UniqueHashCode()))), // numen
																					// (must
																					// be
																					// unique)
				new NotNull(new Trim(new StrNotNullOrEmpty())), // firstName
				new NotNull(new Trim(new StrNotNullOrEmpty())), // lastName
				new NotNull(new ParseDate("dd/MM/yyyy")), // birthDate
				new NotNull(new Trim(new StrNotNullOrEmpty())) // class
		};
		return processors;
	}

	private static Reader GetUploadedFileReader(String uploadFileToken, String charset) throws Exception {
		return new InputStreamReader(new FileInputStream(new File(uploadFileToken)),charset);
	}

	public static ImportOptions CreateImportOptionsFrom(String uploadFileToken) {
		ImportOptions importOptions = new ImportOptions();
		importOptions.setFileRessourceID(uploadFileToken);
		if (uploadFileToken.toLowerCase().endsWith(".xls") || uploadFileToken.toLowerCase().endsWith(".xlsx")) {
			importOptions.setFileExcel(true);
		} else {
			importOptions.setFileExcel(false);
			try {
                                importOptions.setFileFormat(Charset.defaultCharset().name());
				Reader reader = GetUploadedFileReader(uploadFileToken,Charset.defaultCharset().name());
				BufferedReader br = new BufferedReader(reader);
				String firstLine = br.readLine();
				br.close();
				reader.close();
				char[] delimiters = { ';', '|', ',', '\t' };
				for (char delimiter : delimiters) {
					if (firstLine.contains("" + delimiter)) {
						importOptions.setDelimiter(delimiter);
						break;
					}
				}
				importOptions.setSkipFirstLine(false);
				try {
					ElevesImport ei = new ElevesImport(importOptions);
					ei.readAsExample(1);
				} catch (Exception e) {
					importOptions.setSkipFirstLine(true);
					try {
						ElevesImport ei = new ElevesImport(importOptions);
						ei.readAsExample(1);
					} catch (Exception e2) {
						importOptions.setSkipFirstLine(false);
					}
				}
			} catch (Exception e) {
				// discard !
			}
		}
		return importOptions;
	}

}

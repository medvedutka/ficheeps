/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficheeps.view.utils;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.ListBox;


public class ListBoxHasValue extends ListBox implements HasValue<String> {

    public ListBoxHasValue() {
        setMultipleSelect(false);
        addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                ValueChangeEvent.fire(ListBoxHasValue.this,getValue());
            }
        });
    }
    
    @Override
    public String getValue() {
        int index = getSelectedIndex();
        if(index >= 0) {
            return getValue(index);
        }
        return "";
    }

    @Override
    public void setValue(String value) {
        setValue(value,false);
    }

    @Override
    public void setValue(String value, boolean fireEvents) {
        if(value == null) {
            setSelectedIndex(-1);
        }
        else {
            int c = getItemCount();
            int i = 0;
            for(;i<c;i++) {
                if(value.equals(getValue(i))) {
                    setSelectedIndex(i);
                    break;
                }
            }
            if(i == c) {
                insertItem("#"+value+"#", value, 0);
                setSelectedIndex(0);
            }
        }

        if(fireEvents) {
            ValueChangeEvent.fire(this,value);
        }
    }

    @Override
    public HandlerRegistration addValueChangeHandler(ValueChangeHandler<String> handler) {
        return addHandler(handler, ValueChangeEvent.getType());
    }
    
}

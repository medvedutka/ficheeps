/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficheeps.view.utils;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.BrowserEvents;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;


public class TooltipCell<T> extends AbstractCell<T>
{
    private final AbstractCell<T> cell;
    private final String tooltip;

    public TooltipCell(AbstractCell<T> cell, String tooltip) {
        super(BrowserEvents.CLICK, BrowserEvents.KEYDOWN);
        this.cell = cell;
        this.tooltip = tooltip;
    }

    @Override
    public void render(Context context, T value, SafeHtmlBuilder sb) {
        sb.appendHtmlConstant("<div title=\"" + tooltip + "\">");
        cell.render(context, value, sb);
        sb.appendHtmlConstant("</div>");
    }

    @Override
    public void onBrowserEvent(com.google.gwt.cell.client.Cell.Context context, Element parent, T value, NativeEvent event, ValueUpdater<T> valueUpdater) {
        cell.onBrowserEvent(context, parent, value, event, valueUpdater);
    }


}
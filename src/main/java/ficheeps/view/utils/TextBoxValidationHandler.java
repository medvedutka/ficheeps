package ficheeps.view.utils;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.ui.ValueBoxBase;




public class  TextBoxValidationHandler<T> {
    private final ValueBoxBase<T> valueBox;
    private final Action action;
    private T valueBeforeEditing;
    
    public static interface Action<T> {
        void valueChanged(T newValue);
    }
    
    private TextBoxValidationHandler(ValueBoxBase<T> _valueBox, Action _action) {
        action = _action;
        valueBox = _valueBox;
        valueBox.addFocusHandler(new FocusHandler() {
            @Override
            public void onFocus(FocusEvent event) {
                valueBeforeEditing = valueBox.getValue();
            }
        });
        valueBox.addKeyPressHandler(new KeyPressHandler() {

            @Override
            public void onKeyPress(KeyPressEvent event) {
                if(event.getCharCode() == KeyCodes.KEY_ENTER) {
                    validate();
                    valueBeforeEditing = valueBox.getValue();
                }
            }
        });
        valueBox.addBlurHandler(new BlurHandler() {
            @Override
            public void onBlur(BlurEvent event) {
                validate();
                valueBeforeEditing = null;
            }
        });

    }

    public void validate() {
        T newValue = valueBox.getValue();
        if((valueBeforeEditing == null && newValue != null) || (valueBeforeEditing != null && !valueBeforeEditing.equals(newValue))) {
            action.valueChanged(newValue);
        }
    }
    
    public static <T> TextBoxValidationHandler Handle(ValueBoxBase<T> _valueBox, Action<T> _action) {
        return new TextBoxValidationHandler(_valueBox, _action);
    }
}

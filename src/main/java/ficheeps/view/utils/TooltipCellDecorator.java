package ficheeps.view.utils;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.IconCellDecorator;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import java.util.Set;

/**
 * A {@link Cell} decorator that adds an icon to another {@link Cell}.
 *
 * @param <C> the type that this Cell represents
 */
public class TooltipCellDecorator<C> implements Cell<C> {

    public static interface TooltipProvider<C> {
        String tooltipFor(C value);
    }
    
    interface Template extends SafeHtmlTemplates {
        @Template("<div style=\"position:relative;zoom:1;\" title=\"{0}\">{1}</div>")
        SafeHtml outerDiv(String title, SafeHtml cellContents);
    }

    private static Template template;
    private final Cell<C> cell;
    private final TooltipProvider<C> tooltipProvider;
    
    /**
     * Construct a new {@link IconCellDecorator}.
     *
     * @param cell the cell to decorate
     */
    public TooltipCellDecorator(Cell<C> cell,TooltipProvider<C> tooltipProvider) {
        if (template == null) {
            template = GWT.create(Template.class);
        }
        this.cell = cell;
        this.tooltipProvider = tooltipProvider;
    }

    @Override
    public boolean dependsOnSelection() {
        return cell.dependsOnSelection();
    }

    @Override
    public Set<String> getConsumedEvents() {
        return cell.getConsumedEvents();
    }

    @Override
    public boolean handlesSelection() {
        return cell.handlesSelection();
    }

    @Override
    public boolean isEditing(Context context, Element parent, C value) {
        return cell.isEditing(context, getCellParent(parent), value);
    }

    @Override
    public void onBrowserEvent(Context context, Element parent, C value,NativeEvent event, ValueUpdater<C> valueUpdater) {
        cell.onBrowserEvent(context, getCellParent(parent), value, event,valueUpdater);
    }

    @Override
    public void render(Context context, C value, SafeHtmlBuilder sb) {
        SafeHtmlBuilder cellBuilder = new SafeHtmlBuilder();
        cell.render(context, value, cellBuilder);
        String title = tooltipProvider.tooltipFor(value);
        sb.append(template.outerDiv(title, cellBuilder.toSafeHtml()));
    }

    @Override
    public boolean resetFocus(Context context, Element parent, C value) {
        return cell.resetFocus(context, getCellParent(parent), value);
    }

    @Override
    public void setValue(Context context, Element parent, C value) {
        cell.setValue(context, getCellParent(parent), value);
    }

 
    /**
     * Get the parent element of the decorated cell.
     *
     * @param parent the parent of this cell
     * @return the decorated cell's parent
     */
    private Element getCellParent(Element parent) {
        return parent.getFirstChildElement().getChild(1).cast();
    }
}

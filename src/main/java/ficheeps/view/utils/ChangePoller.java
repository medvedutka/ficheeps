/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficheeps.view.utils;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.FocusWidget;

/**
 *
 * @author cjalady
 */
public class ChangePoller<T> {
        
        public static interface ChangePollerDelegate<T> {
            T getValue();
            void valueChanged(T initialValue, T newValue);
        }
        
        private final ChangePollerDelegate<T> changePollerDelegate;
        private T initialValue;
        private final Timer timer;
        
        private ChangePoller(ChangePollerDelegate<T> delegate) {
            changePollerDelegate = delegate;
            timer = new Timer() {
                @Override
                public void run() {
                    test();
                }
            };
        }
        
        public ChangePoller(FocusWidget widget, ChangePollerDelegate<T> delegate) {
            this(delegate);
            attachTo(widget);
        }
        
        private void attachTo(FocusWidget widget) {
            widget.addBlurHandler(new BlurHandler() {
                @Override
                public void onBlur(BlurEvent event) {
                    timer.cancel();
                    test();
                }
            });
            widget.addFocusHandler(new FocusHandler() {
                @Override
                public void onFocus(FocusEvent event) {
                    initialValue = changePollerDelegate.getValue();
                    timer.scheduleRepeating(1000);
                }
            });
        }
        
        private void test() {
            T currentValue = changePollerDelegate.getValue();
            if((initialValue == null && currentValue != null)
                    ||(initialValue != null && !initialValue.equals(currentValue))) {
                changePollerDelegate.valueChanged(initialValue, currentValue);
            }
        }
        
        

}
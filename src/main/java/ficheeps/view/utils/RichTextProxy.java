package ficheeps.view.utils;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.RichTextArea;

public class RichTextProxy implements  HasValue<String> {
	private final HandlerManager handlerManager = new HandlerManager(this);

	String value = null;
	
	private RichTextArea richText;
	public RichTextProxy(RichTextArea rta) {
		richText = rta;
		richText.addBlurHandler(new BlurHandler() {
			@Override
			public void onBlur(BlurEvent event) {
				String newValue = richText.getHTML();
				if(value == null || !value.equals(newValue)) {
					ValueChangeEvent.fire(RichTextProxy.this, newValue);
				}
			}
		});
	}

	  @Override
	  public HandlerRegistration addValueChangeHandler(ValueChangeHandler<String> handler) {
	    return handlerManager.addHandler(ValueChangeEvent.getType(), handler);
	  }
	
	@Override
	  public void fireEvent(GwtEvent<?> event) {
	    handlerManager.fireEvent(event);
	  }

	@Override
	public String getValue() {
		return richText.getHTML();
	}

	@Override
	public void setValue(String value) { 
		this.value = value;
		richText.setHTML(value);
	}

	 @Override
	  public void setValue(String value, boolean fireEvents) {
	    setValue(value);
	    if (fireEvents) ValueChangeEvent.fire(this, value);
	  }
	
}
package ficheeps.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.AttachEvent;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import ficheeps.presenter.FicheElevePresenter;
import ficheeps.view.utils.RichTextProxy;
import ficheeps.view.utils.RichTextToolbar;
import ficheeps.view.utils.RichTextToolbar.Feature;

public class FicheEleveView extends Composite implements FicheElevePresenter.Display {

	
	  interface MyUiBinder extends UiBinder<Widget, FicheEleveView> {}
	  private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);
	
	  
	  public interface CSSStyle extends CssResource {
		  String oddCommentary();
	  }	  
	  
	  @UiField
	  CSSStyle style;
	  public CSSStyle getCSSStyle() {
		  return style;
	  }
	  
	  
	  @UiField Label id;
	  @UiField Label nomPrenom;
	  @UiField Label classe;
	  @UiField Label dateDeNaissance;
	  @UiField VerticalPanel commentaries;
	  @UiField Button addCommmentary;
	  
	  @UiField RichTextArea suiviMedical;
	  @UiField RichTextToolbar suiviMedicalToolbar;
	  
	  @UiField RichTextArea suggestionFinDeSeconde;
	  @UiField RichTextToolbar suggestionFinDeSecondeToolbar;
	  @UiField SplitLayoutPanel splitLayoutSuiviMedicalSuggestionFinDeSeconde;
	  @UiField VerticalPanel suggestionFinDeSecondePanel;
	  
	  //@UiField Button ficheElevePDF;
          @UiField Button moreInfosButton;
	  
	  public FicheEleveView() {
	    initWidget(uiBinder.createAndBindUi(this));
	    
	    suiviMedicalToolbar.setRichTextArea(suiviMedical);
	    suiviMedicalToolbar.disableFeature(Feature.CreateLink
				,Feature.Fonts
				,Feature.Hr
				,Feature.Indent
				,Feature.InsertImage
				,Feature.JustifyCenter
				,Feature.JustifyLeft
				,Feature.JustifyRight
				,Feature.RemoveLink
				,Feature.Subscript
				,Feature.Superscript
				,Feature.FontSizes
				,Feature.Ol
				,Feature.Ul
				,Feature.Strike
				);
	    
	    suiviMedical.getElement().getStyle().setBorderWidth(1.0, Unit.PX);
	    
	    
	    suggestionFinDeSecondeToolbar.setRichTextArea(suggestionFinDeSeconde);
	    suggestionFinDeSecondeToolbar.disableFeature(Feature.CreateLink
				,Feature.Fonts
				,Feature.Hr
				,Feature.Indent
				,Feature.InsertImage
				,Feature.JustifyCenter
				,Feature.JustifyLeft
				,Feature.JustifyRight
				,Feature.RemoveLink
				,Feature.Subscript
				,Feature.Superscript
				,Feature.FontSizes
				,Feature.Ol
				,Feature.Ul
				,Feature.Strike
				);
	    suggestionFinDeSeconde.getElement().getStyle().setBorderWidth(1.0, Unit.PX);
	   
	    //splitLayoutSuiviMedicalSuggestionFinDeSeconde.
	  }

        /*
	@Override
	public HasClickHandlers getFichePDFHandler() {
		return ficheElevePDF;
	}*/
  
        @Override
	public Button getMoreInfosHandler() {
		return moreInfosButton;
	}
        
	@Override
	public HasClickHandlers getAddCommentaryHandler() {
		return addCommmentary;
	}
	@Override
	public HasText getId() {
		return id;
	}
	@Override
	public HasText getNomPrenom() {
		return nomPrenom;
	}
	@Override
	public HasText getClasse() {
		return classe;
	}
	@Override
	public HasText getDateDeNaissance() {
		return dateDeNaissance;
	}
	
	//@Override 
	//public HasWidgets getCommentaries() {
	//	return commentaries;
	//}
	
	@Override 
	public void addCommentaryAtPositionZero(Widget view) {
		commentaries.insert(view,0);
		resetCommentaryBackground();
	}
		
	@Override 
	public void addCommentary(Widget view) {
		commentaries.add(view);
		resetCommentaryBackground();
	}
	
	private void resetCommentaryBackground() {
		String cssStyle = getCSSStyle().oddCommentary();
		for(int i=0;i<commentaries.getWidgetCount();i++) {
			Widget w = commentaries.getWidget(i);
			w.removeStyleName(cssStyle);
			if((i % 2) != 0) {
				w.addStyleName(cssStyle);
			}
		}		
	}
	
	private RichTextProxy suiviMedicalProxy = null;
	@Override
	public HasValue<String> getSuiviMedical() {
		if(suiviMedicalProxy == null) {
			suiviMedicalProxy =  new RichTextProxy(suiviMedical);
		}
		return suiviMedicalProxy;
	}

	private RichTextProxy suggestionFinDeSecondeProxy = null;
	@Override
	public HasValue<String> getSuggestionFinDeSeconde() {
		if(suggestionFinDeSecondeProxy == null) {
			suggestionFinDeSecondeProxy =  new RichTextProxy(suggestionFinDeSeconde);
		}
		return suggestionFinDeSecondeProxy;
	}
	
	
	private double suggestionFinDeSecondeSize = 300.;
	
	@Override
	public void ensureSuggestionFinDeSecondVisible() {
		if(suggestionFinDeSecondePanel.isAttached()) {
			splitLayoutSuiviMedicalSuggestionFinDeSeconde.setWidgetSize(suggestionFinDeSecondePanel, suggestionFinDeSecondeSize);
		}
		else {
			suggestionFinDeSecondePanel.addAttachHandler(new AttachEvent.Handler() {
				@Override
				public void onAttachOrDetach(AttachEvent event) {
					if(event.isAttached()) {
						splitLayoutSuiviMedicalSuggestionFinDeSeconde.setWidgetSize(suggestionFinDeSecondePanel, suggestionFinDeSecondeSize);
					}
				}
			});
		}
	}
	
	
	
	
}

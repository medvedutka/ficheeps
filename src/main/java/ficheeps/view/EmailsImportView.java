package ficheeps.view;

import gwtupload.client.SingleUploader;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.client.HasSafeHtml;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import ficheeps.model.EmailsDiff;
import gwtupload.client.IUploader;

public class EmailsImportView extends Composite implements ficheeps.presenter.EmailsImportPresenter.Display {

    interface MyUiBinder extends UiBinder<Widget, EmailsImportView> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    SingleUploader uploader;

    @UiField
    Button commitImport;

    @UiField
    HTML commitImportResult;

    @UiField
    SimplePanel uploadPanel;

    @UiField
    DataGrid<EmailsDiff> suggestTable;

    @UiField
    Widget panelResultImport; 
    
    @UiField
    RadioButton appendRadioButton;
    
    @UiField
    RadioButton replaceRadioButton;
    
    public EmailsImportView() {
        initWidget(uiBinder.createAndBindUi(this));

        uploader = new SingleUploader();
        uploader.setI18Constants((IUploader.UploaderConstants)GWT.create(MyUploaderConstants.class));
        uploader.setTitle("Importer le fichier Excel choisi.");
        uploadPanel.add(uploader);
    }    
    
    @Override
    public Widget getResultImportView() {
        return panelResultImport;
    }
    
    @Override
    public DataGrid<EmailsDiff> getSuggestTable() {
        return suggestTable;
    }

    @Override
    public Button getCommitImportClickHandler() {
        return commitImport;
    }

    @Override
    public SingleUploader getUploader() {
        return uploader;
    }

    @Override
    public HasSafeHtml getCommitImportResult() {
        return commitImportResult;
    }

    
    public static interface MyUploaderConstants extends IUploader.UploaderConstants {
        @DefaultStringValue("Envoyer")
        @Override
        String uploaderSend();
    }
    
    @Override
    public HasValue<Boolean> getAppendModeSelector() {
        return appendRadioButton;
    }
    @Override
    public HasValue<Boolean> getReplaceModeSelector() {
        return replaceRadioButton;
    }    
    
}

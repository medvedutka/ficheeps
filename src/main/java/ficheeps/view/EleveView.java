package ficheeps.view;

import java.util.Date;

import org.gwt.advanced.client.ui.widget.DatePicker;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import ficheeps.presenter.ElevePresenter;

public class EleveView extends Composite implements ElevePresenter.Display {

	  interface MyUiBinder extends UiBinder<Widget, EleveView> {}
	  private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

	  @UiField TextBox id;
	  @UiField TextBox nom;
	  @UiField TextBox prenom;
	  @UiField TextBox classe;
	  @UiField DatePicker birthDate;
	  
	  public EleveView() {
		  initWidget(uiBinder.createAndBindUi(this));
		  birthDate.setFormat("dd/MM/yyyy");
		  birthDate.setTimeVisible(false);
	  }


	  HasValue<java.util.Date> birthDateWrapper = new HasValue<java.util.Date>() {
		@Override
		public HandlerRegistration addValueChangeHandler(ValueChangeHandler<Date> handler) {
			return null;
		}
		@Override
		public void fireEvent(GwtEvent<?> event) {
		}
		@Override
		public Date getValue() {
			return birthDate.getDate();
		}
		@Override
		public void setValue(Date value) {
			birthDate.setDate(value);
		}
		@Override
		public void setValue(Date value, boolean fireEvents) {
			birthDate.setDate(value);
		}
	};

	
	
	@Override
	public HasValue<java.util.Date> getDateDeNaissance() {
		return birthDateWrapper;
	}
	 
	@Override
	public HasValue<String> getId() {
		return id;
	}
	@Override
	public HasValue<String> getNom() {
		return nom;
	}
	@Override
	public HasValue<String> getPrenom() {
		return prenom;
	}
	@Override
	public HasValue<String> getClasse() {
		return classe;
	}
	  
	  
}

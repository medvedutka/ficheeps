package ficheeps.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import ficheeps.presenter.ApsaConfigurationPresenter;



public class ApsaConfigurationView extends Composite implements ApsaConfigurationPresenter.Display {

    interface MyUiBinder extends UiBinder<Widget, ApsaConfigurationView> {
    }
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    public ApsaConfigurationView() {
        initWidget(uiBinder.createAndBindUi(this));
    }

    @UiField CheckBox checkboxConstraintLabel;
    @UiField ListBox apsaLabels;
    @UiField TextBox textboxNewApsaLabel;
    @UiField Button buttonNewApsaLabel;
    
    public CheckBox getCheckboxConstraintLabel() {
        return checkboxConstraintLabel;
    }
    public ListBox getApsaLabels() {
        return apsaLabels;
    }
    public TextBox getTextboxNewApsaLabel() {
        return textboxNewApsaLabel;
    }
    public Button getButtonNewApsaLabel() {
        return buttonNewApsaLabel;
    }
    
}

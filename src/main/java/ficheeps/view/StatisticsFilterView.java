package ficheeps.view;


import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy;
import com.google.gwt.user.client.TakesValue;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import ficheeps.presenter.StatisticsFilterPresenter;

public class StatisticsFilterView extends Composite implements StatisticsFilterPresenter.Display {

    interface MyUiBinder extends UiBinder<Widget, StatisticsFilterView> {
    }
    final private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField(provided = true)
    CellTable listNiveau;
    @UiField(provided = true)
    CellTable listAnnee;
    @UiField(provided = true)
    CellTable listAPSA;
    @UiField
    CheckBox checkBoxNiveau;
    @UiField
    CheckBox checkBoxAnnee;
    @UiField
    CheckBox checkBoxAPSA;
    @UiField 
    TextBox searchAPSA;
    @UiField
    DisclosurePanel disclosurePanelAPSA;
    @UiField 
    DisclosurePanel disclosurePanelAnnee;
    @UiField
    DisclosurePanel disclosurePanelNiveau;
    @UiField
    Button downloadButton;
    @UiField(provided = true)
    FormPanel downloadFormPanel;
    @UiField
    Hidden filterObjectHiddenOption;
    @UiField
    Hidden actionHiddenOption;
    @UiField 
    Hidden dummyPreventCacheHiddenOption;
    
    public StatisticsFilterView() {
        downloadFormPanel = new FormPanel("_blank");
        
        listAPSA = new CellTable();
        listAPSA.setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.DISABLED);
        listAPSA.setPageSize(Integer.MAX_VALUE);
        listAnnee = new CellTable();
        listAnnee.setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.DISABLED);
        listAnnee.setPageSize(Integer.MAX_VALUE);        
        listNiveau = new CellTable();
        listNiveau.setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.DISABLED);
        listNiveau.setPageSize(Integer.MAX_VALUE);        

        initWidget(uiBinder.createAndBindUi(this));
        
        searchAPSA.getElement().setAttribute("placeholder", "filtrer ...");
        searchAPSA.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                event.stopPropagation();
            }
        });
        
        manageDisclosurePanelAndCheckbox(disclosurePanelAPSA,checkBoxAPSA);
        manageDisclosurePanelAndCheckbox(disclosurePanelNiveau,checkBoxNiveau);
        manageDisclosurePanelAndCheckbox(disclosurePanelAnnee,checkBoxAnnee);
        
        searchAPSA.setVisible(false);
        disclosurePanelAPSA.addCloseHandler(new CloseHandler<DisclosurePanel>() {
            @Override
            public void onClose(CloseEvent<DisclosurePanel> event) {
                searchAPSA.setVisible(false);
            }
        });
        disclosurePanelAPSA.addOpenHandler(new OpenHandler<DisclosurePanel>() {
            @Override
            public void onOpen(OpenEvent<DisclosurePanel> event) {
                searchAPSA.setVisible(true);
            }
        });
    }

    @Override
    public CellTable getListNiveau() {
        return listNiveau;
    }
    @Override
    public CellTable getListAnnee() {
        return listAnnee;
    }
    @Override
    public CellTable getListAPSA() {
        return listAPSA;
    }
    @Override
    public CheckBox getCheckBoxNiveau() {
        return checkBoxNiveau;
    }
    @Override
    public CheckBox getCheckBoxAnnee() {
        return checkBoxAnnee;
    }
    @Override
    public CheckBox getCheckBoxAPSA() {
        return checkBoxAPSA;
    }
    @Override
    public TextBox getSearchAPSA() {
        return searchAPSA;
    }
    @Override
    public Button getDownloadButton() {
        return downloadButton;
    }
    @Override
    public FormPanel getDownloadFormPanel() {
        return downloadFormPanel;
    }
    @Override
    public TakesValue<String> getFilterObjectHiddenOption() {
        return filterObjectHiddenOption;
    }
    @Override
    public TakesValue<String> getActionHiddenOption() {
        return actionHiddenOption;
    }
    @Override
    public TakesValue<String> getDummyPreventCacheHiddenOption() {
        return dummyPreventCacheHiddenOption;
    }        


    
    private void manageDisclosurePanelAndCheckbox(final DisclosurePanel dp, final CheckBox cb) {
        cb.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                event.stopPropagation();
            }
        });
        cb.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                dp.setOpen(event.getValue());
            }
        }); 
        dp.addCloseHandler(new CloseHandler<DisclosurePanel>() {
            @Override
            public void onClose(CloseEvent<DisclosurePanel> event) {
                cb.setValue(Boolean.FALSE);
            }
        });
        dp.addOpenHandler(new OpenHandler<DisclosurePanel>() {
            @Override
            public void onOpen(OpenEvent<DisclosurePanel> event) {
                cb.setValue(Boolean.TRUE);
            }
        });       
    }
}

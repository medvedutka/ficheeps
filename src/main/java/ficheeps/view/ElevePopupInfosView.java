package ficheeps.view;


import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import ficheeps.presenter.ElevePopupInfosPresenter;

public class ElevePopupInfosView extends Composite implements ElevePopupInfosPresenter.Display {

        interface MyUiBinder extends UiBinder<Widget, ElevePopupInfosView> {}
        private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

        @UiField Anchor classAnchor;
        @UiField VerticalPanel groupsPanel;
        @UiField Button ficheElevePDF;
        @UiField Button emailEleve;
        
        public ElevePopupInfosView() {
                initWidget(uiBinder.createAndBindUi(this));
        }
	
	@Override
	public Anchor getClassAnchor() {
            return classAnchor;
	}
	@Override
	public VerticalPanel getGroupsPanel() {
            return groupsPanel;
	}
	@Override
	public Button getFicheElevePDF() {
            return ficheElevePDF;
	}
	
	@Override
	public Button getEmailEleveButton() {
            return emailEleve;
	}
		
}

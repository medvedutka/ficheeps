package ficheeps.view;


import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import ficheeps.presenter.ExportPDFPresenter;



public class ExportPDFView  extends Composite implements ExportPDFPresenter.Display {

	  interface MyUiBinder extends UiBinder<Widget, ExportPDFView> {}
	  private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

	  
	  public ExportPDFView() {
	    initWidget(uiBinder.createAndBindUi(this));
	    
	    fileUpload.setName("pdfFileToUpload");
	    
	    ficheSuggestionFinDeSecondeUseDefaultTemplate.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				manageSuggestionFinDeSecondePDFChooserVisibility();
			}
		});
	    ficheSuggestionFinDeSecondeUseCustomTemplate.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				manageSuggestionFinDeSecondePDFChooserVisibility();
			}
		});
	    
	    ficheSuggestionFinDeSecondeUseDefaultTemplate.setValue(true,true);
	    manageSuggestionFinDeSecondePDFChooserVisibility();
	  }
	    
	  private void manageSuggestionFinDeSecondePDFChooserVisibility() {
		  if(ficheSuggestionFinDeSecondeUseDefaultTemplate.getValue()) {
			  fileUpload.setVisible(false);
		  }
		  else {
			  fileUpload.setVisible(true);
		  }
	  }
	  
	  @UiField FileUpload fileUpload;
	  
	  @UiField CheckBox sortByLevel;
	  @UiField CheckBox exportSuiviMedial;
	  @UiField RadioButton ficheSuggestionFinDeSeconde;
	  @UiField RadioButton ficheEleve;
	  
	  @UiField SimplePanel panelOptionsExportFicheEleve;
	  @UiField FormPanel panelOptionsSuggestionFinDeSeconde;
	  
	  @UiField RadioButton ficheSuggestionFinDeSecondeUseDefaultTemplate;
	  @UiField RadioButton ficheSuggestionFinDeSecondeUseCustomTemplate;
	  
	  @UiField RadioButton fSFDSDoNotExportCartoucheSeconde;
	  @UiField RadioButton fSFDSExportCartoucheSeconde;
	  @UiField RadioButton fSFDSExportCartoucheSecondeIfNotEmpty;
	  @UiField RadioButton fSFDSDoNotExportSuggestionFinDeSeconde;
	  @UiField RadioButton fSFDSExportSuggestionFinDeSeconde;
	  @UiField RadioButton fSFDSExportSuggestionFinDeSecondeIfNotEmpty;

	  
	  @Override
	  public HasValue<Boolean> getFSFDSDoNotExportCartoucheSeconde() {
		  return fSFDSDoNotExportCartoucheSeconde;
	  }
	  @Override
	  public HasValue<Boolean> getFSFDSExportCartoucheSeconde() {
		  return fSFDSExportCartoucheSeconde;
	  }
	  @Override
	  public HasValue<Boolean> getFSFDSExportCartoucheSecondeIfNotEmpty() {
		  return fSFDSExportCartoucheSecondeIfNotEmpty;
	  }
	  @Override
	  public HasValue<Boolean> getFSFDSDoNotExportSuggestionFinDeSeconde() {
		  return fSFDSDoNotExportSuggestionFinDeSeconde;
	  }
	  @Override
	  public HasValue<Boolean> getFSFDSExportSuggestionFinDeSeconde() {
		  return fSFDSExportSuggestionFinDeSeconde;
	  }
	  @Override
	  public HasValue<Boolean> getFSFDSExportSuggestionFinDeSecondeIfNotEmpty() {
		  return fSFDSExportSuggestionFinDeSecondeIfNotEmpty;
	  }
	  
	  @Override
	  public Widget getFileUpload() {
		  return fileUpload;
	  }
	  @Override
	  public RadioButton getFicheSuggestionFinDeSecondeUseDefaultTemplate() {
		  return ficheSuggestionFinDeSecondeUseDefaultTemplate;
	  }
	  @Override
	  public RadioButton getFicheSuggestionFinDeSecondeUseCustomTemplate() {
		  return ficheSuggestionFinDeSecondeUseCustomTemplate;
	  }	  
	  
	  @Override
	  public HasValue<Boolean> getSortByLevel() {
		  return sortByLevel;
	  }
	  @Override
	  public HasValue<Boolean> getExportSuiviMedial() {
		  return exportSuiviMedial;
	  }
	  @Override
	  public HasValue<Boolean> getFicheSuggestionFinDeSeconde() {
		  return ficheSuggestionFinDeSeconde;
	  }
	  @Override
	  public HasValue<Boolean> getFicheEleve() {
		  return ficheEleve;
	  }
	  
	  @Override
	  public SimplePanel getPanelOptionsExportFicheEleve() {
		  return panelOptionsExportFicheEleve;
	  }
	  @Override
	  public FormPanel getPanelOptionsSuggestionFinDeSeconde() {
		  return panelOptionsSuggestionFinDeSeconde;
	  }
}

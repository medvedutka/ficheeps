package ficheeps.view;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.Constants;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.client.ui.SimpleLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SelectionModel;

import ficheeps.model.*;
import ficheeps.model.ContactDatabase.Category;
import ficheeps.model.ContactDatabase.ContactInfo;

//@ShowcaseRaw({"ContactDatabase.java", "CwCellTable.ui.xml"})
public class CwCellTable extends SimpleLayoutPanel {

/**
 * The UiBinder interface used by this example.
 */
interface Binder extends UiBinder<Widget, CwCellTable> {
}

/**
 * The constants used in this Content Widget.
 */
public static interface CwConstants extends Constants {
  String cwCellTableColumnAddress();

  String cwCellTableColumnCategory();

  String cwCellTableColumnFirstName();

  String cwCellTableColumnLastName();

  String cwCellTableDescription();

  String cwCellTableName();
}

/**
 * The main CellTable.
 */
@UiField(provided = true)
CellTable<ContactInfo> cellTable;

/**
 * The pager used to change the range of data.
 */
@UiField(provided = true)
SimplePager pager;

/**
 * An instance of the constants.
 */
private final CwConstants constants;


/**
 * Constructor.
 *
 * @param constants the constants
 */
public CwCellTable() {
  this.constants = new CwConstants() {
	@Override
	public String cwCellTableColumnAddress() {
		return "adress";
	}
	@Override
	public String cwCellTableColumnCategory() {
		return "category";
	}

	@Override
	public String cwCellTableColumnFirstName() {
		return "firstName";
	}

	@Override
	public String cwCellTableColumnLastName() {
		return "lastName";
	}

	@Override
	public String cwCellTableDescription() {
		return "description";
	}

	@Override
	public String cwCellTableName() {
		return "name";
	};
  };
  
  
}


/**
 * Initialize this example.
 */
//@Override
public Widget onInitialize() {
  // Create a CellTable.

  // Set a key provider that provides a unique key for each contact. If key is
  // used to identify contacts when fields (such as the name and address)
  // change.
  cellTable = new CellTable<ContactInfo>(
      ContactDatabase.ContactInfo.KEY_PROVIDER);
  cellTable.setWidth("100%", true);

  // Do not refresh the headers and footers every time the data is updated.
  cellTable.setAutoHeaderRefreshDisabled(true);
  cellTable.setAutoFooterRefreshDisabled(true);

  // Attach a column sort handler to the ListDataProvider to sort the list.
  ListHandler<ContactInfo> sortHandler = new ListHandler<ContactInfo>(
      ContactDatabase.get().getDataProvider().getList());
  cellTable.addColumnSortHandler(sortHandler);

  // Create a Pager to control the table.
  SimplePager.Resources pagerResources = GWT.create(SimplePager.Resources.class);
  pager = new SimplePager(TextLocation.CENTER, pagerResources, false, 0, true);
  pager.setDisplay(cellTable);

  // Add a selection model so we can select cells.
  final SelectionModel<ContactInfo> selectionModel = new MultiSelectionModel<ContactInfo>(
      ContactDatabase.ContactInfo.KEY_PROVIDER);
  cellTable.setSelectionModel(selectionModel,
      DefaultSelectionEventManager.<ContactInfo> createCheckboxManager());

  // Initialize the columns.
  initTableColumns(selectionModel, sortHandler);

  // Add the CellList to the adapter in the database.
  ContactDatabase.get().addDataDisplay(cellTable);

  // Create the UiBinder.
  Binder uiBinder = GWT.create(Binder.class);
  Widget widget = uiBinder.createAndBindUi(this);

  return widget;
}

/**
 * Add the columns to the table.
 */
private void initTableColumns(
    final SelectionModel<ContactInfo> selectionModel,
    ListHandler<ContactInfo> sortHandler) {
  // Checkbox column. This table will uses a checkbox column for selection.
  // Alternatively, you can call cellTable.setSelectionEnabled(true) to enable
  // mouse selection.
  Column<ContactInfo, Boolean> checkColumn = new Column<ContactInfo, Boolean>(
      new CheckboxCell(true, false)) {
    @Override
    public Boolean getValue(ContactInfo object) {
      // Get the value from the selection model.
      return selectionModel.isSelected(object);
    }
  };
  cellTable.addColumn(checkColumn, SafeHtmlUtils.fromSafeConstant("<br/>"));
  cellTable.setColumnWidth(checkColumn, 40, Unit.PX);

  // First name.
  Column<ContactInfo, String> firstNameColumn = new Column<ContactInfo, String>(
      new EditTextCell()) {
    @Override
    public String getValue(ContactInfo object) {
      return object.getFirstName();
    }
  };
  firstNameColumn.setSortable(true);
  sortHandler.setComparator(firstNameColumn, new Comparator<ContactInfo>() {
    @Override
    public int compare(ContactInfo o1, ContactInfo o2) {
      return o1.getFirstName().compareTo(o2.getFirstName());
    }
  });
  cellTable.addColumn(firstNameColumn, constants.cwCellTableColumnFirstName());
  firstNameColumn.setFieldUpdater(new FieldUpdater<ContactInfo, String>() {
    @Override
    public void update(int index, ContactInfo object, String value) {
      // Called when the user changes the value.
      object.setFirstName(value);
      ContactDatabase.get().refreshDisplays();
    }
  });
  cellTable.setColumnWidth(firstNameColumn, 20, Unit.PCT);

  // Last name.
  Column<ContactInfo, String> lastNameColumn = new Column<ContactInfo, String>(
      new EditTextCell()) {
    @Override
    public String getValue(ContactInfo object) {
      return object.getLastName();
    }
  };
  lastNameColumn.setSortable(true);
  sortHandler.setComparator(lastNameColumn, new Comparator<ContactInfo>() {
    @Override
    public int compare(ContactInfo o1, ContactInfo o2) {
      return o1.getLastName().compareTo(o2.getLastName());
    }
  });
  cellTable.addColumn(lastNameColumn, constants.cwCellTableColumnLastName());
  lastNameColumn.setFieldUpdater(new FieldUpdater<ContactInfo, String>() {
    @Override
    public void update(int index, ContactInfo object, String value) {
      // Called when the user changes the value.
      object.setLastName(value);
      ContactDatabase.get().refreshDisplays();
    }
  });
  cellTable.setColumnWidth(lastNameColumn, 20, Unit.PCT);

  // Category.
  final Category[] categories = ContactDatabase.get().queryCategories();
  List<String> categoryNames = new ArrayList<String>();
  for (Category category : categories) {
    categoryNames.add(category.getDisplayName());
  }
  SelectionCell categoryCell = new SelectionCell(categoryNames);
  Column<ContactInfo, String> categoryColumn = new Column<ContactInfo, String>(
      categoryCell) {
    @Override
    public String getValue(ContactInfo object) {
      return object.getCategory().getDisplayName();
    }
  };
  cellTable.addColumn(categoryColumn, constants.cwCellTableColumnCategory());
  categoryColumn.setFieldUpdater(new FieldUpdater<ContactInfo, String>() {
    @Override
    public void update(int index, ContactInfo object, String value) {
      for (Category category : categories) {
        if (category.getDisplayName().equals(value)) {
          object.setCategory(category);
        }
      }
      ContactDatabase.get().refreshDisplays();
    }
  });
  cellTable.setColumnWidth(categoryColumn, 130, Unit.PX);

  // Address.
  Column<ContactInfo, String> addressColumn = new Column<ContactInfo, String>(
      new TextCell()) {
    @Override
    public String getValue(ContactInfo object) {
      return object.getAddress();
    }
  };
  addressColumn.setSortable(true);
  addressColumn.setDefaultSortAscending(false);
  sortHandler.setComparator(addressColumn, new Comparator<ContactInfo>() {
    @Override
    public int compare(ContactInfo o1, ContactInfo o2) {
      return o1.getAddress().compareTo(o2.getAddress());
    }
  });
  cellTable.addColumn(addressColumn, constants.cwCellTableColumnAddress());
  cellTable.setColumnWidth(addressColumn, 60, Unit.PCT);
}

}
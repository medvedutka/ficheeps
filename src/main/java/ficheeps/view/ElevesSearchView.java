package ficheeps.view;


import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import ficheeps.client.Utils;
import ficheeps.model.Eleve;
import ficheeps.presenter.ElevesSearchPresenter;
import ficheeps.presenter.ElevesSearchPresenter.EleveWidget;


public class ElevesSearchView extends Composite implements ElevesSearchPresenter.Display{

	
	interface MyUiBinder extends UiBinder<Widget, ElevesSearchView> {
	}

	private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

	public ElevesSearchView() {
            initWidget(uiBinder.createAndBindUi(this));
	}

	
	@UiField FlowPanel eleveResultPanel;
	@UiField TextBox searchTextBox;
	@UiField Image imageAjaxLoader;
        @UiField CheckBox includeAllEleveCheckBox;
        
	@Override
	public Image getImageAjaxLoader() {
		return imageAjaxLoader;
	}
	
	@Override
	public Panel getSearchEleveResultPanel() {
		return eleveResultPanel;
	}

	@Override
	public TextBox getSearchTextField() {
		return searchTextBox;
	}

	@Override
	public ElevesSearchPresenter.EleveWidget createEleveWidget(Eleve eleve) {
		return new BasicEleveWidget(eleve);
	}
        
        @Override
        public HasValue<Boolean> getOptionIncludeAllEleve() {
            return includeAllEleveCheckBox;
        }
        
	 static class BasicEleveWidget extends DockPanel implements EleveWidget {
		 	
                    private final Image image;
                    private final Label nameLabel;
                    private final Label dateLabel;
                    private final Label classeLabel;
                    private Eleve eleve;
                    private final Button button;
                    private final Button moreInfosButton;
		
		    public BasicEleveWidget(Eleve eleve) {
                        LayoutPanel imgsPanel = new LayoutPanel();
		    	image = new Image();
		    	image.setSize("64px", "64px");
		    	button = new Button();
		    	imgsPanel.add(image);
                        imgsPanel.add(button);
                        //imgsPanel.add(moreInfosButton);
                        imgsPanel.setWidgetLeftWidth(image, 0, Style.Unit.PX, 64, Style.Unit.PX);
                        imgsPanel.setWidgetTopHeight(image, 0, Style.Unit.PX, 64, Style.Unit.PX);
                        imgsPanel.setWidgetLeftWidth(button, 3, Style.Unit.PX, 18, Style.Unit.PX);
                        imgsPanel.setWidgetTopHeight(button, 3, Style.Unit.PX, 18, Style.Unit.PX);
                        imgsPanel.setWidth("64px");
		    	this.add(imgsPanel,DockPanel.WEST);
		    	VerticalPanel vp = new VerticalPanel();
		    	nameLabel = new Label();
                        nameLabel.getElement().getStyle().setFontWeight(Style.FontWeight.BOLD);
		    	vp.add(nameLabel);
		    	dateLabel = new Label();
		    	vp.add(dateLabel);
                        HorizontalPanel hz = new HorizontalPanel();
                        classeLabel = new Label();
                        hz.add(classeLabel);
                        moreInfosButton = new Button();
                        hz.add(moreInfosButton);
		    	vp.add(hz);
		    	this.add(vp,DockPanel.CENTER);
		    	setEleve(eleve);
		    	this.setWidth("160px");
                        this.addStyleName(ficheeps.client.Resources.INSTANCE.ficheepsStyle().eleveGroupViewBasicEleveWidget());
		    }
		    
		    public void setEleve(Eleve eleve) {
		    	this.eleve = eleve;
		    	image.setUrl("ficheeps/elevePhotoServlet?id=" + eleve.getId());
		    	nameLabel.setText(eleve.getNom().toUpperCase() + " " + eleve.getPrenom());
		    	dateLabel.setText(Utils.FormatDateDDSlashMMSlashYYYY(eleve.getDateDeNaissance()));
		    	classeLabel.setText(eleve.getClasse());
		    }
		    
                    @Override
		    public Eleve getEleve() {
		    	return eleve;
		    }
		    @Override
		    public Button getButton() {
		    	return button;
		    }
                    @Override
                    public Button getMoreInfosButton() {
		    	return moreInfosButton;
		    }
                    @Override
                    public void disable() {
                        this.button.setVisible(false);
                        this.getElement().addClassName(ficheeps.client.Resources.INSTANCE.ficheepsStyle().groupEleveWidgetDisable());
                    }
                    @Override
                    public void enable() {
                        this.button.setVisible(true);
                        this.getElement().removeClassName(ficheeps.client.Resources.INSTANCE.ficheepsStyle().groupEleveWidgetDisable());
                    }
		  }	
	

}

package ficheeps.view;

import ficheeps.model.CSVLine;
import ficheeps.model.EleveDiff;
import gwtupload.client.SingleUploader;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class ImportView extends Composite implements ficheeps.presenter.ImportPresenter.Display {

	interface MyUiBinder extends UiBinder<Widget, ImportView> {
	}

	private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

	
	SingleUploader uploader;
	
	@UiField
	Button unregisterElevesFromClasse;
	
	@UiField
	Button deleteAllGroupes;
	
        @UiField
        Button deleteAllListesLibre;
        
	@UiField
	Button deleteElevesWithoutClass;
	
        @UiField
	Button deleteElevesWithoutClassBornBefore;
        
	@UiField
	Button tryImport;

	@UiField
	Button commitImport;
	
	@UiField
	TextArea commitImportResult;
	
	@UiField
	SimplePanel uploadPanel;

	@UiField
	CellTable<CSVLine> sampleTable;
	
	@UiField
	DataGrid<EleveDiff> classUpdatedTable;
	
	@UiField
	DataGrid<EleveDiff> moreUpdatedTable;
	
	@UiField
	DataGrid<EleveDiff> newElevesTable;
	
	
	@UiField
	TextBox charSeparator;
	@UiField 
	CheckBox skipFirstLine;
	@UiField
	RadioButton excelFile;
	@UiField
	RadioButton csvFile;	
	@UiField
	Button applyOptions;
	
	
	@UiField 
	Widget panelTryImport;
	@UiField 
	Widget panelTryImportResult;	
	@UiField 
	Widget panelCommitImport;	
	@UiField 
	Widget panelSampleResult;
	@UiField
	ListBox fileEncodingFormat;
	
        @Override
	public ListBox getFileEncodingFormatOption() {
		return fileEncodingFormat;
	}
        @Override
	public TextBox getCharSeparatorOption() {
		return charSeparator;
	}
        @Override
	public CheckBox getSkipFirstLineOption() {
		return skipFirstLine;
	}
        @Override
	public RadioButton getExcelFileOption() {
		return excelFile;
	}
        @Override
	public RadioButton getCSVFileOption() {
		return csvFile;
	}
        @Override
	public HasClickHandlers getApplyOptions() {
		return applyOptions;
	}
	
	
        @Override
	public Widget getPanelSampleResult() {
		return panelSampleResult;
	}
        @Override
	public Widget getPanelTryImport() {
		return panelTryImport;
	}
        @Override
	public Widget getPanelTryImportResult() {
		return panelTryImportResult;
	}
        @Override
	public Widget getPanelCommitImport() {
		return panelCommitImport;
	}
	
        @Override
	public CellTable<CSVLine> getSampleTable() {
		return sampleTable;
	}
	
        @Override
	public DataGrid<EleveDiff> getMoreUpdateTable() {
		return moreUpdatedTable;
	}
	
        @Override
	public DataGrid<EleveDiff> getClassUpdatedTable() {
		return classUpdatedTable;
	}
        @Override
	public DataGrid<EleveDiff> getNewElevesTable() {
		return newElevesTable;
	}
	
        @Override
	public Button getTryImportClickHandler() {
		return tryImport;
	}
	
        @Override
	public Button getCommitImportClickHandler() {
		return commitImport;
	}
	
	
        @Override
        public HasClickHandlers getDeleteAllListesLibre() {
            return deleteAllListesLibre;
        }
        @Override
	public HasClickHandlers getDeleteElevesWithoutClasses() {
		return deleteElevesWithoutClass;
	}
        @Override
	public HasClickHandlers getDeleteElevesWithoutClassesBornBefore() {
		return deleteElevesWithoutClassBornBefore;
	}       
        
        @Override
	public HasClickHandlers getDeleteAllGroupesClickHandlers() {
		return deleteAllGroupes;
	}
        @Override
	public HasClickHandlers getUnregisterElevesFromClasse() {
		return unregisterElevesFromClasse;
	}
        @Override
	public SingleUploader getUploader() {
		return uploader;
	}
	
        @Override
	public TextArea getCommitImportResult() {
		return commitImportResult;
	}
	
	
	public ImportView() {
		initWidget(uiBinder.createAndBindUi(this));

		uploader = new SingleUploader();
		uploadPanel.add(uploader);

	}


}

package ficheeps.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy;
import com.google.gwt.user.client.ui.ButtonBase;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import ficheeps.presenter.ClassAndGroupNameInfosPresenter;
import java.util.Map;



public class ClassAndGroupNameInfosView extends Composite implements ClassAndGroupNameInfosPresenter.Display {

    interface MyUiBinder extends UiBinder<Widget, ClassAndGroupNameInfosView> {
    }
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    public ClassAndGroupNameInfosView() {
        cellTable = new CellTable();
        cellTable.setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.DISABLED);
        cellTable.setPageSize(Integer.MAX_VALUE);
        initWidget(uiBinder.createAndBindUi(this));
    }

    @UiField(provided = true) CellTable<Map.Entry<String,String>> cellTable;
    @UiField ButtonBase buttonSaveEntry;
    @UiField ButtonBase buttonImportWizard;
    @UiField TextBox nameTextBox;
    @UiField TextArea infosTextBox;
    
    @Override
    public CellTable<Map.Entry<String,String>> getCellTable() {
        return cellTable;
    }
    
    @Override
    public ButtonBase getButtonSaveEntry() {
        return buttonSaveEntry;
    }

    @Override
    public ButtonBase getButtonImportWizard() {
        return buttonImportWizard;
    }
    
    @Override
    public TextBox getNameTextBox() {
        return nameTextBox;
    }
    @Override
    public TextArea getInfosTextBox() {
        return infosTextBox;
    }

}

package ficheeps.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.PopupPanel.PositionCallback;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import ficheeps.client.AppController;

import ficheeps.presenter.CommentaryPresenter;
import ficheeps.presenter.CommentaryPresenter.ApsaDisplay;
import ficheeps.view.utils.ListBoxHasValue;
import ficheeps.view.utils.RichTextProxy;
import ficheeps.view.utils.RichTextToolbar;
import ficheeps.view.utils.RichTextToolbar.Feature;

public class CommentaryView extends Composite implements CommentaryPresenter.Display {

	interface MyUiBinder extends UiBinder<Widget, CommentaryView> {}
	private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

	public static MultiWordSuggestOracle APSA_ORACLE= new MultiWordSuggestOracle();
	
	
	@UiField
	ListBox niveau;
	
	@UiField
	TextBox anneeScolaire;
	
	@UiField
	TextBox professeur;
	
	@UiField
	CheckBox associationSportive;
	
	@UiField
	FlowPanel apsa;
	
	@UiField 
	PushButton newApsa;

	@UiField
	PushButton wizardApsa;
	
	@UiField RichTextArea appreciations;
	@UiField RichTextArea autre;

	@UiField
	Label moyenneApsa;
	
        @Override
	public ListBox getNiveau() {
		return niveau;
	}
        @Override
	public HasValue<String> getAnneeScolaire() {
		return anneeScolaire;
	}
        @Override
	public HasValue<String> getProfesseur() {
		return professeur;
	}
	
	private RichTextProxy appreciationsProxy = null;
        @Override
	public HasValue<String> getAppreciations() {	
		if(appreciationsProxy == null) {
			appreciationsProxy = new RichTextProxy(appreciations);
		}
		return appreciationsProxy;
	}
	private RichTextProxy autreProxy = null;
        @Override
	public HasValue<String> getAutre() {
		if(autreProxy == null) {
			autreProxy =  new RichTextProxy(autre);
		}
		return autreProxy;
	}
	
	
        @Override
	public HasValue<java.lang.Boolean> getAssociationSportive() {
		return associationSportive;
	}
	
	@Override
	public Label getMoyenneApsa() {
		return moyenneApsa;
	}
	
	@Override
	public HasClickHandlers getWizardApsaClickHandler() {
		return wizardApsa;
	}
	
        @Override
	public HasClickHandlers newApsaDisplay() {
		return newApsa;
	}
	
        @Override
	public ApsaDisplay createApsaDisplay() {
		ApsaDisplayView v = new ApsaDisplayView();
		apsa.insert(v,0);
		return v;
	}
	
	private class ApsaDisplayView extends HorizontalPanel implements ApsaDisplay {
		
		private HasValue<String> activite;
		private TextBox resultat;
		
		ApsaDisplayView() {
			this.setSpacing(1);
                        Widget activiteWidget = null;
                        if(AppController.SETTINGS.isConstraintApsaLabel()) {
                            ListBoxHasValue activiteListBox = new ListBoxHasValue();
                            activiteWidget = activiteListBox;
                            activite = activiteListBox;
                            if(AppController.SETTINGS.getApsaLabels() != null) {
                                for(String s : AppController.SETTINGS.getApsaLabels()) {
                                    activiteListBox.addItem(s);
                                }
                            }
                        }
                        else {
                            SuggestBox activiteSuggestBox = new SuggestBox(APSA_ORACLE);
                            activiteWidget = activiteSuggestBox;
                            activite = activiteSuggestBox;
                            activiteSuggestBox.addSelectionHandler(new SelectionHandler<SuggestOracle.Suggestion>() {
                                    @Override
                                    public void onSelection(SelectionEvent<Suggestion> event) {
                                        String text = activite.getValue();
                                        ValueChangeEvent.fire(activite, text);
                                    }
                            });
                        }
			activiteWidget.setWidth("150px");
			resultat = new TextBox();
			resultat.setWidth("50px");
                        resultat.getElement().getStyle().setBackgroundColor("transparent");
                        activiteWidget.getElement().getStyle().setBackgroundColor("transparent");
                        resultat.getElement().getStyle().setPadding(2, Unit.PX);
                        activiteWidget.getElement().getStyle().setPadding(2, Unit.PX);
			this.add(activiteWidget);
			this.add(resultat);	
			this.getElement().getStyle().setDisplay(Display.INLINE_BLOCK);
			this.getElement().getStyle().setPaddingRight(10, Unit.PX);
                        this.getElement().getStyle().setMargin(0,Unit.PX);
		}

		@Override
		public HasValue<String> getActivite() {
			return activite;
		}

		@Override
		public HasValue<String> getResultat() {
			return resultat;
		}
	}
	
	
	private PopupPanel richTextToolbarPopup;
	
	public CommentaryView() {
		initWidget(uiBinder.createAndBindUi(this));
		
		autre.getElement().getStyle().setBorderWidth(1.0, Unit.PX);
		appreciations.getElement().getStyle().setBorderWidth(1.0, Unit.PX);
		
		richTextToolbarPopup = new PopupPanel();
		richTextToolbarPopup.setAutoHideEnabled(false);
		richTextToolbarPopup.setGlassEnabled(false);
		richTextToolbarPopup.setModal(false);
		richTextToolbarPopup.getElement().getStyle().setBorderWidth(0.0, Unit.PX);
		richTextToolbarPopup.getElement().getStyle().setMargin(0.0, Unit.PX);
		richTextToolbarPopup.getElement().getStyle().setPadding(0.0, Unit.PX);
		richTextToolbarPopup.sinkEvents(Event.ONMOUSEOVER);
					
		MouseOutHandler outMouseHander = new MouseOutHandler() {
			@Override
			public void onMouseOut(MouseOutEvent event) {
				hideRichTextToolbarPopup();
			}
		};
		
		richTextToolbarPopup.addDomHandler(new MouseOverHandler() {
			@Override
			public void onMouseOver(MouseOverEvent event) {
				richTextToolbarPopupHideTimer.cancel();
			}
		}, MouseOverEvent.getType()); 
		appreciations.addMouseOverHandler(new MouseOverHandler() {
			@Override
			public void onMouseOver(MouseOverEvent event) {
				showRichTextToolbarPopup(appreciations);
			}
		});
		autre.addMouseOverHandler(new MouseOverHandler() {
			@Override
			public void onMouseOver(MouseOverEvent event) {
				showRichTextToolbarPopup(autre);
			}
		});
		richTextToolbarPopup.addDomHandler(outMouseHander, MouseOutEvent.getType()); 
		appreciations.addMouseOutHandler(outMouseHander);
		autre.addMouseOutHandler(outMouseHander);
	}
	
	
	private RichTextToolbar richTextToolbar;
	
	private void showRichTextToolbarPopup(final RichTextArea richTextArea) {
		richTextToolbarPopupHideTimer.cancel();
		if(richTextToolbarPopup.isShowing() && richTextToolbar.getRichTextArea() == richTextArea) {
			return;
		}
		richTextToolbarPopup.clear();
		richTextToolbar = new RichTextToolbar();
		richTextToolbar.setRichTextArea(richTextArea);
		richTextToolbar.disableFeature(Feature.CreateLink
											,Feature.Fonts
											,Feature.Hr
											,Feature.Indent
											,Feature.InsertImage
											,Feature.JustifyCenter
											,Feature.JustifyLeft
											,Feature.JustifyRight
											,Feature.RemoveLink
											,Feature.Subscript
											,Feature.Superscript
											,Feature.FontSizes
											,Feature.Ol
											,Feature.Ul
											,Feature.Strike
											);
		richTextToolbarPopup.add(richTextToolbar);
		richTextToolbarPopup.setPopupPositionAndShow(new PositionCallback() {
			@Override
			public void setPosition(int offsetWidth, int offsetHeight) {
				richTextToolbarPopup.setPopupPosition(richTextArea.getAbsoluteLeft(), richTextArea.getAbsoluteTop() - (offsetHeight - 5));
			}
		});
	}
	private void hideRichTextToolbarPopup() {
		richTextToolbarPopupHideTimer.schedule(200);
	}
	com.google.gwt.user.client.Timer richTextToolbarPopupHideTimer = new com.google.gwt.user.client.Timer() {
              @Override
	      public void run() {
	    	  richTextToolbarPopup.hide();
	        }
	};
	
	
}

package ficheeps.view;


import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.SelectElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.ButtonBase;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import ficheeps.client.Utils;
import ficheeps.model.Eleve;
import ficheeps.presenter.GroupeManagementPresenter;
import ficheeps.presenter.GroupeManagementPresenter.EleveWidget;
import ficheeps.presenter.GroupeManagementPresenter.GroupeBasicEditWidget;


public class GroupeManagementView extends Composite implements GroupeManagementPresenter.Display{

	
	interface MyUiBinder extends UiBinder<Widget, GroupeManagementView> {
	}

	private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

	public GroupeManagementView() {
		initWidget(uiBinder.createAndBindUi(this));
                
                try {
                    sortListBox.getElement().<SelectElement>cast().getOptions().getItem(0).setDisabled(true);
                }
                catch(Exception e){
                    GWT.log("Cannot disable 'Tris' element in the ListBox" + e.getMessage());
                }
	}

	
	@UiField FlowPanel eleveResultPanel;
	@UiField TextBox searchTextBox;
	@UiField VerticalPanel groupesListContainer;
	@UiField FlowPanel groupeEleveListPanel;
	@UiField ButtonBase removeGroupButton;
	@UiField ButtonBase addGroupButton;
	@UiField ButtonBase modifyGroupButton;
	@UiField ButtonBase exportButton;
	@UiField ButtonBase checkStatusButton;
	@UiField Image imageAjaxLoader;
	@UiField ButtonBase sortAlphabeticalButton;
        @UiField ButtonBase sortByClassButton;
        @UiField ButtonBase sortByDateButton;
        @UiField CheckBox includeAllEleveCheckBox;
	@UiField ListBox sortListBox;
        
	@Override
	public Image getImageAjaxLoader() {
		return imageAjaxLoader;
	}
	
	@Override
	public HasClickHandlers getModifyGroupButtonClickHandlers() {
		return modifyGroupButton;
	}
	
	@Override
	public ButtonBase getRemoveGroupClickHandlers() {
		return removeGroupButton;
	}
	
	@Override
	public ButtonBase getAddGroupClickHandlers() {
		return addGroupButton;
	}
	
	@Override
	public HasClickHandlers getExportClickHandlers() {
		return exportButton;
	}
	
	@Override
	public HasClickHandlers getCheckStatusClickHandlers() {
		return checkStatusButton;
	}
	
	@Override
	public VerticalPanel getGroupesListContainer() {
		return groupesListContainer;
	}
	@Override
	public FlowPanel getGroupeEleveListPanel() {
		return groupeEleveListPanel;
	}
	
	@Override
	public Panel getSearchEleveResultPanel() {
		return eleveResultPanel;
	}

	@Override
	public TextBox getSearchTextField() {
		return searchTextBox;
	}

	@Override
	public EleveWidget createEleveWidget(Eleve eleve) {
		return new BasicEleveWidget(eleve);
	}

        @Override
	public HasClickHandlers getSortAlphabeticallClickHandlers() {
		return sortAlphabeticalButton;
	}

        @Override
	public HasClickHandlers getSortByDateClickHandlers() {
		return sortByDateButton;
	}

        @Override
	public HasClickHandlers getSortByClassClickHandlers() {
		return sortByClassButton;
	}
        
        @Override
        public HasValue<Boolean> getOptionIncludeAllEleve() {
            return includeAllEleveCheckBox;
        }
	
        @Override
        public ListBox getSortGroupeOptions() {
            return sortListBox;
        }
        
	 static class BasicEleveWidget extends DockPanel implements EleveWidget {
		 	
                    private Image image;
                    private Label nameLabel;
                    private Label dateLabel;
                    private Label classeLabel;
                    private Eleve eleve;
                    private Button button;
                    private Button moreInfosButton;
		
		    public BasicEleveWidget(Eleve eleve) {
                        LayoutPanel imgsPanel = new LayoutPanel();
		    	image = new Image();
		    	image.setSize("64px", "64px");
		    	button = new Button();
		    	imgsPanel.add(image);
                        imgsPanel.add(button);
                        //imgsPanel.add(moreInfosButton);
                        imgsPanel.setWidgetLeftWidth(image, 0, Style.Unit.PX, 64, Style.Unit.PX);
                        imgsPanel.setWidgetTopHeight(image, 0, Style.Unit.PX, 64, Style.Unit.PX);
                        imgsPanel.setWidgetLeftWidth(button, 3, Style.Unit.PX, 18, Style.Unit.PX);
                        imgsPanel.setWidgetTopHeight(button, 3, Style.Unit.PX, 18, Style.Unit.PX);
                        imgsPanel.setWidth("64px");
		    	this.add(imgsPanel,DockPanel.WEST);
		    	VerticalPanel vp = new VerticalPanel();
		    	nameLabel = new Label();
                        nameLabel.getElement().getStyle().setFontWeight(Style.FontWeight.BOLD);
		    	vp.add(nameLabel);
		    	dateLabel = new Label();
		    	vp.add(dateLabel);
                        HorizontalPanel hz = new HorizontalPanel();
                        classeLabel = new Label();
                        hz.add(classeLabel);
                        moreInfosButton = new Button();
                        hz.add(moreInfosButton);
		    	vp.add(hz);
		    	this.add(vp,DockPanel.CENTER);
		    	setEleve(eleve);
		    	this.setWidth("160px");
                        this.addStyleName(ficheeps.client.Resources.INSTANCE.ficheepsStyle().eleveGroupViewBasicEleveWidget());
		    }
		    
		    public void setEleve(Eleve eleve) {
		    	this.eleve = eleve;
		    	image.setUrl("ficheeps/elevePhotoServlet?id=" + eleve.getId());
		    	nameLabel.setText(eleve.getNom().toUpperCase() + " " + eleve.getPrenom());
		    	dateLabel.setText(Utils.FormatDateDDSlashMMSlashYYYY(eleve.getDateDeNaissance()));
		    	classeLabel.setText(eleve.getClasse());
		    }
		    
                    @Override
		    public Eleve getEleve() {
		    	return eleve;
		    }
		    @Override
		    public Button getButton() {
		    	return button;
		    }
                    @Override
                    public Button getMoreInfosButton() {
		    	return moreInfosButton;
		    }
                    @Override
                    public void disable() {
                        this.button.setVisible(false);
                        this.getElement().addClassName(ficheeps.client.Resources.INSTANCE.ficheepsStyle().groupEleveWidgetDisable());
                    }
                    @Override
                    public void enable() {
                        this.button.setVisible(true);
                        this.getElement().removeClassName(ficheeps.client.Resources.INSTANCE.ficheepsStyle().groupEleveWidgetDisable());
                    }
		  }	
	
	 

	 public static class BasicGroupeBasicEditWidget extends DockPanel implements GroupeBasicEditWidget {
		 	
		 	private TextBox nameTextBox;
		 	private TextBox profnameTextBox;
		 	private TextArea descriptionTextArea;
		 	private RadioButton groupEPS;
		 	private RadioButton groupAS;
		
		    public BasicGroupeBasicEditWidget() {
		    	groupEPS = new RadioButton("groupsRadiogroup","EPS");
		    	groupAS = new RadioButton("groupsRadiogroup","A.S.");
		    	
		    	nameTextBox = new TextBox();
		    	descriptionTextArea = new TextArea();
		    	profnameTextBox = new TextBox();
		    	VerticalPanel vp = new VerticalPanel();
		    	FlowPanel fp = new FlowPanel();
		    	fp.add(new Label("Groupe: "));
		    	fp.add(groupEPS);
		    	fp.add(groupAS);
		    	vp.add(fp);
		    	fp = new FlowPanel();
		    	fp.add(new Label("Nom du groupe: "));
		    	fp.add(nameTextBox);
		    	vp.add(fp);
		    	fp = new FlowPanel();
		    	fp.add(new Label("Nom du professeur: "));
		    	fp.add(profnameTextBox);
		    	vp.add(fp);
		    	fp = new FlowPanel();
		    	fp.add(new Label("Description:"));
		    	fp.add(descriptionTextArea);
		    	vp.add(fp);
		    	this.add(vp,DockPanel.CENTER);
		    }
		    
			public TextBox getNom() {
				return nameTextBox;
			}
			public TextBox getNomProfesseur() {
				return profnameTextBox;
			}
			public TextArea getDescription(){
				return descriptionTextArea;
			}
			public HasValue<Boolean> getGroupEPSRadio() {
				return groupEPS;
			}
			public HasValue<Boolean> getGroupASRadio() {
				return groupAS;
			}
		    
		  }	
	 
	 
	

}

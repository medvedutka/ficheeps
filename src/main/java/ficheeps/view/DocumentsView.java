package ficheeps.view;

import org.moxieapps.gwt.uploader.client.Uploader;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;


public class DocumentsView extends Composite implements ficheeps.presenter.DocumentsPresenter.Display {
	
	
	interface MyUiBinder extends UiBinder<Widget, DocumentsView> {}
	private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

	@UiField
	Button deleteTemplateFinDeSeconde;
	
	@UiField
	Uploader uploadNewFicheFinDeSecondeTemplate;

	@UiField
	Label noTemplateFinDeSeconde;

	public DocumentsView() {
		initWidget(uiBinder.createAndBindUi(this));
		
		uploadNewFicheFinDeSecondeTemplate.setButtonText("<input type='button' value='Définir le modèle \"suggestion fin de seconde\" (choisir un fichier PDF)'></input>");
		uploadNewFicheFinDeSecondeTemplate.setButtonWidth(475).setButtonHeight(30);
	}
	
	@Override
	public Uploader getUploadNewFicheFinDeSecondeTemplate() {
		return uploadNewFicheFinDeSecondeTemplate;
	}
	
	@Override
	public Button getDeleteTemplateFinDeSecondeButton() {
		return deleteTemplateFinDeSeconde;
	}
	
	@Override
	public Label getNoTemplateFinDeSecondeLabel() {
		return noTemplateFinDeSeconde;
	}
	
}

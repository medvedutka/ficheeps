package ficheeps.view;


import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.ButtonBase;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import ficheeps.presenter.EleveEmailPresenter;


public class EleveEmailView extends Composite implements EleveEmailPresenter.Display {

        interface MyUiBinder extends UiBinder<Widget, EleveEmailView> {}
        private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

        @UiField TextBox mailEleveTextBox;
        @UiField PushButton mailEleveSaveButton;

        public EleveEmailView() {
            initWidget(uiBinder.createAndBindUi(this));
        }
	
        @Override
	public TextBox getMailEleveTextBox() {
            return mailEleveTextBox;
        }
        @Override
        public ButtonBase getMailEleveSaveButton() {
            return mailEleveSaveButton;
        }
		
}

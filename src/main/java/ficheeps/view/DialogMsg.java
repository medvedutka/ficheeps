package ficheeps.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import ficheeps.client.Resources;

public class DialogMsg extends DialogBox {

	
	
	public static void ShowYesNoCancel(String title, String msg, 
			final String yesLabel,
			final ClickHandler yes,
			final String noLabel,
			final ClickHandler no, 
			final String cancelLabel,
			final ClickHandler cancel) {
		final DialogMsg dialogBox = new DialogMsg();
		dialogBox.setText(title);
	    VerticalPanel dialogContents = new VerticalPanel();
	    dialogContents.setSpacing(4);
		dialogBox.setWidget(dialogContents);
		dialogContents.add(new HTML(msg));
	
	    Button cancelButton = null;
	    if(cancel != null) {
	    	cancelButton = new Button(cancelLabel==null?"Annuler":cancelLabel, new ClickHandler() {
		          public void onClick(ClickEvent event) {
		        	if(cancel != null) {
		        		cancel.onClick(event);
		        	}
		            dialogBox.hide();
		          }
		      });
	    }
	    Button noButton = new Button(yesLabel==null?"Oui":yesLabel , new ClickHandler() {
	          public void onClick(ClickEvent event) {
	        	if(yes != null) {
	        		yes.onClick(event);
	        	}
	            dialogBox.hide();
	          }
	        });
	    Button yesButton = new Button(noLabel==null?"Non":noLabel, new ClickHandler() {
	          public void onClick(ClickEvent event) {
	        	if(no != null) {
	        		no.onClick(event);
	        	}
	            dialogBox.hide();
	          }
	        });
	    FlowPanel buttonPanel = new FlowPanel();
	    buttonPanel.add(yesButton);
	    buttonPanel.add(noButton);
	    if(cancel != null) {
	    	buttonPanel.add(cancelButton);
	    }
	    
	    dialogContents.add(buttonPanel);
	    if (LocaleInfo.getCurrentLocale().isRTL()) {
	      dialogContents.setCellHorizontalAlignment(
	    		  buttonPanel, HasHorizontalAlignment.ALIGN_LEFT);
	    } else {
	      dialogContents.setCellHorizontalAlignment(
	    		  buttonPanel, HasHorizontalAlignment.ALIGN_RIGHT);
	    }
	    
	    dialogBox.setGlassEnabled(true);
	    dialogBox.setAnimationEnabled(true);
        dialogBox.center();
        dialogBox.show();			
	}
	
	
	public static void ShowOkCancel(String title, String msg, final ClickHandler ok, final ClickHandler cancel) {
		ShowOkCancel( title, new HTML(msg), ok, cancel);
	}

	
	public static void ShowOkCancel(String title, Widget content, final ClickHandler ok, final ClickHandler cancel) {
		final DialogMsg dialogBox = new DialogMsg();
		dialogBox.setText(title);
	    VerticalPanel dialogContents = new VerticalPanel();
	    dialogContents.setSpacing(4);
		dialogBox.setWidget(dialogContents);
		dialogContents.add(content);
		
		
	    Button cancelButton = new Button("Annuler", new ClickHandler() {
	          public void onClick(ClickEvent event) {
	        	if(cancel != null) {
	        		cancel.onClick(event);
	        	}
	            dialogBox.hide();
	          }
	        });
	    Button okButton = new Button("Continuer", new ClickHandler() {
	          public void onClick(ClickEvent event) {
	        	if(ok != null) {
	        		ok.onClick(event);
	        	}
	            dialogBox.hide();
	          }
	        });
	    
	    FlowPanel buttonPanel = new FlowPanel();
	    buttonPanel.add(okButton);
	    buttonPanel.add(cancelButton);
	    
	    dialogContents.add(buttonPanel);
	    if (LocaleInfo.getCurrentLocale().isRTL()) {
	      dialogContents.setCellHorizontalAlignment(
	    		  buttonPanel, HasHorizontalAlignment.ALIGN_LEFT);
	    } else {
	      dialogContents.setCellHorizontalAlignment(
	    		  buttonPanel, HasHorizontalAlignment.ALIGN_RIGHT);
	    }
	    
	    dialogBox.setGlassEnabled(true);
	    dialogBox.setAnimationEnabled(true);
        dialogBox.center();
        dialogBox.show();			
	}
	
	
	public static abstract class OkCancelValueCallback<T> {
		public abstract void onOk(T value);
		public void onCancel() {}
	}
	
	public static void ShowPasswordTextBox(String title, final OkCancelValueCallback<String> callback) {
		final PasswordTextBox pwdTextBox = new PasswordTextBox();
		ShowOkCancel(title,pwdTextBox,new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				callback.onOk(pwdTextBox.getText());
			}
		},new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				callback.onCancel();
			}});
	}
        
	public static void ShowTextBox(String title,String msg, final OkCancelValueCallback<String> callback) {
                final VerticalPanel vp = new VerticalPanel();
                if(msg != null) {
                    vp.add(new Label(msg));
                }
		final TextBox textBox = new TextBox();
                vp.add(textBox);
		ShowOkCancel(title,vp,new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				callback.onOk(textBox.getText());
			}
		},new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				callback.onCancel();
			}});
	}        
	
	
	
	public static void ShowMessage(String title, String msg) {
		ShowContent(title,new HTML(msg));
	}
        
        public static void ShowContent(String title,  Widget content) {
            ShowContent(title,content, null);
        }
	
	public static void ShowContent(String title,  Widget content, final ClickHandler closeHandler) {
            final DialogMsg dialogBox = new DialogMsg();
            dialogBox.setText(title);
            VerticalPanel dialogContents = new VerticalPanel();
            dialogContents.setSpacing(4);
            dialogBox.setWidget(dialogContents);
            dialogContents.add(content);

            Button cancelButton = new Button("Fermer", new ClickHandler() {
                public void onClick(ClickEvent event) {
                  dialogBox.hide();
                  if(closeHandler != null) {
                      closeHandler.onClick(event);
                  }
                }
            });

            FlowPanel buttonPanel = new FlowPanel();
            buttonPanel.add(cancelButton);

            dialogContents.add(buttonPanel);
            if (LocaleInfo.getCurrentLocale().isRTL()) {
                dialogContents.setCellHorizontalAlignment(
                buttonPanel, HasHorizontalAlignment.ALIGN_LEFT);
            } else {
                dialogContents.setCellHorizontalAlignment(
                buttonPanel, HasHorizontalAlignment.ALIGN_RIGHT);
            }

            dialogBox.setGlassEnabled(true);
            dialogBox.setAnimationEnabled(false);
            dialogBox.center();
            dialogBox.show();			
	}        
        
	
	public static DialogMsg ShowWaiter(String title,String msg) {
		return ShowWaiter(title,msg,null);
	}
	
	public static DialogMsg ShowWaiter(String title,String msg,final ClickHandler cancelAction) {
		final DialogMsg dialogBox = new DialogMsg();
		dialogBox.setText(title);
	    VerticalPanel dialogContents = new VerticalPanel();
	    dialogContents.setSpacing(4);
		dialogBox.setWidget(dialogContents);
		Resources resources = GWT.create(Resources.class);
		dialogContents.add(new Image(resources.wait01()));
		dialogContents.add(new HTML(msg));
		
	    Button cancelButton = new Button("Annuler", new ClickHandler() {
	          public void onClick(ClickEvent event) {
	        	if(cancelAction != null) {
	        		cancelAction.onClick(event);
	        	}
	            dialogBox.hide();
	          }
	        });
	    
	    FlowPanel buttonPanel = new FlowPanel();
	    buttonPanel.add(cancelButton);

	    
	    dialogContents.add(buttonPanel);
	    if (LocaleInfo.getCurrentLocale().isRTL()) {
	      dialogContents.setCellHorizontalAlignment(
	    		  buttonPanel, HasHorizontalAlignment.ALIGN_LEFT);
	    } else {
	      dialogContents.setCellHorizontalAlignment(
	    		  buttonPanel, HasHorizontalAlignment.ALIGN_RIGHT);
	    }
	    
	    if(cancelAction == null) {
	    	cancelButton.setVisible(false);
	    }
	    
	    dialogBox.setGlassEnabled(true);
	    dialogBox.setAnimationEnabled(true);
        dialogBox.center();
        dialogBox.show();	
        
        return dialogBox;
	}
}

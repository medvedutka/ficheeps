package ficheeps.view;


import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;
import ficheeps.presenter.ElevesListEmailsPresenter;


public class ElevesListEmailsView extends Composite implements ElevesListEmailsPresenter.Display {

        interface MyUiBinder extends UiBinder<Widget, ElevesListEmailsView> {}
        private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

        @UiField TabLayoutPanel tabLayoutPanel;
        @UiField TextArea maillingTextArea;
        @UiField TextArea listMailTextArea;
        
        
        public ElevesListEmailsView() {
            initWidget(uiBinder.createAndBindUi(this));
                
            maillingTextArea.getElement().setAttribute("spellCheck", "false");
            listMailTextArea.getElement().setAttribute("spellCheck", "false"); 
        }
	
	@Override
	public TabLayoutPanel getTabLayoutPanel() {
            return tabLayoutPanel;
	}
	
	@Override
	public TextArea getMaillingText() {
            return maillingTextArea;
	}
	@Override
	public TextArea getListMailText() {
            return listMailTextArea;
	}        
		
}

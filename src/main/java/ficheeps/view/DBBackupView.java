package ficheeps.view;

import org.moxieapps.gwt.uploader.client.Uploader;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import ficheeps.model.BackupFile;

public class DBBackupView extends Composite implements ficheeps.presenter.DBBackupPresenter.Display {
	
	
	interface MyUiBinder extends UiBinder<Widget, DBBackupView> {}
	private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

	@UiField
	Button createBackup;
	
	@UiField
	Uploader restoreBackupFromLocalFile;

	@UiField
	DataGrid<BackupFile> backupFileTable;

	public DBBackupView() {
		initWidget(uiBinder.createAndBindUi(this));
		
		//restoreBackupFromLocalFile.setButtonText("Restaurer une sauvegarde depuis un fichier");
		restoreBackupFromLocalFile.setButtonText("<input type='button' value='Restaurer une sauvegarde depuis un fichier'></input>");
		restoreBackupFromLocalFile.setButtonWidth(300).setButtonHeight(30);
	}
	
	@Override
	public Uploader getRestoreBackupFromLocalFile() {
		return restoreBackupFromLocalFile;
	}
	
	@Override
	public HasClickHandlers getCreateBackupClickHandler() {
		return createBackup;
	}

	@Override
	public DataGrid<BackupFile> getBackupList() {
		return backupFileTable;
	}
}

package ficheeps.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.cellview.client.AbstractCellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.ColumnSortList;
import com.google.gwt.user.cellview.client.ColumnSortList.ColumnSortInfo;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SelectionModel;

import ficheeps.model.Eleve;
import ficheeps.presenter.ListeElevesPresenter;
import ficheeps.presenter.ListeElevesPresenter.CSSStyle;

//@ShowcaseRaw({"ContactDatabase.java", "CwCellTable.ui.xml"})
public class ListeElevesView extends Composite implements ListeElevesPresenter.Display {

	interface MyUiBinder extends UiBinder<Widget, ListeElevesView> {
	}

	private static final MyUiBinder uiBinder = GWT.create(MyUiBinder.class);
	
	
	public ListeElevesView() {
		initWidget(uiBinder.createAndBindUi(this));

		// Set a key provider that provides a unique key for each contact. If
		// key is
		// used to identify contacts when fields (such as the name and address)
		// change.
		// cellTable = new CellTable<Eleve>(Eleve.KEY_PROVIDER);
		// cellTable.setWidth("100%", true);
		// cellTable.setWidth("100%");

		// Do not refresh the headers and footers every time the data is
		// updated.
		// cellTable.setAutoHeaderRefreshDisabled(true);
		// cellTable.setAutoFooterRefreshDisabled(true);

		// Attach a column sort handler to the ListDataProvider to sort the
		// list.
		// ListHandler<Eleve> sortHandler =null;// = new
		// ListHandler<Eleve>(getEleveManager().getDataProvider().getList());
		// cellTable.addColumnSortHandler(sortHandler);

		// Create a Pager to control the table.
		// SimplePager.Resources pagerResources =
		// GWT.create(SimplePager.Resources.class);
		// pager = new SimplePager(TextLocation.CENTER, pagerResources, false,
		// 0, true);
		pager.setDisplay(cellTable);

		// Add a selection model so we can select cells.
		final SelectionModel<Eleve> selectionModel = new MultiSelectionModel<Eleve>(Eleve.KEY_PROVIDER);
		cellTable.setSelectionModel(selectionModel, DefaultSelectionEventManager.<Eleve> createCheckboxManager());

		// Add the CellList to the adapter in the database.
		// getEleveManager().addDataDisplay(cellTable);

		filterTextbox.setTitle("Entrez ici un filtre");

		filterTextbox.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if ((event.getCharCode() == KeyCodes.KEY_ENTER)
						|| (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER)) {
					filterButton.click();
				}
			}
		});
	}

        @Override
	public CSSStyle getCSSStyle() {
		return style;
	}

	@UiField
	CSSStyle style;

	@UiField
	AbstractCellTable<Eleve> cellTable;

	@UiField
	SimplePager pager;

	@UiField
	Button filterButton;

	@UiField
	TextBox filterTextbox;

	@UiField
	Button newEleve;

	@UiField Image imageAjaxLoader;
	
        @UiField
        CheckBox includeAllEleveCheckBox;
        
        @Override
	public Widget getWidgetWaitForSearchIndicator() {
		return imageAjaxLoader;
	}
	
        @Override
	public String getFilter() {
		return filterTextbox.getText();
	}

        @Override
	public HasClickHandlers getRefreshRequest() {
		return filterButton;
	}

        @Override
	public Button getNewEleveClickHandler() {
		return newEleve;
	}

	@Override
	public AbstractCellTable<Eleve> getTable() {
		return cellTable;
	}

        @Override
        public HasValue<Boolean> getOptionIncludeAllEleve() {
            return includeAllEleveCheckBox;
        }
        
        @Override
	public HandlerRegistration addColumnSortHandler(ColumnSortEvent.Handler handler) {
		return cellTable.addColumnSortHandler(handler);
	}

        @Override
	public ColumnSortInfo getSortInfo() {
		ColumnSortList sortList = cellTable.getColumnSortList();
		if (sortList.size() > 0) {
			return sortList.get(0);
		}
		return null;
	}

        @Override
	public void addColumn(Column<Eleve, ?> column, String columnName) {
            cellTable.addColumn(column, columnName);
	}
        
        @Override
	public void addColumn(Column<Eleve, ?> column, String columnName,int value,Style.Unit unit) {
            cellTable.addColumn(column, columnName);
            if(value != -1) {
                cellTable.setColumnWidth(column, value,unit);
            }            
	}        

}
package ficheeps.view;

import gwtupload.client.SingleUploader;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.client.HasSafeHtml;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import ficheeps.model.Tuple;
import gwtupload.client.IUploader;

public class ClassAndGroupNameInfosImportView extends Composite implements ficheeps.presenter.ClassAndGroupNameInfosImportPresenter.Display {

    interface MyUiBinder extends UiBinder<Widget, ClassAndGroupNameInfosImportView> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    SingleUploader uploader;

    @UiField
    Button commitImport;

    @UiField
    HTML commitImportResult;

    @UiField
    SimplePanel uploadPanel;

    @UiField
    DataGrid<Tuple<String,String>> suggestTable;

    @UiField
    Widget panelResultImport; 

    
    public ClassAndGroupNameInfosImportView() {
        initWidget(uiBinder.createAndBindUi(this));

        uploader = new SingleUploader();
        uploader.setI18Constants((IUploader.UploaderConstants)GWT.create(MyUploaderConstants.class));
        uploader.setTitle("Importer le fichier Excel choisi.");
        uploadPanel.add(uploader);
    }    
    
    @Override
    public Widget getResultImportView() {
        return panelResultImport;
    }
    
    @Override
    public DataGrid<Tuple<String,String>> getSuggestTable() {
        return suggestTable;
    }

    @Override
    public Button getCommitImportClickHandler() {
        return commitImport;
    }

    @Override
    public SingleUploader getUploader() {
        return uploader;
    }

    @Override
    public HasSafeHtml getCommitImportResult() {
        return commitImportResult;
    }

    
    public static interface MyUploaderConstants extends IUploader.UploaderConstants {
        @DefaultStringValue("Envoyer")
        @Override
        String uploaderSend();
    }
}

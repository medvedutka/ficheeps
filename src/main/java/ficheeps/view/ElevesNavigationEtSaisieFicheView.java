package ficheeps.view;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeUri;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import ficheeps.client.AppController;

import ficheeps.client.Resources;
import ficheeps.client.Utils;
import ficheeps.model.Eleve;
import ficheeps.model.Groupe;
import ficheeps.presenter.ElevesNavigationEtSaisieFichePresenter;
import ficheeps.view.utils.TooltipCellDecorator;

public class ElevesNavigationEtSaisieFicheView extends Composite implements
		ElevesNavigationEtSaisieFichePresenter.Display {

	interface MyUiBinder extends UiBinder<Widget, ElevesNavigationEtSaisieFicheView> {
	}

	private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

	@UiField
	SplitLayoutPanel splitPanel;

	DockLayoutPanel fichePanel = new DockLayoutPanel(Unit.EM);

        @Override
	public HasWidgets getFichePanel() {
		return fichePanel;
	}

	private CellList<String> classesCellList;
        @Override
	public CellList<String> getClassesCellList() {
		return classesCellList;
	}
        
        private TabLayoutPanel groupeTabPanel;
	private CellList<Groupe> groupesEPSCellList;
        private CellList<Groupe> groupesASCellList;
        
        @Override
	public CellList<Groupe> getGroupesEPSCellList() {
		return groupesEPSCellList;
	}
        @Override
	public CellList<Groupe> getGroupesASCellList() {
		return groupesASCellList;
	}

	
	private CellList<Eleve> elevesCellList;
        @Override
	public CellList<Eleve> getElevesCellList() {
		return elevesCellList;
	}
	
	private Button downLoadFichesButton;
        private Button listEmailsButton;
	private PushButton saveEleve;
	private PopupPanel popupSave;
	

	  @Override
	  public void showSave() {
		  if(! popupSave.isShowing()) {
			  popupSave.show();
			  popupSave.getElement().getStyle().setProperty("left", "auto"); // hack: must be set after 'show' to work
		  }
	  }
	  
	  @Override
	  public void hideSave() {
		  if(popupSave.isShowing()) {
			  popupSave.hide();
		  }
	  }
	  
	  @Override
	  public HasClickHandlers getSaveAction() {
		  return saveEleve;
	  }
	  
	  @Override
	  public HasClickHandlers getDownloadFiches()
	  {
		  return downLoadFichesButton;
	  }
          
          @Override
	  public HasClickHandlers getListEmailsButton()
	  {
		  return listEmailsButton;
	  }
          
          
        @Override  
        public void bringGroupesEPSCellListFront() {
            groupeTabPanel.selectTab(0);
        }
        
        @Override
        public void bringGroupesASCellListFront() {
            groupeTabPanel.selectTab(1);
        }
          
	public ElevesNavigationEtSaisieFicheView() {
            initWidget(uiBinder.createAndBindUi(this));
		
	    Resources resources = GWT.create(Resources.class);
	    Image img = new Image(resources.save_64x64());
	    saveEleve = new PushButton(img);
	    saveEleve.setTitle("Enregistrer les modifications");
	    popupSave = new PopupPanel(false);
	    popupSave.add(saveEleve);
	    popupSave.getElement().getStyle().setRight(0, Unit.EM);
		
            TooltipCellDecorator.TooltipProvider<String> tooltipProviderString = new TooltipCellDecorator.TooltipProvider<String>() {
                @Override
                public String tooltipFor(String value) {
                    if(AppController.SETTINGS != null  && AppController.SETTINGS.getInfosClassesGroupes() != null) {
                        if(AppController.SETTINGS.getInfosClassesGroupes().containsKey(value)) {
                            return value + ": " + AppController.SETTINGS.getInfosClassesGroupes().get(value);
                        }
                    }
                    return value;
                }
            };

            TooltipCellDecorator.TooltipProvider<Groupe> tooltipProviderGroup = new TooltipCellDecorator.TooltipProvider<Groupe>() {
                @Override
                public String tooltipFor(Groupe groupe) {
                    if(AppController.SETTINGS != null  && AppController.SETTINGS.getInfosClassesGroupes() != null) {
                        String value = groupe.getNom();
                        if(AppController.SETTINGS.getInfosClassesGroupes().containsKey(value)) {
                            return value + ": " + AppController.SETTINGS.getInfosClassesGroupes().get(value);
                        }
                        return "";
                    }
                    return "";
                }
            };
            
            classesCellList = new CellList<String>(new TooltipCellDecorator(new TextCell(), tooltipProviderString),(com.google.gwt.user.cellview.client.CellList.Resources) GWT.create(CustomCellListResources.class));
            groupesEPSCellList = new CellList<Groupe>(new TooltipCellDecorator(new GroupeCell(), tooltipProviderGroup),(com.google.gwt.user.cellview.client.CellList.Resources) GWT.create(CustomCellListResources.class));
            groupesASCellList = new CellList<Groupe>(new TooltipCellDecorator(new GroupeCell(), tooltipProviderGroup),(com.google.gwt.user.cellview.client.CellList.Resources) GWT.create(CustomCellListResources.class));
            elevesCellList = new CellList<Eleve>(new EleveCell(),(com.google.gwt.user.cellview.client.CellList.Resources) GWT.create(CustomCellListResources.class));

            classesCellList.setPageSize(Integer.MAX_VALUE);
            groupesEPSCellList.setPageSize(Integer.MAX_VALUE);
            groupesASCellList.setPageSize(Integer.MAX_VALUE);
            elevesCellList.setPageSize(Integer.MAX_VALUE);

            groupesEPSCellList.setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.DISABLED);
            groupesASCellList.setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.DISABLED);
            classesCellList.setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.DISABLED);
            elevesCellList.setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.DISABLED);

            DockLayoutPanel dpClasses = new DockLayoutPanel(Unit.PX);
            VerticalPanel vpClasses = new VerticalPanel();
            //Label classesLabel = new Label("Classes:");
            //classesLabel.getElement().getStyle().setFontWeight(FontWeight.BOLD);
            //fichesClasseButton = new Button("Fiches");
            //fichesClasseButton.setTitle("Export PDF des fiches");
            //fichesClasseButton.setWidth("100%");
            vpClasses.add(classesCellList);
            vpClasses.setWidth("100%");
            //dpClasses.addNorth(classesLabel, 24);
            //dpClasses.addSouth(fichesClasseButton,24);
            dpClasses.add(new ScrollPanel(vpClasses));

            groupeTabPanel = new TabLayoutPanel(2.5, Unit.EM);
            groupeTabPanel.setHeight("100%");
            groupeTabPanel.setWidth("100%");

            DockLayoutPanel dpGroupes = new DockLayoutPanel(Unit.PX);
            //Label groupesLabel = new Label("Groupes:");
            //groupesLabel.getElement().getStyle().setFontWeight(FontWeight.BOLD);
            //fichesGroupeButton = new Button("Fiches");
            //fichesGroupeButton.setTitle("Export PDF des fiches");
            //fichesGroupeButton.setWidth("100%");
            //dpGroupes.addNorth(groupesLabel, 24);
            //dpGroupes.addSouth(fichesGroupeButton,24);

            ScrollPanel spEPS = new ScrollPanel(groupesEPSCellList);
            groupesEPSCellList.getElement().setAttribute("jcdebug", "groupesEPSCellList");
            //spEPS.setHeight("100%");

            groupeTabPanel.add(spEPS, "EPS");
            groupeTabPanel.add(new ScrollPanel(groupesASCellList), "A.S.");
            groupeTabPanel.getTabWidget(0).setTitle("Groupes EPS");
            groupeTabPanel.getTabWidget(1).setTitle("Groupes de l'Association Sportive");

            HorizontalPanel hpButtons = new HorizontalPanel();
            hpButtons.getElement().getStyle().setPadding(0, Unit.PX);
            downLoadFichesButton = new Button("");
            downLoadFichesButton.setTitle("Export PDF des fiches");
            downLoadFichesButton.addStyleName(ficheeps.client.Resources.INSTANCE.ficheepsStyle().eleveNavigationDownloadfichePDF24());
            listEmailsButton = new Button("");
            listEmailsButton.setTitle("Liste des emails");
            listEmailsButton.addStyleName(ficheeps.client.Resources.INSTANCE.ficheepsStyle().eleveNavigationEmailEleve24());
            listEmailsButton.getElement().getStyle().setMarginLeft(5, Unit.PX);
            hpButtons.add(downLoadFichesButton);
            hpButtons.add(listEmailsButton);
            dpGroupes.addSouth(hpButtons, 28);

            //groupeTabPanel.getElement().getStyle().setMarginBottom(60, Unit.PX); // keep space for hpButtons

            dpGroupes.add(groupeTabPanel);
            dpGroupes.setHeight("100%");

            SplitLayoutPanel splitPanelGroupesClasses = new SplitLayoutPanel();
            splitPanelGroupesClasses.addSouth(dpGroupes,250.0);
            splitPanelGroupesClasses.add(dpClasses);

            splitPanel.addWest(splitPanelGroupesClasses, 113);
            splitPanel.addWest(new ScrollPanel(elevesCellList), 230);

            fichePanel.setWidth("100%");
            fichePanel.setHeight("100%");
            splitPanel.add(fichePanel);
	}

	
	
	interface CustomCellListResources extends CellList.Resources {
		  @Source("CustomCellListResources.css")		  CellList.Style cellListStyle();
	}


	
	
	 static class EleveCell extends AbstractCell<Eleve> {
		 	
		 	private String healthIconURi = "";
		    
		 	public EleveCell() {
		 		Resources resources = GWT.create(Resources.class);
		 		SafeUri uri = resources.health_16x16().getSafeUri();
		 		healthIconURi = uri.asString();
		    }

		    @Override
		    public void render(Context context, Eleve eleve, SafeHtmlBuilder sb) {
		      // Value can be null, so do a null check..
		      if (eleve == null) {
		        return;
		      }

		      if(!eleve.isSuiviMedicalEmpty()) {
		    	  sb.appendHtmlConstant("<img title='Suivi médical !' src='"  + healthIconURi + "' style='position:absolute;right:0;'></img>");
		      }
		      
		      sb.appendHtmlConstant("<table>");
		      // Add the contact image.
		      sb.appendHtmlConstant("<tr><td rowspan='3'>");
		      //Image image = new Image("ficheeps/elevePhotoServlet?id=" + eleve.getId());
		      //sb.appendHtmlConstant(AbstractImagePrototype.create(image.get).getHTML());
		      sb.appendHtmlConstant("<img width='64px' height='64px' src='" + "ficheeps/elevePhotoServlet?id=" + eleve.getId() + "' />");
		      sb.appendHtmlConstant("</td>");

		      // Add the name and address.
		      sb.appendHtmlConstant("<td>");
		      sb.appendEscaped(eleve.getNom().toUpperCase() + " " + eleve.getPrenom());
		      //sb.appendHtmlConstant("</td></tr><tr><td>");
		      sb.appendHtmlConstant("<br />");
		      sb.appendEscaped(Utils.FormatDateDDSlashMMSlashYYYY(eleve.getDateDeNaissance()) + " " + Eleve.GetAge(eleve.getDateDeNaissance()) + " ans");
		      sb.appendHtmlConstant("</td></tr></table>");
		    }
		  }	
	 
	 
	 
	 private static class GroupeCell extends AbstractCell<Groupe> {
		 	
		    public GroupeCell() {
		    }

		    @Override
		    public void render(Context context, Groupe groupe, SafeHtmlBuilder sb) {
		      if (groupe == null) {
		        return;
		      }
		      sb.appendHtmlConstant("<table>");
		      sb.appendHtmlConstant("<tr><td>");
		      sb.appendEscaped(groupe.getNom());
		      sb.appendHtmlConstant("</td></tr>");
                      String desc = groupe.getDescription();
                      if(desc != null && !desc.isEmpty()) {
                        sb.appendHtmlConstant("<tr><td style='font-size:smaller;font-style:italic;'>");
                        sb.appendEscaped(groupe.getDescription());
                        sb.appendHtmlConstant("</td></tr>");
                      }
                      String nomProfesseur = groupe.getNomProfesseur();
                      if(nomProfesseur != null && !nomProfesseur.isEmpty()) {
                        sb.appendHtmlConstant("<tr><td><i>");
                        sb.appendEscaped(groupe.getNomProfesseur());
                        sb.appendHtmlConstant("</i></td></tr>");	
                      }
		      sb.appendHtmlConstant("</table>");
		    }
		  }
}

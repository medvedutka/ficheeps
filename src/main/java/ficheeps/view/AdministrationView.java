package ficheeps.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;

import ficheeps.presenter.AdministrationPresenter;

public class AdministrationView extends Composite implements AdministrationPresenter.Display {

    interface MyUiBinder extends UiBinder<Widget, AdministrationView> {
    }
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    public AdministrationView() {
        initWidget(uiBinder.createAndBindUi(this));
    }

    @UiField
    Button createDummyDB;

    @UiField
    Button createSnapshotDB;
    
    @UiField
    Button dropDB;

    @UiField
    Button googleAuthentication;

    @UiField
    Button massGroupImport;

    @UiField
    Button tryLogin;

    @UiField
    Button logout;

    @UiField
    SimplePanel usersPanel;

    @UiField
    SimplePanel backupPanel;
    
    @UiField
    SimplePanel apsasPanel;

    @UiField
    Panel debugPanel;

    @UiField
    SimplePanel documentsPanel;

    @UiField
    Button dateRA12;

    @UiField
    TextArea updateIDTextArea;

    @UiField
    Button updateIDButton;

    @UiField
    Button buttonStatistiques;

    @UiField
    Button buttonMailsImport;
    
    @UiField
    ClassAndGroupNameInfosView classAndGroupNameInfosViewPanel;
    
    @UiField
    Button downloadPDFAll;

    @UiField
    Button deleteAllUser;
        
    @Override
    public Panel getAPSAPanel() {
        return apsasPanel;
    }
    
    @Override
    public Panel getDocumentsPanel() {
        return documentsPanel;
    }

    @Override
    public Panel getDebugPanel() {
        return debugPanel;
    }

    @Override
    public Panel getUsersPanel() {
        return usersPanel;
    }

    @Override
    public Panel getBackupPanel() {
        return backupPanel;
    }

    @Override
    public HasClickHandlers getCreateDummyDB() {
        return createDummyDB;
    }
    
    @Override
    public HasClickHandlers getCreateSnapshotDB() {
        return createSnapshotDB;
    }

    @Override
    public HasClickHandlers getDropDB() {
        return dropDB;
    }

    @Override
    public HasClickHandlers getGoogleAuthentication() {
        return googleAuthentication;
    }

    @Override
    public HasClickHandlers getTryLogin() {
        return tryLogin;
    }

    @Override
    public HasClickHandlers getLogout() {
        return logout;
    }

    @Override
    public HasClickHandlers getMassGroupImport() {
        return massGroupImport;
    }

    @Override
    public HasText getupdateIDText() {
        return updateIDTextArea;
    }

    @Override
    public HasClickHandlers getupdateIDButton() {
        return updateIDButton;
    }

    @Override
    public HasClickHandlers getDateRA12() {
        return dateRA12;
    }

    @Override 
    public HasClickHandlers getStatisticsDownloader() {
        return buttonStatistiques;
    }
    
    @Override 
    public HasClickHandlers getButtonMailsImport() {
        return buttonMailsImport;
    }
    
    @Override
    public ClassAndGroupNameInfosView getClassAndGroupNameInfosViewPanel() {
        return classAndGroupNameInfosViewPanel;
    }
    
    @Override 
    public HasClickHandlers getDownloadPDFAll() {
        return downloadPDFAll;
    }
    
    @Override 
    public HasClickHandlers getDeleteAllUser() {
        return deleteAllUser;
    }    
   ;
}

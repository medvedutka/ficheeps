package ficheeps.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.ButtonBase;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.TextBoxBase;
import com.google.gwt.user.client.ui.Widget;

import ficheeps.presenter.ListesLibreManagementPresenter;

public class ListesLibreManagementView extends Composite implements ListesLibreManagementPresenter.Display {

    interface MyUiBinder extends UiBinder<Widget, ListesLibreManagementView> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    public ListesLibreManagementView() {
        ((CustomDataGridResources)GWT.create(CustomDataGridResources.class)).dataGridStyle().ensureInjected();
        
        dataGridItems = new DataGrid(Integer.MAX_VALUE, (CustomDataGridResources)GWT.create(CustomDataGridResources.class));
        initWidget(uiBinder.createAndBindUi(this));
        textBoxListeLibreName.getElement().setAttribute("placeholder", "Nom de la liste");
        textBoxListeLibreDescription.getElement().setAttribute("placeholder", "Description ...");
        textBoxListeLibreDescription.getElement().getStyle().setProperty("resize","none");
    }

    @UiField
    ListBox listBoxListesLibre;
    @UiField
    TextBox textBoxListeLibreName;
    @UiField
    TextBoxBase textBoxListeLibreDescription;
    @UiField
    ElevesSearchView eleveSearchView;
    @UiField(provided = true)
    DataGrid dataGridItems;
    @UiField
    ButtonBase createButton;
    @UiField
    ButtonBase deleteButton;
    @UiField
    Label nbItems;
    @UiField
    ButtonBase buttonExportXLS;
    @UiField
    ButtonBase buttonExportEmails;
    
    @Override
    public ListBox getListBoxListesLibre() {
        return listBoxListesLibre;
    }
    @Override
    public TextBox getTextBoxListeLibreName() {
        return textBoxListeLibreName;
    }
    @Override
    public TextBoxBase getTextBoxListeLibreDescription() {
        return textBoxListeLibreDescription;
    }
    @Override
    public ElevesSearchView getEleveSearchView() {
        return eleveSearchView;
    }
    @Override
    public DataGrid getDataGridItems() {
        return dataGridItems;
    }
 
    
    @Override
    public ButtonBase getCreateListLibreButton() {
        return createButton;
    }
    @Override
    public ButtonBase getDeleteListLibreButton() {
        return deleteButton;
    }
    
    @Override
    public HasText getNbItemsText() {
        return nbItems;
    }
    
    @Override 
    public ButtonBase getButtonExportXLS() {
        return buttonExportXLS;
    }
    
    @Override 
    public ButtonBase getButtonExportEmails() {
        return buttonExportEmails;
    }
    
    
    
    public interface CustomDataGridResources extends DataGrid.Resources {

        interface CustomDataGridStyle extends DataGrid.Style {
        }

        @Override
        @Source({ DataGrid.Style.DEFAULT_CSS, "CustomDataGrid.css" })
        CustomDataGridStyle dataGridStyle();

    }
}

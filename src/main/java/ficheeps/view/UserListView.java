package ficheeps.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import ficheeps.model.User;

public class UserListView extends Composite implements ficheeps.presenter.UserListPresenter.Display {

	  interface MyUiBinder extends UiBinder<Widget, UserListView> {}
	  private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

	
	public UserListView() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@UiField
	DataGrid<User> localUserTable;
	
	@UiField
	DataGrid<User> openIDUserTable;

	@UiField
	Button newOpenIDUser;	

	@UiField
	Button newLocalUser;	
	
	public DataGrid<User> getLocalUserTable() {
		return localUserTable;
	}
	public DataGrid<User> getOpenIDUserTable() {
		return openIDUserTable;	
	}	
	
	public HasClickHandlers getNewOpenIdUser() {
		return newOpenIDUser;
	}
	
	public HasClickHandlers getNewLocalUser() {
		return newLocalUser;
	}
	
	
}

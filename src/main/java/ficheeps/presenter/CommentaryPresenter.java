package ficheeps.presenter;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;

import ficheeps.model.Commentaire;
import ficheeps.model.Apsa;
import ficheeps.model.Commentaire.Niveau;

public class CommentaryPresenter implements HasValueChangeHandlers<Commentaire> {

	public interface ApsaWizard {
		void execute(Commentaire.Niveau niveau, String annee, CommentaryPresenter commentairePresenter);
	}
	
	
	public interface Display {
		ListBox getNiveau();
		HasValue<String> getAnneeScolaire();
		HasValue<String> getProfesseur();
		HasValue<String> getAppreciations();
		HasValue<String> getAutre();
		HasValue<java.lang.Boolean> getAssociationSportive();
		Label getMoyenneApsa();
		
		HasClickHandlers newApsaDisplay();		
		ApsaDisplay createApsaDisplay();
		HasClickHandlers getWizardApsaClickHandler();
	}
	
	public interface ApsaDisplay {
		HasValue<String> getActivite();
		HasValue<String> getResultat();
	}
	
	
	private Commentaire commentary;
	
	public CommentaryPresenter(Commentaire commentaire) {
		commentary = commentaire;
	}
	
	
	private ApsaWizard apsaWizard;
	public void setApsaWizard(ApsaWizard wizard) {
		this.apsaWizard = wizard;
	}
	
	
	private Display display;
	public void bind(final Display display) {
		this.display = display;
		int index = 0;
		int selectedIndex = 0;
		for(Commentaire.Niveau n : Commentaire.Niveau.values()) {
			display.getNiveau().addItem(n.toString());
			if(n.equals(commentary.getNiveau())) {
				selectedIndex = index;
			}
			index++;
		}
		display.getNiveau().setSelectedIndex(selectedIndex);

		display.getAnneeScolaire().setValue(commentary.getAnneeScolaire());
		
		display.getProfesseur().setValue(commentary.getProfesseur());
		
		display.getAppreciations().setValue(commentary.getAppreciation());
		
		display.getAutre().setValue(commentary.getAutre());
		
		display.getAssociationSportive().setValue(commentary.isAssociationSportive());
		
		if(commentary.getApsas() != null && !commentary.getApsas().isEmpty()) {
			for(Apsa apsa : commentary.getApsas()) {
				showApsa(display,apsa);
			}
		}
		
		display.newApsaDisplay().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				newApsa("");
			}
		});
		
		display.getWizardApsaClickHandler().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				wizard();
			}
		});
		
		attachHandlers(display);
		
		updateDisplayMoyeneApsa();
	}
	
	
	private void updateDisplayMoyeneApsa() {
		String moyeneApsa = commentary.getMoyenne();
		if(moyeneApsa != null && !moyeneApsa.isEmpty()) {
			display.getMoyenneApsa().setText("(Moyenne: " +  moyeneApsa + ")");
			if(moyeneApsa.endsWith("!?")) {
				display.getMoyenneApsa().setTitle("Moyenne calculé avec des erreurs.");
			}
			else {
				display.getMoyenneApsa().setTitle("");
			}
		}
		else {
			display.getMoyenneApsa().setText("");
			display.getMoyenneApsa().setTitle("");
		}
	}
	
	private void wizard() {
		if(apsaWizard != null && commentary != null) {
			apsaWizard.execute(commentary.getNiveau(), commentary.getAnneeScolaire(),this);
		}
	}
	
	private void attachHandlers(final Display display) {
		display.getAnneeScolaire().addValueChangeHandler(new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				commentary.setAnneeScolaire(event.getValue());
				fireChange();
			}
		});
		display.getAppreciations().addValueChangeHandler(new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				commentary.setAppreciation(event.getValue());
				fireChange();
			}
		});	
		display.getAssociationSportive().addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				commentary.setAssociationSportive(event.getValue());
				fireChange();
			}
		});	
		display.getAutre().addValueChangeHandler(new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				commentary.setAutre(event.getValue());
				fireChange();
			}
		});
		display.getProfesseur().addValueChangeHandler(new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				commentary.setProfesseur(event.getValue());
				fireChange();
			}
		});
		display.getNiveau().addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				int index = display.getNiveau().getSelectedIndex();
				String niveauStr = display.getNiveau().getItemText(index);
				Niveau niveau = Niveau.valueOf(niveauStr);
				commentary.setNiveau(niveau);
				fireChange();
			}
		});
	}
	
	
	public void newApsa(String description) {
		Apsa apsa = new Apsa();
		apsa.setActivite(description);
		commentary.addApsa(apsa);
		showApsa(display,apsa);
		if(description != null && !description.isEmpty()) {
			fireChange();
		}
	}
	
	private void showApsa(Display display, final Apsa apsa) {
		ApsaDisplay ad = display.createApsaDisplay();
		ad.getActivite().setValue(apsa.getActivite());
		ad.getResultat().setValue(apsa.getResultat());	
		
		ad.getActivite().addValueChangeHandler(new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				apsa.setActivite(event.getValue());
				fireChange();
			}
		});
		ad.getResultat().addValueChangeHandler(new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				apsa.setResultat(event.getValue());
				fireChange();
				updateDisplayMoyeneApsa();
			}
		});
	}

	
	
	private void fireChange() {
		ValueChangeEvent.fire(this, commentary);
	}
	
	EventBus bus = new SimpleEventBus();
	
	@Override
	public HandlerRegistration addValueChangeHandler(ValueChangeHandler<Commentaire> handler) {
		return bus.addHandler(ValueChangeEvent.getType(), handler);
	}

	@Override
	public void fireEvent(GwtEvent<?> event) {
		bus.fireEvent(event);
	}
	
	

}

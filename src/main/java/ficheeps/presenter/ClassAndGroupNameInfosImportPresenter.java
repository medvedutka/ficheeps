package ficheeps.presenter;

import java.util.LinkedList;
import java.util.List;

import ficheeps.client.ErrorReportEvent;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.view.DialogMsg;
import gwtupload.client.IUploader;
import gwtupload.client.SingleUploader;
import gwtupload.client.IUploadStatus.Status;
import gwtupload.client.IUploader.UploadedInfo;

import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.client.HasSafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import ficheeps.model.Tuple;
import java.util.Comparator;

public class ClassAndGroupNameInfosImportPresenter {

    private FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);

    private ListDataProvider<Tuple<String,String>> dataProviderInfos = new ListDataProvider<Tuple<String,String>>();

    public interface Display {
        Button getCommitImportClickHandler();
        SingleUploader getUploader();
        HasSafeHtml getCommitImportResult();
        DataGrid<Tuple<String,String>> getSuggestTable();
        Widget getResultImportView();
    }

    public ClassAndGroupNameInfosImportPresenter() {
    }

    private Display display;

    public void bind(Display display) {
        this.display = display;

        cleanupAll();

        display.getUploader().addOnFinishUploadHandler(onFinishUploaderHandler);
        display.getUploader().addOnStartUploadHandler(new IUploader.OnStartUploaderHandler() {
            @Override
            public void onStart(IUploader uploader) {
                cleanupAll();
            }
        });

        display.getCommitImportClickHandler().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                commitImport();
            }
        });

        
        ColumnSortEvent.ListHandler<Tuple<String,String>> columnSortHandler = new ColumnSortEvent.ListHandler<Tuple<String,String>>(dataProviderInfos.getList());
        {
            ActionCell<Tuple<String,String>> actionCell = new ActionCell<Tuple<String,String>>("", new ActionCell.Delegate<Tuple<String,String>>() {
                @Override
                public void execute(final Tuple<String,String> eleve) {
                    dataProviderInfos.getList().remove(eleve);
                    dataProviderInfos.refresh();
                }
            });

            Column<Tuple<String,String>, Tuple<String,String>> c = new Column<Tuple<String,String>, Tuple<String,String>>(actionCell) {
                @Override
                public Tuple<String,String> getValue(Tuple<String,String> object) {
                    return object;
                }
            };
            c.setCellStyleNames(ficheeps.client.Resources.INSTANCE.ficheepsStyle().delete16());
            display.getSuggestTable().addColumn(c, "");
            display.getSuggestTable().setColumnWidth(c, 30, Style.Unit.PX);
        }        
        {
            TextColumn<Tuple<String,String>> c = new TextColumn<Tuple<String,String>>() {
                @Override
                public String getValue(Tuple<String,String> object) {
                    return object.getFirst();
                }
            };
            c.setSortable(true);
            columnSortHandler.setComparator(c,new Comparator<Tuple<String,String>>() {
                @Override
                public int compare(Tuple<String,String> o1, Tuple<String,String> o2) {
                    if (o1 == o2) {return 0;}
                    if (o1 != null) {
                      return (o2 != null) ? o1.getFirst().compareTo(o2.getFirst()) : 1;
                    }
                    return -1;
                }
            });
            display.getSuggestTable().addColumn(c, "Nom");
            display.getSuggestTable().setColumnWidth(c, 8, Style.Unit.EM);
        }
        {
            TextColumn<Tuple<String,String>> c = new TextColumn<Tuple<String,String>>() {
                @Override
                public String getValue(Tuple<String,String> object) {
                    return object.getSecond();
                }
            };
            c.setSortable(true);
            columnSortHandler.setComparator(c,new Comparator<Tuple<String,String>>() {
                @Override
                public int compare(Tuple<String,String> o1, Tuple<String,String> o2) {
                    if (o1 == o2) {return 0;}
                    if (o1 != null) {
                      return (o2 != null) ? o1.getSecond().compareTo(o2.getSecond()) : 1;
                    }
                    return -1;
                }
            });            
            display.getSuggestTable().addColumn(c, "Information");
        }         
        
        display.getSuggestTable().addColumnSortHandler(columnSortHandler);
        dataProviderInfos.addDataDisplay(display.getSuggestTable());
    }

    private IUploader.OnFinishUploaderHandler onFinishUploaderHandler = new IUploader.OnFinishUploaderHandler() {
        @Override
        public void onFinish(IUploader uploader) {
            if (uploader.getStatus() == Status.SUCCESS) {
                    // new PreloadedImage(uploader.fileUrl(), showImage);
                // The server sends useful information to the client by default
                UploadedInfo info = uploader.getServerInfo();
                /*System.out.println("File name " + info.name);
                 System.out.println("File content-type " + info.ctype);
                 System.out.println("File size " + info.size);
                 // You can send any customized message and parse it
                 System.out.println("Server message " + info.message);*/
                String uploadToken = uploader.getServerMessage().getMessage();
                tryImport(uploadToken);
            }
        }
    };

    private void cleanupAll() {
        dataProviderInfos.getList().clear();
        dataProviderInfos.refresh();
        display.getCommitImportResult().setHTML(new SafeHtmlBuilder().toSafeHtml());
        display.getResultImportView().setVisible(false);
    }

    private void tryImport(final String uploadToken) {
        final DialogMsg wd = DialogMsg.ShowWaiter("Test d'import", "Merci de patienter");
        ficheEPSService.tryInfosClassesGroupesImport(uploadToken, new AsyncCallback<List<Tuple<String,String>>>() {
            @Override
            public void onFailure(Throwable caught) {
                ErrorReportEvent.Fire("Impossible de tester l'import: '" + caught.getMessage() + "'.");
                wd.hide();
            }

            @Override
            public void onSuccess(List<Tuple<String,String>> result) {
                setSuggestedImport(result);
                wd.hide();
            }
        });
    }

    private void setSuggestedImport(List<Tuple<String,String>> list) {
        dataProviderInfos.getList().clear();
        dataProviderInfos.getList().addAll(list);

        display.getSuggestTable().setHeight("190px");
        
        display.getResultImportView().setVisible(true);
    }


    private void commitImport() {
        final DialogMsg wd = DialogMsg.ShowWaiter("Import des informations", "Veuillez patienter");
        List<Tuple<String,String>> list = new LinkedList<Tuple<String,String>>();
        list.addAll(dataProviderInfos.getList());

        ficheEPSService.commitInfosClassesGroupesImport(list, new AsyncCallback<String>() {
            @Override
            public void onFailure(Throwable caught) {
                ErrorReportEvent.Fire("Erreur lors de l'import: '" + caught.getMessage() + "'.");
                wd.hide();
            }

            @Override
            public void onSuccess(String result) {
                if(result == null) {
                    result = "";
                }
                SafeHtmlBuilder sb = new SafeHtmlBuilder();
                sb.appendEscapedLines(result);
                display.getCommitImportResult().setHTML(sb.toSafeHtml());
                wd.hide();
            }
        });
    }

}

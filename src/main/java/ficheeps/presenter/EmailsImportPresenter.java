package ficheeps.presenter;

import com.google.gwt.cell.client.AbstractCell;
import java.util.LinkedList;
import java.util.List;

import ficheeps.client.ErrorReportEvent;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.view.DialogMsg;
import gwtupload.client.IUploader;
import gwtupload.client.SingleUploader;
import gwtupload.client.IUploadStatus.Status;
import gwtupload.client.IUploader.UploadedInfo;

import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.safehtml.client.HasSafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import ficheeps.model.EmailsDiff;
import ficheeps.model.EmailsDiffImportResult;
import ficheeps.model.EmailsImportOptions;
import java.util.Comparator;

public class EmailsImportPresenter {

    private FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);

    private ListDataProvider<EmailsDiff> dataProviderEmailsDiff = new ListDataProvider<EmailsDiff>();
    private EmailsImportOptions currentImportOptions = null;

    public interface Display {
        Button getCommitImportClickHandler();
        SingleUploader getUploader();
        HasSafeHtml getCommitImportResult();
        DataGrid<EmailsDiff> getSuggestTable();
        Widget getResultImportView();
        HasValue<Boolean> getAppendModeSelector();
        HasValue<Boolean> getReplaceModeSelector();
    }

    public EmailsImportPresenter() {
    }

    private Display display;

    public void bind(Display display) {
        this.display = display;

        cleanupAll();

        display.getUploader().addOnFinishUploadHandler(onFinishUploaderHandler);
        display.getUploader().addOnStartUploadHandler(new IUploader.OnStartUploaderHandler() {
            @Override
            public void onStart(IUploader uploader) {
                cleanupAll();
            }
        });

        display.getCommitImportClickHandler().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                commitImport();
            }
        });

        
        ColumnSortEvent.ListHandler<EmailsDiff> columnSortHandler = new ColumnSortEvent.ListHandler<EmailsDiff>(dataProviderEmailsDiff.getList());
        {
            ActionCell<EmailsDiff> actionCell = new ActionCell<EmailsDiff>("", new ActionCell.Delegate<EmailsDiff>() {
                @Override
                public void execute(final EmailsDiff eleve) {
                    dataProviderEmailsDiff.getList().remove(eleve);
                    dataProviderEmailsDiff.refresh();
                }
            });

            Column<EmailsDiff, EmailsDiff> c = new Column<EmailsDiff, EmailsDiff>(actionCell) {
                @Override
                public EmailsDiff getValue(EmailsDiff object) {
                    return object;
                }
            };
            c.setCellStyleNames(ficheeps.client.Resources.INSTANCE.ficheepsStyle().delete16());
            display.getSuggestTable().addColumn(c, "");
            display.getSuggestTable().setColumnWidth(c, 30, Style.Unit.PX);
        }        
        {
            TextColumn<EmailsDiff> c = new TextColumn<EmailsDiff>() {
                @Override
                public String getValue(EmailsDiff object) {
                    return object.getEleveId();
                }
            };
            c.setSortable(true);
            columnSortHandler.setComparator(c,new Comparator<EmailsDiff>() {
                @Override
                public int compare(EmailsDiff o1, EmailsDiff o2) {
                    if (o1 == o2) {return 0;}
                    if (o1 != null) {
                      return (o2 != null) ? o1.getEleveId().compareTo(o2.getEleveId()) : 1;
                    }
                    return -1;
                }
            });
            display.getSuggestTable().addColumn(c, "Id");
            display.getSuggestTable().setColumnWidth(c,15, Style.Unit.PCT);
        }
        {
            TextColumn<EmailsDiff> c = new TextColumn<EmailsDiff>() {
                @Override
                public String getValue(EmailsDiff object) {
                    return object.getEleveClasse();
                }
            };
            c.setSortable(true);
            columnSortHandler.setComparator(c,new Comparator<EmailsDiff>() {
                @Override
                public int compare(EmailsDiff o1, EmailsDiff o2) {
                    if (o1 == o2) {return 0;}
                    if (o1 != null) {
                      return (o2 != null) ? o1.getEleveClasse().compareTo(o2.getEleveClasse()) : 1;
                    }
                    return -1;
                }
            });            
            display.getSuggestTable().addColumn(c, "Classe");
            display.getSuggestTable().setColumnWidth(c,15, Style.Unit.PCT);
        }         
        {
            TextColumn<EmailsDiff> c = new TextColumn<EmailsDiff>() {
                @Override
                public String getValue(EmailsDiff object) {
                    return object.getEleveName();
                }
            };
            c.setSortable(true);
            columnSortHandler.setComparator(c,new Comparator<EmailsDiff>() {
                @Override
                public int compare(EmailsDiff o1, EmailsDiff o2) {
                    if (o1 == o2) {return 0;}
                    if (o1 != null) {
                      return (o2 != null) ? o1.getEleveName().compareTo(o2.getEleveName()) : 1;
                    }
                    return -1;
                }
            });                  
            display.getSuggestTable().addColumn(c, "Nom");
            display.getSuggestTable().getColumnSortList().push(c); // By default we sort on name
        }
        {
            Column<EmailsDiff, EmailsDiff> c = new Column<EmailsDiff, EmailsDiff>(new EmailsDiffCell()) {
                @Override
                public EmailsDiff getValue(EmailsDiff ed) {
                    return ed;
                }
            };
            display.getSuggestTable().addColumn(c, "");
        }
        display.getSuggestTable().addColumnSortHandler(columnSortHandler);
        dataProviderEmailsDiff.addDataDisplay(display.getSuggestTable());
        
        ValueChangeHandler<Boolean> modeChangeHandler = new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                tryImport();
            }
        };
        display.getAppendModeSelector().addValueChangeHandler(modeChangeHandler);
        display.getReplaceModeSelector().addValueChangeHandler(modeChangeHandler);
    }

    private IUploader.OnFinishUploaderHandler onFinishUploaderHandler = new IUploader.OnFinishUploaderHandler() {
        @Override
        public void onFinish(IUploader uploader) {
            if (uploader.getStatus() == Status.SUCCESS) {
                    // new PreloadedImage(uploader.fileUrl(), showImage);
                // The server sends useful information to the client by default
                UploadedInfo info = uploader.getServerInfo();
                /*System.out.println("File name " + info.name);
                 System.out.println("File content-type " + info.ctype);
                 System.out.println("File size " + info.size);
                 // You can send any customized message and parse it
                 System.out.println("Server message " + info.message);*/
                currentImportOptions = new EmailsImportOptions();
                currentImportOptions.setFileUploadToken(uploader.getServerMessage().getMessage());
                tryImport();
            }
        }
    };

    private void cleanupAll() {
        dataProviderEmailsDiff.getList().clear();
        dataProviderEmailsDiff.refresh();
        display.getCommitImportResult().setHTML(new SafeHtmlBuilder().toSafeHtml());
        display.getResultImportView().setVisible(false);
    }

    private void tryImport() {
        if(currentImportOptions == null) {
            return;
        }
        
        final DialogMsg wd = DialogMsg.ShowWaiter("Test d'import", "Merci de patienter");
        currentImportOptions.setMode(EmailsImportOptions.APPEND); //default
        if(display.getReplaceModeSelector().getValue()) {
            currentImportOptions.setMode(EmailsImportOptions.REPLACE);
        }

        ficheEPSService.tryEmailsImport(currentImportOptions, new AsyncCallback<EmailsDiffImportResult>() {
            @Override
            public void onFailure(Throwable caught) {
                ErrorReportEvent.Fire("Impossible de tester l'import: '" + caught.getMessage() + "'.");
                wd.hide();
            }

            @Override
            public void onSuccess(EmailsDiffImportResult result) {
                setSuggestedImport(result.getEmailsDiffs());
                SafeHtmlBuilder sb = new SafeHtmlBuilder();
                sb.appendHtmlConstant("<div style='color:#AA0000'>");
                sb.appendEscapedLines(result.getErrorMessage());
                sb.appendHtmlConstant("</div>");
                sb.appendHtmlConstant("<div>");
                sb.appendEscapedLines(result.getInfosMessage());
                sb.appendHtmlConstant("</div>");
                display.getCommitImportResult().setHTML(sb.toSafeHtml());
                wd.hide();
            }
        });
    }

    private void setSuggestedImport(List<EmailsDiff> list) {
        dataProviderEmailsDiff.getList().clear();
        dataProviderEmailsDiff.getList().addAll(list);

        display.getSuggestTable().setHeight("190px");
        
        display.getResultImportView().setVisible(true);
    }


    private void commitImport() {
        final DialogMsg wd = DialogMsg.ShowWaiter("Import des élèves", "Veuillez patienter");
        List<EmailsDiff> list = new LinkedList<EmailsDiff>();
        list.addAll(dataProviderEmailsDiff.getList());

        ficheEPSService.commitEmailsImport(currentImportOptions, list, new AsyncCallback<String>() {
            @Override
            public void onFailure(Throwable caught) {
                ErrorReportEvent.Fire("Erreur lors de l'import: '" + caught.getMessage() + "'.");
                wd.hide();
            }

            @Override
            public void onSuccess(String result) {
                if(result == null) {
                    result = "";
                }
                SafeHtmlBuilder sb = new SafeHtmlBuilder();
                sb.appendEscapedLines(result);
                display.getCommitImportResult().setHTML(sb.toSafeHtml());
                wd.hide();
            }
        });
    }

    
    static class EmailsDiffCell extends AbstractCell<EmailsDiff> {

        private final static String keepedValueStyleName = ficheeps.client.Resources.INSTANCE.ficheepsStyle().keepedValue();
        private final static String removedValueStyleName = ficheeps.client.Resources.INSTANCE.ficheepsStyle().removedValue();
        private final static String addedValueStyleName = ficheeps.client.Resources.INSTANCE.ficheepsStyle().addedValue();
        private final static String emailsDiffValuesStyleName = ficheeps.client.Resources.INSTANCE.ficheepsStyle().emailsDiffValues();
        
        
        public EmailsDiffCell() {}
       
        @Override
        public void render(Cell.Context context, EmailsDiff ed, SafeHtmlBuilder sb) {
            if (ed == null) {// Value can be null, so do a null check..
                return;
            }
            sb.appendHtmlConstant("<div class='" + emailsDiffValuesStyleName +"'>");
            if(ed.getKeepedMails() != null) {
                sb.appendHtmlConstant("<span class='" + keepedValueStyleName+ "'>");
                for(String s : ed.getKeepedMails()) {
                    sb.appendEscaped(s + " ");
                }
                sb.appendHtmlConstant("</span>");
            }
            if(ed.getRemovedMails() != null) {
                sb.appendHtmlConstant("<span class='" + removedValueStyleName + "'>");
                for(String s : ed.getRemovedMails()) {
                    sb.appendEscaped("-" + s + " ");
                }
                sb.appendHtmlConstant("</span>");
            }     
            if(ed.getAddedMails() != null) {
                sb.appendHtmlConstant("<span class='" + addedValueStyleName + "'>");
                for(String s : ed.getAddedMails()) {
                    sb.appendEscaped("+" + s + " ");
                }
                sb.appendHtmlConstant("</span>");
            }            
            sb.appendHtmlConstant("</div>");
        }
    }
}

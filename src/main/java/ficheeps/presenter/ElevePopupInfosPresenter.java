package ficheeps.presenter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import ficheeps.client.AppController;
import ficheeps.client.ErrorReportEvent;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;

import ficheeps.model.Eleve;
import ficheeps.view.DialogMsg;
import ficheeps.view.EleveEmailView;
import ficheeps.view.ElevePopupInfosView;
import ficheeps.view.ExportPDFView;
import java.util.List;

public class ElevePopupInfosPresenter {

    FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);
    final private Eleve eleve;

    public ElevePopupInfosPresenter(Eleve eleve) {
            this.eleve = eleve;
    }

    public interface Display {
        Anchor getClassAnchor();
        VerticalPanel getGroupsPanel();
        Button getFicheElevePDF();
        Button getEmailEleveButton();
    }

    public void bind(final Display display) {
            display.getClassAnchor().setText(eleve.getClasse());
            display.getClassAnchor().addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    goToClass(eleve.getClasse());
                }
            });
            
            display.getFicheElevePDF().addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                        exportPDF();
                }
            });
            
            if(eleve != null && !eleve.getEmailsAsList().isEmpty()) { 
                display.getEmailEleveButton().addStyleName(ficheeps.client.Resources.INSTANCE.ficheepsStyle().emailEleve32());
            }
            else {
                display.getEmailEleveButton().addStyleName(ficheeps.client.Resources.INSTANCE.ficheepsStyle().noemailEleve32());
            }
            
            display.getEmailEleveButton().addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    showEmail();
                }
            });
            
            ficheEPSService.getAllGroupsForEleve(eleve.getOrid(), new AsyncCallback<List<String>>() {
                @Override
                public void onFailure(Throwable caught) {
                    ErrorReportEvent.Fire("Impossible de retrouver la liste des groupes pour l'élève: " + caught.getMessage());
                }
                @Override
                public void onSuccess(List<String> result) {
                    display.getGroupsPanel().remove(1);
                    if(result.isEmpty()) {
                        Label label = new Label("Aucun groupes");
                        label.getElement().getStyle().setFontStyle(Style.FontStyle.ITALIC);
                        display.getGroupsPanel().add(label);
                    }
                    else {
                        for(String s: result) {
                            final String gs = s;
                            if(s!=null) {
                                Anchor anchor = new Anchor(s);
                                anchor.addClickHandler(new ClickHandler() {
                                    @Override
                                    public void onClick(ClickEvent event) {
                                        goToGroup(gs);
                                    }
                                });
                                display.getGroupsPanel().add(anchor);
                            }
                        }
                    }
                }
            });
    }

    private void showEmail() {
        EleveEmailPresenter eleveEmailPresenter = new EleveEmailPresenter(eleve);
        EleveEmailView eleveEmailView = new EleveEmailView();
        eleveEmailPresenter.bind(eleveEmailView);
        DialogMsg.ShowContent("Courriels de l'élève", eleveEmailView);
    }
    
    private void goToClass(String className) {
        if(LastPopupPanel != null) {
            LastPopupPanel.hide();
        }
        AppController.GoToEleveNavigationEtSaisie(className, true, eleve.getId());
    }
    private void goToGroup(String groupName) {
        if(LastPopupPanel != null) {
            LastPopupPanel.hide();
        }
        AppController.GoToEleveNavigationEtSaisie(groupName, false, eleve.getId());
    }
    
    private void exportPDF() {
            final ExportPDFPresenter exportPDFPresenter = new ExportPDFPresenter();
            exportPDFPresenter.setExport(ExportPDFPresenter.ExportType.ExportEleve, eleve.getId());
            ExportPDFView exportPDFView = new ExportPDFView();
            exportPDFPresenter.bind(exportPDFView);

            DialogMsg.ShowOkCancel("Export PDF", 
                            exportPDFView, 
                            new ClickHandler() {
                                    @Override
                                    public void onClick(ClickEvent event) {
                                            exportPDFPresenter.startExport();
                                    }
                            }, 
                            null);
    }

    
    private static Element LastWidgetRelativeTo = null;
    private static PopupPanel LastPopupPanel = null;
    
    public static void ShowPopup(Eleve eleve,Widget locationRelativeTo) {
        ShowPopup(eleve,locationRelativeTo.getElement());
    }
    public static void ShowPopup(Eleve eleve,Element locationRelativeTo) {
        if(LastPopupPanel != null && locationRelativeTo == LastWidgetRelativeTo) {
            return; // will close itself nicely
        }
        ElevePopupInfosPresenter epip = new ElevePopupInfosPresenter(eleve);
        ElevePopupInfosView epiv = new ElevePopupInfosView();
        epip.bind(epiv);
        PopupPanel popup = new PopupPanel(true,false);
        popup.addCloseHandler(new CloseHandler<PopupPanel>() {
            @Override
            public void onClose(CloseEvent<PopupPanel> event) {
                LastWidgetRelativeTo = null;
                LastPopupPanel = null;
            }
        });
        popup.setWidth("175px");
        popup.getElement().setAttribute("style","z-index:20");
        popup.setWidget(epiv);
        //popup.setStyleName("popup-hint");
        int xPosition = locationRelativeTo.getAbsoluteLeft() - 190 + locationRelativeTo.getOffsetWidth();
        if(xPosition < 0) {
            xPosition = 0;
        }
        int yPosition = locationRelativeTo.getAbsoluteTop() + locationRelativeTo.getOffsetHeight();
        popup.setPopupPosition(xPosition,yPosition);
        LastPopupPanel = popup;
        popup.show();
    }
}

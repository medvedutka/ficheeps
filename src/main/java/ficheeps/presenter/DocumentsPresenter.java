package ficheeps.presenter;


import org.moxieapps.gwt.uploader.client.Uploader;
import org.moxieapps.gwt.uploader.client.events.FileQueuedEvent;
import org.moxieapps.gwt.uploader.client.events.FileQueuedHandler;
import org.moxieapps.gwt.uploader.client.events.UploadErrorEvent;
import org.moxieapps.gwt.uploader.client.events.UploadErrorHandler;
import org.moxieapps.gwt.uploader.client.events.UploadStartEvent;
import org.moxieapps.gwt.uploader.client.events.UploadStartHandler;
import org.moxieapps.gwt.uploader.client.events.UploadSuccessEvent;
import org.moxieapps.gwt.uploader.client.events.UploadSuccessHandler;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;

import ficheeps.client.ErrorReportEvent;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.view.DialogMsg;


public class DocumentsPresenter {

	private FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);

	
	public interface Display {
		Button getDeleteTemplateFinDeSecondeButton();
		Label getNoTemplateFinDeSecondeLabel();
		Uploader getUploadNewFicheFinDeSecondeTemplate();
	}
	
	public DocumentsPresenter() {
	}
	
	private Display display;
	
	public void bind(Display display) {
		this.display = display;
		
		display.getDeleteTemplateFinDeSecondeButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				deleteTemplateFinDeSeconde();
			}
		});
		
			
		String url = GWT.getModuleBaseURL() + "elevePhotoServlet?suggestionFinDeSecondeTemplate=upload";
		final Uploader uploader = display.getUploadNewFicheFinDeSecondeTemplate();
		uploader.setUploadURL(url);
		uploader.setFileQueuedHandler(new FileQueuedHandler() {
			@Override
			public boolean onFileQueued(FileQueuedEvent fileQueuedEvent) {
				uploader.startUpload();
				return false;
			}
		});
		uploader.setUploadStartHandler(new UploadStartHandler() {
			@Override
			public boolean onUploadStart(UploadStartEvent uploadStartEvent) {
				wd = DialogMsg.ShowWaiter("Envois", "Envois du modèle 'fiche suggestion fin de seconde', merci de patienter");
				return false;
			}
		});
		uploader.setUploadErrorHandler(new UploadErrorHandler() {
			@Override
			public boolean onUploadError(UploadErrorEvent uploadErrorEvent) {
				if(wd!=null) {
					wd.hide();
					wd = null;
				}
				DialogMsg.ShowMessage("Erreur","Envois du modèle 'fiche suggestion fin de seconde': " + uploadErrorEvent.getMessage());
				refresh();
				return false;
			}
		});
		uploader.setUploadSuccessHandler(new UploadSuccessHandler() {
			@Override
			public boolean onUploadSuccess(UploadSuccessEvent uploadSuccessEvent) {
				if(wd!=null) {
					wd.hide();
					wd = null;
				}
				DialogMsg.ShowMessage("Envois terminé","Envois terminé avec succés.");
				refresh();
				return false;
			}
		});
	}
	
	private DialogMsg wd;
	
	
	public void refresh() {
		ficheEPSService.isTemplateSuggestionFinDeSeondeSet(new AsyncCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean result) {
				if(result) {
					display.getNoTemplateFinDeSecondeLabel().setVisible(false);
					display.getDeleteTemplateFinDeSecondeButton().setVisible(true);
				}
				else {
					display.getNoTemplateFinDeSecondeLabel().setVisible(true);
					display.getDeleteTemplateFinDeSecondeButton().setVisible(false);					
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				ErrorReportEvent.Fire("Impossible de savoir si le modèle 'suggestion fin de seconde' est renseigné '" + caught.getMessage() + "'");
			}
		});
	}
	

	private void deleteTemplateFinDeSeconde() {
		ficheEPSService.deleteTemplateFinDeSeconde(new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				ErrorReportEvent.Fire("Erreur lors de la supression du modèle 'suggestion fin de seconde': '" + caught.getMessage() + "'");
			}
			@Override
			public void onSuccess(Void result) {
				refresh();
			}
		});
	}
}

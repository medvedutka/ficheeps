package ficheeps.presenter;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import ficheeps.client.ErrorReportEvent;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.model.Eleve;
import ficheeps.model.Tuple;
import java.util.HashMap;
import java.util.Map;

public class ElevesSearchPresenter {

    private final static Comparator<Eleve> CurrentEleveComparator = ficheeps.client.Utils.ELEVE_ALPHABETICAL_COMPARATOR;

    public interface AddEleveAction {
        void addEleve(Eleve eleve);
    }
    
    public interface Display {
        Panel getSearchEleveResultPanel();
        TextBox getSearchTextField();
        EleveWidget createEleveWidget(Eleve eleve);
        Image getImageAjaxLoader();
        HasValue<Boolean> getOptionIncludeAllEleve();
    }

    public interface EleveWidget extends IsWidget {
        Button getButton();
        Button getMoreInfosButton();
        Eleve getEleve();
        void disable();
        void enable();
    }


    private final FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);

    public ElevesSearchPresenter() {
    }

    public void refresh() {
        display.getSearchEleveResultPanel().clear();
    }

    
    private Display display;
    private final int timerDelay = 300;
    private final Timer keyboardTimer = new Timer() {
        @Override
        public void run() {
            search(false);
        }
    };

    public void bind(final Display display) {
        this.display = display;
        display.getSearchTextField().addKeyPressHandler(new KeyPressHandler() {
            @Override
            public void onKeyPress(KeyPressEvent event) {
                display.getImageAjaxLoader().setVisible(true);
                keyboardTimer.cancel();
                keyboardTimer.schedule(timerDelay);
            }
        });
        display.getOptionIncludeAllEleve().addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                search(false);
            }
        });
        
        display.getImageAjaxLoader().setVisible(false);
    }


    private int currentNbDisplayedEleve = 0;
    private boolean searching = false;

    private void search(final boolean searchMore) {
        if (searching) {
            keyboardTimer.cancel();
            keyboardTimer.schedule(timerDelay);
        }
        searching = true;

        if (!searchMore) {
            currentNbDisplayedEleve = 0;
            display.getSearchEleveResultPanel().clear();
        }

        final int start = searchMore ? currentNbDisplayedEleve : 0;

        AsyncCallback<Tuple<List<Eleve>, Integer>> callback = new AsyncCallback<Tuple<List<Eleve>, Integer>>() {
            public void onSuccess(Tuple<List<Eleve>, Integer> result) {
                updateListEleve(searchMore, result.getFirst());
                display.getImageAjaxLoader().setVisible(false);
                searching = false;
            }

            public void onFailure(Throwable caught) {
                ErrorReportEvent.Fire("Erreur serveur: " + caught.getMessage());
                display.getImageAjaxLoader().setVisible(false);
                searching = false;
            }
        };

        String searchTerm = ("" + display.getSearchTextField().getText()).toLowerCase();
        boolean mustIncludeAllEleves = display.getOptionIncludeAllEleve().getValue();
        ficheEPSService.getListElevesRange(start, searchResultSize, searchTerm, null, true, mustIncludeAllEleves, callback);
    }
    
    
    private AddEleveAction addEleveAction;
    public void setAddEleveAction(AddEleveAction addEleveAction) {
        this.addEleveAction = addEleveAction;
    }
    
    
    private Map<String,Eleve> disabledEleves = new HashMap<String,Eleve>();
    public void disableEleve(Eleve eleve) {
        if(eleve != null) {
            disabledEleves.put(eleve.getOrid(),eleve);
        }
        changeEleveWidgetState(eleve, false);
    }
    public void enableEleve(Eleve eleve) {
        if(eleve != null && disabledEleves.containsKey(eleve.getOrid())) {
            disabledEleves.remove(eleve.getOrid());
        }
        changeEleveWidgetState(eleve, true);
    }
    public void clearDisabledEleve() {
        disabledEleves.clear();
        enableAllEleveWidget();
    }

    private int searchResultSize = 15;

    private void updateListEleve(boolean append, List<Eleve> list) {
        if (!append) {
            currentNbDisplayedEleve = 0;
            display.getSearchEleveResultPanel().clear();
        } else {
            //remove the "more" button
            Iterator<Widget> iter = display.getSearchEleveResultPanel().iterator();
            while (iter.hasNext()) {
                Widget w = iter.next();
                if (!(w instanceof EleveWidget)) {
                    iter.remove();
                }
            }
        }
        currentNbDisplayedEleve += list.size();

        for (final Eleve eleve : list) {
            final EleveWidget ew = display.createEleveWidget(eleve);
            //ew.getButton().setHTML("ajouter au groupe");
            if(disabledEleves.containsKey(eleve.getOrid())) {
                ew.disable();
            }
            ew.getButton().addStyleName(ficheeps.client.Resources.INSTANCE.ficheepsStyle().addImageBackground());
            ew.getButton().setTitle("Ajouter au groupe");
            ew.getButton().addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    if(addEleveAction != null) {
                        addEleveAction.addEleve(eleve);
                    }
                }
            });
            
            ew.getMoreInfosButton().addStyleName(ficheeps.client.Resources.INSTANCE.ficheepsStyle().helpImageBackgroundEleveGroupe());
            ew.getMoreInfosButton().setTitle("Plus d'infos ...");
            ew.getMoreInfosButton().addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    displayMoreInfosForEleve(ew.getEleve(),ew.getMoreInfosButton());
                }
            });
            display.getSearchEleveResultPanel().add(ew);
        }

        if (searchResultSize == list.size()) {
            Button moreButton = new Button("Plus de résultats");
            moreButton.getElement().getStyle().setMarginLeft(5, Style.Unit.PX);
            moreButton.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    search(true);
                }
            });
            display.getSearchEleveResultPanel().add(moreButton);
        }

    }



    private void changeEleveWidgetState(Eleve eleve, boolean enable) {
        Iterator<Widget> iter = display.getSearchEleveResultPanel().iterator();
        while (iter.hasNext()) {
            Widget w = iter.next();
            if(w instanceof EleveWidget) {
                EleveWidget ew = (EleveWidget) w;
                if(ew.getEleve() != null && eleve.getId().equals(ew.getEleve().getId())) {
                    if(enable) {
                        ew.enable();
                    }
                    else {
                        ew.disable();
                    }
                    break;
                }
            }
        }
    }

    private void enableAllEleveWidget() {
        Iterator<Widget> iter = display.getSearchEleveResultPanel().iterator();
        while (iter.hasNext()) {
            Widget w = iter.next();
            if(w instanceof EleveWidget) {
                EleveWidget ew = (EleveWidget) w;
                ew.enable();
            }
        }
    }

    private void displayMoreInfosForEleve(Eleve eleve, Widget widgetRelativeTo) {
        ElevePopupInfosPresenter.ShowPopup(eleve, widgetRelativeTo);
    }
    



}

package ficheeps.presenter;


import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import ficheeps.client.ErrorReportEvent;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.model.Settings;
import java.util.LinkedList;
import java.util.List;


public class ApsaConfigurationPresenter {

	FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);

	public interface Display {
		CheckBox getCheckboxConstraintLabel();
                ListBox getApsaLabels();
                TextBox getTextboxNewApsaLabel();
                Button getButtonNewApsaLabel();
	}
	
	public ApsaConfigurationPresenter() {
	}
	
	
	private Display display;
	
	public void bind(Display display) {
            this.display = display;
            
            display.getButtonNewApsaLabel().addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    newApsaLabel();
                }
            });
            
            display.getApsaLabels().addKeyUpHandler(new KeyUpHandler() {
                @Override
                public void onKeyUp(KeyUpEvent event) {
                    if(event.getNativeKeyCode()== KeyCodes.KEY_DELETE) {
                        deleteApsaLabel();
                    }
                }
            });
            
            display.getCheckboxConstraintLabel().addValueChangeHandler(new ValueChangeHandler<Boolean>() {
                @Override
                public void onValueChange(ValueChangeEvent<Boolean> event) {
                    saveContraintLabelOption();
                }
            });
	}
	

	public void refresh() {
            ficheEPSService.getSettings(new AsyncCallback<Settings>() {
                @Override
                public void onFailure(Throwable caught) {
                    ErrorReportEvent.Fire("Impossible de retrouver la configuration: '" +caught.getMessage() + "'");
                }
                @Override
                public void onSuccess(Settings result) {
                    setSettings(result);
                }
            });
	}

        private Settings settings = null;
        private void setSettings(Settings settings) {
            this.settings = settings;
            display.getCheckboxConstraintLabel().setValue(settings.isConstraintApsaLabel());
            setSettingsApsaLabels();
        }

        private void setSettingsApsaLabels() {
            List<String> apsaLabels = settings.getApsaLabels();
            display.getApsaLabels().clear();
            if(apsaLabels != null) {
                for(String s : apsaLabels) {
                    display.getApsaLabels().addItem(s);
                }
            }
        }

	private void newApsaLabel() {
            String apsaLabel = display.getTextboxNewApsaLabel().getText();
            if(apsaLabel != null) {
                apsaLabel = apsaLabel.trim();
            }
            if(apsaLabel.isEmpty()) {
               return; 
            }
            ficheEPSService.addApsaLabel(apsaLabel, new AsyncCallback<List<String>>() {
                @Override
                public void onFailure(Throwable caught) {
                    ErrorReportEvent.Fire("Impossible d'enregistrer le nouveau nom d'APSA: '" +caught.getMessage() + "'");
                }
                @Override
                public void onSuccess(List<String> result) {
                    if(result == null) {
                        result = new LinkedList<String>();
                    }
                    settings.setApsaLabels(result);
                    setSettingsApsaLabels();
                    display.getTextboxNewApsaLabel().setText("");
                    FicheElevePresenter.APSA_SUGGESTION_LOADED = false;
                }
            });
        }
        
        private void deleteApsaLabel() {
            int index =  display.getApsaLabels().getSelectedIndex();
            if(index != -1) {
                final String apsaLabel = display.getApsaLabels().getItemText(index);
                ficheEPSService.deleteApsaLabel(apsaLabel, new AsyncCallback<List<String>>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        ErrorReportEvent.Fire("Impossible de supprimer l'APSA: '" + apsaLabel + "' : '" +caught.getMessage() + "'");
                    }
                    @Override
                    public void onSuccess(List<String> result) {
                        if(result == null) {
                            result = new LinkedList<String>();
                        }
                        settings.setApsaLabels(result);
                        setSettingsApsaLabels();
                        FicheElevePresenter.APSA_SUGGESTION_LOADED = false;
                    }
                });
            }
        }
        
        private void saveContraintLabelOption() {
            final boolean value = display.getCheckboxConstraintLabel().getValue();
            ficheEPSService.setConstraintApsaLabels(value, new AsyncCallback<Boolean>() {
                @Override
                public void onFailure(Throwable caught) {
                    ErrorReportEvent.Fire("Impossible d'enregistrer l'option: '" + caught.getMessage() + "'");
                }
                @Override
                public void onSuccess(Boolean result) {
                    settings.setConstraintApsaLabel(value);
                }
            });
        }
}

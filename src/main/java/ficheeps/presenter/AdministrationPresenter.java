package ficheeps.presenter;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Panel;

import ficheeps.client.ErrorReportEvent;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.model.Commentaire;
import ficheeps.model.Eleve;
import ficheeps.model.Commentaire.Niveau;
import ficheeps.model.StatisticsFilter;
import ficheeps.model.User;
import ficheeps.server.DBAccess;
import ficheeps.view.ApsaConfigurationView;
import ficheeps.view.ClassAndGroupNameInfosView;
import ficheeps.view.DBBackupView;
import ficheeps.view.DialogMsg;
import ficheeps.view.DocumentsView;
import ficheeps.view.EmailsImportView;
import ficheeps.view.ExportPDFView;
import ficheeps.view.StatisticsFilterView;
import ficheeps.view.UserListView;


public class AdministrationPresenter {

	FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);

	public interface Display {
		HasClickHandlers getCreateDummyDB();
                HasClickHandlers getCreateSnapshotDB();
		HasClickHandlers getDropDB();
		
		HasClickHandlers getGoogleAuthentication();
		HasClickHandlers getTryLogin();
		HasClickHandlers getLogout();
		HasClickHandlers getMassGroupImport();
	
                HasClickHandlers getDateRA12();
                
                HasText getupdateIDText();
                HasClickHandlers getupdateIDButton();
                
                HasClickHandlers getStatisticsDownloader();
                HasClickHandlers getButtonMailsImport();
                
                HasClickHandlers getDownloadPDFAll();
                HasClickHandlers getDeleteAllUser();
                
		Panel getUsersPanel();
		Panel getBackupPanel();
		Panel getDebugPanel();
		Panel getDocumentsPanel();
                Panel getAPSAPanel();
                ClassAndGroupNameInfosView getClassAndGroupNameInfosViewPanel();
	}
	
	private User user;
	public AdministrationPresenter(User user) {
		this.user = user;
	}
	
	
	private DBBackupPresenter dbBackupPresenter;
	private UserListPresenter userListPresenter;
	private DocumentsPresenter documentsPresenter;
        private ApsaConfigurationPresenter apsaPresenter;
        private ClassAndGroupNameInfosPresenter classAndGroupNameInfosPresenter;
	private Display display;
	
	
	public void bind(Display display) {
		this.display = display;
		
                display.getDebugPanel().setVisible(false);
		if("debugadmin".equals(user.getLogin())) {
			display.getDebugPanel().setVisible(true);
                        display.getCreateDummyDB().addClickHandler(new ClickHandler() {
                                @Override
                                public void onClick(ClickEvent event) {
                                        createDummyDB();
                                }
                        });
                        display.getDropDB().addClickHandler(new ClickHandler() {
                                @Override
                                public void onClick(ClickEvent event) {
                                        dropDB();
                                }
                        });
                        display.getCreateSnapshotDB().addClickHandler(new ClickHandler() {
                            @Override
                            public void onClick(ClickEvent event) {
                                createSnapshot();
                            }
                        });
                        /*display.getMassGroupImport().addClickHandler(new ClickHandler() {
                                @Override
                                public void onClick(ClickEvent event) {
                                        massImportGroups();
                                }
                        });*/
                        display.getupdateIDText().setText("");
                        /*display.getDateRA12().addClickHandler(new ClickHandler() {
                            @Override
                            public void onClick(ClickEvent event) {
                                dateRA12();
                            }
                        });*/
                        
		}


		
		UserListView userListView = new UserListView();
		userListPresenter = new UserListPresenter();
		userListPresenter.bind(userListView);
		display.getUsersPanel().add(userListView);
		
		display.getStatisticsDownloader().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
                            exportStatistics();
			}
		});                
                
                display.getButtonMailsImport().addClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        mailsImport();
                    }
		});                
                
                display.getDeleteAllUser().addClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        deleteAllUsers();
                    }
                });
                
                display.getDownloadPDFAll().addClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        downloadALLPDF();
                    }
                });
                
                
		dbBackupPresenter = new DBBackupPresenter();
		DBBackupView dbBackupView = new DBBackupView();
		dbBackupPresenter.bind(dbBackupView);
		display.getBackupPanel().add(dbBackupView);
		
		documentsPresenter = new DocumentsPresenter();
		DocumentsView documentsView = new DocumentsView();
		documentsPresenter.bind(documentsView);
		display.getDocumentsPanel().add(documentsView);
                
                apsaPresenter = new ApsaConfigurationPresenter();
                ApsaConfigurationView apsaView = new ApsaConfigurationView();
                apsaPresenter.bind(apsaView);
                display.getAPSAPanel().add(apsaView);
                
                classAndGroupNameInfosPresenter = new ClassAndGroupNameInfosPresenter();
                classAndGroupNameInfosPresenter.bind(display.getClassAndGroupNameInfosViewPanel());
	}
	
	
	public void refresh() {
		userListPresenter.refresh();
		dbBackupPresenter.refresh();
		documentsPresenter.refresh();
                apsaPresenter.refresh();
                classAndGroupNameInfosPresenter.refresh();
	}

        private void deleteAllUsers() {
            DialogMsg.ShowOkCancel("Suppression de tous les utilisateurs", "Etes vous sûr de vouloir supprimer tous les utilisateurs ?", new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    ficheEPSService.deleteAllUsers(new AsyncCallback<Void>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            ErrorReportEvent.Fire("Une erreur est survenue lors de la suppression de tous les utilisateurs: '" + caught.getMessage() + "'");
                        }

                        @Override
                        public void onSuccess(Void result) {
                            //Nothing to do!
                        }
                    });
                }
            }, null);
        }
        
        private void downloadALLPDF() {
            final ExportPDFPresenter exportPDFPresenter = new ExportPDFPresenter();
            exportPDFPresenter.setExport(ExportPDFPresenter.ExportType.All, "");
            ExportPDFView exportPDFView = new ExportPDFView();
            exportPDFPresenter.bind(exportPDFView);
            DialogMsg.ShowOkCancel("Export PDF", 
                            exportPDFView, 
                            new ClickHandler() {
                                    @Override
                                    public void onClick(ClickEvent event) {
                                            exportPDFPresenter.startExport();
                                    }
                            }, 
                            null);
        }
        
        private void exportStatistics() {
            ficheEPSService.getFullStatisticsFilter(new AsyncCallback<StatisticsFilter>() {
                @Override
                public void onFailure(Throwable caught) {
                    ErrorReportEvent.Fire("Une erreur est survenue lors de la récupération des critères de filtres: '" + caught.getMessage() + "'");
                }

                @Override
                public void onSuccess(StatisticsFilter result) {
                    exportStatistics_showUI(result);
                }
            });
        }
	
        private void exportStatistics_showUI(StatisticsFilter fullFilter) {
            StatisticsFilterPresenter sfp = new StatisticsFilterPresenter(fullFilter);
            StatisticsFilterView sfv = new StatisticsFilterView();
            sfp.bind(sfv);
            DialogMsg.ShowContent("Export des statistiques", sfv);
            //String url = com.google.gwt.core.client.GWT.getModuleBaseURL() + "exportServlet?action=exportStats&dummyPreventCache="+System.currentTimeMillis();
            //Window.open(url, "_blank", "enabled");	
        }
        
        
	private void dropDB() {
		final DialogMsg dlg = DialogMsg.ShowWaiter("Suppression de la base de donnée", "Veuillez patienter ...");
		ficheEPSService.dropDB(new AsyncCallback<Void>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        dlg.hide();
                        ErrorReportEvent.Fire("Une erreur est survenue lors de la suppression de la base de donnée: '" + caught.getMessage() + "'");
                    }
                    @Override
                    public void onSuccess(Void result) {
                        dlg.hide();
                    }
		});
	}

        private void createSnapshot() {
            final DialogMsg dlg = DialogMsg.ShowWaiter("Création d'un snapshot", "Veuillez patienter ...");
            ficheEPSService.createDBSnapshot(new AsyncCallback<Void>() {
                @Override
                public void onFailure(Throwable caught) {
                    dlg.hide();
                    ErrorReportEvent.Fire("Une erreur est survenue lors de la création du snapshot: '" + caught.getMessage() + "'");
                }

                @Override
                public void onSuccess(Void result) {
                    dlg.hide();
                }
            });
        }
        
	private void createDummyDB() {
		ficheEPSService.createDummyDB(new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				ErrorReportEvent.Fire("Erreur lors de la création de la base de démonstration: '" + caught.getMessage() + "'");
			}
			@Override
			public void onSuccess(Void result) {
			}
		});
	}
	
	
	
	
	private static int dummyI = 1;
	public static Eleve GetDummyEleve() {
		return GetDummyEleve(dummyI++);
	}

	public static Eleve GetDummyEleve(int i) {
		Eleve e = new Eleve();
		e.setId("" + i);
		e.setNom(/*String.format("%05d", i)*/ i + "Doe");
		e.setPrenom(/*String.format("%05d", i)*/i + "Jhon");
		
		//GregorianCalendar cal = new GregorianCalendar();
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(GregorianCalendar.DATE, -1 * (i+1));
		e.setDateDeNaissance(cal.getTime());
		
		e.setClasse("" + (i / 100));
		
		e.addCommentaire(GetDummyCommentaire());
		e.addCommentaire(GetDummyCommentaire());
		return e;
	}
	
	public static Commentaire GetDummyCommentaire() {
		Commentaire c = new Commentaire();
		c.setAnneeScolaire(Commentaire.CurrentAnneeScolaire());
		c.setAppreciation("dummy appreciation");
		c.setAutre("autre ...");
		c.setNiveau(Niveau.Premiere);
		c.setProfesseur("prof" + System.currentTimeMillis());
		return c;
	}	
	
	
	
	
	/*
	private void massImportGroups() {
		VerticalPanel panel = new VerticalPanel();
		HTML label = new HTML("Import de groupes en masse.<br/> Vous devez entrer une liste de lignes contenant 'groupe|eleveID'. Ex:<br/>grp01|0989232<br/>grp01|12482312<br/>grp02|23425234<br/>...");
		panel.add(label);
		final TextArea inputArea = new TextArea();
		inputArea.setWidth("100%");
		panel.add(inputArea);
		final CheckBox checkBox = new CheckBox("Test only");
		checkBox.setValue(true);
		panel.add(checkBox);
		Button button = new Button("Import");
		panel.add(button);
		final TextArea resultArea = new TextArea();
		resultArea.setWidth("100%");
		panel.add(resultArea);
		
		panel.setWidth("350px");
		
		button.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ficheEPSService.massImportElevesInGroups(inputArea.getText(), checkBox.getValue(), new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						resultArea.setText("Erreur:\n"+caught.getMessage());
					}
					@Override
					public void onSuccess(String result) {
						resultArea.setText(result);
					}
				});
			}
		});
		
		DialogMsg.ShowOkCancel("Import de groupes en masse", panel, null, null);
	}
        */

        /*
        private void dateRA12() {
            ficheEPSService.dateRA12(new AsyncCallback<String>() {
                @Override
                public void onFailure(Throwable caught) {
                    DialogMsg.ShowMessage("Erreur de remise à 12h des dates de naissances",  caught.getMessage());
                }
                @Override
                public void onSuccess(String result) {
                    DialogMsg.ShowMessage("Succés de la remise à 12h des dates de naissances",  result);
                }
            });
        }*/
        
        
        private void mailsImport() {
            EmailsImportPresenter presenter = new EmailsImportPresenter();
            EmailsImportView view = new EmailsImportView();
            presenter.bind(view);
            view.setSize("640px", "480px");
            DialogMsg.ShowContent("Import massif d'adresse mails", view);
        }
        
}

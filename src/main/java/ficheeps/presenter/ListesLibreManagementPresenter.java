package ficheeps.presenter;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.ValueUpdater;
import java.util.Comparator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.URL;
import com.google.gwt.i18n.client.HasDirection;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ButtonBase;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.TextBoxBase;
import com.google.gwt.view.client.ListDataProvider;

import ficheeps.client.ErrorReportEvent;
import ficheeps.client.Resources;
import ficheeps.client.Utils;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.model.Eleve;
import ficheeps.model.ListeLibre;
import ficheeps.model.ListeLibreItem;
import ficheeps.model.ListeLibreLight;
import ficheeps.view.DialogMsg;
import ficheeps.view.ElevesListEmailsView;
import ficheeps.view.ElevesSearchView;
import ficheeps.view.utils.TextBoxValidationHandler;
import ficheeps.view.utils.TooltipCell;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class ListesLibreManagementPresenter {

    public interface Display {
        ListBox getListBoxListesLibre();
        TextBox getTextBoxListeLibreName();
        TextBoxBase getTextBoxListeLibreDescription();
        ButtonBase getCreateListLibreButton();
        ButtonBase getDeleteListLibreButton();
        ButtonBase getButtonExportXLS();
        ButtonBase getButtonExportEmails();
        HasText getNbItemsText();
        ElevesSearchView getEleveSearchView();
        DataGrid getDataGridItems();
    }

   

    private FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);

    private boolean allowFullModif = false;
    private final ListDataProvider<ListeLibreItem> itemDataProvider = new ListDataProvider<ListeLibreItem>();
    private final ElevesSearchPresenter eleveSearchPresenter = new ElevesSearchPresenter();
    
    public ListesLibreManagementPresenter(boolean allowFullModif) {
        this.allowFullModif = allowFullModif;
    }
    
    
    private String previousSelectedListeLibreId = null;
    
    public void refresh() {
        itemDataProvider.getList().clear();
        itemDataProvider.refresh();
        display.getListBoxListesLibre().clear();
        updateNbItemsDisplay();
        
        ficheEPSService.getAllListeLibreLight(new AsyncCallback<Collection<ListeLibreLight>>() {
            @Override
            public void onFailure(Throwable caught) {
                ErrorReportEvent.Fire("Impossible de retrouver la liste des listes libre: " + caught.getMessage());
            }

            @Override
            public void onSuccess(Collection<ListeLibreLight> result) {
                display.getListBoxListesLibre().clear();
                if(result == null || result.size() == 0) {
                    display.getListBoxListesLibre().insertItem("Aucune liste disponible", "", 0);
                }
                else {
                    display.getListBoxListesLibre().insertItem("Sélectionner une liste ...", "", 0);
                    if(result != null) {
                        for(ListeLibreLight lll : result) {
                            display.getListBoxListesLibre().addItem(lll.getName(), lll.getId());
                        }
                    }
                }
                
                display.getListBoxListesLibre().getElement().getFirstChildElement().setAttribute("disabled", "disabled");
                
                int indexToSelect = 0;
                if(previousSelectedListeLibreId != null) {
                    for(int i = 0;i<display.getListBoxListesLibre().getItemCount();i++) {
                        String llid = display.getListBoxListesLibre().getValue(i);
                        if(previousSelectedListeLibreId.equals(llid)) {
                            indexToSelect = i;
                            break;
                        }
                    }
                    
                    if(indexToSelect == 0) {
                        previousSelectedListeLibreId = null;
                    }
                }
                
                display.getListBoxListesLibre().setSelectedIndex(indexToSelect);
                if(indexToSelect != 0) {
                    displayListeLibre(getCurrentSelectedListeLibreId());
                }
            }
        });
    }

    
    
    private static final Comparator<ListeLibreItem> ITEM_ELEVENOM_COMPARATOR = new Comparator<ListeLibreItem>() {
                                                                @Override
                                                                public int compare(ListeLibreItem o1, ListeLibreItem o2) {
                                                                    if (o1 == o2) {return 0;} 
                                                                    else if (o1 == null || o1.getEleve() == null) {return 1;} 
                                                                    else if (o2 == null || o2.getEleve()== null) {return -1;}
                                                                    return Utils.StringCompareIgnoreCase(o1.getEleve().getNom(),o2.getEleve().getNom());
                                                                }
                                                            };
    
    private static final Comparator<ListeLibreItem> ITEM_ELEVEPRENOM_COMPARATOR = new Comparator<ListeLibreItem>() {
                                                                @Override
                                                                public int compare(ListeLibreItem o1, ListeLibreItem o2) {
                                                                    if (o1 == o2) {return 0;} 
                                                                    else if (o1 == null || o1.getEleve() == null) {return 1;} 
                                                                    else if (o2 == null || o2.getEleve()== null) {return -1;}
                                                                    return Utils.StringCompareIgnoreCase(o1.getEleve().getPrenom(),o2.getEleve().getPrenom());
                                                                }
                                                            };

    private static final Comparator<ListeLibreItem> ITEM_ELEVENUMEN_COMPARATOR = new Comparator<ListeLibreItem>() {
                                                                @Override
                                                                public int compare(ListeLibreItem o1, ListeLibreItem o2) {
                                                                    if (o1 == o2) {return 0;} 
                                                                    else if (o1 == null || o1.getEleve() == null) {return 1;} 
                                                                    else if (o2 == null || o2.getEleve()== null) {return -1;}
                                                                    return Utils.StringCompareIgnoreCase(o1.getEleve().getId(),o2.getEleve().getId());
                                                                }
                                                            };

    private static final Comparator<ListeLibreItem> ITEM_ELEVECLASSE_COMPARATOR = new Comparator<ListeLibreItem>() {
                                                                @Override
                                                                public int compare(ListeLibreItem o1, ListeLibreItem o2) {
                                                                    if (o1 == o2) {return 0;} 
                                                                    else if (o1 == null || o1.getEleve() == null) {return 1;} 
                                                                    else if (o2 == null || o2.getEleve()== null) {return -1;}
                                                                    return Utils.StringCompareIgnoreCase(o1.getEleve().getClasse(),o2.getEleve().getClasse());
                                                                }
                                                            };    
    
    private static final Comparator<ListeLibreItem> ITEM_ELEVEDATADENAISSANCE_COMPARATOR = new Comparator<ListeLibreItem>() {
                                                                @Override
                                                                public int compare(ListeLibreItem o1, ListeLibreItem o2) {
                                                                    if (o1 == o2) {return 0;} 
                                                                    else if (o1 == null || o1.getEleve() == null) {return 1;} 
                                                                    else if (o2 == null || o2.getEleve()== null) {return -1;}
                                                                    return Utils.ELEVE_DATEBIRTH_COMPARATOR.compare(o1.getEleve(), o2.getEleve());
                                                                }
                                                            };

    private static final Comparator<ListeLibreItem> ITEM_PROFNAME_COMPARATOR = new Comparator<ListeLibreItem>() {
                                                                @Override
                                                                public int compare(ListeLibreItem o1, ListeLibreItem o2) {
                                                                    if (o1 == o2) {return 0;} 
                                                                    else if (o1 == null) {return 1;} 
                                                                    else if (o2 == null) {return -1;}
                                                                    return Utils.StringCompareIgnoreCase(o1.getProfName(),o2.getProfName());
                                                                }
                                                            }; 
    
    private static final Comparator<ListeLibreItem> ITEM_COMMENT_COMPARATOR = new Comparator<ListeLibreItem>() {
                                                                @Override
                                                                public int compare(ListeLibreItem o1, ListeLibreItem o2) {
                                                                    if (o1 == o2) {return 0;} 
                                                                    else if (o1 == null) {return 1;} 
                                                                    else if (o2 == null) {return -1;}
                                                                    return Utils.StringCompareIgnoreCase(o1.getComment(),o2.getComment());
                                                                }
                                                            };    

    
    private Display display;

    public void bind(final Display display) {
        this.display = display;
        
        display.getListBoxListesLibre().addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                String llId = getCurrentSelectedListeLibreId();
                displayListeLibre(llId);
            }
        });
        
        if(allowFullModif) {
            display.getCreateListLibreButton().addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    createListeLibre();
                }
            });
            display.getDeleteListLibreButton().addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    deleteListeLibre();
                }
            });
            
            TextBoxValidationHandler.Handle(display.getTextBoxListeLibreName(), new TextBoxValidationHandler.Action<String>() {
                @Override
                public void valueChanged(final String newValue) {
                    if(!Utils.IsStringNullOrEmpty(getCurrentSelectedListeLibreId())) {
                        final int index = display.getListBoxListesLibre().getSelectedIndex();
                        ficheEPSService.setListesLibreName(getCurrentSelectedListeLibreId(), newValue,new AsyncCallback<Void>() {
                            
                            @Override
                            public void onFailure(Throwable caught) {
                                ErrorReportEvent.Fire("Impossible d'enregistrer le changement de nom: " + caught.getMessage());
                            }

                            @Override
                            public void onSuccess(Void result) {
                                if(index >0) {
                                    display.getListBoxListesLibre().setItemText(index, newValue);
                                }
                            }
                        });
                    }
                }
            });
            
            TextBoxValidationHandler.Handle(display.getTextBoxListeLibreDescription(), new TextBoxValidationHandler.Action<String>() {
                @Override
                public void valueChanged(String newValue) {
                    ficheEPSService.setListesLibreDescription(getCurrentSelectedListeLibreId(), newValue,new AsyncCallback<Void>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            ErrorReportEvent.Fire("Impossible d'enregistrer le changement de description: " + caught.getMessage());
                        }

                        @Override
                        public void onSuccess(Void result) {
                            //TODO ? upate light list
                        }
                    });
                }
            });
            
        }
        
        
        eleveSearchPresenter.bind(display.getEleveSearchView());
        eleveSearchPresenter.setAddEleveAction(new ElevesSearchPresenter.AddEleveAction() {
            @Override
            public void addEleve(Eleve eleve) {
                String currentSelectedListeLibre = getCurrentSelectedListeLibreId();
                if(Utils.IsStringNullOrEmpty(currentSelectedListeLibre)) {
                    DialogMsg.ShowMessage("Erreur: aucune liste libre n'est sélectionné !", "Veuillez d'abord sélectionner une liste libre.");
                }
                else {
                    ficheEPSService.createListeLibreItemFromEleve(currentSelectedListeLibre,eleve.getOrid(), new AsyncCallback<ListeLibreItem>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            ErrorReportEvent.Fire("Impossible d'ajouter le nouvel élève: " + caught.getMessage());
                        }

                        @Override
                        public void onSuccess(ListeLibreItem result) {
                            itemDataProvider.getList().add(result);
                            itemDataProvider.refresh();
                            eleveSearchPresenter.disableEleve(result.getEleve());
                            updateNbItemsDisplay();
                            //TODO: update list light & count number!
                        }
                    });
                }
            }
        });
        
        display.getTextBoxListeLibreDescription().setReadOnly(!allowFullModif);
        display.getTextBoxListeLibreName().setReadOnly(!allowFullModif);
        display.getCreateListLibreButton().setEnabled(allowFullModif);
        display.getDeleteListLibreButton().setEnabled(allowFullModif);
        display.getCreateListLibreButton().setVisible(allowFullModif);
        display.getDeleteListLibreButton().setVisible(allowFullModif);
    
        
        ColumnSortEvent.ListHandler<ListeLibreItem> columnSortHandler = new ColumnSortEvent.ListHandler<ListeLibreItem>(itemDataProvider.getList());
        display.getDataGridItems().addColumnSortHandler(columnSortHandler);
        
        //dataTable listItem:
        //Photos
        {
                Column<ListeLibreItem, ListeLibreItem> photoColumn = new Column<ListeLibreItem, ListeLibreItem>(new ListeLibreItemPhotoCell()) {
                        @Override
                        public ListeLibreItem getValue(ListeLibreItem object) {
                                return object;
                        }
                };
                photoColumn.setSortable(false);
                display.getDataGridItems().addColumn(photoColumn, "");
                display.getDataGridItems().setColumnWidth(photoColumn,80,Style.Unit.PX);
        }

        { //Id
            Column<ListeLibreItem, String> idColumn = new Column<ListeLibreItem, String>(new TextCell()) {
                                                    @Override
                                                    public String getValue(ListeLibreItem object) {
                                                            return object.getEleve().getId();
                                                    }
                                                };
            idColumn.setDataStoreName("id");
            idColumn.setSortable(true);
            display.getDataGridItems().addColumn(idColumn, "Numen");   
            display.getDataGridItems().setColumnWidth(idColumn, 14, Style.Unit.EX);
            columnSortHandler.setComparator(idColumn, ITEM_ELEVENUMEN_COMPARATOR);
        }

        { //Nom
            Column<ListeLibreItem, String> firstNameColumn = new Column<ListeLibreItem, String>(new TextCell()) {
                                                    @Override
                                                    public String getValue(ListeLibreItem object) {
                                                            return object.getEleve().getNom();
                                                    }
                                                };
            firstNameColumn.setDataStoreName("nom");
            firstNameColumn.setSortable(true);
            display.getDataGridItems().addColumn(firstNameColumn, "Nom");
            display.getDataGridItems().setColumnWidth(firstNameColumn, 11 , Style.Unit.EM);
            columnSortHandler.setComparator(firstNameColumn, ITEM_ELEVENOM_COMPARATOR);
        }
        
        { //Prénom
            Column<ListeLibreItem, String> firstNameColumn = new Column<ListeLibreItem, String>(new TextCell()) {
                                                    @Override
                                                    public String getValue(ListeLibreItem object) {
                                                            return object.getEleve().getPrenom();
                                                    }
                                                };
            firstNameColumn.setDataStoreName("prenom");
            firstNameColumn.setSortable(true);
            display.getDataGridItems().addColumn(firstNameColumn, "Prénom");
            display.getDataGridItems().setColumnWidth(firstNameColumn, 11 , Style.Unit.EM);
            columnSortHandler.setComparator(firstNameColumn, ITEM_ELEVEPRENOM_COMPARATOR);
        }

        { //Classe
            Column<ListeLibreItem, String> column = new Column<ListeLibreItem, String>(new TextCell()) {
                                                    @Override
                                                    public String getValue(ListeLibreItem object) {
                                                            return object.getEleve().getClasse();
                                                    }
                                                };
            column.setDataStoreName("classe");
            column.setSortable(true);
            display.getDataGridItems().addColumn(column, "Classe");
            display.getDataGridItems().setColumnWidth(column, 7, Style.Unit.EM);
            columnSortHandler.setComparator(column, ITEM_ELEVECLASSE_COMPARATOR);
        }    
        
        { //DateDeNaissance
            Column<ListeLibreItem, String> birthdateColumn = new Column<ListeLibreItem, String>(new TextCell()) {
                                                    @Override
                                                    public String getValue(ListeLibreItem object) {
                                                            return Utils.FormatDateDDSlashMMSlashYYYY(object.getEleve().getDateDeNaissance());
                                                    }
                                                };
            birthdateColumn.setDataStoreName("dateDeNaissance");
            birthdateColumn.setSortable(true);
            display.getDataGridItems().addColumn(birthdateColumn, "Date de naissance");
            display.getDataGridItems().setColumnWidth(birthdateColumn, 7, Style.Unit.EM);
            columnSortHandler.setComparator(birthdateColumn, ITEM_ELEVEDATADENAISSANCE_COMPARATOR);
        }
        
        
        { //Prof
            Column<ListeLibreItem, String> column = new Column<ListeLibreItem, String>(new EditTextCell()) {
                                                            @Override
                                                            public String getValue(ListeLibreItem object) {
                                                                return object.getProfName();
                                                            }

                                                        };

            column.setDataStoreName("profName");
            column.setSortable(true);
            display.getDataGridItems().addColumn(column, "Enseignant");
            column.setFieldUpdater(new FieldUpdater<ListeLibreItem, String>() {
                @Override
                public void update(int index, final ListeLibreItem item, final String value) {
                    ficheEPSService.setListesLibreItemProfName(getCurrentSelectedListeLibreId(),item.getId(),value, new AsyncCallback<Void>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            ErrorReportEvent.Fire("Impossible d'enregistrer le nom de l'enseignant: " + caught.getMessage());
                        }

                        @Override
                        public void onSuccess(Void result) {
                            item.setProfName(value);
                            //TODO ?
                        }
                    });   
                }
            });
            display.getDataGridItems().setColumnWidth(column, 13, Style.Unit.EM);
            columnSortHandler.setComparator(column, ITEM_PROFNAME_COMPARATOR);
        }
        
        { //Commentaire
            Column<ListeLibreItem, String> column = new Column<ListeLibreItem, String>(new EditTextCell()) {
                                                            @Override
                                                            public String getValue(ListeLibreItem object) {
                                                                return object.getComment();
                                                            }

                                                        };

            column.setDataStoreName("comment");
            column.setSortable(true);
            display.getDataGridItems().addColumn(column, "Commentaire");
            column.setFieldUpdater(new FieldUpdater<ListeLibreItem, String>() {
                @Override
                public void update(int index, final ListeLibreItem item, final String value) {
                    ficheEPSService.setListesLibreItemComment(getCurrentSelectedListeLibreId(),item.getId(),value, new AsyncCallback<Void>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            ErrorReportEvent.Fire("Impossible d'enregistrer le commentaire: " + caught.getMessage());
                        }

                        @Override
                        public void onSuccess(Void result) {
                            item.setComment(value);
                            //TODO ?
                        }
                    });      
                }
            });
            columnSortHandler.setComparator(column, ITEM_COMMENT_COMPARATOR);
        }
        
        /*{ //Information supplémentaires
            ActionCell<ListeLibreItem> actionCell = new ActionCell<ListeLibreItem>("", new ActionCell.Delegate<ListeLibreItem>() {
                @Override
                public void execute(final ListeLibreItem item) {
                    ElevePopupInfosPresenter.ShowPopup(item.getEleve(), );
                }
            });
            Column<ListeLibreItem, ListeLibreItem> c = new Column<ListeLibreItem, ListeLibreItem>(new TooltipCell<ListeLibreItem>(actionCell,"Informations supplémentaires")) {
                @Override
                public ListeLibreItem getValue(ListeLibreItem object) {
                        return object;
                }
            };
            c.setHorizontalAlignment(HasHorizontalAlignment.HorizontalAlignmentConstant.startOf(HasDirection.Direction.LTR));
            c.setCellStyleNames(Resources.INSTANCE.ficheepsStyle().listeEleveDeleteSprite());
            display.getDataGridItems().addColumn(c, "");
        }*/
        
        { //Delete
            ActionCell<ListeLibreItem> actionCell = new ActionCell<ListeLibreItem>("", new ActionCell.Delegate<ListeLibreItem>() {
                @Override
                public void execute(final ListeLibreItem item) {
                    DialogMsg.ShowOkCancel("Supression d'un élève", "Etes vous sûr de vouloir supprimer l'élève '"
                                    + item.getEleve().getPrettyNomPrenom() + "' ?", new ClickHandler() {
                        @Override
                        public void onClick(ClickEvent event) {
                            deleteItem(item);
                        }
                    }, null);

                }
            });
            Column<ListeLibreItem, ListeLibreItem> c = new Column<ListeLibreItem, ListeLibreItem>(new TooltipCell<ListeLibreItem>(actionCell,"Supprimer l'élève")) {
                @Override
                public ListeLibreItem getValue(ListeLibreItem object) {
                        return object;
                }
            };
            c.setHorizontalAlignment(HasHorizontalAlignment.HorizontalAlignmentConstant.startOf(HasDirection.Direction.LTR));
            c.setCellStyleNames(Resources.INSTANCE.ficheepsStyle().listeEleveDeleteSprite());
            display.getDataGridItems().addColumn(c, "");
            display.getDataGridItems().setColumnWidth(c, 40, Style.Unit.PX);
        }
        

        display.getDataGridItems().setPageSize(Integer.MAX_VALUE);
        display.getDataGridItems().setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.DISABLED);
        itemDataProvider.addDataDisplay(display.getDataGridItems());
        
        display.getButtonExportXLS().setEnabled(false);
        display.getButtonExportXLS().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                exportXls();
            }
        });
        
        display.getButtonExportEmails().setEnabled(false);
        display.getButtonExportEmails().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                exportEmails();
            }
        });
        
    }

    private void updateNbItemsDisplay() {
        String msg = "";
        if(getCurrentSelectedListeLibreId() != null && !getCurrentSelectedListeLibreId().isEmpty()) {
            int size = itemDataProvider.getList().size();
            if(size == 0) {
                msg = "Aucun élève";
            }
            else if(size == 1) {
                msg = "1 élève";
            }
            else {
                msg = "" + size + " élèves";
            }
        }
        display.getNbItemsText().setText(msg);
    }
    
    private void deleteItem(final ListeLibreItem item) {
        ficheEPSService.deleteListeLibreItem(getCurrentSelectedListeLibreId(),item.getId(), new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
                ErrorReportEvent.Fire("Impossible d'enregistrer le commentaire: " + caught.getMessage());
            }

            @Override
            public void onSuccess(Void result) {
                itemDataProvider.getList().remove(item);
                itemDataProvider.refresh();
                eleveSearchPresenter.enableEleve(item.getEleve());
                updateNbItemsDisplay();
                //TODO: update light & count number!
            }
        });            
    }
    
    private String getCurrentSelectedListeLibreId() {
        return display.getListBoxListesLibre().getSelectedValue();
    }
    
    private void createListeLibre() {
        ficheEPSService.createListeLibre(new AsyncCallback<ListeLibre>() {
            @Override
            public void onFailure(Throwable caught) {
                ErrorReportEvent.Fire("Impossible de créer une liste libre: " + caught.getMessage());
            }

            @Override
            public void onSuccess(ListeLibre ll) {
                display.getListBoxListesLibre().addItem(ll.getName(), ll.getId());
                display.getListBoxListesLibre().setSelectedIndex(display.getListBoxListesLibre().getItemCount()-1);
                displayListeLibre(ll.getId());
            }
        });
    }
    
    private void deleteListeLibre() {
        final String selectedLLId = getCurrentSelectedListeLibreId();
        DialogMsg.ShowOkCancel("Supression d'une liste libre", "Etes vous sûr de vouloir supprimer cette liste ?", new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ficheEPSService.deleteListeLibre(selectedLLId, new AsyncCallback<Void>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        ErrorReportEvent.Fire("Impossible de supprimer la liste libre '" + selectedLLId + ")' dans la base: " + caught.getMessage());
                    }
                    @Override
                    public void onSuccess(Void result) {
                        previousSelectedListeLibreId = null;
                        refresh();
                    }
                });
            }
        }, null);        
    }
    
    private void exportXls() {
        String currentListeLibreId = getCurrentSelectedListeLibreId();
        if(!Utils.IsStringNullOrEmpty(currentListeLibreId)) {
            String url = com.google.gwt.core.client.GWT.getModuleBaseURL() + "exportServlet?action=exportListeLibre&listeLibreId="+URL.encodeQueryString(currentListeLibreId)+"&dummyPreventCache=" + System.currentTimeMillis();
            Window.open(url, "_blank", "enabled");
        }
    }
    
    private void exportEmails() {
        if(getCurrentSelectedListeLibreId() != null && !getCurrentSelectedListeLibreId().isEmpty()) {
            List<String> llIds = new LinkedList<String>();
            llIds.add(getCurrentSelectedListeLibreId());
            ElevesListEmailsPresenter presenter = new ElevesListEmailsPresenter(null, null,llIds);
            ElevesListEmailsView view = new ElevesListEmailsView();
            presenter.bind(view);
            view.setSize("640px", "480px");
            DialogMsg.ShowContent("Liste des courriels", view);
        }
    }    
    
    private void displayListeLibre(final String llId) {
        itemDataProvider.getList().clear();
        eleveSearchPresenter.clearDisabledEleve();
        display.getTextBoxListeLibreDescription().setText("");
        display.getTextBoxListeLibreName().setText("");
        display.getTextBoxListeLibreDescription().setEnabled(false);
        display.getTextBoxListeLibreName().setEnabled(false);
        display.getButtonExportXLS().setEnabled(false);
        display.getButtonExportEmails().setEnabled(false);
        
        if(llId != null && !llId.isEmpty()) {
            ficheEPSService.getListeLibreById(llId, new AsyncCallback<ListeLibre>() {
                @Override
                public void onFailure(Throwable caught) {
                    ErrorReportEvent.Fire("Impossible de retrouver la liste libre '" + llId + ")' dans la base: " + caught.getMessage());
                }

                @Override
                public void onSuccess(ListeLibre result) {
                    previousSelectedListeLibreId = llId;
                    itemDataProvider.getList().clear();
                    if(result != null) {
                        itemDataProvider.getList().addAll(result.getItems());
                        itemDataProvider.refresh();
                        
                        for(ListeLibreItem item : result.getItems()) {
                            eleveSearchPresenter.disableEleve(item.getEleve());
                        }

                        display.getTextBoxListeLibreDescription().setText(result.getDescription());
                        display.getTextBoxListeLibreName().setText(result.getName());
                        display.getTextBoxListeLibreDescription().setEnabled(true);
                        display.getTextBoxListeLibreName().setEnabled(true);
                        display.getButtonExportXLS().setEnabled(true);
                        display.getButtonExportEmails().setEnabled(true);
                        updateNbItemsDisplay();
                    }
                }
            });
        }
        
        itemDataProvider.refresh();
        updateNbItemsDisplay();
    }
   


    
    
    
    private class ListeLibreItemPhotoCell extends AbstractCell<ListeLibreItem> {
        public ListeLibreItemPhotoCell() {
            super(com.google.gwt.dom.client.BrowserEvents.CLICK);
        }

        @Override
        public void onBrowserEvent(final Context context, final Element parent, final ListeLibreItem value, NativeEvent event,
                        final ValueUpdater<ListeLibreItem> valueUpdater) {
            super.onBrowserEvent(context, parent, value, event, valueUpdater);

            event.stopPropagation();
            ElevePopupInfosPresenter.ShowPopup(value.getEleve(), parent);
        }
        
        @Override
        public void render(Cell.Context context, ListeLibreItem item, SafeHtmlBuilder sb) {
            if (item == null || item.getEleve() == null) {
                return;
            }
            sb.appendHtmlConstant("<img height='64px' width='64px'  src='" + "ficheeps/elevePhotoServlet?id="
                            + item.getEleve().getId() + "&vUpdate=" + System.currentTimeMillis() + "' />");
        }
    }
    
    
}

package ficheeps.presenter;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Widget;
import ficheeps.client.AppController;

import ficheeps.client.Utils;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.model.Commentaire;
import ficheeps.model.Commentaire.Niveau;
import ficheeps.model.Eleve;
import ficheeps.model.User;
import ficheeps.presenter.CommentaryPresenter.ApsaWizard;
import ficheeps.view.CommentaryView;
import ficheeps.view.DialogMsg;

public class FicheElevePresenter implements HasValueChangeHandlers<Eleve> {

	FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);
	public static boolean APSA_SUGGESTION_LOADED = false;
	
	private Eleve eleve;
	
	private User currentUser;
	public FicheElevePresenter(User currentUser, Eleve eleve) {
		this.eleve = eleve;
		this.currentUser = currentUser;
		
		if(APSA_SUGGESTION_LOADED == false) {
                        APSA_SUGGESTION_LOADED = true; //Single threaded make things simple ...
                        CommentaryView.APSA_ORACLE.clear();
                        if(!AppController.SETTINGS.isConstraintApsaLabel()) {
                            ficheEPSService.getApsaForSuggestion(new AsyncCallback<List<String>>() {
                                    @Override
                                    public void onFailure(Throwable caught) {
                                            DialogMsg.ShowMessage("Une erreur est survenue", "Impossible de charger les suggestions pour les Apsa");
                                    }
                                    @Override
                                    public void onSuccess(List<String> result) {
                                            CommentaryView.APSA_ORACLE.addAll(result);
                                    }
                            });
                        }
		}
	}
	
	
	public interface Display {
            HasText getId();
            HasText getNomPrenom();
            HasText getClasse();
            HasText getDateDeNaissance();
	    //HasWidgets getCommentaries();
            void addCommentary(Widget view);
            void addCommentaryAtPositionZero(Widget view);
	    HasValue<String> getSuiviMedical();
	    HasClickHandlers getAddCommentaryHandler();
	    HasValue<String> getSuggestionFinDeSeconde();
	    void ensureSuggestionFinDeSecondVisible();
	    //HasClickHandlers getFichePDFHandler();
            Button getMoreInfosHandler();
	}

	
	private ApsaWizard commentaryApsaWizard;
	public void setCommentaryApsaWizard(ApsaWizard commentaryApsaWizard) {
		this.commentaryApsaWizard  = commentaryApsaWizard;
	}
	
	private Display display;
	
	

	
	public void bind(final Display display) {
		this.display = display;
		display.getId().setText(eleve.getId());
		display.getNomPrenom().setText((""+eleve.getNom()) + " " + eleve.getPrenom());
		display.getClasse().setText(eleve.getClasse());
		display.getSuiviMedical().setValue(eleve.getSuiviMedical());
		display.getSuggestionFinDeSeconde().setValue(eleve.getSuggestionApresSeconde());
		DateTimeFormat birthFormat = DateTimeFormat.getFormat("dd/MM/yyyy");
		
		//Wed May 12 00:00:00 WEST 1976
		display.getDateDeNaissance().setText(Utils.FormatDateDDSlashMMSlashYYYY(eleve.getDateDeNaissance())
											+ " (" + Eleve.GetAge(eleve.getDateDeNaissance()) + " ans)"
											/*+ " " + eleve.getDateDeNaissance() 
											+ " # " + eleve.getDateDeNaissance().getTime()
											+ " # " + eleve.getDateDeNaissance().getTimezoneOffset()
											+ " # " + Utils.SERVER_TIMEZONE_OFFSET*/);
		
		if(eleve.getCommentaires() != null && !eleve.getCommentaires().isEmpty()) {
                    Collections.sort(eleve.getCommentaires(), new Commentaire.NiveauComparator(false));
			for(Commentaire c : eleve.getCommentaires()) {
				CommentaryPresenter cp = new CommentaryPresenter(c);
				cp.setApsaWizard(commentaryApsaWizard);
				cp.addValueChangeHandler(new ValueChangeHandler<Commentaire>() {
					@Override
					public void onValueChange(ValueChangeEvent<Commentaire> event) {
						ValueChangeEvent.fire(FicheElevePresenter.this, eleve);
					}
				});
				CommentaryView cv = new CommentaryView();
				cp.bind(cv);
				//display.getCommentaries().add(cv);
				display.addCommentary(cv);
			}
		}
		
		display.getAddCommentaryHandler().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Commentaire c = createNewCommentary();
				eleve.addCommentaire(c);
				CommentaryPresenter cp = new CommentaryPresenter(c);
				cp.setApsaWizard(commentaryApsaWizard);
				cp.addValueChangeHandler(new ValueChangeHandler<Commentaire>() {
					@Override
					public void onValueChange(ValueChangeEvent<Commentaire> event) {
						ValueChangeEvent.fire(FicheElevePresenter.this, eleve);
					}
				});
				CommentaryView cv = new CommentaryView();
				cp.bind(cv);
				display.addCommentaryAtPositionZero(cv);
			}
		});
		
		display.getSuiviMedical().addValueChangeHandler(new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				eleve.setSuiviMedical(event.getValue());
				ValueChangeEvent.fire(FicheElevePresenter.this, eleve);
			}
		});

		display.getSuggestionFinDeSeconde().addValueChangeHandler(new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				eleve.setSuggestionApresSeconde(event.getValue());
				ValueChangeEvent.fire(FicheElevePresenter.this, eleve);
			}
		});
		
                /*
		display.getFichePDFHandler().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				exportPDF();
			}
		});*/
                
                display.getMoreInfosHandler().addClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        displayMoreInfosPopup(display.getMoreInfosHandler());
                    }
                });
		
		ensureSuggestionFinDeSecondIsVisible();
	}	
	
	
	/*
	private void exportPDF() {
		final ExportPDFPresenter exportPDFPresenter = new ExportPDFPresenter();
		exportPDFPresenter.setExport(ExportType.ExportEleve, eleve.getId());
		ExportPDFView exportPDFView = new ExportPDFView();
		exportPDFPresenter.bind(exportPDFView);
	
		DialogMsg.ShowOkCancel("Export PDF", 
				exportPDFView, 
				new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						exportPDFPresenter.startExport();
					}
				}, 
				null);
	}*/
	
	
	
        private void displayMoreInfosPopup(Widget locationRelativeTo) {
            ElevePopupInfosPresenter.ShowPopup(eleve, locationRelativeTo);
        }
        
	private void ensureSuggestionFinDeSecondIsVisible() {
            if(eleve.getCommentaires() != null) {
		for(Commentaire c : eleve.getCommentaires()) {
			if(c.getNiveau() == Niveau.Seconde 
					|| c.getNiveau() == Niveau.Premiere
					|| c.getNiveau() == Niveau.Terminale) {
				display.ensureSuggestionFinDeSecondVisible();
				break;
			}
		}
            }
	}

	
	private Commentaire createNewCommentary() {
		Commentaire c = new Commentaire();
		if(currentUser != null) {
			c.setProfesseur("" + currentUser.getFullName());
		}
		c.setAnneeScolaire(Commentaire.CurrentAnneeScolaire());
		c.setNiveau(Commentaire.Niveau.GetNiveauForClasse(eleve.getClasse()));
		c.setDateCreation(new Date());		
		return c;
	}


	EventBus bus = new SimpleEventBus();
	
	@Override
	public HandlerRegistration addValueChangeHandler(ValueChangeHandler<Eleve> handler) {
		return bus.addHandler(ValueChangeEvent.getType(), handler);
	}

	@Override
	public void fireEvent(GwtEvent<?> event) {
		bus.fireEvent(event);
	}

}

package ficheeps.presenter;

import java.util.List;

import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.view.client.ListDataProvider;

import ficheeps.client.ErrorReportEvent;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.model.User;
import ficheeps.view.DialogMsg;


public class UserListPresenter {

	
	private ListDataProvider<User> localUserDataProvider = new ListDataProvider<User>();
	private ListDataProvider<User> openIdDataProvider = new ListDataProvider<User>();

	private FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);

	
	public interface Display {
		DataGrid<User> getOpenIDUserTable(); 
		DataGrid<User> getLocalUserTable();
		
		HasClickHandlers getNewOpenIdUser();
		HasClickHandlers getNewLocalUser();
	}


	public void bind(Display display) {
		
		display.getNewLocalUser().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				newUser(true);
			}
		});
		display.getNewOpenIdUser().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				newUser(false);
			}
		});
		
		setupLocalTable(display);
		setupOpenIdTable(display);
	}
	
	
	private void newUser(final boolean local) {
            ficheEPSService.createUser(new AsyncCallback<User>() {
                @Override
                public void onFailure(Throwable caught) {
                    ErrorReportEvent.Fire("Impossible de créer un nouvel utilisateur: '" + caught.getMessage() + "'");
                }

                @Override
                public void onSuccess(User user) {
                    user.setFullName("nom complet");
                    user.setLogin("login");
                    if(local) {
                        user.setLocal(true);
                        user.setPwd("" + System.currentTimeMillis());
                    }
                    else {
                        user.setLocal(false);
                    }
                    save(user);
                }
            });
	}
	

	private void setupLocalTable(Display display) {
		{
			Column<User, String> c = new Column<User, String>(new EditTextCell()) {
				@Override
				public String getValue(User object) {
					return object.getFullName();
				}
			};
			c.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
			display.getLocalUserTable().addColumn(c, "Nom/Prenom");
			c.setFieldUpdater(new FieldUpdater<User, String>() {
				@Override
				public void update(int index, User object, String value) {
					object.setFullName(value);
					save(object);
				}
			});
		}
		{
			Column<User, String> c = new Column<User, String>(new EditTextCell()) {
				@Override
				public String getValue(User object) {
					return object.getLogin();
				}
			};
			c.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
			display.getLocalUserTable().addColumn(c, "Login");
			c.setFieldUpdater(new FieldUpdater<User, String>() {
				@Override
				public void update(int index, User object, String value) {
					object.setLogin(value);
					save(object);
				}
			});
		}
		{
			Column<User, String> c = new Column<User, String>(new EditTextCell()) {
				@Override
				public String getValue(User object) {
					return "*******";
				}
			};
			c.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
			display.getLocalUserTable().addColumn(c, "Mot de passe");
			c.setFieldUpdater(new FieldUpdater<User, String>() {
				@Override
				public void update(int index, User object, String value) {
					object.setPwd(ficheeps.client.Utils.Sha1(value));
					save(object);
				}
			});
		}
		{
			Column<User, Boolean> c = new Column<User, Boolean>(new CheckboxCell()) {
				@Override
				public Boolean getValue(User object) {
					return object.isAdmin();
				}
			};
			c.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
			display.getLocalUserTable().addColumn(c, "Admin.");
			display.getLocalUserTable().setColumnWidth(c, 6, Unit.EM);
			c.setFieldUpdater(new FieldUpdater<User, Boolean>() {
				@Override
				public void update(int index, User object, Boolean value) {
					object.setAdmin(value);
					save(object);
				}
			});
		}
		{
			ActionCell<User> actionCell = new ActionCell<User>("supprimer", new ActionCell.Delegate<User>() {
			      @Override
			      public void execute(final User user) {
			    	  DialogMsg.ShowOkCancel("Supression d'un utilisateur","Etes vous certains de vouloir supprimer l'utilistaeur '" + user.getLogin() + "' ?", 
			    			  new ClickHandler() {
								@Override
								public void onClick(ClickEvent event) {
									delete(user);
								}
							}, null);
			      }
			    });
			Column<User,User> c = new Column<User,User>(actionCell){
			      @Override
			      public User getValue(User object) {
			        return object;
			      }
			};
			c.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
			display.getLocalUserTable().addColumn(c, "");
			display.getLocalUserTable().setColumnWidth(c, 9, Unit.EM);
		}
		
		localUserDataProvider.addDataDisplay(display.getLocalUserTable());
	}

	
	
	
	private Display display;
	
	private void setupOpenIdTable(Display display) {
		this.display = display;
		{
			Column<User, String> c = new Column<User, String>(new EditTextCell()) {
				@Override
				public String getValue(User object) {
					return object.getLogin();
				}
			};
			c.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
			display.getOpenIDUserTable().addColumn(c, "Login");
			c.setFieldUpdater(new FieldUpdater<User, String>() {
				@Override
				public void update(int index, User object, String value) {
					object.setLogin(value);
					save(object);
				}
			});	
		}
		{
			Column<User, Boolean> c = new Column<User, Boolean>(new CheckboxCell()) {
				@Override
				public Boolean getValue(User object) {
					return object.isAdmin();
				}
			};
			c.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
			display.getOpenIDUserTable().addColumn(c, "Admin.");
			display.getOpenIDUserTable().setColumnWidth(c, 6, Unit.EM);
			c.setFieldUpdater(new FieldUpdater<User, Boolean>() {
				@Override
				public void update(int index, User object, Boolean value) {
					object.setAdmin(value);
					save(object);
				}
			});	
		}
		{
			ActionCell<User> actionCell = new ActionCell<User>("supprimer", new ActionCell.Delegate<User>() {
			      @Override
			      public void execute(final User user) {
			    	  DialogMsg.ShowOkCancel("Supression d'un utilisateur","Etes vous certains de vouloir supprimer l'utilistaeur '" + user.getLogin() + "' ?", 
			    			  new ClickHandler() {
								@Override
								public void onClick(ClickEvent event) {
									delete(user);
								}
							}, null);
			    	  
			      }
			    });
			Column<User,User> c = new Column<User,User>(actionCell){
			      @Override
			      public User getValue(User object) {
			        return object;
			      }
			};
			c.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
			display.getOpenIDUserTable().addColumn(c, "");
			display.getOpenIDUserTable().setColumnWidth(c, 9, Unit.EM);
		}
		
		openIdDataProvider.addDataDisplay(display.getOpenIDUserTable());
	}
	
	
	public void refresh() {
		openIdDataProvider.getList().clear();
		localUserDataProvider.getList().clear();
		ficheEPSService.retrieveAllUsers(new AsyncCallback<List<User>>() {
			@Override
			public void onSuccess(List<User> result) {
				for(User u : result) {
					if(u.isLocal()) {
						localUserDataProvider.getList().add(u);
					}
					else {
						openIdDataProvider.getList().add(u);
					}
				}
				openIdDataProvider.refresh();
				localUserDataProvider.refresh();
			}
			@Override
			public void onFailure(Throwable caught) {
				ErrorReportEvent.Fire("Impossible de retrouver la liste des utilisateurs: '" + caught.getMessage() + "'");
			}
		});
	}
	
	
	private void delete(final User user) {
		ficheEPSService.deleteUser(user, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				ErrorReportEvent.Fire("Erreur lors suppression de l'utilisateur: '" + caught.getMessage() + "'");
			}
			@Override
			public void onSuccess(Void result) {
				ListDataProvider<User> ldpu = null;
				if(user.isLocal()) {
					ldpu = localUserDataProvider;
				}
				else {
					ldpu = openIdDataProvider;
				}
				ldpu.getList().remove(user);
				ldpu.refresh();
			}
		});
	}
	
	private void save(final User user) {
		ficheEPSService.updateUserFirstLevel(user, new AsyncCallback<User>() {
			@Override
			public void onFailure(Throwable caught) {
				ErrorReportEvent.Fire("Impossible de sauver l'utilisateur: '" + caught.getMessage() + "'");
			}
			@Override
			public void onSuccess(User result) {
				ListDataProvider<User> ldpu = null;
				if(user.isLocal()) {
					ldpu = localUserDataProvider;
				}
				else {
					ldpu = openIdDataProvider;
				}
				
				int index = ldpu.getList().indexOf(user);
				if(index >= 0) {
                                    ldpu.getList().set(index, result);
				}
				else {
                                    ldpu.getList().add(0, result);
				}
				
				ldpu.refresh();
				
				if(index >= 0) {
					if(result.isLocal()) {
						display.getLocalUserTable().setKeyboardSelectedRow(index, true);
					}
					else {
						display.getOpenIDUserTable().setKeyboardSelectedRow(index, true);	
					}
				}
			}
		});
	}
	

}

package ficheeps.presenter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.TextArea;
import ficheeps.client.ErrorReportEvent;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.model.ListPrettyMail;

import java.util.LinkedList;
import java.util.List;

public class ElevesListEmailsPresenter {

    final private FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);
    final private List<String> classes = new LinkedList<String>();
    final private List<String> groupes = new LinkedList<String>();
    final private List<String> listesLibres = new LinkedList<String>();
    
    public ElevesListEmailsPresenter(List<String> classes,List<String> groupes,List<String> listeLibres) {
        if(classes != null) {
            this.classes.addAll(classes);
        }
        if(groupes != null) {
            this.groupes.addAll(groupes);
        }
        if(listeLibres != null) {
            this.listesLibres.addAll(listeLibres);
        }
    }
    
    public interface Display {
        TabLayoutPanel getTabLayoutPanel();
        TextArea getMaillingText();
        TextArea getListMailText();
    }

    public void bind(final Display display) {
        ficheEPSService.retrieveAllMails(classes,groupes,listesLibres, new AsyncCallback<ListPrettyMail>() {
            @Override
            public void onFailure(Throwable caught) {
                ErrorReportEvent.Fire("Impossible de retrouver la liste des mails: " + caught.getMessage());
            }

            @Override
            public void onSuccess(ListPrettyMail result) {
                display.getMaillingText().setText(result.getMaillingPrettyMails());
                display.getListMailText().setText(result.getListPrettyMails());
            }
        });
    }

    
}

package ficheeps.presenter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ButtonBase;
import com.google.gwt.user.client.ui.TextBox;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;

import ficheeps.model.Eleve;
import ficheeps.view.DialogMsg;
import ficheeps.view.utils.ChangePoller;

public class EleveEmailPresenter {

    FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);
    Display display;
    private final Eleve eleve;
    
    public EleveEmailPresenter(Eleve eleve) {
        this.eleve = eleve;
    }

    public interface Display {
        TextBox getMailEleveTextBox();
        ButtonBase getMailEleveSaveButton();
    }

    public void bind(final Display display) {
        this.display = display;
        display.getMailEleveTextBox().setText(eleve.getEmails());
        display.getMailEleveSaveButton().setVisible(false);
        final ChangePoller<String> changePoller = new ChangePoller<String>(display.getMailEleveTextBox(), new ChangePoller.ChangePollerDelegate<String>() {
            @Override
            public String getValue() {
                return display.getMailEleveTextBox().getText();
            }
            @Override
            public void valueChanged(String initialValue, String newValue) {
                display.getMailEleveSaveButton().setVisible(true);
            }
        });
        
        display.getMailEleveSaveButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                saveMail();
            }
        });
    }    

    private void saveMail() {
        try {
            ficheEPSService.setEleveEmails(eleve.getOrid(), display.getMailEleveTextBox().getText(), new AsyncCallback<String>() {
                @Override
                public void onFailure(Throwable caught) {
                    DialogMsg.ShowMessage("Erreur", caught.getMessage());
                }
                @Override
                public void onSuccess(String result) {
                    eleve.setEmails(result);
                    display.getMailEleveTextBox().setText(result);
                    display.getMailEleveSaveButton().setVisible(false);
                }
            });
        }
        catch(Exception e) {
            DialogMsg.ShowMessage("Erreur", e.getMessage());
        }
    }


}

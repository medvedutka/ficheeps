package ficheeps.presenter;

import java.util.Comparator;
import java.util.List;

import org.moxieapps.gwt.uploader.client.Uploader;
import org.moxieapps.gwt.uploader.client.events.FileQueuedEvent;
import org.moxieapps.gwt.uploader.client.events.FileQueuedHandler;
import org.moxieapps.gwt.uploader.client.events.UploadErrorEvent;
import org.moxieapps.gwt.uploader.client.events.UploadErrorHandler;
import org.moxieapps.gwt.uploader.client.events.UploadStartEvent;
import org.moxieapps.gwt.uploader.client.events.UploadStartHandler;
import org.moxieapps.gwt.uploader.client.events.UploadSuccessEvent;
import org.moxieapps.gwt.uploader.client.events.UploadSuccessHandler;

import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.ListDataProvider;

import ficheeps.client.ErrorReportEvent;
import ficheeps.client.Utils;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.model.BackupFile;
import ficheeps.view.DialogMsg;


public class DBBackupPresenter {

	private FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);

	
	public interface Display {
		HasClickHandlers getCreateBackupClickHandler();
		DataGrid<BackupFile> getBackupList();
		Uploader getRestoreBackupFromLocalFile();
	}
	
	private ListDataProvider<BackupFile> listDataProviderBackupFile = new ListDataProvider<BackupFile>();
	
	public DBBackupPresenter() {
	}
	
		
	public void bind(Display display) {
		
		ListHandler<BackupFile> columnSortHandler = new ListHandler<BackupFile>(listDataProviderBackupFile.getList());

		
		display.getCreateBackupClickHandler().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				createBackup();
			}
		});
		
		{
			Column<BackupFile, String> c = new Column<BackupFile, String>(new TextCell()) {
				@Override
				public String getValue(BackupFile object) {
					return object.getName();
				}
			};
			c.setSortable(true);
			columnSortHandler.setComparator(c,
			        new Comparator<BackupFile>() {
						@Override
				          public int compare(BackupFile o1, BackupFile o2) {
				            if (o1 == o2) {
				              return 0;
				            }

			            // Compare the name columns.
			            if (o1 != null) {
			              return (o2 != null) ? o1.getName().compareTo(o2.getName()) : 1;
			            }
			            return -1;
			          }
			        });		
			display.getBackupList().addColumn(c, "Nom");
		}
		{
			Column<BackupFile, String> c = new Column<BackupFile, String>(new TextCell()) {
				@Override
				public String getValue(BackupFile object) {
					return object.getDate();
				}
			};
			c.setSortable(true);
			columnSortHandler.setComparator(c,
			        new Comparator<BackupFile>() {
						@Override
				          public int compare(BackupFile o1, BackupFile o2) {
				            if (o1 == o2) {
				              return 0;
				            }

			            // Compare the name columns.
			            if (o1 != null) {
			              return (o2 != null) ? o1.getDate().compareTo(o2.getDate()) : 1;
			            }
			            return -1;
			          }
			        });	
			display.getBackupList().addColumn(c, "Date");
		}
		{
			Column<BackupFile, String> c = new Column<BackupFile, String>(new TextCell()) {
				@Override
				public String getValue(BackupFile object) {
					return ""+ ((int)(object.getSize() / 1024)) +" Ko";
				}
			};
			display.getBackupList().addColumn(c, "Taille");
		}
		{
			ActionCell<BackupFile> actionCell = new ActionCell<BackupFile>("restaurer", new ActionCell.Delegate<BackupFile>() {
			      @Override
			      public void execute(final BackupFile backupFile) {
			    	  DialogMsg.ShowOkCancel("Restauration d'une sauvegarde"
			    			  				, "Etes-vous sûr de vouloir écraser la base existante avec la sauvegarde '" +backupFile.getName() + "' ?"
			    			  				, new ClickHandler() {
						@Override
						public void onClick(ClickEvent event) {
							//DialogMsg.ShowPasswordTextBox("Entrez le mot de passe de la sauvegarde:", new DialogMsg.OkCancelValueCallback<String>() {
							//	@Override
							//	public void onOk(String value) {
									restoreBackup(backupFile);
							//	}
							//});
						}
					}, null);
			      }
			    });
			Column<BackupFile,BackupFile> c = new Column<BackupFile,BackupFile>(actionCell){
			      @Override
			      public BackupFile getValue(BackupFile object) {
			        return object;
			      }
			};
			display.getBackupList().addColumn(c, "");
		}
		{
			ActionCell<BackupFile> actionCell = new ActionCell<BackupFile>("télécharger", new ActionCell.Delegate<BackupFile>() {
			      @Override
			      public void execute(BackupFile backupFile) {
			    	  String url = com.google.gwt.core.client.GWT.getModuleBaseURL() + "downloadBackupServlet?backupFile="+backupFile.getFileName() +"&dummyPreventCache="+System.currentTimeMillis();
			    	  Window.open(url, "_blank", "enabled");
			      }
			    });
			Column<BackupFile,BackupFile> c = new Column<BackupFile,BackupFile>(actionCell){
			      @Override
			      public BackupFile getValue(BackupFile object) {
			        return object;
			      }
			};
			display.getBackupList().addColumn(c, "");
		}
		{
			ActionCell<BackupFile> actionCell = new ActionCell<BackupFile>("supprimer", new ActionCell.Delegate<BackupFile>() {
			      @Override
			      public void execute(final BackupFile backupFile) {
			    	  DialogMsg.ShowOkCancel("Suppression d'une sauvegarde"
  			  				, "Etes-vous sûr de vouloir supprimer la sauvegarde '" +backupFile.getName() + "' ?"
  			  				, new ClickHandler() {
						@Override
						public void onClick(ClickEvent event) {
							 deleteBackup(backupFile);
						}
					}, null);
			      }
			    });
			Column<BackupFile,BackupFile> c = new Column<BackupFile,BackupFile>(actionCell){
			      @Override
			      public BackupFile getValue(BackupFile object) {
			        return object;
			      }
			};
			display.getBackupList().addColumn(c, "");
		}

		listDataProviderBackupFile.addDataDisplay(display.getBackupList());
		display.getBackupList().addColumnSortHandler(columnSortHandler);
		
		final String url = GWT.getModuleBaseURL() + "downloadBackupServlet?importFile=true";
		final Uploader uploader = display.getRestoreBackupFromLocalFile();
		uploader.setUploadURL(url);
		uploader.setFileQueuedHandler(new FileQueuedHandler() {
			@Override
			public boolean onFileQueued(FileQueuedEvent fileQueuedEvent) {
				DialogMsg.ShowPasswordTextBox("Entrez le mot de passe de la sauvegarde:", new DialogMsg.OkCancelValueCallback<String>() {
					@Override
					public void onOk(String value) {
                                            uploader.setUploadURL(url);
                                            if(value != null && !value.isEmpty()) {
                                                String sha1pwd = Utils.Sha1(value);
                                                uploader.setUploadURL(url+"&pwd=" + sha1pwd);

                                                /*JSONObject params = new JSONObject();
                                                params.put("importFile", new JSONString("true"));
                                                params.put("pwd", new JSONString(sha1pwd));
                                                uploader.setPostParams(params);*/
                                            }
                                            uploader.startUpload();
					}
				});
				
				return false;
			}
		});
		uploader.setUploadStartHandler(new UploadStartHandler() {
			@Override
			public boolean onUploadStart(UploadStartEvent uploadStartEvent) {
				wd = DialogMsg.ShowWaiter("Import", "Import en cours, merci de patienter");
				return false;
			}
		});
		uploader.setUploadErrorHandler(new UploadErrorHandler() {
			@Override
			public boolean onUploadError(UploadErrorEvent uploadErrorEvent) {
				if(wd!=null) {
					wd.hide();
					wd = null;
				}
				DialogMsg.ShowMessage("Erreur","Erreur lors de l'import: " + uploadErrorEvent.getMessage());
				return false;
			}
		});
		uploader.setUploadSuccessHandler(new UploadSuccessHandler() {
			@Override
			public boolean onUploadSuccess(UploadSuccessEvent uploadSuccessEvent) {
				if(wd!=null) {
					wd.hide();
					wd = null;
				}
				DialogMsg.ShowMessage("Import terminé","Import terminé avec succés.");
				return false;
			}
		});
	}
	
	private DialogMsg wd;
	
	
	public void refresh() {
		listDataProviderBackupFile.getList().clear();
		ficheEPSService.getAllBackupFiles(new AsyncCallback<List<BackupFile>>() {
			@Override
			public void onSuccess(List<BackupFile> result) {
				for(BackupFile bf : result) {
					listDataProviderBackupFile.getList().add(bf);
				}
				listDataProviderBackupFile.refresh();
			}
			@Override
			public void onFailure(Throwable caught) {
				ErrorReportEvent.Fire("Impossible de retrouver la liste des sauvegardes: '" + caught.getMessage() + "'");
			}
		});
	}
	
	private void restoreBackup(BackupFile bf) {
		ficheEPSService.restoreBackup(bf, new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				ErrorReportEvent.Fire("Erreur lors de la restauration de la sauvegarde: '" + caught.getMessage() + "'");
			}
			@Override
			public void onSuccess(String result) {
			}
		});
	}
	private void deleteBackup(BackupFile bf) {
		ficheEPSService.deleteBackupFile(bf, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				ErrorReportEvent.Fire("Erreur lors de la supression de la sauvegarde: '" + caught.getMessage() + "'");
			}
			@Override
			public void onSuccess(Void result) {
				refresh();
			}
		});
	}
	
	private void createBackup() {
		final DialogMsg wd = DialogMsg.ShowWaiter("Création de la sauvegarde", "Merci de patienter");
		ficheEPSService.createBackup(new AsyncCallback<BackupFile>() {
			@Override
			public void onFailure(Throwable caught) {
				wd.hide();
				ErrorReportEvent.Fire("Impossible de créer la sauvegarde: '"+caught.getMessage()+"'");
			}
			@Override
			public void onSuccess(BackupFile result) {
				refresh();
				wd.hide();
			}
		});
	}
}

package ficheeps.presenter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.VerticalPanel;

import ficheeps.client.ErrorReportEvent;
import ficheeps.client.Utils;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.model.Eleve;
import ficheeps.view.EleveView;
import java.util.Date;

public class CreateNewElevePresenter {

	private FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);

	
	public CreateNewElevePresenter() {	
	}

        public interface EleveCreatedHandler {
            void onEleveCreated(Eleve eleve);
        }
	
	private EleveView display;
	private DialogBox dialogBox;
	private EleveCreatedHandler eleveCreatedHandler = null;
        
	public void showCreateDialog(EleveCreatedHandler handler) {
            eleveCreatedHandler = handler;
            display = new EleveView();
            dialogBox = new DialogBox();
            dialogBox.setText("Création d'un nouvel élève");
	    VerticalPanel dialogContents = new VerticalPanel();
	    dialogContents.setSpacing(4);
		dialogBox.setWidget(dialogContents);
		dialogContents.add(display);
		
	    Button okButton = new Button("Créer", new ClickHandler() {
	          public void onClick(ClickEvent event) {
	        	createAndSaveEleve();
	          }
	        });
	    Button cancelButton = new Button("Annuler", new ClickHandler() {
	          public void onClick(ClickEvent event) {
	            dialogBox.hide();
	          }
	        });
	    
	    FlowPanel buttonPanel = new FlowPanel();
	    buttonPanel.add(cancelButton);
	    buttonPanel.add(okButton);
	    
	    dialogContents.add(buttonPanel);
	    if (LocaleInfo.getCurrentLocale().isRTL()) {
	      dialogContents.setCellHorizontalAlignment(
	    		  buttonPanel, HasHorizontalAlignment.ALIGN_LEFT);
	    } else {
	      dialogContents.setCellHorizontalAlignment(
	    		  buttonPanel, HasHorizontalAlignment.ALIGN_RIGHT);
	    }
	    
	    dialogBox.setGlassEnabled(true);
	    dialogBox.setAnimationEnabled(true);
            dialogBox.center();
            dialogBox.show();
	}
	
	private void createAndSaveEleve() {
                String id = Utils.StringToTrimedOrEmpty(display.getId().getValue());
                if(Utils.IsStringNullOrEmpty(id)) {
                    ErrorReportEvent.Fire("Le NUMEN ne peut pas être vide.");
                    return;
                }
                String nom = Utils.StringToTrimedOrEmpty(display.getNom().getValue());
                if(Utils.IsStringNullOrEmpty(nom)) {
                    ErrorReportEvent.Fire("Le nom ne peut pas être vide.");
                    return;
                }
                String prenom = Utils.StringToTrimedOrEmpty(display.getPrenom().getValue());
                if(Utils.IsStringNullOrEmpty(prenom)) {
                    ErrorReportEvent.Fire("Le prénom ne peut pas être vide.");
                    return;
                }
                Date date = display.getDateDeNaissance().getValue();
                if(date == null) {
                    ErrorReportEvent.Fire("La date de naissance ne peut pas être vide.");
                    return;                    
                }
                
		final Eleve eleve = new Eleve();
		eleve.setId(id);
		eleve.setNom(nom);
		eleve.setPrenom(prenom);
                date.setHours(12);
                date.setMinutes(0);
                date.setSeconds(0);
		eleve.setDateDeNaissance(date);
		eleve.setClasse(display.getClasse().getValue());
		ficheEPSService.createEleveLike(eleve, new AsyncCallback<Eleve>() {
                    @Override
                    public void onSuccess(Eleve result) {
                        dialogBox.hide();
                        if(eleveCreatedHandler != null) {
                            eleveCreatedHandler.onEleveCreated(result);
                        }
                    }
                    @Override
                    public void onFailure(Throwable caught) {
                        ErrorReportEvent.Fire("Impossible de créer l'élève: '" + caught.getMessage()+"'");
                    }
		});
	}
}

package ficheeps.presenter;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.TakesValue;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.view.client.CellPreviewEvent;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import ficheeps.client.Utils;
import ficheeps.model.IStatisticsFilter;
import ficheeps.model.StatisticsFilter;
import ficheeps.view.DialogMsg;
import java.util.HashSet;

public class StatisticsFilterPresenter {

    private final StatisticsFilter filter;

    public StatisticsFilterPresenter(StatisticsFilter filter) {
        this.filter = filter;
    }

    public interface Display {
        CellTable getListNiveau();
        CellTable getListAnnee();
        CellTable getListAPSA();
        CheckBox getCheckBoxNiveau();
        CheckBox getCheckBoxAnnee();
        CheckBox getCheckBoxAPSA();
        TextBox getSearchAPSA();
        Button getDownloadButton();
        FormPanel getDownloadFormPanel();
        TakesValue<String> getFilterObjectHiddenOption();
        TakesValue<String> getActionHiddenOption();
        TakesValue<String> getDummyPreventCacheHiddenOption();
    }

    private Display display;
    private final ListDataProvider<String> apsaProvider = new ListDataProvider<String>();
    private final ListDataProvider<String> anneeProvider = new ListDataProvider<String>();
    private final ListDataProvider<String> niveauProvider = new ListDataProvider<String>();
    private final MultiSelectionModel<String> apsaSelectionModel = new MultiSelectionModel<String>();
    private final MultiSelectionModel<String> anneeSelectionModel = new MultiSelectionModel<String>();
    private final MultiSelectionModel<String> niveauSelectionModel = new MultiSelectionModel<String>();
        
    public void bind(Display _display) {
        this.display = _display;

        apsaProvider.getList().addAll(filter.getApsas());
        anneeProvider.getList().addAll(filter.getAnnees());
        niveauProvider.getList().addAll(filter.getNiveaux());
        
        display.getListAnnee().setSelectionModel(apsaSelectionModel, DefaultSelectionEventManager.<String>createCustomManager(new DefaultSelectionEventManager.EventTranslator<String>() {
            @Override
            public boolean clearCurrentSelection(CellPreviewEvent<String> event) {
                return false;
            }
            @Override
            public DefaultSelectionEventManager.SelectAction translateSelectionEvent(CellPreviewEvent<String> event) {
                return DefaultSelectionEventManager.SelectAction.TOGGLE;
            }
        }));
        display.getListNiveau().setSelectionModel(apsaSelectionModel, DefaultSelectionEventManager.<String>createCustomManager(new DefaultSelectionEventManager.EventTranslator<String>() {
            @Override
            public boolean clearCurrentSelection(CellPreviewEvent<String> event) {
                return false;
            }
            @Override
            public DefaultSelectionEventManager.SelectAction translateSelectionEvent(CellPreviewEvent<String> event) {
                return DefaultSelectionEventManager.SelectAction.TOGGLE;
            }
        }));        
        
        // APSA
        {
            display.getListAPSA().setSelectionModel(apsaSelectionModel, DefaultSelectionEventManager.<String>createCustomManager(new DefaultSelectionEventManager.EventTranslator<String>() {
                @Override
                public boolean clearCurrentSelection(CellPreviewEvent<String> event) {
                    return false;
                }
                @Override
                public DefaultSelectionEventManager.SelectAction translateSelectionEvent(CellPreviewEvent<String> event) {
                    return DefaultSelectionEventManager.SelectAction.TOGGLE;
                }
            }));        
            Column<String, Boolean> checkColumn = new Column<String, Boolean>(new CheckboxCell(true, false)) {
                @Override
                public Boolean getValue(String object) {
                    return apsaSelectionModel.isSelected(object);
                }
            };
            display.getListAPSA().addColumn(checkColumn);
            //display.getListAPSA().setColumnWidth(checkColumn, 10, Style.Unit.PX);
            TextColumn<String> labelColumn = new TextColumn<String>() {
                @Override
                public String getValue(String object) {
                    return object;
                }
            };
            display.getListAPSA().addColumn(labelColumn);
            apsaProvider.addDataDisplay(display.getListAPSA());
            apsaProvider.refresh();
        }
        
        // Annees
        {
            display.getListAnnee().setSelectionModel(anneeSelectionModel, DefaultSelectionEventManager.<String>createCustomManager(new DefaultSelectionEventManager.EventTranslator<String>() {
                @Override
                public boolean clearCurrentSelection(CellPreviewEvent<String> event) {
                    return false;
                }
                @Override
                public DefaultSelectionEventManager.SelectAction translateSelectionEvent(CellPreviewEvent<String> event) {
                    return DefaultSelectionEventManager.SelectAction.TOGGLE;
                }
            }));        
            Column<String, Boolean> checkColumn = new Column<String, Boolean>(new CheckboxCell(true, false)) {
                @Override
                public Boolean getValue(String object) {
                    return anneeSelectionModel.isSelected(object);
                }
            };
            display.getListAnnee().addColumn(checkColumn);
            //display.getListAnnee().setColumnWidth(checkColumn, 10, Style.Unit.PX);
            TextColumn<String> labelColumn = new TextColumn<String>() {
                @Override
                public String getValue(String object) {
                    return object;
                }
            };
            display.getListAnnee().addColumn(labelColumn);
            anneeProvider.addDataDisplay(display.getListAnnee());
            anneeProvider.refresh();        
        }
    
        // Niveau
        {
            display.getListNiveau().setSelectionModel(niveauSelectionModel, DefaultSelectionEventManager.<String>createCustomManager(new DefaultSelectionEventManager.EventTranslator<String>() {
                @Override
                public boolean clearCurrentSelection(CellPreviewEvent<String> event) {
                    return false;
                }
                @Override
                public DefaultSelectionEventManager.SelectAction translateSelectionEvent(CellPreviewEvent<String> event) {
                    return DefaultSelectionEventManager.SelectAction.TOGGLE;
                }
            }));        
            Column<String, Boolean> checkColumn = new Column<String, Boolean>(new CheckboxCell(true, false)) {
                @Override
                public Boolean getValue(String object) {
                    return niveauSelectionModel.isSelected(object);
                }
            };
            display.getListNiveau().addColumn(checkColumn);
            //display.getListNiveau().setColumnWidth(checkColumn, 10, Style.Unit.PX);
            TextColumn<String> labelColumn = new TextColumn<String>() {
                @Override
                public String getValue(String object) {
                    return object;
                }
            };
            display.getListNiveau().addColumn(labelColumn);
            niveauProvider.addDataDisplay(display.getListNiveau());
            niveauProvider.refresh();        
        }        
        
        display.getSearchAPSA().addKeyUpHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                    @Override
                    public void execute() {
                        updateListAPSA();
                    }
                });
            }
        });
        
        display.getDownloadButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                startDownload();
            }
        });
    }
    
    
    static interface JSONBeanFactory extends AutoBeanFactory {  
        AutoBean<IStatisticsFilter> createStatisticsFilter();
        AutoBean<IStatisticsFilter> createStatisticsFilter(IStatisticsFilter sf);
    }
    
    private void startDownload() {
        display.getDownloadFormPanel().reset();
        String url = com.google.gwt.core.client.GWT.getModuleBaseURL() + "exportServlet";
        display.getDownloadFormPanel().setMethod(FormPanel.METHOD_POST);
        display.getDownloadFormPanel().setAction(url);
        display.getActionHiddenOption().setValue("exportStats");
        display.getDummyPreventCacheHiddenOption().setValue( "" +System.currentTimeMillis());
        
        StatisticsFilter statfilter = new StatisticsFilter();
        statfilter.setNiveau(display.getCheckBoxNiveau().getValue());
        statfilter.setAnnee(display.getCheckBoxAnnee().getValue());
        statfilter.setApsa(display.getCheckBoxAPSA().getValue());
        if(statfilter.isNiveau()) {
            statfilter.getNiveaux().addAll(niveauSelectionModel.getSelectedSet());
        }
        if(statfilter.isAnnee()) {
            statfilter.getAnnees().addAll(anneeSelectionModel.getSelectedSet());
        }
        if(statfilter.isApsa()) {
            statfilter.getApsas().addAll(apsaSelectionModel.getSelectedSet());
        }        
        
        if(statfilter.isNiveau() && statfilter.getNiveaux().isEmpty()) {
            DialogMsg.ShowMessage("Erreur: Filtre incohérent !","Vous souhaitez filtrer par <b>niveaux</b> mais aucun niveau n'est sélectionné ! </br>&nbsp;&nbsp;-Déselectionnez le filtre par niveau</br>&nbsp;ou</br>&nbsp;&nbsp;-Sélectionnez un ou plusieurs niveaux");
            return;
        }
        if(statfilter.isAnnee() && statfilter.getAnnees().isEmpty()) {
            DialogMsg.ShowMessage("Erreur: Filtre incohérent !","Vous souhaitez filtrer par <b>années</b> mais aucune année n'est sélectionné ! </br>&nbsp;&nbsp;-Déselectionnez le filtre par année</br>&nbsp;ou</br>&nbsp;&nbsp;-Sélectionnez une ou plusieurs années");
            return;
        }
        if(statfilter.isApsa()&& statfilter.getApsas().isEmpty()) {
            DialogMsg.ShowMessage("Erreur: Filtre incohérent !","Vous souhaitez filtrer par <b>APSA</b> mais aucune APSA n'est sélectionné ! </br>&nbsp;&nbsp;-Déselectionnez le filtre par APSA</br>&nbsp;ou</br>&nbsp;&nbsp;-Sélectionnez une ou plusieurs APSA");
            return;
        }            
        
        JSONBeanFactory beanFactory = GWT.create(JSONBeanFactory.class);
        AutoBean<IStatisticsFilter> bean = beanFactory.createStatisticsFilter(statfilter);
        String jsonStr = AutoBeanCodex.encode(bean).getPayload();
        display.getFilterObjectHiddenOption().setValue(jsonStr);
        display.getDownloadFormPanel().submit();
    }
    
    
    private void updateListAPSA() {
        HashSet<String> currentSelection = new HashSet<String>(apsaSelectionModel.getSelectedSet());
        apsaProvider.getList().clear();
        apsaSelectionModel.clear();
        String token = display.getSearchAPSA().getText().trim().toLowerCase();
        if(Utils.IsStringNullOrEmpty(token)) {
            apsaProvider.getList().addAll(filter.getApsas());
        }
        else {
            for(String s : filter.getApsas()) {
                if((s != null && s.toLowerCase().contains(token)) || currentSelection.contains(s)) {
                    apsaProvider.getList().add(s);
                }
            }
        }

        for(String s : currentSelection) {
            apsaSelectionModel.setSelected(s, true);
        }
        
        apsaProvider.refresh();
    }
}

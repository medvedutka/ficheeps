package ficheeps.presenter;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ButtonBase;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import ficheeps.client.ErrorReportEvent;
import ficheeps.client.Resources;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.model.Eleve;
import ficheeps.model.Groupe;
import ficheeps.model.GroupeType;
import ficheeps.model.Tuple;
import ficheeps.view.DialogMsg;
import ficheeps.view.GroupeManagementView;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;

public class GroupeManagementPresenter {

    private static Comparator<Eleve> CurrentEleveComparator = ficheeps.client.Utils.ELEVE_ALPHABETICAL_COMPARATOR;

    public interface Display {
        Panel getSearchEleveResultPanel();
        TextBox getSearchTextField();
        EleveWidget createEleveWidget(Eleve eleve);
        FlowPanel getGroupeEleveListPanel();
        VerticalPanel getGroupesListContainer();
        ButtonBase getRemoveGroupClickHandlers();
        ButtonBase getAddGroupClickHandlers();
        HasClickHandlers getModifyGroupButtonClickHandlers();
        HasClickHandlers getExportClickHandlers();
        HasClickHandlers getCheckStatusClickHandlers();
        Image getImageAjaxLoader();
        HasClickHandlers getSortAlphabeticallClickHandlers();
        HasClickHandlers getSortByDateClickHandlers();
        HasClickHandlers getSortByClassClickHandlers();
        HasValue<Boolean> getOptionIncludeAllEleve();
        ListBox getSortGroupeOptions();
    }

    public interface EleveWidget extends IsWidget {
        Button getButton();
        Button getMoreInfosButton();
        Eleve getEleve();
        void disable();
        void enable();
    }

    public interface GroupeBasicEditWidget extends IsWidget {
        TextBox getNom();
        TextBox getNomProfesseur();
        TextArea getDescription();
        HasValue<Boolean> getGroupEPSRadio();
        HasValue<Boolean> getGroupASRadio();
    }

    private FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);

    private boolean allowFullModif = false;

    
    private static final Comparator<Groupe> groupNameComparator = new Comparator<Groupe>() {
                                                                    @Override
                                                                    public int compare(Groupe o1, Groupe o2) {
                                                                        if (o1 == o2) {return 0;} 
                                                                        else if (o1 == null || o1.getNom() == null) {return 1;} 
                                                                        else if (o2 == null || o2.getNom() == null) {return -1;}
                                                                        return o1.getNom().compareToIgnoreCase(o2.getNom());
                                                                    }
                                                                };
    private static final Comparator<Groupe> groupProfesseurComparator = new Comparator<Groupe>() {
                                                                    @Override
                                                                    public int compare(Groupe o1, Groupe o2) {
                                                                        if (o1 == o2) {return 0;}
                                                                        else if (o1 == null || o1.getNomProfesseur() == null) {return 1;} 
                                                                        else if (o2 == null || o2.getNomProfesseur() == null) {return -1;}
                                                                        else if (o1.getNomProfesseur().equalsIgnoreCase(o2.getNomProfesseur())) {
                                                                            return groupNameComparator.compare(o1, o2);
                                                                        }
                                                                        return o1.getNomProfesseur().compareToIgnoreCase(o2.getNomProfesseur());
                                                                    }
                                                                };
    private static final Comparator<Groupe> groupDescriptionComparator = new Comparator<Groupe>() {
                                                                    @Override
                                                                    public int compare(Groupe o1, Groupe o2) {
                                                                        if (o1 == o2) {return 0;} 
                                                                        else if (o1 == null || o1.getDescription()== null) {return 1;} 
                                                                        else if (o2 == null || o2.getDescription() == null) {return -1;}
                                                                        else if (o1.getDescription().equalsIgnoreCase(o2.getDescription())) {
                                                                            return groupNameComparator.compare(o1, o2);
                                                                        }
                                                                        return o1.getDescription().compareToIgnoreCase(o2.getDescription());
                                                                    }
                                                                };    
    private final Comparator<Groupe> groupTypeComparator = new Comparator<Groupe>() {
                                                                    @Override
                                                                    public int compare(Groupe o1, Groupe o2) {
                                                                        if (o1 == o2) {return 0;} 
                                                                        else if (o1 == null || o1.getGroupeType() == null) {return 1;} 
                                                                        else if (o2 == null || o2.getGroupeType() == null) {return -1;}
                                                                        else if (o1.getGroupeType().equals(o2.getGroupeType())) {
                                                                            return groupNameComparator.compare(o1, o2);
                                                                        }
                                                                       
                                                                        return o1.getGroupeType().compareTo(o2.getGroupeType());
                                                                    }
                                                                };    
    
    public GroupeManagementPresenter(boolean allowFullModif) {
        this.allowFullModif = allowFullModif;
    }

    public void refresh() {
        groupeListSelectionModel.clear();
        groupesDataProvider.getList().clear();
        ficheEPSService.getGroupesLight(new AsyncCallback<List<Groupe>>() {
            @Override
            public void onFailure(Throwable caught) {
                ErrorReportEvent.Fire("Impossible de retrouver la liste des groupes: " + caught.getMessage());
            }

            @Override
            public void onSuccess(List<Groupe> result) {
                java.util.Collections.sort(result, groupNameComparator);
                groupesDataProvider.getList().addAll(result);
                groupesDataProvider.refresh();
            }
        });
        display.getGroupeEleveListPanel().clear();
        display.getSearchEleveResultPanel().clear();
    }

    private ListDataProvider<Groupe> groupesDataProvider = new ListDataProvider<Groupe>();
    private SingleSelectionModel<Groupe> groupeListSelectionModel;
    private Display display;
    private int timerDelay = 300;
    private Timer keyboardTimer = new Timer() {
        @Override
        public void run() {
            search(false);
        }
    };

    public void bind(final Display display) {
        this.display = display;
        display.getSearchTextField().addKeyPressHandler(new KeyPressHandler() {
            @Override
            public void onKeyPress(KeyPressEvent event) {
                display.getImageAjaxLoader().setVisible(true);
                keyboardTimer.cancel();
                keyboardTimer.schedule(timerDelay);
            }
        });
        display.getOptionIncludeAllEleve().addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                search(false);
            }
        });
        /*
         CellList<String> groupesCellList = new CellList<String>(new TextCell());
         groupesCellList.setPageSize(Integer.MAX_VALUE);
         groupeListSelectionModel = new SingleSelectionModel<String>();
         groupesCellList.setSelectionModel(groupeListSelectionModel);
         groupeListSelectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
         public void onSelectionChange(SelectionChangeEvent event) {
         String groupe = groupeListSelectionModel.getSelectedObject();
         if(groupe != null && !groupe.isEmpty()) {
         showElevesForGroupe(groupe);
         }
         }
         });*/
        CellList<Groupe> groupesCellList = new CellList<Groupe>(new GroupeCell());
        groupesCellList.setPageSize(Integer.MAX_VALUE);
        groupesCellList.setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.DISABLED);
        groupeListSelectionModel = new SingleSelectionModel<Groupe>();
        groupesCellList.setSelectionModel(groupeListSelectionModel);
        groupeListSelectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            public void onSelectionChange(SelectionChangeEvent event) {
                Groupe groupe = groupeListSelectionModel.getSelectedObject();
                if (groupe != null) {
                    showElevesForGroupe(groupe);
                }
            }
        });

        groupesDataProvider.addDataDisplay(groupesCellList);
        display.getGroupesListContainer().add(groupesCellList);

        if (allowFullModif) {
            display.getRemoveGroupClickHandlers().addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    deleteGroupe();
                }
            });
            display.getAddGroupClickHandlers().addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    newGroupe();
                }
            });
        } else {
            display.getAddGroupClickHandlers().setVisible(false);
            display.getAddGroupClickHandlers().setEnabled(false);
            display.getRemoveGroupClickHandlers().setVisible(false);
            display.getRemoveGroupClickHandlers().setEnabled(false);
        }

        display.getModifyGroupButtonClickHandlers().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                modifyGroupe();
            }
        });
        display.getExportClickHandlers().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String url = com.google.gwt.core.client.GWT.getModuleBaseURL() + "exportServlet?action=exportGroup&dummyPreventCache=" + System.currentTimeMillis();
                Window.open(url, "_blank", "enabled");
            }
        });
        display.getCheckStatusClickHandlers().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String url = com.google.gwt.core.client.GWT.getModuleBaseURL() + "checkGroupsStatusServlet?dummyPreventCache=" + System.currentTimeMillis();
                Window.open(url, "_blank", "");
            }
        });

        display.getImageAjaxLoader().setVisible(false);

        display.getSortAlphabeticallClickHandlers().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                CurrentEleveComparator = ficheeps.client.Utils.ELEVE_ALPHABETICAL_COMPARATOR;
                displayElevesForCurrentGroupe(new LinkedList<Eleve>(_currentGroupEleves));
            }
        });
        display.getSortByClassClickHandlers().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                CurrentEleveComparator = ficheeps.client.Utils.ELEVE_CLASSE_COMPARATOR;
                displayElevesForCurrentGroupe(new LinkedList<Eleve>(_currentGroupEleves));
            }
        });
        display.getSortByDateClickHandlers().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                CurrentEleveComparator = ficheeps.client.Utils.ELEVE_DATEBIRTH_COMPARATOR;
                displayElevesForCurrentGroupe(new LinkedList<Eleve>(_currentGroupEleves));
            }
        });
        display.getSortGroupeOptions().addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                sortGroupChanged();
            }
        });
    }

    private void newGroupe() {
        final GroupeManagementView.BasicGroupeBasicEditWidget bgew = new GroupeManagementView.BasicGroupeBasicEditWidget();

        DialogMsg.ShowOkCancel("Nouveau groupe", bgew.asWidget(), new ClickHandler() { //Ok
            @Override
            public void onClick(ClickEvent event) {
                ficheEPSService.createGroupe(new AsyncCallback<Groupe>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        ErrorReportEvent.Fire("Impossible de créer un nouveau groupe: " + caught.getMessage());
                    }

                    @Override
                    public void onSuccess(Groupe g) {
                        g.setNom(bgew.getNom().getText());
                        g.setNomProfesseur(bgew.getNomProfesseur().getText());
                        g.setDescription(bgew.getDescription().getText());
                        if (bgew.getGroupASRadio().getValue()) {
                            g.setGroupeType(GroupeType.AS);
                        } 
                        else {
                            g.setGroupeType(GroupeType.EPS);
                        }
                        trySaveGroupe(g);
                    }
                });
            }
        }, new ClickHandler() { //Cancel
            @Override
            public void onClick(ClickEvent event) {
                //Nothing to do !
            }
        });
    }

    private void modifyGroupe() {
        final Groupe groupe = groupeListSelectionModel.getSelectedObject();
        if (groupe != null) {
            ficheEPSService.getGroupeByOrid(groupe.getOrid(), new AsyncCallback<Groupe>() {
                @Override
                public void onFailure(Throwable caught) {
                    ErrorReportEvent.Fire("Impossible de retrouver le groupe '" + groupe.getNom() + "': " + caught.getMessage());
                }

                @Override
                public void onSuccess(final Groupe result) {
                    final GroupeManagementView.BasicGroupeBasicEditWidget bgew = new GroupeManagementView.BasicGroupeBasicEditWidget();
                    bgew.getNom().setText(result.getNom());
                    bgew.getNomProfesseur().setText(result.getNomProfesseur());
                    bgew.getDescription().setText(result.getDescription());
                    bgew.getGroupASRadio().setValue(GroupeType.AS.equals(result.getGroupeType()));
                    bgew.getGroupEPSRadio().setValue(GroupeType.EPS.equals(result.getGroupeType()));
                    DialogMsg.ShowOkCancel("Modifier le groupe", bgew.asWidget(), new ClickHandler() { //Ok
                        @Override
                        public void onClick(ClickEvent event) {
                            result.setNom(bgew.getNom().getText());
                            result.setDescription(bgew.getDescription().getText());
                            result.setNomProfesseur(bgew.getNomProfesseur().getText());
                            if (bgew.getGroupASRadio().getValue()) {
                                result.setGroupeType(GroupeType.AS);
                            } else {
                                result.setGroupeType(GroupeType.EPS);
                            }
                            trySaveGroupe(result);
                        }
                    }, new ClickHandler() { //Cancel
                        @Override
                        public void onClick(ClickEvent event) {
                            //Nothing to do !
                        }
                    });
                }
            });
        }
    }

    private void trySaveGroupe(Groupe g) {
        ficheEPSService.updateGroupeFirstLevel(g, new AsyncCallback<Groupe>() {
            @Override
            public void onFailure(Throwable caught) {
                ErrorReportEvent.Fire("Impossible de sauvegarder le groupe: " + caught.getMessage());
            }

            @Override
            public void onSuccess(Groupe result) {
                refresh();
            }
        });
    }

    private void deleteGroupe() {
        final Groupe groupe = groupeListSelectionModel.getSelectedObject();
        if (groupe != null) {
            DialogMsg.ShowOkCancel("Suppression d'un groupe", "Etes-vous sûr de vouloir supprimer le groupe '" + groupe.getNom() + "' ?", new ClickHandler() { //Ok
                @Override
                public void onClick(ClickEvent event) {
                    ficheEPSService.deleteGroupe(groupe.getOrid(), new AsyncCallback<Void>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            ErrorReportEvent.Fire("Impossible de supprimer le groupe '" + groupe.getNom() + "' :" + caught.getMessage());
                        }

                        @Override
                        public void onSuccess(Void result) {
                            refresh();
                        }
                    });
                }
            }, new ClickHandler() { //Cancel
                @Override
                public void onClick(ClickEvent event) {
                    // Nothing to do !
                }
            });
        }
    }

    private void sortGroupChanged() {
        Comparator<Groupe> comparator = groupNameComparator;
        String sortField = display.getSortGroupeOptions().getSelectedValue();
        if("nom".equals(sortField)) {
            comparator = groupNameComparator;
        }
        else if("professeur".equals(sortField)) {
            comparator = groupProfesseurComparator;
        }
        else if("description".equals(sortField)) {
            comparator = groupDescriptionComparator;
        }
        else if("type".equals(sortField)) {
            comparator = groupTypeComparator;
        }
        
        java.util.Collections.sort(groupesDataProvider.getList(), comparator);
        groupesDataProvider.refresh();
    }
    
    private void showElevesForGroupe(final Groupe groupe) {
        display.getGroupeEleveListPanel().clear();
        ficheEPSService.getGroupeByOrid(groupe.getOrid(), new AsyncCallback<Groupe>() {
            @Override
            public void onFailure(Throwable caught) {
                ErrorReportEvent.Fire("Impossible de retrouver le groupe '" + groupe.getNom() +" ("+groupe.getOrid() + ")' dans la base: " + caught.getMessage());
            }

            @Override
            public void onSuccess(Groupe result) {
                displayElevesForCurrentGroupe(result.getEleves());
            }
        });
    }


    private List<Eleve> _currentGroupEleves = new LinkedList<Eleve>();
    private HashSet<String> _currentGroupElevesIds = new HashSet<String>();

    /**
     * Insert the new Eleve at the right position (according to the current sort order). Hightlist it.
     */
    private void displayNewEleveForCurrentGroupe(Eleve eleve) {
        _currentGroupEleves.add(eleve);
        _currentGroupElevesIds.add(eleve.getId());
        Collections.sort(_currentGroupEleves, CurrentEleveComparator);
        int position = _currentGroupEleves.indexOf(eleve);
        displayEleveForCurrentGroupe(eleve,position,true);
    }

    /**
     * Clear current group displayed and display all Eleves from listEleves.
     */
    private void displayElevesForCurrentGroupe(List<Eleve> listEleves) {
        display.getGroupeEleveListPanel().clear();
        _currentGroupEleves.clear();
        _currentGroupElevesIds.clear();
        if (listEleves == null || listEleves.isEmpty()) {
            //
        }
        else {
            _currentGroupEleves.addAll(listEleves);
            Collections.sort(_currentGroupEleves, CurrentEleveComparator);
            for (Eleve eleve : _currentGroupEleves) {
                _currentGroupElevesIds.add(eleve.getId());
                displayEleveForCurrentGroupe(eleve, -1, false);
            }
        }
        enableAllEleveFromCurrentSearchResultExceptOnesInCurrentGroup();
    }
    
    private void displayEleveForCurrentGroupe(Eleve eleve, int position, boolean highlight) {
        final EleveWidget ew = display.createEleveWidget(eleve);
        ew.getButton().addStyleName(ficheeps.client.Resources.INSTANCE.ficheepsStyle().deleteImageBackground());
        ew.getButton().setTitle("Supprimer du groupe");
        ew.getButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                removeEleveFromCurrentGroupe(ew.getEleve());
            }
        });
        
        ew.getMoreInfosButton().addStyleName(ficheeps.client.Resources.INSTANCE.ficheepsStyle().helpImageBackgroundEleveGroupe());
        ew.getMoreInfosButton().setTitle("Plus d'infos ...");
        ew.getMoreInfosButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                displayMoreInfosForEleve(ew.getEleve(),ew.getMoreInfosButton());
            }
        });
        
        if (highlight) {
            ew.asWidget().addStyleName("animated fadeInUp");
        }

        if(position == -1) {
            display.getGroupeEleveListPanel().add(ew.asWidget());
        }
        else {
            display.getGroupeEleveListPanel().insert(ew.asWidget(), position);
        }
    }
    
    private void highlightCurrentEleveForCurrentGroupe(Eleve eleve) {
        String eleveId = "" + eleve.getId();
        int wc = display.getGroupeEleveListPanel().getWidgetCount();
        for(int i = 0;i<wc;i++) {
            Widget w = display.getGroupeEleveListPanel().getWidget(i);
            if(w instanceof EleveWidget) {
                if(eleveId.equals(((EleveWidget)w).getEleve().getId())) {
                    w.addStyleName("animated rotateIn");
                }
            }
        }
    }
    

    private int currentNbDisplayedEleve = 0;
    private boolean searching = false;

    private void search(final boolean searchMore) {
        if (searching) {
            keyboardTimer.cancel();
            keyboardTimer.schedule(timerDelay);
        }
        searching = true;

        if (!searchMore) {
            currentNbDisplayedEleve = 0;
            display.getSearchEleveResultPanel().clear();
        }

        final int start = searchMore ? currentNbDisplayedEleve : 0;

        AsyncCallback<Tuple<List<Eleve>, Integer>> callback = new AsyncCallback<Tuple<List<Eleve>, Integer>>() {
            public void onSuccess(Tuple<List<Eleve>, Integer> result) {
                updateListEleve(searchMore, result.getFirst());
                display.getImageAjaxLoader().setVisible(false);
                searching = false;
            }

            public void onFailure(Throwable caught) {
                ErrorReportEvent.Fire("Erreur serveur: " + caught.getMessage());
                display.getImageAjaxLoader().setVisible(false);
                searching = false;
            }
        };

        String searchTerm = ("" + display.getSearchTextField().getText()).toLowerCase();
        boolean mustIncludeAllEleves = display.getOptionIncludeAllEleve().getValue();
        ficheEPSService.getListElevesRange(start, searchResultSize, searchTerm, null, true, mustIncludeAllEleves, callback);
    }

    private int searchResultSize = 15;

    private void updateListEleve(boolean append, List<Eleve> list) {
        if (!append) {
            currentNbDisplayedEleve = 0;
            display.getSearchEleveResultPanel().clear();
        } else {
            //remove the "more" button
            Iterator<Widget> iter = display.getSearchEleveResultPanel().iterator();
            while (iter.hasNext()) {
                Widget w = iter.next();
                if (!(w instanceof EleveWidget)) {
                    iter.remove();
                }
            }
        }
        currentNbDisplayedEleve += list.size();

        for (final Eleve eleve : list) {
            final EleveWidget ew = display.createEleveWidget(eleve);
            //ew.getButton().setHTML("ajouter au groupe");
            if(_currentGroupElevesIds.contains(eleve.getId())) {
                ew.disable();
            }
            ew.getButton().addStyleName(ficheeps.client.Resources.INSTANCE.ficheepsStyle().addImageBackground());
            ew.getButton().setTitle("Ajouter au groupe");
            ew.getButton().addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    addEleveToCurrentGroupe(eleve);
                }
            });
            
            ew.getMoreInfosButton().addStyleName(ficheeps.client.Resources.INSTANCE.ficheepsStyle().helpImageBackgroundEleveGroupe());
            ew.getMoreInfosButton().setTitle("Plus d'infos ...");
            ew.getMoreInfosButton().addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    displayMoreInfosForEleve(ew.getEleve(),ew.getMoreInfosButton());
                }
            });
            display.getSearchEleveResultPanel().add(ew);
        }

        if (searchResultSize == list.size()) {
            Button moreButton = new Button("Plus de résultats");
            moreButton.getElement().getStyle().setMarginLeft(5, Style.Unit.PX);
            moreButton.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    search(true);
                }
            });
            display.getSearchEleveResultPanel().add(moreButton);
        }

    }

    private void addEleveToCurrentGroupe(final Eleve eleve) {
        final Groupe groupe = groupeListSelectionModel.getSelectedObject();
        if (groupe != null) {
            boolean alreadyInTheGroup = false;
            Iterator<Widget> iter = display.getGroupeEleveListPanel().iterator();
            while (iter.hasNext()) {
                Widget w = iter.next();
                if (w instanceof EleveWidget) {
                    if (eleve.getId().equals(((EleveWidget) w).getEleve().getId())) {
                        alreadyInTheGroup = true;
                        break;
                    }
                }
            }
            if (!alreadyInTheGroup) {
                ficheEPSService.addEleveToGroupe(groupe.getOrid(), eleve.getOrid(), new AsyncCallback<Void>() {
                    @Override
                    public void onSuccess(Void result) {
                        displayNewEleveForCurrentGroupe(eleve);
                        disableEleveFromCurrentSearchResult(eleve);
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        ErrorReportEvent.Fire("Impossible d'ajouter l'élève '" + eleve.getId() + "' au groupe '" + groupe.getNom() + "' : " + caught.getMessage());
                    }
                });
            } else {
                highlightCurrentEleveForCurrentGroupe(eleve);
                disableEleveFromCurrentSearchResult(eleve);
            }
        } else {
            ErrorReportEvent.Fire("Veuillez sélectionner un groupe.");
        }
    }

    private void disableEleveFromCurrentSearchResult(Eleve eleve) {
        Iterator<Widget> iter = display.getSearchEleveResultPanel().iterator();
        while (iter.hasNext()) {
            Widget w = iter.next();
            EleveWidget ew = (EleveWidget) w;
            if(ew.getEleve() != null && eleve.getId().equals(ew.getEleve().getId())) {
                ew.disable();
                break;
            }
        }
    }
    
    private void enableAllEleveFromCurrentSearchResultExceptOnesInCurrentGroup() {
        Iterator<Widget> iter = display.getSearchEleveResultPanel().iterator();
        while (iter.hasNext()) {
            Widget w = iter.next();
            if (w instanceof EleveWidget) {
                EleveWidget ew = (EleveWidget) w;
                if(ew.getEleve() != null && ew.getEleve().getId() !=  null) {
                    if(_currentGroupElevesIds.contains(ew.getEleve().getId())) {
                        ew.disable();
                    }
                    else {
                        ew.enable();
                    }
                }
            }
        }
    }
    
    private void enableEleveFromCurrentSearchResult(Eleve eleve) {
        if(eleve == null || eleve.getId() == null) {
            return;
        }
        Iterator<Widget> iter = display.getSearchEleveResultPanel().iterator();
        while (iter.hasNext()) {
            Widget w = iter.next();
            if (w instanceof EleveWidget) {
                EleveWidget ew = (EleveWidget) w;
                if(ew.getEleve() != null && eleve.getId().equals(ew.getEleve().getId())) {
                    ((EleveWidget) w).enable();
                    break;
                }
            }
        }
    }    

    private void displayMoreInfosForEleve(Eleve eleve, Widget widgetRelativeTo) {
        ElevePopupInfosPresenter.ShowPopup(eleve, widgetRelativeTo);
    }
    
    private void removeEleveFromCurrentGroupe(final Eleve eleve) {
        final Groupe groupe = groupeListSelectionModel.getSelectedObject();
        if (groupe != null) {
            ficheEPSService.removeEleveFromGroupe(groupe.getOrid(), eleve.getOrid(), new AsyncCallback<Void>() {
                @Override
                public void onSuccess(Void result) {
                    Iterator<Widget> iter = display.getGroupeEleveListPanel().iterator();
                    while (iter.hasNext()) {
                        Widget w = iter.next();
                        if (w instanceof EleveWidget) {
                            if (eleve == ((EleveWidget) w).getEleve()) {
                                iter.remove();
                                break;
                            }
                        }
                    }
                    enableEleveFromCurrentSearchResult(eleve);
                }

                @Override
                public void onFailure(Throwable caught) {
                    ErrorReportEvent.Fire("Impossible de supprimer l'eleve '" + eleve.getId() + "' du groupe '" + groupe.getNom() + "' : " + caught.getMessage());
                }
            });
        }
    }

    private static class GroupeCell extends AbstractCell<Groupe> {

        private String epsCSSClassName = "";
        private String asCSSClassName = "";
        
        public GroupeCell() {
            Resources resources = GWT.create(Resources.class);
            epsCSSClassName = resources.ficheepsStyle().epsGroupBoxShadow();
            asCSSClassName = resources.ficheepsStyle().asGroupBoxShadow();
        }

        @Override
        public void render(Context context, Groupe groupe, SafeHtmlBuilder sb) {
            if (groupe == null) {
                return;
            }
            
            String cssClassName = epsCSSClassName;
            String nomGroupe = "Groupe E.P.S.";
            if(GroupeType.AS.equals(groupe.getGroupeType())) {
                nomGroupe = "Groupe A.S.";
                cssClassName = asCSSClassName;
            }
            
            sb.appendHtmlConstant("<table title='" + nomGroupe + "' class='"+cssClassName+"'>");
            sb.appendHtmlConstant("<tr><td><b>");
            sb.appendEscaped(groupe.getNom());
            sb.appendHtmlConstant("</b></td></tr>");
            String desc = groupe.getDescription();
            if(desc != null && !desc.isEmpty()) {
                sb.appendHtmlConstant("<tr><td style='font-size:smaller;'><i>");
                sb.appendEscaped(groupe.getDescription());
                sb.appendHtmlConstant("</i></td></tr>");
            }
            String nomProfesseur = groupe.getNomProfesseur();
            if(nomProfesseur != null && !nomProfesseur.isEmpty()) {
                sb.appendHtmlConstant("<tr><td><i>");
                sb.appendEscaped(groupe.getNomProfesseur());
                sb.appendHtmlConstant("</i></td></tr>");
            }
            sb.appendHtmlConstant("</table>");
        }
    }

}

package ficheeps.presenter;


import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ButtonBase;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import ficheeps.client.ErrorReportEvent;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.view.ClassAndGroupNameInfosImportView;
import ficheeps.view.DialogMsg;
import java.util.AbstractMap;
import java.util.Map;


public class ClassAndGroupNameInfosPresenter {

	FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);

	public interface Display {
            CellTable<Map.Entry<String,String>> getCellTable();
            ButtonBase getButtonSaveEntry();
            ButtonBase getButtonImportWizard();
            TextBox getNameTextBox();
            TextArea getInfosTextBox();
	}
	
	public ClassAndGroupNameInfosPresenter() {
	}
	
	
        private final ListDataProvider<Map.Entry<String,String>> dataProvider = new ListDataProvider<Map.Entry<String,String>>();
        private final SingleSelectionModel<Map.Entry<String,String>> selectionModel = new SingleSelectionModel<Map.Entry<String,String>>();
    
	private Display display;
	
	public void bind(Display _display) {
            this.display = _display;
            
            display.getCellTable().setSelectionModel(selectionModel);
            selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
                @Override
                public void onSelectionChange(SelectionChangeEvent event) {
                    select(selectionModel.getSelectedObject());
                }
            });
            
            Column<Map.Entry<String,String>, String> nameColumn = new TextColumn<Map.Entry<String,String>>() {
                @Override
                public String getValue(Map.Entry<String, String> object) {
                    return object.getKey();
                }
            };
            display.getCellTable().addColumn(nameColumn, "Nom");
            display.getCellTable().setColumnWidth(nameColumn, 8, Style.Unit.EM);
            nameColumn.setSortable(true);

            Column<Map.Entry<String,String>,String> infosColumn = new TextColumn<Map.Entry<String,String>>() {
                @Override
                public String getValue(Map.Entry<String, String> object) {
                    return object.getValue();
                }
            };
            display.getCellTable().addColumn(infosColumn, "Informations");
            infosColumn.setSortable(true);
            
            display.getButtonSaveEntry().addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    saveEntry();
                }
            });
            
            display.getButtonImportWizard().addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    importWizard();
                }
            });
            
            dataProvider.addDataDisplay(display.getCellTable());
        }
	
        private void saveEntry() {
            String name = display.getNameTextBox().getText();
            String infos = display.getInfosTextBox().getText();
            if(name == null || infos == null) {
                return;
            }
            name = name.trim();
            infos = infos.trim();
            final AbstractMap.SimpleEntry<String,String> newEntry = new AbstractMap.SimpleEntry<String,String>(name, infos);
            ficheEPSService.putInfosClassesGroupes(name, infos, new AsyncCallback<Void>() {
                @Override
                public void onFailure(Throwable caught) {
                    ErrorReportEvent.Fire("Impossible de créer une nouvelle entrée: '" +caught.getMessage() + "'");
                }
                @Override
                public void onSuccess(Void result) {
                    Map.Entry<String,String> e = setValueInDataProvider(newEntry);
                    select(e);
                    dataProvider.refresh();
                    int index = dataProvider.getList().indexOf(e);
                    display.getCellTable().getRowElement(index).scrollIntoView();
                }
            });            
        }
        
        
        private Map.Entry<String,String> setValueInDataProvider(Map.Entry<String,String> entry) {
            for(Map.Entry<String,String> e : dataProvider.getList()) {
                if(e.getKey() != null && e.getKey().equals(entry.getKey())) {
                    e.setValue(entry.getValue());
                    return e;
                }
            }
            dataProvider.getList().add(entry);
            return entry;
        }
        
        private void select(Map.Entry<String,String> entry) {
            if(selectionModel.getSelectedObject() != entry) {
                selectionModel.clear();
                selectionModel.setSelected(entry, true);
            }
            
            if(entry == null) {
                display.getNameTextBox().setText("");
                display.getInfosTextBox().setText("");                
            }
            else {
                display.getNameTextBox().setText(entry.getKey());
                display.getInfosTextBox().setText(entry.getValue());
            }
        }
        
        private void importWizard() {
            ClassAndGroupNameInfosImportPresenter presenter = new ClassAndGroupNameInfosImportPresenter();
            ClassAndGroupNameInfosImportView view = new ClassAndGroupNameInfosImportView();
            presenter.bind(view);
            view.setSize("640px", "450px");
            DialogMsg.ShowContent("Import des informations classes/groupes", view, new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    refresh();
                }
            });
        }

        
	public void refresh() {
            display.getNameTextBox().setText("");
            display.getInfosTextBox().setText("");   
            selectionModel.clear();
            dataProvider.getList().clear();
            ficheEPSService.getInfosClassesGroupes(new AsyncCallback<Map<String,String>>() {
                @Override
                public void onFailure(Throwable caught) {
                    ErrorReportEvent.Fire("Impossible de retrouver la liste des informations pour les classes/groupes: '" +caught.getMessage() + "'");
                }
                @Override
                public void onSuccess(Map<String,String> result) {
                    dataProvider.getList().clear();
                    dataProvider.getList().addAll(result.entrySet());
                    dataProvider.refresh();
                }
            });
            dataProvider.refresh();
	}

}

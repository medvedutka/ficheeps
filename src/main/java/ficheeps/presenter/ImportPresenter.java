package ficheeps.presenter;

import java.util.LinkedList;
import java.util.List;

import ficheeps.client.ErrorReportEvent;
import ficheeps.client.Utils;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.model.ImportOptions;
import ficheeps.model.CSVLine;
import ficheeps.model.EleveDiff;
import ficheeps.view.DialogMsg;
import gwtupload.client.IUploader;
import gwtupload.client.SingleUploader;
import gwtupload.client.IUploadStatus.Status;
import gwtupload.client.IUploader.UploadedInfo;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.AbstractCellTable;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import java.util.Date;

public class ImportPresenter {

	private FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);

	private ListDataProvider<CSVLine> dataProviderSample = new ListDataProvider<CSVLine>();
	private ListDataProvider<EleveDiff> dataProviderClassUpdated = new ListDataProvider<EleveDiff>();
	private ListDataProvider<EleveDiff> dataProviderMoreUpdated = new ListDataProvider<EleveDiff>();
	private ListDataProvider<EleveDiff> dataProviderNewEleves = new ListDataProvider<EleveDiff>();
	
	public interface Display {
                HasClickHandlers getDeleteAllListesLibre();
		HasClickHandlers getDeleteAllGroupesClickHandlers();
		HasClickHandlers getUnregisterElevesFromClasse();
		HasClickHandlers getDeleteElevesWithoutClasses();
                HasClickHandlers getDeleteElevesWithoutClassesBornBefore();
		Button getTryImportClickHandler();
		Button getCommitImportClickHandler();
		CellTable<CSVLine> getSampleTable();
		SingleUploader getUploader();
		
		DataGrid<EleveDiff> getMoreUpdateTable();
		DataGrid<EleveDiff> getClassUpdatedTable();
		DataGrid<EleveDiff> getNewElevesTable();
		
		Widget getPanelTryImport();
		Widget getPanelTryImportResult();
		Widget getPanelCommitImport();
		Widget getPanelSampleResult();
		TextArea getCommitImportResult();
		
		TextBox getCharSeparatorOption();
		CheckBox getSkipFirstLineOption();
		RadioButton getExcelFileOption();
		RadioButton getCSVFileOption();
		ListBox getFileEncodingFormatOption();
		HasClickHandlers getApplyOptions();
	}

	
	public ImportPresenter() {
	}

	
	private Display display;
	
	public void bind(Display display) {
		this.display = display;
		
		cleanupAll();
		
		display.getUploader().addOnFinishUploadHandler(onFinishUploaderHandler);
		display.getUploader().addOnStartUploadHandler(new IUploader.OnStartUploaderHandler() {
			@Override
			public void onStart(IUploader uploader) {
				cleanupAll();
			}
		});
		
		display.getTryImportClickHandler().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				tryImport();
			}
		});
		
		display.getCommitImportClickHandler().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				commitImport();
			}
		});		
		
		{
			Column<CSVLine, String> c = new Column<CSVLine, String>(new TextCell()) {
				@Override
				public String getValue(CSVLine object) {
					return object.getId();
				}
			};
			display.getSampleTable().addColumn(c, "Id");
		}
		{
			Column<CSVLine, String> c = new Column<CSVLine, String>(new TextCell()) {
				@Override
				public String getValue(CSVLine object) {
					return object.getNom();
				}
			};
			display.getSampleTable().addColumn(c, "Nom");
		}
		{
			Column<CSVLine, String> c = new Column<CSVLine, String>(new TextCell()) {
				@Override
				public String getValue(CSVLine object) {
					return object.getPrenom();
				}
			};
			display.getSampleTable().addColumn(c, "Prénom");
		}
		{
			Column<CSVLine, String> c = new Column<CSVLine, String>(new TextCell()) {
				@Override
				public String getValue(CSVLine object) {
					return Utils.FormatDateDDSlashMMSlashYYYY(object.getDateDeNaissance());
				}
			};
			display.getSampleTable().addColumn(c, "Date de naissance");
		}
		{
			Column<CSVLine, String> c = new Column<CSVLine, String>(new TextCell()) {
				@Override
				public String getValue(CSVLine object) {
					return object.getClasse();
				}
			};
			display.getSampleTable().addColumn(c, "Classe");
		}
		
		addColumnsToEleveDiffTable(display.getNewElevesTable());
		addColumnsToEleveDiffTableShowingDiff(display.getClassUpdatedTable());
		addColumnsToEleveDiffTableShowingDiff(display.getMoreUpdateTable());
		
		dataProviderSample.addDataDisplay(display.getSampleTable());
		dataProviderClassUpdated.addDataDisplay(display.getClassUpdatedTable());
		dataProviderMoreUpdated.addDataDisplay(display.getMoreUpdateTable());
		dataProviderNewEleves.addDataDisplay(display.getNewElevesTable());
		
		display.getFileEncodingFormatOption().clear();
		display.getFileEncodingFormatOption().addItem("UTF-8","UTF-8");
		display.getFileEncodingFormatOption().addItem("WINDOWS-1252","WINDOWS-1252");
		display.getFileEncodingFormatOption().addItem("ISO-8859-1","ISO-8859-1");
		
		display.getExcelFileOption().addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				enableStateOptions();
			}
		});
		display.getCSVFileOption().addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				enableStateOptions();
			}
		});
		
		display.getApplyOptions().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				applyOptions();
			}
		});
		
		{
			ActionCell<EleveDiff> actionCell = new ActionCell<EleveDiff>("Ne pas importer", new ActionCell.Delegate<EleveDiff>() {
				@Override
				public void execute(final EleveDiff eleve) {
					dataProviderNewEleves.getList().remove(eleve);
					dataProviderNewEleves.refresh();
				}
			});
			Column<EleveDiff, EleveDiff> c = new Column<EleveDiff, EleveDiff>(actionCell) {
				@Override
				public EleveDiff getValue(EleveDiff object) {
					return object;
				}
			};
			display.getNewElevesTable().addColumn(c, "");
		}	
		
		{
			ActionCell<EleveDiff> actionCell = new ActionCell<EleveDiff>("Ne pas importer", new ActionCell.Delegate<EleveDiff>() {
				@Override
				public void execute(final EleveDiff eleve) {
					dataProviderClassUpdated.getList().remove(eleve);
					dataProviderClassUpdated.refresh();
				}
			});
			Column<EleveDiff, EleveDiff> c = new Column<EleveDiff, EleveDiff>(actionCell) {
				@Override
				public EleveDiff getValue(EleveDiff object) {
					return object;
				}
			};
			display.getClassUpdatedTable().addColumn(c, "");
		}
		
		{
			ActionCell<EleveDiff> actionCell = new ActionCell<EleveDiff>("Ne pas importer", new ActionCell.Delegate<EleveDiff>() {
				@Override
				public void execute(final EleveDiff eleve) {
					dataProviderMoreUpdated.getList().remove(eleve);
				}
			});
			Column<EleveDiff, EleveDiff> c = new Column<EleveDiff, EleveDiff>(actionCell) {
				@Override
				public EleveDiff getValue(EleveDiff object) {
					return object;
				}
			};
			display.getMoreUpdateTable().addColumn(c, "");
		}	
		
		display.getDeleteAllGroupesClickHandlers().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				deleteAllGroupes();
			}
		});
                display.getDeleteAllListesLibre().addClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        deleteAllListesLibre();
                    }
                });
		display.getUnregisterElevesFromClasse().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				unregisterAllElvesFromClasses();
			}
		});
		
		display.getDeleteElevesWithoutClasses().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				deleteAllElevesWithoutClasses();
			}
		});
                
                display.getDeleteElevesWithoutClassesBornBefore().addClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        deleteAllElevesWithoutClassesBornBefore();
                    }
                });
	}
	
	
	private void deleteAllGroupes() {
		DialogMsg.ShowOkCancel("Suppression de tous les groupes", "Etes-vous sûr de vouloir supprimer tous les groupes ?"
				, new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				final DialogMsg wd = DialogMsg.ShowWaiter("Suppression des groupes", "Merci de patienter");
				ficheEPSService.deleteAllGroupes(new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						ErrorReportEvent.Fire("Erreur lors de la suppression de tous les groupes: " + caught.getMessage());
						wd.hide();
					}
					@Override
					public void onSuccess(Void result) {
						wd.hide();
					}
				});
			}
		}, null);
	}
	
	private void deleteAllListesLibre() {
            DialogMsg.ShowOkCancel("Suppression de toutes les listes libres", "Etes-vous sûr de vouloir supprimer toutes les listes libres ?"
				, new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				final DialogMsg wd = DialogMsg.ShowWaiter("Suppression des listes libres", "Merci de patienter");
				ficheEPSService.deleteAllListesLibre(new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						ErrorReportEvent.Fire("Erreur lors de la suppression de toutes les listes libres: " + caught.getMessage());
						wd.hide();
					}
					@Override
					public void onSuccess(Void result) {
						wd.hide();
					}
				});
			}
		}, null);
        }
	
	private void deleteAllElevesWithoutClasses() {
		DialogMsg.ShowOkCancel("Suppression de tous les élèves sans classes"
								, "Etes-vous sûr de vouloir supprimer tous les élèves sans classses ?"
				, new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				final DialogMsg wd = DialogMsg.ShowWaiter("Suppression des élèves sans classes", "Merci de patienter");
				ficheEPSService.deleteAllElevesWithoutClasses(new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						ErrorReportEvent.Fire("Erreur lors de la suppresion de toutes les élèves sans classes: " + caught.getMessage());
						wd.hide();
					}
					@Override
					public void onSuccess(Void result) {
						wd.hide();
					}
				});
			}
		}, null);
	}
	
        
        private void deleteAllElevesWithoutClassesBornBefore() {
            DialogMsg.ShowTextBox("Suppression de tous les élèves sans classes nées avant ...","Veuillez saisir une date au format 'JJ/MM/AAAA'", new DialogMsg.OkCancelValueCallback<String>() {
                @Override
                public void onOk(String value) {
                    DateTimeFormat dtf = DateTimeFormat.getFormat("dd/MM/yyyy");
                    Date date = dtf.parse(value);
                    deleteAllElevesWithoutClassesBornBefore_step2(date);
                }
            } );
	}
        private void deleteAllElevesWithoutClassesBornBefore_step2(final Date date) {
            DateTimeFormat dtf = DateTimeFormat.getFormat("dd MM yyyy");
            final String dateStr = dtf.format(date);
            String msg = "Etes-vous sûr de vouloir supprimer tous les élèves sans classses nés avant le: " + dateStr;
            DialogMsg.ShowOkCancel("Suppression de tous les élèves sans classes",msg, new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        DateTimeFormat dtf = DateTimeFormat.getFormat("yyyyMMdd");
                        final DialogMsg wd = DialogMsg.ShowWaiter("Suppression des élèves sans classes nés avant le " + dateStr, "Merci de patienter");
                        ficheEPSService.deleteAllEleveWithoutClasseBornBefore(dtf.format(date),new AsyncCallback<String>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                ErrorReportEvent.Fire("Erreur lors de la suppresion de toutes les élèves sans classes: " + caught.getMessage());
                                wd.hide();
                            }
                            @Override
                            public void onSuccess(String result) {
                                wd.hide();
                                DialogMsg.ShowMessage("Suppression réussie", result);
                            }
                        });
                    }
            }, null);
	}
                
	private void unregisterAllElvesFromClasses() {
		DialogMsg.ShowOkCancel("Suppression de toutes les classes", "Etes-vous sûr de vouloir supprimer toutes les classes ?"
				, new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				final DialogMsg wd = DialogMsg.ShowWaiter("Suppression des classes", "Merci de patienter");
				ficheEPSService.unregisterAllElevesFromClasses(new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						ErrorReportEvent.Fire("Erreur lors de la suppresion de toutes les classes: " + caught.getMessage());
						wd.hide();
					}
					@Override
					public void onSuccess(Void result) {
						wd.hide();
					}
				});
			}
		}, null);
	}
	

	private void enableStateOptions() {
		boolean state = !display.getExcelFileOption().getValue(); 
		display.getCharSeparatorOption().setEnabled(state);
		display.getFileEncodingFormatOption().setEnabled(state);
	}
	
	private void applyOptions() {
		if(display.getCharSeparatorOption().getText() != null
		   && display.getCharSeparatorOption().getText().length() >0) {
			currentImportOptions.setDelimiter(display.getCharSeparatorOption().getText().charAt(0));
		}
		currentImportOptions.setFileExcel(display.getExcelFileOption().getValue());
		currentImportOptions.setSkipFirstLine(display.getSkipFirstLineOption().getValue());
		currentImportOptions.setFileFormat(display.getFileEncodingFormatOption().getSelectedValue());
		
		reloadImportSample();
	}
	
	
	private void addColumnsToEleveDiffTable(AbstractCellTable<EleveDiff> table) {
		{
			Column<EleveDiff, String> c = new Column<EleveDiff, String>(new TextCell()) {
				@Override
				public String getValue(EleveDiff ed) {
					return ed.getId();
				}
			};
			table.addColumn(c, "Id");
		}
		{
			Column<EleveDiff, String> c = new Column<EleveDiff, String>(new TextCell()) {
				@Override
				public String getValue(EleveDiff ed) {
					return ed.getNewNom();
				}
			};
			table.addColumn(c, "Nom");
		}
		{
			Column<EleveDiff, String> c = new Column<EleveDiff, String>(new TextCell()) {
				@Override
				public String getValue(EleveDiff ed) {
					return ed.getNewPrenom();
				}
			};
			table.addColumn(c, "Prénom");
		}
		{
			Column<EleveDiff, String> c = new Column<EleveDiff, String>(new TextCell()) {
				@Override
				public String getValue(EleveDiff ed) {
					return (ed.getNewDateDeNaissance() == null) ? "" : Utils.FormatDateDDSlashMMSlashYYYY(ed.getNewDateDeNaissance());
				}
			};
			table.addColumn(c, "Date de naissance");
		}
		{
			Column<EleveDiff, String> c = new Column<EleveDiff, String>(new TextCell()) {
				@Override
				public String getValue(EleveDiff ed) {
					return ed.getNewClasse();
				}
			};
			table.addColumn(c, "Classe");
		}
	}
	

	private void addColumnsToEleveDiffTableShowingDiff(AbstractCellTable<EleveDiff> table) {
		{
			Column<EleveDiff, String> c = new Column<EleveDiff, String>(new TextCell()) {
				@Override
				public String getValue(EleveDiff ed) {
					return ed.getId();
				}
			};
			table.addColumn(c, "Id");
		}
		{
			Column<EleveDiff, EleveDiff> c = new Column<EleveDiff, EleveDiff>(new EleveDiffCell(){
				@Override
				protected String getOldValue(EleveDiff ed) {
					return (ed.getEleve() == null) ? "" : ed.getEleve().getNom();
				}
				@Override
				protected String getNewValue(EleveDiff ed) {
					return ed.getNewNom();
				}
			}) {
				@Override
				public EleveDiff getValue(EleveDiff ed) {
					return ed;
				}
			};
			table.addColumn(c, "Nom");
		}
		{
			Column<EleveDiff, EleveDiff> c = new Column<EleveDiff, EleveDiff>(new EleveDiffCell(){
				@Override
				protected String getOldValue(EleveDiff ed) {
					return (ed.getEleve() == null) ? "" : ed.getEleve().getPrenom();
				}
				@Override
				protected String getNewValue(EleveDiff ed) {
					return ed.getNewPrenom();
				}
			}) {
				@Override
				public EleveDiff getValue(EleveDiff ed) {
					return ed;
				}
			};
			table.addColumn(c, "Prénom");
		}
		{
			Column<EleveDiff, EleveDiff> c = new Column<EleveDiff, EleveDiff>(new EleveDiffCell(){
				@Override
				protected String getOldValue(EleveDiff ed) {
					return (ed.getEleve() == null) ? "" : Utils.FormatDateDDSlashMMSlashYYYY(ed.getEleve().getDateDeNaissance());
				}
				@Override
				protected String getNewValue(EleveDiff ed) {
					return (ed.getNewDateDeNaissance() != null) ? Utils.FormatDateDDSlashMMSlashYYYY(ed.getNewDateDeNaissance()): null;
				}
			}) {
				@Override
				public EleveDiff getValue(EleveDiff ed) {
					return ed;
				}
			};
			table.addColumn(c, "Date de naissance");
		}		
		{
			Column<EleveDiff, EleveDiff> c = new Column<EleveDiff, EleveDiff>(new EleveDiffCell(){
				@Override
				protected String getOldValue(EleveDiff ed) {
					return (ed.getEleve() == null) ? "" : ed.getEleve().getClasse();
				}
				@Override
				protected String getNewValue(EleveDiff ed) {
					return ed.getNewClasse();
				}
			}) {
				@Override
				public EleveDiff getValue(EleveDiff ed) {
					return ed;
				}
			};
			table.addColumn(c, "Classe");
		}
	}
	
	
	
	private IUploader.OnFinishUploaderHandler onFinishUploaderHandler = new IUploader.OnFinishUploaderHandler() {
		public void onFinish(IUploader uploader) {
			if (uploader.getStatus() == Status.SUCCESS) {
				// new PreloadedImage(uploader.fileUrl(), showImage);
				// The server sends useful information to the client by default
				UploadedInfo info = uploader.getServerInfo();
				/*System.out.println("File name " + info.name);
				System.out.println("File content-type " + info.ctype);
				System.out.println("File size " + info.size);
				// You can send any customized message and parse it
				System.out.println("Server message " + info.message);*/
				retrieveImportOptions(uploader.getServerMessage().getMessage());
			}
		}
	};

	
	
	private ImportOptions currentImportOptions = null;
	
	
	private void retrieveImportOptions(String fileUploadToken) {
		cleanupAll();
		final DialogMsg wd = DialogMsg.ShowWaiter("Lecture du fichier d'import (1/2)", "Merci de patienter");
		ficheEPSService.initImportFromUploadedFile(fileUploadToken, new AsyncCallback<ImportOptions>() {
			@Override
			public void onSuccess(ImportOptions result) {
				currentImportOptions = result;
				
				display.getCharSeparatorOption().setText(""+currentImportOptions.getDelimiter());
				display.getExcelFileOption().setValue(currentImportOptions.isFileExcel());
				display.getCSVFileOption().setValue(!currentImportOptions.isFileExcel());
				display.getSkipFirstLineOption().setValue(currentImportOptions.isSkipFirstLine());
				String encoding = ((currentImportOptions.getFileFormat() == null) 
									|| currentImportOptions.getFileFormat().isEmpty())
									? "UTF-8" : currentImportOptions.getFileFormat();
				boolean bFound = false;
				for(int i =0;i<display.getFileEncodingFormatOption().getItemCount();i++) {
					String s = display.getFileEncodingFormatOption().getValue(i);
					if(encoding.equalsIgnoreCase(s)) {
						display.getFileEncodingFormatOption().setSelectedIndex(i);
						bFound=true;
						break;
					}			
				}
				if(!bFound) {
					display.getFileEncodingFormatOption().insertItem(currentImportOptions.getFileFormat(),currentImportOptions.getFileFormat(), 0);
					display.getFileEncodingFormatOption().setSelectedIndex(0);
				}
				
				wd.hide();
				reloadImportSample();
			}
			@Override
			public void onFailure(Throwable caught) {
				ErrorReportEvent.Fire("Impossible d'analyser le fichier sélectioné (Etape 1): '" + caught.getMessage() +"'.");
				wd.hide();
			}
		});
	}
	
	private void reloadImportSample() {
		cleanupSample();
		final DialogMsg wd = DialogMsg.ShowWaiter("Lecture du fichier d'import (2/2)", "Merci de patienter");
		ficheEPSService.verifyImportOptions(currentImportOptions,new AsyncCallback<List<CSVLine>>() {
			@Override
			public void onFailure(Throwable caught) {
				//ErrorReportEvent.Fire("Impossible d'analyser le fichier sélectioné (Etape 2): '" + caught.getMessage() +"'.");
				setSample(new LinkedList<CSVLine>());
				wd.hide();
				DialogMsg.ShowMessage("Impossible d'analyser le fichier sélectionné","Erreur: " + caught.getMessage()+"<br><br>Est-ce que le fichier est bien formaté ?<br>Vous pouvez aussi essayer de spécifier les options d'import manuellement.<br>Peut-être devez vous <i>ignorer la première ligne</i> ?");
			}
			@Override
			public void onSuccess(List<CSVLine> result) {
				setSample(result);
				wd.hide();
				if(result.size() ==0) {
					DialogMsg.ShowMessage("L'analyser du fichier sélectionné n'a renvoyé aucune ligne d'import","Essayez de spécifier les options d'import manuellement.");
				}
			}
		});
	}
	
	private void cleanupAll() {
		dataProviderNewEleves.getList().clear();
		dataProviderClassUpdated.getList().clear();
		dataProviderMoreUpdated.getList().clear();
		
		dataProviderNewEleves.refresh();
		dataProviderClassUpdated.refresh();
		dataProviderMoreUpdated.refresh();
		
		display.getCommitImportResult().setText("");
		
		cleanupSample();
	
		display.getPanelCommitImport().setVisible(false);
		display.getPanelTryImport().setVisible(false);
		display.getPanelTryImportResult().setVisible(false);
		display.getPanelSampleResult().setVisible(false);
	}
	
	private void cleanupSample() {
		dataProviderSample.getList().clear();
		dataProviderSample.refresh();
	}
	
	private void setSample(List<CSVLine> sample) {
		cleanupSample();
		for(CSVLine line: sample) {
			dataProviderSample.getList().add(line);
		}
		dataProviderSample.refresh();
		
		display.getPanelSampleResult().setVisible(true);
		display.getPanelTryImport().setVisible(true);
		
		enableStateOptions();
	}
	
	
	private void tryImport() {
		final DialogMsg wd = DialogMsg.ShowWaiter("Test d'import", "Merci de patienter");
		ficheEPSService.tryImport(currentImportOptions, new AsyncCallback<List<EleveDiff>>() {
			@Override
			public void onFailure(Throwable caught) {
				ErrorReportEvent.Fire("Impossible de tester l'import: '" + caught.getMessage() +"'.");
				wd.hide();
			}
			@Override
			public void onSuccess(List<EleveDiff> result) {
				setImportTrial(result);
				wd.hide();
			}
		});
	}
	
	private void setImportTrial(List<EleveDiff> list) {
		dataProviderNewEleves.getList().clear();
		dataProviderClassUpdated.getList().clear();
		dataProviderMoreUpdated.getList().clear();
		
		for(EleveDiff ed : list) {
			if(ed.isNewEleve()) {
				dataProviderNewEleves.getList().add(ed);
			}
			else if(ed.isJustClasseUpdated()) {
				dataProviderClassUpdated.getList().add(ed);
			}
			else {
				dataProviderMoreUpdated.getList().add(ed);
			}
		}
		
		display.getNewElevesTable().setHeight(computeHeight(dataProviderNewEleves.getList().size()));
		display.getMoreUpdateTable().setHeight(computeHeight(dataProviderMoreUpdated.getList().size()));
		display.getClassUpdatedTable().setHeight(computeHeight(dataProviderClassUpdated.getList().size()));
		
		display.getPanelTryImportResult().setVisible(true);
		display.getPanelCommitImport().setVisible(true);
	}
	
	private String computeHeight(int nbItems) {
		if(nbItems == 0) {
			return "";
		}
		return "" + (50 + (10 *nbItems)) +"px";
	}
	
	private void commitImport() {
		final DialogMsg wd = DialogMsg.ShowWaiter("Import des élèves", "Veuillez patienter");
		List<EleveDiff> list = new LinkedList<EleveDiff>();
		list.addAll(dataProviderNewEleves.getList());
		list.addAll(dataProviderClassUpdated.getList());
		list.addAll(dataProviderMoreUpdated.getList());
		
		ficheEPSService.commitImport(currentImportOptions, list, new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				ErrorReportEvent.Fire("Erreur lors de l'import: '" + caught.getMessage() +"'.");
				wd.hide();
			}
			@Override
			public void onSuccess(String result) {
				display.getCommitImportResult().setText(result);
				wd.hide();
			}
		});
	}
	
	
	
	
	
	static abstract class EleveDiffCell extends AbstractCell<EleveDiff> {

		public EleveDiffCell() {}
		
		protected abstract String getOldValue(EleveDiff ed);
		protected abstract String getNewValue(EleveDiff ed);

		@Override
		public void render(Context context, EleveDiff eleve, SafeHtmlBuilder sb) {
			// Value can be null, so do a null check..
			if (eleve == null) {
				return;
			}

			String oldValue = getOldValue(eleve);
			String newValue = getNewValue(eleve);
			
			if(newValue != null) {
				sb.appendHtmlConstant("<span class='oldValue'>");
				sb.appendEscaped(oldValue);
				sb.appendHtmlConstant("</span>");
				sb.appendHtmlConstant("→");
				sb.appendHtmlConstant("<span class='newValue'>");
				sb.appendEscaped(newValue);
				sb.appendHtmlConstant("</span>");
			}
			else {
				sb.appendHtmlConstant("<span>");
				sb.appendEscaped(oldValue);
				sb.appendHtmlConstant("</span>");
			}
		}
	}
	
	
}

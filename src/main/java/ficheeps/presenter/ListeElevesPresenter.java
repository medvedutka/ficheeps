package ficheeps.presenter;

import java.util.Date;

import org.gwt.advanced.client.ui.CalendarListener;
import org.gwt.advanced.client.ui.widget.Calendar;
import org.moxieapps.gwt.uploader.client.Uploader;
import org.moxieapps.gwt.uploader.client.events.FileDialogCompleteEvent;
import org.moxieapps.gwt.uploader.client.events.FileDialogCompleteHandler;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.AbstractCellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.ColumnSortList.ColumnSortInfo;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import ficheeps.client.Action;
import ficheeps.client.AsyncListeElevesDataProvider;
import ficheeps.client.Utils;
import ficheeps.client.AsyncListeElevesDataProvider.AsycListEleveDataProviderEvent;
import ficheeps.client.AsyncListeElevesDataProvider.SortAndFilterInfoRetriever;
import ficheeps.client.ErrorReportEvent;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.model.Eleve;
import ficheeps.presenter.ExportPDFPresenter.ExportType;
import ficheeps.view.DialogMsg;
import ficheeps.view.ExportPDFView;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.HasDirection;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import ficheeps.client.Resources;
import ficheeps.view.utils.TooltipCell;
import org.moxieapps.gwt.uploader.client.events.*;

import java.util.LinkedHashMap;
import java.util.Map;

public class ListeElevesPresenter {

	public interface CSSStyle extends CssResource {
		String dropFilesLabelHover();

		String dropFilesLabel();

		String cancelButton();
	}

	public interface Display {
		ColumnSortInfo getSortInfo();
		void addColumn(Column<Eleve, ?> column, String columnName);
                void addColumn(Column<Eleve, ?> column, String columnName,int value,Unit unit);
		HandlerRegistration addColumnSortHandler(ColumnSortEvent.Handler handler);
		HasClickHandlers getRefreshRequest();
		String getFilter();
		AbstractCellTable<Eleve> getTable();
		Button getNewEleveClickHandler();
		CSSStyle getCSSStyle();
		Widget getWidgetWaitForSearchIndicator();
                HasValue<Boolean> getOptionIncludeAllEleve();
	}

	
	private boolean allowModif = false;
	public ListeElevesPresenter(boolean allowModif) {
		this.allowModif = allowModif;
	}
	
	
	FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);

	private AsyncListeElevesDataProvider dataProvider = new AsyncListeElevesDataProvider(ficheEPSService);

	private Display display;
        private EditTextCell idEditTextCell = null; // Hack to be able to refresh the table

	public void bind(final Display display) {
                dataProvider.freeze();
		this.display = display;

		{
			Column<Eleve, Eleve> photoColumn = new Column<Eleve, Eleve>(new ElevePhotoCell(allowModif)) {
				@Override
				public Eleve getValue(Eleve object) {
					return object;
				}
			};
			photoColumn.setSortable(false);
			display.addColumn(photoColumn, "",80,Unit.PX);
		}

		{ //Id
			Column<Eleve, String> idColumn = 
					allowModif ? 
							new Column<Eleve, String>(idEditTextCell = new EditTextCell()) {
								@Override
								public String getValue(Eleve object) {
									return object.getId();
								}
							}
							:
							new Column<Eleve, String>(new TextCell()) {
								@Override
								public String getValue(Eleve object) {
									return object.getId();
								}
							};
			
			idColumn.setDataStoreName("id");
			idColumn.setSortable(true);
			display.addColumn(idColumn, "Numen");
			if(allowModif) {
				idColumn.setFieldUpdater(new FieldUpdater<Eleve, String>() {
					@Override
					public void update(int index, Eleve eleve, String value) {
                                            if(!Utils.StringEquals(eleve.getId(), value)) {
                                                setEleveNumen(eleve,value);
                                            }
					}
				});
			}
		}

		{ //Nom
			Column<Eleve, String> firstNameColumn = allowModif ? 
							new Column<Eleve, String>(new EditTextCell()) {
								@Override
								public String getValue(Eleve object) {
									return object.getNom();
								}
							}
							:
							new Column<Eleve, String>(new TextCell()) {
								@Override
								public String getValue(Eleve object) {
									return object.getNom();
								}
							};
			firstNameColumn.setDataStoreName("nom");
			firstNameColumn.setSortable(true);
			display.addColumn(firstNameColumn, "Nom");
			if(allowModif) {
				firstNameColumn.setFieldUpdater(new FieldUpdater<Eleve, String>() {
					@Override
					public void update(int index, Eleve object, String value) {
						object.setNom(value);
						save(object);
					}
				});
			}
		}
		
		{ //Prenom
			Column<Eleve, String> lastNameColumn = allowModif ?
					new Column<Eleve, String>(new EditTextCell()) {
						@Override
						public String getValue(Eleve object) {
							return object.getPrenom();
						}
					}
					:
					new Column<Eleve, String>(new TextCell()) {
						@Override
						public String getValue(Eleve object) {
							return object.getPrenom();
						}
					};
			lastNameColumn.setDataStoreName("prenom");
			lastNameColumn.setSortable(true);
			display.addColumn(lastNameColumn, "Prénom");
			if(allowModif) {
				lastNameColumn.setFieldUpdater(new FieldUpdater<Eleve, String>() {
					@Override
					public void update(int index, Eleve object, String value) {
						object.setPrenom(value);
						save(object);
					}
				});
			}
		}
		
		{ //DateDeNaissance
			Column<Eleve, Eleve> birthdateColumn = new Column<Eleve, Eleve>(new EleveBirthdateCell(allowModif)) {
				@Override
				public Eleve getValue(Eleve object) {
					return object;
				}
			};	
			birthdateColumn.setDataStoreName("dateDeNaissance");
			birthdateColumn.setSortable(true);
			display.addColumn(birthdateColumn, "Date de naissance");
		}
		
		{//Classe
			Column<Eleve, String> classeColumn = allowModif ?
					new Column<Eleve, String>(new EditTextCell()) {
						@Override
						public String getValue(Eleve object) {
							return object.getClasse();
						}
					}
					:
					new Column<Eleve, String>(new TextCell()) {
						@Override
						public String getValue(Eleve object) {
							return object.getClasse();
						}
					};

			classeColumn.setSortable(true);
			classeColumn.setDataStoreName("classe");
			display.addColumn(classeColumn, "Classe");
			if(allowModif) {
				classeColumn.setFieldUpdater(new FieldUpdater<Eleve, String>() {
					@Override
					public void update(int index, Eleve object, String value) {
						object.setClasse(value);
						save(object);
					}
				});
			}
		}

		// Download Fiche Eleve
		{
                    ActionCell<Eleve> cell = new ActionCell<Eleve>("", new ActionCell.Delegate<Eleve>() {
                            @Override
                            public void execute(final Eleve eleve) {
                                    exportPDF(ExportType.ExportEleve,eleve.getId());
                            }
                    });
                    Column<Eleve, Eleve> c = new Column<Eleve, Eleve>(new TooltipCell<Eleve>(cell,"Télécharger la fiche PDF")) {
                            @Override
                            public Eleve getValue(Eleve object) {
                                    return object;
                            }
                    };
                    c.setCellStyleNames(Resources.INSTANCE.ficheepsStyle().listeEleveDownloadfichePDF32());
                    display.addColumn(c, "Fiche",80,Unit.PX);
		}		
		
		// Delete Eleve
		if(allowModif) {
			{
				ActionCell<Eleve> actionCell = new ActionCell<Eleve>("", new ActionCell.Delegate<Eleve>() {
					@Override
					public void execute(final Eleve eleve) {
						DialogMsg.ShowOkCancel("Supression d'un élève", "Etes vous sûr de vouloir supprimer l'élève '"
								+ eleve.getNom() + " " + eleve.getPrenom() + "' ?", new ClickHandler() {
							@Override
							public void onClick(ClickEvent event) {
								delete(eleve);
							}
						}, null);
	
					}
				});
				Column<Eleve, Eleve> c = new Column<Eleve, Eleve>(new TooltipCell<Eleve>(actionCell,"Supprimer l'élève de la base")) {
					@Override
					public Eleve getValue(Eleve object) {
						return object;
					}
				};
                                c.setHorizontalAlignment(HasHorizontalAlignment.HorizontalAlignmentConstant.startOf(HasDirection.Direction.LTR));
                                c.setCellStyleNames(Resources.INSTANCE.ficheepsStyle().listeEleveDeleteSprite());
				display.addColumn(c, "",55,Unit.PX);
			}
		}
		
		display.getRefreshRequest().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dataProvider.refresh(display.getTable());
			}
		});

		dataProvider.setSortInfoRetriever(new SortAndFilterInfoRetriever() {
			@Override
			public ColumnSortInfo getSortInfo() {
				return display.getSortInfo();
			}

			@Override
			public String getFilter() {
				return display.getFilter();
			}
                        
                        @Override
                        public boolean mustIncludeAllEleve() {
                            return display.getOptionIncludeAllEleve().getValue();
                        }
		});

		ColumnSortEvent.Handler columnSortHandler = new ColumnSortEvent.Handler() {
			@Override
			public void onColumnSort(ColumnSortEvent event) {
				dataProvider.refresh(display.getTable());
			}
		};
		display.addColumnSortHandler(columnSortHandler);

		dataProvider.addDataDisplay(display.getTable());

		if(allowModif) {
			display.getNewEleveClickHandler().addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					createNewEleve();
				}
			});
		}
		else {
			display.getNewEleveClickHandler().setVisible(false);
		}
		
		
		dataProvider.addHandler(new AsycListEleveDataProviderEvent.Handler() {
			@Override
			public void onStatusChanged(AsycListEleveDataProviderEvent event) {
				display.getWidgetWaitForSearchIndicator().setVisible(false);
				if(event.getStatus() == AsycListEleveDataProviderEvent.Status.Searching) {
					display.getWidgetWaitForSearchIndicator().setVisible(true);
				}
			}
		});
                
                display.getOptionIncludeAllEleve().addValueChangeHandler(new ValueChangeHandler<Boolean>() {
                    @Override
                    public void onValueChange(ValueChangeEvent<Boolean> event) {
                        dataProvider.refresh(display.getTable());
                    }
                });
                
                dataProvider.unfreeze();
	}

	private void exportPDF(ExportType exportType, String id) {
		final ExportPDFPresenter exportPDFPresenter = new ExportPDFPresenter();
		exportPDFPresenter.setExport(exportType, id);
		ExportPDFView exportPDFView = new ExportPDFView();
		exportPDFPresenter.bind(exportPDFView);
	
		DialogMsg.ShowOkCancel("Export PDF", 
				exportPDFView, 
				new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						exportPDFPresenter.startExport();
					}
				}, 
				null);
	}
	
	private void createNewEleve() {
		CreateNewElevePresenter elevePresenter = new CreateNewElevePresenter();
		elevePresenter.showCreateDialog(new CreateNewElevePresenter.EleveCreatedHandler() {
                    @Override
                    public void onEleveCreated(Eleve eleve) {
                        refresh();
                    }
                });
	}

	public void refresh() {
                dataProvider.refresh(display.getTable());
	}

	public void delete(Eleve eleve) {
		ficheEPSService.deleteEleve(eleve, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				ErrorReportEvent.Fire("Erreur lors de la suppresion de l'élève: '" + caught.getMessage() + "'");
			}

			@Override
			public void onSuccess(Void result) {
				refresh();
			}
		});
	}

	private void save(final Eleve eleve) {
		if (eleve == null) {
			return;
		}

		ficheEPSService.updateEleveFirstLevel(eleve, new AsyncCallback<Eleve>() {
			@Override
			public void onSuccess(Eleve result) {
				// Nothing to do !
			}
			@Override
			public void onFailure(Throwable caught) {
				ErrorReportEvent.Fire("Impossible d'enregistrer l'élève: '" + caught.getMessage() + "'");
			}
		});
	}
        
        private void setEleveNumen(final Eleve eleve,final String newId) {
            if(eleve == null || newId == null || newId.isEmpty()) {
                return;
            }
            
            ficheEPSService.setEleveNumen(eleve.getOrid(),newId, new AsyncCallback<Eleve>() {
                @Override
                public void onSuccess(Eleve result) {
                        eleve.setId(newId);
                }
                @Override
                public void onFailure(Throwable caught) {
                        ErrorReportEvent.Fire("Impossible de modifier l'ID de l'élève: '" + caught.getMessage() + "'");
                        if(idEditTextCell!=null) { //Hack to refresh the cell: see gwt-bug#8724 or gwt-bug#6987
                            Object key = display.getTable().getValueKey(eleve);
                            if(key != null) {
                                idEditTextCell.clearViewData(key);
                            }
                        }
                        display.getTable().redraw();
                }
            });
        }

	private Widget createEleveUploadPhotoPanel(final Action uploadDoneAction, Eleve eleve) {
		String url = GWT.getModuleBaseURL() + "elevePhotoServlet?id=" + eleve.getId();
		final VerticalPanel progressBarPanel = new VerticalPanel();
		// final Map<String, ProgressBar> progressBars = new
		// LinkedHashMap<String, ProgressBar>();
		final Map<String, Image> cancelButtons = new LinkedHashMap<String, Image>();
		final Uploader uploader = new Uploader();
		uploader.setUploadURL(url)
				// .setButtonImageURL(GWT.getModuleBaseURL() +
				// "resources/images/buttons/upload_new_version_button.png")
				.setButtonText("Choisir une photo ...").setButtonWidth(133).setButtonHeight(22)
				// .setFileSizeLimit("50 MB")
				.setButtonCursor(Uploader.Cursor.HAND).setButtonAction(Uploader.ButtonAction.SELECT_FILES)
				.setFileQueuedHandler(new FileQueuedHandler() {
					public boolean onFileQueued(final FileQueuedEvent fileQueuedEvent) {
						// Create a Progress Bar for this file
						// final ProgressBar progressBar = new ProgressBar(0.0,
						// 1.0, 0.0, new CancelProgressBarTextFormatter());
						// progressBar.setTitle(fileQueuedEvent.getFile().getName());
						// progressBar.setHeight("18px");
						// progressBar.setWidth("200px");
						// progressBars.put(fileQueuedEvent.getFile().getId(),
						// progressBar);

						// Add Cancel Button Image
						final Image cancelButton = new Image(GWT.getModuleBaseURL()
								+ "resources/images/icons/cancel.png");
						cancelButton.setStyleName(display.getCSSStyle().cancelButton()/* "cancelButton" */);
						cancelButton.addClickHandler(new ClickHandler() {
							public void onClick(ClickEvent event) {
								uploader.cancelUpload(fileQueuedEvent.getFile().getId(), false);
								// progressBars.get(fileQueuedEvent.getFile().getId()).setProgress(-1.0d);
								cancelButton.removeFromParent();
							}
						});
						cancelButtons.put(fileQueuedEvent.getFile().getId(), cancelButton);

						// Add the Bar and Button to the interface
						HorizontalPanel progressBarAndButtonPanel = new HorizontalPanel();
						// progressBarAndButtonPanel.add(progressBar);
						progressBarAndButtonPanel.add(cancelButton);
						progressBarPanel.add(progressBarAndButtonPanel);

						return true;
					}
				}).setUploadProgressHandler(new UploadProgressHandler() {
					public boolean onUploadProgress(UploadProgressEvent uploadProgressEvent) {
						// ProgressBar progressBar =
						// progressBars.get(uploadProgressEvent.getFile().getId());
						// progressBar.setProgress(
						// (double) uploadProgressEvent.getBytesComplete() /
						// uploadProgressEvent.getBytesTotal()
						// );
						return true;
					}
				}).setUploadCompleteHandler(new UploadCompleteHandler() {
					public boolean onUploadComplete(UploadCompleteEvent uploadCompleteEvent) {
						cancelButtons.get(uploadCompleteEvent.getFile().getId()).removeFromParent();
						uploader.startUpload();
						uploadDoneAction.doAction();
						return true;
					}
				}).setFileDialogStartHandler(new FileDialogStartHandler() {
					public boolean onFileDialogStartEvent(FileDialogStartEvent fileDialogStartEvent) {
						if (uploader.getStats().getUploadsInProgress() <= 0) {
							// Clear the uploads that have completed, if none
							// are in process
							progressBarPanel.clear();
							// progressBars.clear();
							cancelButtons.clear();
						}
						return true;
					}
				}).setFileDialogCompleteHandler(new FileDialogCompleteHandler() {
					public boolean onFileDialogComplete(FileDialogCompleteEvent fileDialogCompleteEvent) {
						if (fileDialogCompleteEvent.getTotalFilesInQueue() > 0) {
							if (uploader.getStats().getUploadsInProgress() <= 0) {
								uploader.startUpload();
							}
						}
						return true;
					}
				}).setFileQueueErrorHandler(new FileQueueErrorHandler() {
					public boolean onFileQueueError(FileQueueErrorEvent fileQueueErrorEvent) {
						Window.alert("Upload of file " + fileQueueErrorEvent.getFile().getName() + " failed due to ["
								+ fileQueueErrorEvent.getErrorCode().toString() + "]: "
								+ fileQueueErrorEvent.getMessage());
						return true;
					}
				}).setUploadErrorHandler(new UploadErrorHandler() {
					public boolean onUploadError(UploadErrorEvent uploadErrorEvent) {
						cancelButtons.get(uploadErrorEvent.getFile().getId()).removeFromParent();
						Window.alert("Upload of file " + uploadErrorEvent.getFile().getName() + " failed due to ["
								+ uploadErrorEvent.getErrorCode().toString() + "]: " + uploadErrorEvent.getMessage());
						return true;
					}
				});

		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.add(uploader);

		if (Uploader.isAjaxUploadWithProgressEventsSupported()) {
			final Label dropFilesLabel = new Label("Drop Files Here");
			dropFilesLabel.setStyleName(display.getCSSStyle().dropFilesLabel()/* "dropFilesLabel" */);
			dropFilesLabel.addDragOverHandler(new DragOverHandler() {
				public void onDragOver(DragOverEvent event) {
					if (!uploader.getButtonDisabled()) {
						dropFilesLabel
								.addStyleName(display.getCSSStyle().dropFilesLabelHover()/* "dropFilesLabelHover" */);
					}
				}
			});
			dropFilesLabel.addDragLeaveHandler(new DragLeaveHandler() {
				public void onDragLeave(DragLeaveEvent event) {
					dropFilesLabel
							.removeStyleName(display.getCSSStyle().dropFilesLabelHover()/* "dropFilesLabelHover" */);
				}
			});
			dropFilesLabel.addDropHandler(new DropHandler() {
				public void onDrop(DropEvent event) {
					dropFilesLabel
							.removeStyleName(display.getCSSStyle().dropFilesLabelHover()/* "dropFilesLabelHover" */);

					if (uploader.getStats().getUploadsInProgress() <= 0) {
						progressBarPanel.clear();
						// progressBars.clear();
						cancelButtons.clear();
					}

					uploader.addFilesToQueue(Uploader.getDroppedFiles(event.getNativeEvent()));
					event.preventDefault();
				}
			});
			verticalPanel.add(dropFilesLabel);
		}

		HorizontalPanel horizontalPanel = new HorizontalPanel();
		horizontalPanel.add(verticalPanel);
		horizontalPanel.add(progressBarPanel);
		horizontalPanel.setVerticalAlignment(HorizontalPanel.ALIGN_MIDDLE);
		horizontalPanel.setCellHorizontalAlignment(uploader, HorizontalPanel.ALIGN_LEFT);
		horizontalPanel.setCellHorizontalAlignment(progressBarPanel, HorizontalPanel.ALIGN_RIGHT);

		DockPanel panel = new DockPanel();
		Label label = new Label("Modifier la photo de " + eleve.getNom() + " " + eleve.getPrenom() + ":");
		label.getElement().getStyle().setMarginBottom(8, Unit.PX);
		panel.add(label, DockPanel.NORTH);
		panel.add(horizontalPanel, DockPanel.CENTER);

		return panel;
	}

	
	
	
	
	
	
	private class EleveBirthdateCell extends AbstractCell<Eleve> {
		private boolean allowModif;
		
		public EleveBirthdateCell(boolean allowModif) {
			super(com.google.gwt.dom.client.BrowserEvents.CLICK);
			this.allowModif = allowModif;
		}

		@Override
		public void onBrowserEvent(final Context context, final Element parent, final Eleve value, NativeEvent event,
				final ValueUpdater<Eleve> valueUpdater) {
			super.onBrowserEvent(context, parent, value, event, valueUpdater);

			if(value != null && allowModif) {
				event.stopPropagation();
				final PopupPanel popup = new PopupPanel(true,true);
				final Calendar calendar = new Calendar();
				calendar.setShowTime(false);
				calendar.setShowWeeksColumn(false);
				calendar.getTodayButton().setVisible(false);
				calendar.setSelectedDate(value.getDateDeNaissance() ==  null ? new Date() : value.getDateDeNaissance());
				calendar.addCalendarListener(new CalendarListener<Calendar>() {
					@Override
					public void onChange(Calendar sender, Date oldValue) {
						Date date = calendar.getSelectedDate();
						date = new Date(date.getTime()  - ((Utils.SERVER_TIMEZONE_OFFSET + date.getTimezoneOffset()) * 60 * 1000 ));
						date.setHours(12);
                                                date.setMinutes(0);
                                                date.setSeconds(0);
                                                value.setDateDeNaissance(date);
						save(value);
						popup.hide();
					}
					@Override
					public void onCancel(Calendar sender) {
						//Nothing to do !!
					}
				});
				popup.add(calendar);
				popup.setPopupPosition(parent.getAbsoluteLeft(), parent.getAbsoluteTop());
				popup.addCloseHandler(new CloseHandler<PopupPanel>() {
					@Override
					public void onClose(CloseEvent<PopupPanel> event) {
						display.getTable().redrawRow(context.getIndex());
					}
				});				
				popup.show();
				calendar.display();
			}
		}

		@Override
		public void render(Context context, Eleve eleve, SafeHtmlBuilder sb) {
			if (eleve == null) {
				return;
			}
			sb.appendEscaped(Utils.FormatDateDDSlashMMSlashYYYY(eleve.getDateDeNaissance()));
		}
	}
	
	
	
	
	
	
	private class ElevePhotoCell extends AbstractCell<Eleve> {
		private boolean allowModif;
		
		public ElevePhotoCell(boolean allowModif) {
			super(com.google.gwt.dom.client.BrowserEvents.CLICK);
			this.allowModif = allowModif;
		}

		@Override
		public void onBrowserEvent(final Context context, final Element parent, final Eleve value, NativeEvent event,
				final ValueUpdater<Eleve> valueUpdater) {
			super.onBrowserEvent(context, parent, value, event, valueUpdater);

			if(allowModif) {
				event.stopPropagation();
				final PopupPanel popup = new PopupPanel();
				popup.add(createEleveUploadPhotoPanel(new Action() {
					@Override
					public void doAction() {
						popup.hide();
					}
				}, value));
				popup.setAutoHideEnabled(true);
				popup.setModal(true);
				popup.setPixelSize(parent.getClientWidth() * 2, parent.getClientHeight());
				popup.setPopupPosition(parent.getAbsoluteLeft(), parent.getAbsoluteTop());
				popup.show();
				popup.addCloseHandler(new CloseHandler<PopupPanel>() {
					@Override
					public void onClose(CloseEvent<PopupPanel> event) {
						display.getTable().redrawRow(context.getIndex());
					}
				});
			}
		}

		@Override
		public void render(Context context, Eleve eleve, SafeHtmlBuilder sb) {
			if (eleve == null) {
				return;
			}
			sb.appendHtmlConstant("<img height='64px' width='64px'  src='" + "ficheeps/elevePhotoServlet?id="
					+ eleve.getId() + "&vUpdate=" + System.currentTimeMillis() + "' />");
		}
	}
}

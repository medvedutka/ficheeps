package ficheeps.presenter;


import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import ficheeps.client.Action;
import ficheeps.client.ErrorReportEvent;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.model.Apsa;
import ficheeps.model.Commentaire;
import ficheeps.model.Eleve;
import ficheeps.model.Groupe;
import ficheeps.model.GroupeType;
import ficheeps.model.User;
import ficheeps.model.Commentaire.Niveau;
import ficheeps.presenter.CommentaryPresenter.ApsaWizard;
import ficheeps.presenter.ExportPDFPresenter.ExportType;
import ficheeps.view.CommentaryView;
import ficheeps.view.DialogMsg;
import ficheeps.view.ElevesListEmailsView;
import ficheeps.view.ExportPDFView;
import ficheeps.view.FicheEleveView;

public class ElevesNavigationEtSaisieFichePresenter {

	
	
	
	private ListDataProvider<String> classeDataProvider = new ListDataProvider<String>();
	private ListDataProvider<Groupe> groupesEPSDataProvider = new ListDataProvider<Groupe>();
	private ListDataProvider<Groupe> groupesASDataProvider = new ListDataProvider<Groupe>();
	private ListDataProvider<Eleve> elevesDataProvider = new ListDataProvider<Eleve>();

	private FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);

	
	public interface Display {
		HasWidgets getFichePanel();
                void bringGroupesEPSCellListFront();
                void bringGroupesASCellListFront();
		CellList<Groupe> getGroupesEPSCellList();
		CellList<Groupe> getGroupesASCellList();
		CellList<String> getClassesCellList();
		CellList<Eleve> getElevesCellList();
		
                HasClickHandlers getDownloadFiches();
                HasClickHandlers getListEmailsButton();                
		HasClickHandlers getSaveAction();
		void showSave();
		void hideSave();
	}

	

	public ElevesNavigationEtSaisieFichePresenter(User currentUser) {
		this.currentUser = currentUser;
	}
	
	
	private User currentUser;
	private Display display;
	private SingleSelectionModel<Eleve> eleveSelectionModel;
	private SingleSelectionModel<String> classesSelectionModel;
	private SingleSelectionModel<Groupe> groupesSelectionModel;
	private Eleve currentModifiedEleve = null;
	
	public void bind(final Display display) {
		this.display = display;
		classeDataProvider.addDataDisplay(display.getClassesCellList());
		groupesEPSDataProvider.addDataDisplay(display.getGroupesEPSCellList());
		groupesASDataProvider.addDataDisplay(display.getGroupesASCellList());
		elevesDataProvider.addDataDisplay(display.getElevesCellList());

	    eleveSelectionModel = new SingleSelectionModel<Eleve>(Eleve.KEY_PROVIDER);
	    display.getElevesCellList().setSelectionModel(eleveSelectionModel);
		
	    classesSelectionModel = new SingleSelectionModel<String>();
	    groupesSelectionModel = new SingleSelectionModel<Groupe>();	    
	    
	    display.getClassesCellList().setSelectionModel(classesSelectionModel);
	    	classesSelectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
                  @Override
	          public void onSelectionChange(SelectionChangeEvent event) {
	        	  if(classesSelectionModel.getSelectedObject() == null || classesSelectionModel.getSelectedObject().isEmpty()) {
	        		  return;
	        	  }
	        	  groupesSelectionModel.clear();
	        	  lastChanceBeforeDisapearing(new Action() {
						@Override
						public void doAction() {
                                                    displayAllElevesForSelectedClass(null);
						}
					});
	          }
	        });			
		
	    	display.getGroupesEPSCellList().setSelectionModel(groupesSelectionModel);
	    	display.getGroupesASCellList().setSelectionModel(groupesSelectionModel);
	    	groupesSelectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
                  @Override
	          public void onSelectionChange(SelectionChangeEvent event) {
	        	  if(groupesSelectionModel.getSelectedObject() == null 
	        			  || groupesSelectionModel.getSelectedObject().getNom() == null
	        			  || groupesSelectionModel.getSelectedObject().getNom().isEmpty()) {
	        		  return;
	        	  }
	        	  classesSelectionModel.clear();
	        	  lastChanceBeforeDisapearing(new Action() {
						@Override
						public void doAction() {
                                                    displayAllElevesForSelectedGroup(null);
						}
					});
	          }
	        });		

	    	
	    eleveSelectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
                  @Override
	          public void onSelectionChange(SelectionChangeEvent event) {
	        	  final Eleve selected = eleveSelectionModel.getSelectedObject();	 
	        	  lastChanceBeforeDisapearing(new Action() {
					@Override
					public void doAction() {
						showFicheEleve(selected);
					}
				});
	          }
	        });		

	    display.getSaveAction().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				save(currentModifiedEleve,true);
			}
		});
	    
            display.getDownloadFiches().addClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        String c = classesSelectionModel.getSelectedObject();
                        if(c != null && !c.isEmpty()) {
                            exportPDF(ExportType.ExportClasse,c);
                        }
                        else {
                            Groupe g = groupesSelectionModel.getSelectedObject();
                            if(g != null && g.getNom() != null && !g.getNom().isEmpty()) {
                                    exportPDF(ExportType.ExportGroupe,g.getNom());
                            }
                        }
                    }
                });
            
            display.getListEmailsButton().addClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        List<String> classes = new LinkedList<String>();
                        List<String> groupes = new LinkedList<String>();
                        String c = classesSelectionModel.getSelectedObject();
                        if(c != null && !c.isEmpty()) {
                            classes.add(c);
                        }
                        Groupe g = groupesSelectionModel.getSelectedObject();
                        if(g != null && g.getNom() != null && !g.getNom().isEmpty()) {
                            groupes.add(g.getNom());
                        }
                        showListEmails(classes,groupes);
                    }
                });
	}
	
        
        private void showListEmails(List<String> classes,List<String> groupes) {
            ElevesListEmailsPresenter presenter = new ElevesListEmailsPresenter(classes, groupes, null);
            ElevesListEmailsView view = new ElevesListEmailsView();
            presenter.bind(view);
            view.setSize("640px", "480px");
            DialogMsg.ShowContent("Liste des courriels", view);
        }
	
	private void exportPDF(ExportType exportType, String id) {
		final ExportPDFPresenter exportPDFPresenter = new ExportPDFPresenter();
		exportPDFPresenter.setExport(exportType, id);
		ExportPDFView exportPDFView = new ExportPDFView();
		exportPDFPresenter.bind(exportPDFView);
	
		DialogMsg.ShowOkCancel("Export PDF", 
				exportPDFView, 
				new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						exportPDFPresenter.startExport();
					}
				}, 
				null);

	}
	
	
	private ApsaWizard commentaryWizardCommand = new ApsaWizard() {
		@Override
		public void execute(Niveau niveau, String annee, CommentaryPresenter commentairePresenter) {
			Set<String> descriptions = new HashSet<String>();
			for(Eleve eleve : elevesDataProvider.getList()) {
				Commentaire c = eleve.getCommentaireWithNiveau(niveau);
				if(c != null && ficheeps.client.Utils.StringEquals(annee,c.getAnneeScolaire()) && c.getApsas() != null) {
					for(Apsa apsa : c.getApsas()) {
						String activite = apsa.getActivite();
						if(activite != null && !activite.trim().isEmpty()) {
							descriptions.add(activite);
						}
					}
				}
			}
			
			showCommentaryApsaWizard(commentairePresenter, new LinkedList<String>(descriptions));
		}
	};
	
	
	
	private void showCommentaryApsaWizard(final CommentaryPresenter commentairePresenter, List<String> apsaSuggestion) {
		VerticalPanel panel = new VerticalPanel();
		Label label = new Label("Les Aspsa suivantes seront automatiquement insérés:");
		panel.add(label);
		HTML html = new HTML("<i><font color='grey'>Note: Supprimez des Apsa en utilisant la touche 'suppr' de votre clavier.</font></i>");
		panel.add(html);
		
		final ListBox listBox = new ListBox(true);
		for(String s: apsaSuggestion) {
			listBox.addItem(s,s);
		}
		listBox.setVisibleItemCount(7);
		//listBox.setWidth("35em");
		listBox.setWidth("100%");
		listBox.addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if(event.getNativeKeyCode()== KeyCodes.KEY_DELETE) {
					int index = -1;
					while(( index = listBox.getSelectedIndex()) != -1) {
						listBox.removeItem(index);
					}
				}
			}
		});
		panel.add(listBox);
		
		DialogMsg.ShowOkCancel("Suggestion d'Apsa:", 
				panel, 
				new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						for(int i = 0;i<listBox.getItemCount();i++) {
							String s = listBox.getValue(i);
							commentairePresenter.newApsa(s);
						}
					}
				}, 
				null);		
	}
	
	
	public void lastChanceBeforeDisapearing(final Action continuation) {
		if(currentModifiedEleve == null) {
			continuation.doAction();
		
		}
		else {
			DialogMsg.ShowYesNoCancel("Modifications en cours !", "Souhaitez vous sauvegarder les modifications faites ?"
					, null, new ClickHandler() { // 'yes' clicked
						@Override
						public void onClick(ClickEvent event) {
							save(currentModifiedEleve,false);
							continuation.doAction();
						}
					}
					, null, new ClickHandler() { // 'no' clicked!
						@Override
						public void onClick(ClickEvent event) {
							currentModifiedEleve = null;
							display.hideSave();
							continuation.doAction();
						}
					}
					, null, null/*cancel clicked*/);
		}
	}
	
	public void showFicheEleve(Eleve eleve) {
		display.getFichePanel().clear();
		
		if(eleve == null) {
			return;
		}
		
		  FicheElevePresenter fep = new FicheElevePresenter(currentUser, eleve);
		  fep.setCommentaryApsaWizard(commentaryWizardCommand);
		  fep.addValueChangeHandler(new ValueChangeHandler<Eleve>() {
			@Override
			public void onValueChange(ValueChangeEvent<Eleve> event) {
				currentModifiedEleve = event.getValue();
				display.showSave();
			}
		  });
		  FicheEleveView fev = new FicheEleveView();
		  fep.bind(fev);
		  display.getFichePanel().add(fev);	
	}
	
        
        private void displayAllElevesForSelectedClass(final String eleveIdToSelect) {
            display.getFichePanel().clear();
            eleveSelectionModel.clear();
            retrieveElevesForClasse(classesSelectionModel.getSelectedObject(),eleveIdToSelect);      
        }
        
        private void displayAllElevesForSelectedGroup(final String eleveIdToSelect) {
            display.getFichePanel().clear();
            eleveSelectionModel.clear();
            retrieveElevesForGroupe(groupesSelectionModel.getSelectedObject(),eleveIdToSelect);            
        }        
        
	private void retrieveClasses(final String classToSelect, final String eleveIdToSelect) {
		classesSelectionModel.clear();
		List<String> list = classeDataProvider.getList();
		list.clear();
		ficheEPSService.getClasses(new AsyncCallback<List<String>>() {
			@Override
			public void onSuccess(List<String> result) {
				List<String> list = classeDataProvider.getList();
				list.clear();
				for(String c : result) {
					list.add(c);
				}
				classeDataProvider.refresh();
                                if(classToSelect != null && !classToSelect.isEmpty()) {
                                    classesSelectionModel.setSelected(classToSelect, true);
                                    display.getClassesCellList().getRowElement(list.indexOf(classToSelect)).scrollIntoView();
                                    displayAllElevesForSelectedClass(eleveIdToSelect);
                                }
			}
			@Override
			public void onFailure(Throwable caught) {
				ErrorReportEvent.Fire("Impossible de retouver la liste des classes: '" + caught.getMessage() + "'");
			}
		});	
	}
	
	private void retrieveGroupes(final String groupNameToSelect, final String eleveIdToSelect) {
		groupesSelectionModel.clear();
		groupesEPSDataProvider.getList().clear();
		groupesASDataProvider.getList().clear();
		ficheEPSService.getGroupesLight(new AsyncCallback<List<Groupe>>() {
			@Override
			public void onSuccess(List<Groupe> result) {
                                Groupe groupeToSelect = null;
                                int groupeToSelectIndex = 0;
				List<Groupe> listEPS = groupesEPSDataProvider.getList();
				List<Groupe> listAS = groupesASDataProvider.getList();
				listEPS.clear();
				listAS.clear();
				for(Groupe c : result) {
					if(GroupeType.EPS.equals(c.getGroupeType())) {
						listEPS.add(c);
					}
					else {
						listAS.add(c);
					}
                                        
                                        if(groupNameToSelect != null && !groupNameToSelect.isEmpty() && groupNameToSelect.equals(c.getNom())) {
                                            groupeToSelect = c;
                                            if(groupeToSelect.getGroupeType() == GroupeType.EPS) {
                                                groupeToSelectIndex = listEPS.size() - 1;
                                            }
                                            else {
                                                groupeToSelectIndex = listAS.size() - 1;
                                            }
                                        }
				}
				groupesEPSDataProvider.refresh();
				groupesASDataProvider.refresh();
                                if(groupeToSelect != null) {
                                    final int _groupeToSelectIndex = groupeToSelectIndex;
                                    groupesSelectionModel.setSelected(groupeToSelect, true);
                                    if(groupeToSelect.getGroupeType() == GroupeType.EPS) {
                                        display.bringGroupesEPSCellListFront();
                                        GWT.runAsync(new RunAsyncCallback() {
                                            @Override
                                            public void onFailure(Throwable reason) {}
                                            @Override
                                            public void onSuccess() {
                                                display.getGroupesEPSCellList().getRowElement(_groupeToSelectIndex).scrollIntoView();
                                            }
                                        });
                                    }
                                    else {
                                        display.bringGroupesASCellListFront();
                                        GWT.runAsync(new RunAsyncCallback() {
                                            @Override
                                            public void onFailure(Throwable reason) {}
                                            @Override
                                            public void onSuccess() {
                                                display.getGroupesASCellList().getRowElement(_groupeToSelectIndex).scrollIntoView();
                                            }
                                        });
                                    }
                                    
                                    displayAllElevesForSelectedGroup(eleveIdToSelect);
                                }
			}
			@Override
			public void onFailure(Throwable caught) {
				ErrorReportEvent.Fire("Impossible de retouver la liste des groupes: '" + caught.getMessage() + "'");
			}
		});	
	}
	
        
        private final Comparator<Eleve> eleveComparator = new Comparator<Eleve>() {
					@Override
					public int compare(Eleve e0, Eleve e1) {
						return (e0.getNom() + " " + e0.getPrenom()).compareTo(e1.getNom()+" "+e1.getPrenom());
					}
				};
	
	private void retrieveElevesForClasse(final String classe, final String eleveIdToSelect) {
		ficheEPSService.getElevesFromClasse(classe,new AsyncCallback<List<Eleve>>() {
			@Override
			public void onSuccess(List<Eleve> result) {
				List<Eleve> list = elevesDataProvider.getList();
				list.clear();
				Collections.sort(result, eleveComparator);
				list.addAll(result);
				elevesDataProvider.refresh();
                                
                                if(eleveIdToSelect != null && !eleveIdToSelect.isEmpty()) {
                                    int rowIndex = 0;
                                    for(Eleve eleve : result) {
                                        if(eleveIdToSelect.equals(eleve.getId())) {
                                            eleveSelectionModel.setSelected(eleve, true);
                                            display.getElevesCellList().getRowElement(rowIndex).scrollIntoView();
                                            break;
                                        }
                                        rowIndex++;
                                    }
                                }
			}
			@Override
			public void onFailure(Throwable caught) {
				ErrorReportEvent.Fire("Impossible de retrouver les élèves de la classe " + classe + ": '" + caught.getMessage() + "'");
			}
		});			
	}

	private void retrieveElevesForGroupe(final Groupe groupe, final String eleveIdToSelect) {
		ficheEPSService.getGroupeByOrid(groupe.getOrid(), new AsyncCallback<Groupe>() {
			@Override
			public void onSuccess(Groupe result) {
				List<Eleve> list = elevesDataProvider.getList();
				list.clear();
				if(result.getEleves() != null) {
					list.addAll(result.getEleves());
				}
				Collections.sort(list,eleveComparator);
				elevesDataProvider.refresh();
                                
                                 if(eleveIdToSelect != null && !eleveIdToSelect.isEmpty()) {
                                    int rowIndex = 0;
                                    for(Eleve eleve : list) {
                                        if(eleveIdToSelect.equals(eleve.getId())) {
                                            eleveSelectionModel.setSelected(eleve, true);
                                            display.getElevesCellList().getRowElement(rowIndex).scrollIntoView();
                                            break;
                                        }
                                        rowIndex++;
                                    }
                                }
			}
			@Override
			public void onFailure(Throwable caught) {
                            ErrorReportEvent.Fire("Impossible de retrouver les élèves du groupe " + groupe.getNom() + " ("+groupe.getOrid()+") : '" + caught.getMessage() + "'");
			}
		});			
	}
	

        public void refresh() {
            refresh(true,null,null);
        }
        public void refresh(boolean classOrGroup, String classOrGroupName, String eleveId) {
            display.getFichePanel().clear();
            elevesDataProvider.getList().clear();
            elevesDataProvider.refresh();
            retrieveClasses(classOrGroup ? classOrGroupName : null,eleveId);	
            retrieveGroupes((!classOrGroup) ? classOrGroupName : null,eleveId);
        }
	
	
	private void save(final Eleve eleve, final boolean refreshFicheEleveAfterSave) {
		if(eleve == null) {
			return;
		}
		
		if(eleve.getCommentaires() != null) {
			eleve.cleanupForSave();
			
			// Update ApsaOracle:
			if(eleve.getCommentaires() !=  null) {
				for(Commentaire c : eleve.getCommentaires()) {
					if(c.getApsas() != null) {
						for(Apsa apsa : c.getApsas()) {
							CommentaryView.APSA_ORACLE.add(apsa.getActivite());
						}
					}
				}
			}
			
		}
		ficheEPSService.updateEleveDeep(eleve, new AsyncCallback<Eleve>() {
			@Override
			public void onSuccess(Eleve result) {
				currentModifiedEleve = null;
				int index = elevesDataProvider.getList().indexOf(eleve);
				elevesDataProvider.getList().remove(index);
				elevesDataProvider.getList().add(index,result);
				elevesDataProvider.refresh();
				if(refreshFicheEleveAfterSave) {
					showFicheEleve(result);
				}
				display.hideSave();
			}
			@Override
			public void onFailure(Throwable caught) {
				ErrorReportEvent.Fire("Erreur lors de l'enregistrement de l'élève: '" + caught.getMessage() + "'");
			}
		});
	}

}

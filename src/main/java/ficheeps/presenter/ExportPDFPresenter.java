package ficheeps.presenter;

import java.util.Iterator;


import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.FormElement;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FocusWidget;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.view.DialogMsg;

public class ExportPDFPresenter {

	private FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);

	
	public interface Display {
		  HasValue<Boolean> getSortByLevel();
		  HasValue<Boolean> getExportSuiviMedial();
		  HasValue<Boolean> getFicheSuggestionFinDeSeconde();
		  HasValue<Boolean> getFicheEleve();
		  RadioButton getFicheSuggestionFinDeSecondeUseDefaultTemplate();
		  RadioButton getFicheSuggestionFinDeSecondeUseCustomTemplate();
		  Widget getFileUpload();
		  
		  HasValue<Boolean> getFSFDSDoNotExportCartoucheSeconde(); 
		  HasValue<Boolean> getFSFDSExportCartoucheSeconde();
		  HasValue<Boolean> getFSFDSExportCartoucheSecondeIfNotEmpty();
		  HasValue<Boolean> getFSFDSDoNotExportSuggestionFinDeSeconde();
		  HasValue<Boolean> getFSFDSExportSuggestionFinDeSeconde();
		  HasValue<Boolean> getFSFDSExportSuggestionFinDeSecondeIfNotEmpty();
		  
		  SimplePanel getPanelOptionsExportFicheEleve();
		  FormPanel getPanelOptionsSuggestionFinDeSeconde();
	}
	
	
	public ExportPDFPresenter() {
	}
	
	public enum ExportType {
		ExportClasse,
		ExportGroupe,
		ExportEleve,
                All
	}
	
	private boolean noDefaultTemplateForSuggestionFinDeSeconde = false;
	private ExportType exportType;
	private String exportID;
	
	public void setExport(ExportType type, String id) {
		exportType = type;
		exportID = id;
	}
	
	private Display display;
	
	public void bind(Display display) {
		this.display = display;
		display.getFicheSuggestionFinDeSeconde().addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				enableOrDisableUIItems();
			}
		});
		display.getFicheEleve().addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				enableOrDisableUIItems();
			}
		});

		display.getPanelOptionsSuggestionFinDeSeconde().setEncoding(FormPanel.ENCODING_MULTIPART);
		display.getPanelOptionsSuggestionFinDeSeconde().setMethod(FormPanel.METHOD_POST);
		display.getPanelOptionsSuggestionFinDeSeconde().getElement().<FormElement>cast().setTarget("_blank");
		
		display.getFSFDSExportCartoucheSeconde().setValue(true, true);
		display.getFSFDSExportSuggestionFinDeSeconde().setValue(true, true);
		
		display.getFicheEleve().setValue(true,true);
		
		ficheEPSService.isTemplateSuggestionFinDeSeondeSet(new AsyncCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean result) {
				noDefaultTemplateForSuggestionFinDeSeconde = !result;
				enableOrDisableUIItems();
			}
			@Override
			public void onFailure(Throwable caught) {
				DialogMsg.ShowMessage("Erreur", "Impossible de savoir si un modèle de fiche 'suggestion fin de seconde' est définis");
			}
		});
	}

	
	public void startExport() {
		String url = com.google.gwt.core.client.GWT.getModuleBaseURL() + "elevePhotoServlet?";
		switch (exportType) {
			case ExportEleve:
				url += "id=" + com.google.gwt.http.client.URL.encodeQueryString(exportID);
				break;
			case ExportClasse:
				url += "classe=" + com.google.gwt.http.client.URL.encodeQueryString(exportID);
				break;
			case ExportGroupe:
				url += "groupe=" + com.google.gwt.http.client.URL.encodeQueryString(exportID);
				break;
                        case All:
                                url += "all=true";
                                break;
		}
		url += "&fiche=pdf&dummyPreventCache=" + System.currentTimeMillis();
		
		if(display.getFicheSuggestionFinDeSeconde().getValue()) {
			url += "&ExportFicheSuggestionFinSeconde=true";
			if(display.getFicheSuggestionFinDeSecondeUseDefaultTemplate().getValue()) {
				url += "&FicheSuggestionFinSecondeUseDefaultTemplate=true";
			}
			
			if(display.getFSFDSDoNotExportCartoucheSeconde().getValue()) {
				url += "&FSFDSExportCartoucheSeconde=false";
			}
			else if(display.getFSFDSExportCartoucheSeconde().getValue()) {
				url += "&FSFDSExportCartoucheSeconde=true";
			}
			else if(display.getFSFDSExportCartoucheSecondeIfNotEmpty().getValue()) {
				url += "&FSFSExportCartoucheSecondeSiNonVide=true";
			}
			
			if(display.getFSFDSDoNotExportSuggestionFinDeSeconde().getValue()) {
				url += "&FSFDSExportSuggestionFinDeSeconde=false";
			}
			else if(display.getFSFDSExportSuggestionFinDeSeconde().getValue()) {
				url += "&FSFDSExportSuggestionFinDeSeconde=true";
			}
			else if(display.getFSFDSExportSuggestionFinDeSecondeIfNotEmpty().getValue()) {
				url += "&FSFSExportSuggestionFinDeSecondeSiNonVide=true";
			}
			
			display.getPanelOptionsSuggestionFinDeSeconde().setAction(url);
			display.getPanelOptionsSuggestionFinDeSeconde().submit();
		}
		else { // fiche Eleve standard.
			if(display.getExportSuiviMedial().getValue()) {
				url += "&ShowSuiviMedical=true";
			}
			if(display.getSortByLevel().getValue()) {
				url += "&CommentsInLevelOrder=true";
			}
			Window.open(url, "_blank", "enabled");
		}
	}
	
	private void enableOrDisableUIItems() {
		if(display.getFicheSuggestionFinDeSeconde().getValue()) {
			enableAllChildren(false,display.getPanelOptionsExportFicheEleve());
			enableAllChildren(true,display.getPanelOptionsSuggestionFinDeSeconde());
		}
		else {
			enableAllChildren(true,display.getPanelOptionsExportFicheEleve());
			enableAllChildren(false,display.getPanelOptionsSuggestionFinDeSeconde());			
		}
		
		if(noDefaultTemplateForSuggestionFinDeSeconde) {
			display.getFicheSuggestionFinDeSecondeUseDefaultTemplate().setEnabled(false);
			display.getFicheSuggestionFinDeSecondeUseCustomTemplate().setValue(true, true);
		}
		else {
			display.getFicheSuggestionFinDeSecondeUseDefaultTemplate().setEnabled(true);
		}
		
		if(display.getFicheSuggestionFinDeSecondeUseDefaultTemplate().getValue()) {
			display.getFileUpload().setVisible(false);
		}
	}
	
	
	private void enableAllChildren(boolean enable, Widget widget)
	{
	    if (widget instanceof HasWidgets)
	    {
	        Iterator<Widget> iter = ((HasWidgets)widget).iterator();
	        while (iter.hasNext())
	        {
	            Widget nextWidget = iter.next();
	            enableAllChildren(enable, nextWidget);
	            nextWidget.setVisible(enable);
	            if (nextWidget instanceof FocusWidget)
	            {
	                ((FocusWidget)nextWidget).setEnabled(enable);
	            }
	        }
	    }
	}
}

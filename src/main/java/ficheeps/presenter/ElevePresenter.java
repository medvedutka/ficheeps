package ficheeps.presenter;


import java.util.Date;

import com.google.gwt.user.client.ui.HasValue;



import ficheeps.model.Eleve;

public class ElevePresenter {

	private Eleve eleve;
	
	public ElevePresenter(Eleve eleve) {
		this.eleve = eleve;
	}
	
	public interface Display {
		HasValue<String> getId();
		HasValue<String> getNom();
		HasValue<String> getPrenom();
		HasValue<String> getClasse();
		HasValue<Date> getDateDeNaissance();
	}

	public void bind(Display display) {
		display.getId().setValue(eleve.getNom());
		display.getNom().setValue(eleve.getPrenom());	
	}
}

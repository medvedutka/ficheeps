package ficheeps.client;


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class FicheEPS implements EntryPoint {

    /**
     * The message displayed to the user when the server cannot be reached or
     * returns an error.
     */
    private static final String SERVER_ERROR = "An error occurred while "
            + "attempting to contact the server. Please check your network "
            + "connection and try again.";

    @Override
    public void onModuleLoad() {
        Resources.INSTANCE.ficheepsStyle().ensureInjected();
        
        //AppController appViewer = new AppController(rpcService, eventBus);
        AppController appViewer = new AppController();
        appViewer.go(RootPanel.get());

    }
}

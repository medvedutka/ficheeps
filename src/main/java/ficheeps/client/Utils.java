package ficheeps.client;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.googlecode.gwt.crypto.bouncycastle.digests.SHA1Digest;
import ficheeps.model.Eleve;
import java.util.Comparator;

/**
 * *
 * Warning: user only client side !!! (can't be used server side)
 *
 * @author thecat
 */
public class Utils {

    /**
     * Offset in minutes
     */
    public static int SERVER_TIMEZONE_OFFSET = 0;

    private static DateTimeFormat formatDateDDSlashMMSlashYYYY = DateTimeFormat.getFormat("dd/MM/yyyy");

    public static String FormatDateDDSlashMMSlashYYYY(Date date) {
        if (date == null) {
            return "#null#";
        }
        Date d = new Date(date.getTime() + ((Utils.SERVER_TIMEZONE_OFFSET + date.getTimezoneOffset()) * 60 * 1000));
        return formatDateDDSlashMMSlashYYYY.format(d);
    }

    public static class DDSlashMMSlashYYYYDateFormat extends DateTimeFormat {

        public DDSlashMMSlashYYYYDateFormat() {
            super("dd/MM/yyyy");
        }

        @Override
        public String format(Date date) {
            //Date d = new Date(date.getTime()  + ((Utils.SERVER_TIMEZONE_OFFSET + date.getTimezoneOffset()) * 60 * 1000 ));
            return super.format(date);
        }

        @Override
        public int parse(String text, int start, Date date) {
            Date d = new Date(date.getTime() + ((Utils.SERVER_TIMEZONE_OFFSET + date.getTimezoneOffset()) * 60 * 1000));
            return super.parse(text, start, d);
        }

        @Override
        public int parseStrict(String text, int start, Date date) {
            Date d = new Date(date.getTime() + ((Utils.SERVER_TIMEZONE_OFFSET + date.getTimezoneOffset()) * 60 * 1000));
            return super.parseStrict(text, start, d);
        }

        @Override
        public String format(Date date, com.google.gwt.i18n.shared.TimeZone timeZone) {
            //Date d = new Date(date.getTime()  + ((Utils.SERVER_TIMEZONE_OFFSET + date.getTimezoneOffset()) * 60 * 1000 ));
            return super.format(date, timeZone);
        }

        @Override
        public Date parse(String text) throws IllegalArgumentException {
            return super.parse(text);
        }

        @Override
        public Date parseStrict(String text) throws IllegalArgumentException {
            return super.parseStrict(text);
        }
    }

    public static String Sha1(String pwd) {
        try {
            //String passwordString = "happa3";
            SHA1Digest sha1 = new SHA1Digest();
            byte[] bytes;
            byte[] result = new byte[sha1.getDigestSize()];
            bytes = pwd.getBytes();
            sha1.update(bytes, 0, bytes.length);
            int val = sha1.doFinal(result, 0);
            //assertEquals("fe7f3cffd8a5f0512a5f1120f1369f48cd6f47c2", byteArrayToHexString(result));
            return byteArrayToHexString(result);
        } catch (ArrayIndexOutOfBoundsException e) {
            //fail(e.toString());
        } catch (Exception e) {
            //fail(e.toString());
        }
        return null;
    }

    private static String byteArrayToHexString(byte[] b) throws Exception {
        String result = "";
        for (int i = 0; i < b.length; i++) {
            result
                    += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }

    public static Comparator<Eleve> ELEVE_ALPHABETICAL_COMPARATOR = new Comparator<Eleve>() {
        @Override
        public int compare(Eleve e0, Eleve e1) {
            return (e0.getNom() + " " + e0.getPrenom()).compareTo(e1.getNom() + " " + e1.getPrenom());
        }
    };
    
    public static Comparator<Eleve> ELEVE_DATEBIRTH_COMPARATOR = new Comparator<Eleve>() {
        @Override
        public int compare(Eleve e0, Eleve e1) {
            if(e0 ==  e1) {
                return 0;
            }
            if(e0 == null) {
                return -1;
            }
            if(e1 == null) {
                return 1;
            }
            if(e0.getDateDeNaissance() == e1.getDateDeNaissance()) {
                return 0;
            }
            if(e0.getDateDeNaissance() == null) {
                return -1;
            }
            if(e1.getDateDeNaissance() == null) {
                return 1;
            }
            return (e0.getDateDeNaissance().compareTo(e1.getDateDeNaissance()));
        }
    };    

    
    public static Comparator<Eleve> ELEVE_CLASSE_COMPARATOR = new Comparator<Eleve>() {
        @Override
        public int compare(Eleve e0, Eleve e1) {
            if(e0 ==  e1) {
                return 0;
            }
            if(e0 == null) {
                return -1;
            }
            if(e1 == null) {
                return 1;
            }
            if(e0.getClasse() == e1.getClasse()) {
                return 0;
            }
            if(e0.getClasse() == null) {
                return -1;
            }
            if(e1.getClasse() == null) {
                return 1;
            }
            return e0.getClasse().compareToIgnoreCase(e1.getClasse());
        }
    };        
    
    
    public static boolean StringEquals(String s1,String s2) {
        if(s1 == s2) {
            return true;
        }
        if(s1 == null || s2 == null) {
            return false;
        }
        return s1.equals(s2);
    }

    public static int StringCompareIgnoreCase(String s1,String s2) {
        if(s1 == s2) {
            return 0;
        }
        if(s1 == null) {
            return 1;
        }
        if(s2 == null) {
            return -1;
        }
        return s1.compareToIgnoreCase(s2);
    }
    
    public static boolean IsStringNullOrEmpty(String s) {
        if(s == null || s.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Return the string s as en empty string if null or to the trimed lower cased version
     * @param s
     * @return 
     */
    public static String StringToTrimedOrEmpty(String s) {
        if(s == null) {
            return "";
        }
        return s.trim();
    }   
}

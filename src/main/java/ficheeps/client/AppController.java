package ficheeps.client;


import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.dom.client.Style.VerticalAlign;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.BeforeSelectionEvent;
import com.google.gwt.event.logical.shared.BeforeSelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.layout.client.Layout.Alignment;
import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.Event.Type;

import ficheeps.client.AppController.Events.SwitchViewEvent.View;
import ficheeps.client.service.FicheEPSService;
import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.model.Settings;
import ficheeps.model.User;
import ficheeps.presenter.AdministrationPresenter;
import ficheeps.presenter.ElevesNavigationEtSaisieFichePresenter;
import ficheeps.presenter.GroupeManagementPresenter;
import ficheeps.presenter.ImportPresenter;
import ficheeps.presenter.ListeElevesPresenter;
import ficheeps.presenter.ListesLibreManagementPresenter;
import ficheeps.view.AdministrationView;
import ficheeps.view.ElevesNavigationEtSaisieFicheView;
import ficheeps.view.GroupeManagementView;
import ficheeps.view.ImportView;
import ficheeps.view.ListeElevesView;
import ficheeps.view.ListesLibreManagementView;

public class AppController implements ValueChangeHandler<String> {

	/**
	 * Disable Login, authenticate with "debugtest" user with admin role
	 */
	private static boolean DEVELOPPEMENT_MODE = false;
	
	
	FicheEPSServiceAsync ficheEPSService = (FicheEPSServiceAsync) GWT.create(FicheEPSService.class);

        public static Settings SETTINGS = null;
        
	
	
	public AppController() {
		
		ficheEPSService.getServerTimezoneOffset(new AsyncCallback<Integer>() {
			@Override
			public void onFailure(Throwable caught) {
				// TODO /*Well ... pretty fucked right ?*/
			}
			@Override
			public void onSuccess(Integer result) {
				GWT.log("getServerTimezoneOffset (from client): " + result);
				Utils.SERVER_TIMEZONE_OFFSET = result;
			}
		});
		
		ErrorReportEvent.Register(new ErrorReportEvent.Handler() {
			@Override
			public void onErrorReceived(ErrorReportEvent event) {
				reportError(event);
			}
		});
		
		Events.addHandler(Events.SwitchViewEvent.TYPE, new Events.SwitchViewEvent.Handler() {
			@Override
			public void switchView(Events.SwitchViewEvent switchViewEvent) {
				beforeSwitchMainView(switchViewEvent);
			}
		});
	}
	
	
	
	private void beforeSwitchMainView(final Events.SwitchViewEvent switchViewEvent) {
		enesfPresenter.lastChanceBeforeDisapearing(new Action() {
			@Override
			public void doAction() {
				switchMainView(switchViewEvent);
			}
		});
	}
	
	private void switchMainView(final Events.SwitchViewEvent switchViewEvent) {
		switch(switchViewEvent.view) {
			case EleveNavigationEtSaisie:
				tabPanel.selectTab(0,false);
				enesfPresenter.refresh(switchViewEvent.isClassElseGroup,switchViewEvent.classOrGroupName,switchViewEvent.eleveId);
				break;
			case ListeEleveGlobale:
				tabPanel.selectTab(1,false);
				listeElevesPresenter.refresh();
				break;
			case GroupeManagement:
				tabPanel.selectTab(2,false);
				groupeManagementPresenter.refresh();
				break;
                        case ListeLibre:
                                tabPanel.selectTab(3,false);
                                listeLibreManagementPresenter.refresh();
                                break;
			case Administration:
				tabPanel.selectTab(4,false);
				administrationPresenter.refresh();
				break;
			case Import:
				tabPanel.selectTab(5,false);
				break;
		}
	}


	private void reportError(ErrorReportEvent errorEvent) {
		PopupPanel errorPopup = new PopupPanel();
		VerticalPanel contents = new VerticalPanel();
		contents.setSpacing(4);
		contents.add(new Label("Une erreur c'est produite:"));
		Label label = new Label(errorEvent.getMessage());
		contents.add(label);
		errorPopup.setWidget(contents);

		errorPopup.setAnimationEnabled(true);
		errorPopup.setAutoHideEnabled(true);
		errorPopup.setModal(true);
		errorPopup.setGlassEnabled(true);
		errorPopup.center();
		errorPopup.show();
	}
	

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		String token = event.getValue();
		/*
		 * if (token != null) { Presenter presenter = null;
		 * 
		 * if (token.equals("list")) { presenter = new
		 * ContactsPresenter(rpcService, eventBus, new ContactView()); } else if
		 * (token.equals("add")) { presenter = new
		 * EditContactPresenter(rpcService, eventBus, new EditContactView()); }
		 * else if (token.equals("edit")) { presenter = new
		 * EditContactPresenter(rpcService, eventBus, new EditContactView()); }
		 * 
		 * if (presenter != null) { presenter.go(container); } }
		 */
	}

	private void bind() {
		History.addValueChangeHandler(this);
	}
	
	
	public void go(final HasWidgets container) {
		if ("".equals(History.getToken())) {			
			
			if(DEVELOPPEMENT_MODE) {
				initForDevelopementMode();
			}
			else {
				ficheEPSService.getCurrentSessionUser(new AsyncCallback<User>() {
					@Override
					public void onSuccess(User result) {
						if (result != null) {
							firstInitAfterLogon(result);
						} else {
							Window.Location.assign("login");
						}
					}
	
					@Override
					public void onFailure(Throwable caught) {
					}
				});
			}
		} else {
			History.fireCurrentHistoryState();
		}
	}
	
	
	
	private void initForDevelopementMode() {
		User user =new User();
		user.setFullName("Developpement Mode");
		user.setLogin("debugadmin");
		user.setAdmin(true);
		firstInitAfterLogon(user);
	}
	
	
	private ElevesNavigationEtSaisieFichePresenter enesfPresenter;
	private AdministrationPresenter administrationPresenter;
	private ListeElevesPresenter listeElevesPresenter;
	private GroupeManagementPresenter groupeManagementPresenter;
        private ListesLibreManagementPresenter listeLibreManagementPresenter;
	private TabLayoutPanel tabPanel;
	
	private void firstInitAfterLogon(User user) {
                ficheEPSService.getSettings(new AsyncCallback<Settings>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        ErrorReportEvent.Fire("Impossible de retrouver les paramètres de l'applications");
                    }
                    @Override
                    public void onSuccess(Settings result) {
                        SETTINGS = result;
                    }
                });
            
            
		enesfPresenter = new ElevesNavigationEtSaisieFichePresenter(user);
		ElevesNavigationEtSaisieFicheView enesFView = new ElevesNavigationEtSaisieFicheView();

		tabPanel = new TabLayoutPanel(2.5, Unit.EM);
		tabPanel.setAnimationDuration(500);
		//tabPanel.getElement().getStyle().setMarginBottom(10.0, Unit.PX);

		RootLayoutPanel.get().add(tabPanel);
		
		
		FlowPanel infoPanel = new FlowPanel();	
		Button logoutButton = new Button("Se déconnecter");
		logoutButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				logout();
			}
		});
		final Label userNameLabel = new Label();
		userNameLabel.getElement().getStyle().setDisplay(Display.INLINE);
		String userNameLabelString = user.getFullName() + " ";
		userNameLabel.setText(userNameLabelString);
		Resources resources = GWT.create(Resources.class);
		Image img = new Image(resources.help_24x24());
		PushButton helpButton = new PushButton(img, new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				com.google.gwt.user.client.Window.open(GWT.getHostPageBaseURL() + "doc/index.html", "_blank", "");
			}
		});
		helpButton.getElement().getStyle().setDisplay(Display.INLINE);
		helpButton.getElement().getStyle().setPaddingTop(5, Unit.PX);
		helpButton.getElement().getStyle().setPaddingBottom(5, Unit.PX);
		helpButton.getElement().getStyle().setPaddingLeft(2, Unit.PX);
		helpButton.getElement().getStyle().setPaddingRight(2, Unit.PX);
		helpButton.getElement().getStyle().setMarginLeft(2, Unit.PX);
		img.getElement().getStyle().setVerticalAlign(VerticalAlign.MIDDLE);
		infoPanel.getElement().getStyle().setDisplay(Display.INLINE);
		infoPanel.add(userNameLabel);
		infoPanel.add(logoutButton);
		infoPanel.add(helpButton);
		
		infoPanel.getElement().getStyle().setTextAlign(TextAlign.RIGHT);
		RootLayoutPanel.get().add(infoPanel);
		RootLayoutPanel.get().setWidgetTopHeight(infoPanel, 0.0, Unit.EM, 2.2, Unit.EM);
		RootLayoutPanel.get().setWidgetRightWidth(infoPanel, 0.0, Unit.EM, 12 /*shoud represent logout button*/ + (userNameLabelString.length() / 1.4 /*choose wisely*/), Unit.EM);
		RootLayoutPanel.get().setWidgetHorizontalPosition(infoPanel, Alignment.END);
		RootLayoutPanel.get().setWidgetVerticalPosition(infoPanel, Alignment.BEGIN);
		

		tabPanel.add(enesFView, "Saisie fiches eleve");

		listeElevesPresenter = new ListeElevesPresenter(user.isAdmin());
		ListeElevesView listeElevesView = new ListeElevesView();
		tabPanel.add(listeElevesView, "Liste complete des eleves");
		listeElevesPresenter.bind(listeElevesView);
		
		groupeManagementPresenter = new GroupeManagementPresenter(user.isAdmin());
		GroupeManagementView groupeManagementView = new GroupeManagementView();
		groupeManagementPresenter.bind(groupeManagementView);
		tabPanel.add(groupeManagementView,"Gestion des groupes");
			
                
                listeLibreManagementPresenter = new ListesLibreManagementPresenter(user.isAdmin());
                ListesLibreManagementView listeLibreManagementView = new ListesLibreManagementView();
                listeLibreManagementPresenter.bind(listeLibreManagementView);
                tabPanel.add(listeLibreManagementView,"Listes libre");
                
		if(user.isAdmin()) {
			administrationPresenter = new AdministrationPresenter(user);
			AdministrationView av = new AdministrationView();
			tabPanel.add(av, "Administration");

			ImportPresenter importPresenter = new ImportPresenter();
			ImportView importView = new ImportView();
			importPresenter.bind(importView);
			tabPanel.add(importView, "Import");
			
			administrationPresenter.bind(av);
		}
		
		enesfPresenter.bind(enesFView);
		
		enesfPresenter.refresh();

		tabPanel.addBeforeSelectionHandler(new BeforeSelectionHandler<Integer>() {
			@Override
			public void onBeforeSelection(BeforeSelectionEvent<Integer> event) {
				int tabIndex = event.getItem();
				if (tabIndex == 0) {
					Events.fire(new Events.SwitchViewEvent(View.EleveNavigationEtSaisie));
				} else if (tabIndex == 1) {
					Events.fire(new Events.SwitchViewEvent(View.ListeEleveGlobale));
				} else if (tabIndex == 2) {
					Events.fire(new Events.SwitchViewEvent(View.GroupeManagement));
				} else if (tabIndex == 3) {
					Events.fire(new Events.SwitchViewEvent(View.ListeLibre));
				} else if (tabIndex == 4) {
					Events.fire(new Events.SwitchViewEvent(View.Administration));
				} else if (tabIndex == 5) {
					Events.fire(new Events.SwitchViewEvent(View.Import));
				}
				event.cancel();
			}
		});
	}

	public static void GoToEleveNavigationEtSaisie(String classOrGroup, boolean isClassElseGroup, String eleveId) {
            Events.fire(new Events.SwitchViewEvent(isClassElseGroup, classOrGroup, eleveId));
        }
	
	private void logout() {
		if(enesfPresenter == null) {
			realLogout();
		}
		else {
			enesfPresenter.lastChanceBeforeDisapearing(new Action() {
				@Override
				public void doAction() {
					realLogout();
				}
			});
		}
	}
	
	private void realLogout() {
		ficheEPSService.logout(new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(String result) {
				googleAccountLogout();
			}
		});
	}
	
	private void googleAccountLogout() {
		String googleLogoutUrl = "https://www.google.com/accounts/Logout";
		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, googleLogoutUrl);
		try {
			Request response = builder.sendRequest(null, new RequestCallback() {
				public void onError(Request request, Throwable exception) {
					gmailLogout();
				}

				@Override
				public void onResponseReceived(Request request, Response response) {
					gmailLogout();
				}
			});
		} catch (RequestException e) {
			// Code omitted for clarity
		}		
	}
	
	private void gmailLogout() {
		String googleLogoutUrl = "https://mail.google.com/mail/?logout";
		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, googleLogoutUrl);
		try {
			Request response = builder.sendRequest(null, new RequestCallback() {
				public void onError(Request request, Throwable exception) {
					Window.Location.assign("login");
				}

				@Override
				public void onResponseReceived(Request request, Response response) {
					Window.Location.assign("login");
				}
			});
		} catch (RequestException e) {
			// Code omitted for clarity
		}		
	}
	
	
	
	
	
	public static class Events {
				
		private static SimpleEventBus eventBus = new SimpleEventBus();
	    public static <H> HandlerRegistration addHandler(Type<H> type, H handler) {
	        return eventBus.addHandler(type, handler);
	    }    
	    public static void fire(Event<?> event) {
	    	eventBus.fireEvent(event);
	    }     
	    
	    		
		public static class SwitchViewEvent extends Event<SwitchViewEvent.Handler> {
		    public interface Handler {
		        void switchView(SwitchViewEvent switchViewEvent);
		    }
		    
		    public static final Type<SwitchViewEvent.Handler> TYPE = new Type<SwitchViewEvent.Handler>();
		    @Override
		    public Type<SwitchViewEvent.Handler> getAssociatedType() {return TYPE;}
		    
		    public enum View {
		    	EleveNavigationEtSaisie,
		    	ListeEleveGlobale,
		    	GroupeManagement,
                        ListeLibre,
		    	Administration,
		    	Import
		    }
		    
		    private View view;
		    public SwitchViewEvent(View view) {
		    	this.view = view;
		    }
                    
                    private boolean isClassElseGroup = true;
                    private String classOrGroupName = "";
                    private String eleveId = "";
                    public SwitchViewEvent(boolean _classTrueOrGroupFalse,String _classOrGroupName, String _eleveId) {
		    	this.view = View.EleveNavigationEtSaisie;
                        this.isClassElseGroup = _classTrueOrGroupFalse;
                        this.classOrGroupName = _classOrGroupName;
                        this.eleveId = _eleveId;
		    }
		    
		    @Override
		    protected void dispatch(Handler handler) {
		        handler.switchView(this);
		    }
		}	
		
	}
	

}

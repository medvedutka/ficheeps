package ficheeps.client.service;

import java.io.Serializable;

public class ServerServiceException extends Exception implements Serializable {

	public ServerServiceException() {
		
	}
	
	public ServerServiceException(String msg) {
		super(msg);
		//exceptionMsg = msg;
	}
	/*
	private String exceptionMsg;

	public String getExceptionMsg() {
		return exceptionMsg;
	}

	public void setExceptionMsg(String exceptionMsg) {
		this.exceptionMsg = exceptionMsg;
	}
	*/
}

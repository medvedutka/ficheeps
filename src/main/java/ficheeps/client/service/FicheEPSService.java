package ficheeps.client.service;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import ficheeps.model.BackupFile;
import ficheeps.model.CSVLine;
import ficheeps.model.Commentaire;
import ficheeps.model.Settings;
import ficheeps.model.Eleve;
import ficheeps.model.EleveDiff;
import ficheeps.model.EmailsDiff;
import ficheeps.model.EmailsDiffImportResult;
import ficheeps.model.EmailsImportOptions;
import ficheeps.model.Groupe;
import ficheeps.model.ImportOptions;
import ficheeps.model.ListPrettyMail;
import ficheeps.model.ListeLibre;
import ficheeps.model.ListeLibreItem;
import ficheeps.model.ListeLibreLight;
import ficheeps.model.StatisticsFilter;
import ficheeps.model.Tuple;
import ficheeps.model.User;
import java.util.Collection;
import java.util.Map;


@RemoteServiceRelativePath("ficheEPSService")
public interface FicheEPSService extends RemoteService {

	public Tuple<List<Eleve>,Integer> getListElevesRange(int startOffset,int count,String filter,String sortBy, boolean sortAscending, boolean includeAllEleves);
	
        public Eleve setEleveNumen(String eleveOrid, String newNumen) throws ServerServiceException;
	public Eleve updateEleveFirstLevel(Eleve e);
        public Eleve updateEleveDeep(Eleve e);
	public void deleteEleve(Eleve e);
        public Eleve createEleveLike(Eleve e);
        public String setEleveEmails(String eleveOrid,String emails) throws ServerServiceException;
        public ListPrettyMail retrieveAllMails(List<String> classes, List<String> groupes, List<String> listeLibresIds) throws ServerServiceException;
        
        public EmailsDiffImportResult tryEmailsImport(EmailsImportOptions importOptions) throws ServerServiceException;
	public String commitEmailsImport(EmailsImportOptions importOptions,List<EmailsDiff> led) throws ServerServiceException;

        
	public List<String> getClasses();
	
	public List<String> getApsaForSuggestion();
	
	/**
	 * @return a list of 'empty' groups: no eleves inside ! (only text attributes)
	 */
	public List<Groupe> getGroupesLight();
	public Groupe updateGroupeFirstLevel(Groupe g);
	public Groupe createGroupe() throws ServerServiceException;
	public void deleteGroupe(String groupeOrid);
	public Groupe getGroupeByOrid(String groupeOrid) throws ServerServiceException;
	public void addEleveToGroupe(String groupeOrid,String eleveOrid) throws ServerServiceException;
	public void removeEleveFromGroupe(String groupeOrid,String eleveOrid) throws ServerServiceException;
	public void deleteAllGroupes();
	public List<String> getAllGroupsForEleve(String eleveOrid) throws ServerServiceException;
        
        
	public void unregisterAllElevesFromClasses();
	public void deleteAllElevesWithoutClasses();
        public String deleteAllEleveWithoutClasseBornBefore(String dateYYYYMMDD) throws ServerServiceException;
	
	public List<Eleve> getElevesFromClasse(String classe);
	

	public void createDummyDB();
	public void dropDB();
        public void createDBSnapshot() throws ServerServiceException;
        
        
	public List<User> retrieveAllUsers();
	public User createUser() throws ServerServiceException;
        public User updateUserFirstLevel(User u);
	public void deleteUser(User u);
	public void deleteAllUsers() throws ServerServiceException;
	
	public String tryLogin();
	public String logout();
        
	public User getCurrentSessionUser();
	
	public List<BackupFile> getAllBackupFiles();
	public void deleteBackupFile(BackupFile backupFile);
	public BackupFile createBackup();
	public String restoreBackup(BackupFile backupFile);
	
	
	public ImportOptions initImportFromUploadedFile(String token) throws ServerServiceException;
	public List<CSVLine> verifyImportOptions(ImportOptions importOptions) throws ServerServiceException;
	public List<EleveDiff> tryImport(ImportOptions importOptions) throws ServerServiceException;
	public String commitImport(ImportOptions importOptions,List<EleveDiff> led) throws ServerServiceException;
	
	public boolean isTemplateSuggestionFinDeSeondeSet();
	public void deleteTemplateFinDeSeconde();
	
	
	public List<String> getApsaSuggestion(Commentaire.Niveau niveau, String annee,String classOrGroupeName,boolean isClassElseGroupe) throws ServerServiceException;
	public String wizardCreateCommentaireForGroupClass(Commentaire commentaireLike, String classOrGroupeName,boolean isClassElseGroupe) throws ServerServiceException;
        
        
        /*Settings Management*/
        public Settings getSettings() throws ServerServiceException;
        public boolean setConstraintApsaLabels(boolean mode) throws ServerServiceException;
        public List<String> addApsaLabel(String newLabel) throws ServerServiceException;
        public List<String> deleteApsaLabel(String label) throws ServerServiceException;
        
        
	//public String massImportElevesInGroups(String mapFileContent, boolean testOnly) throws ServerServiceException;
	public int getServerTimezoneOffset();
       
        public Collection<ListeLibreLight> getAllListeLibreLight() throws ServerServiceException;
        public ListeLibre getListeLibreById(String id) throws ServerServiceException;
        public ListeLibre createListeLibre() throws ServerServiceException;
        public void deleteListeLibre(String id) throws ServerServiceException;
        public void setListesLibreName(String id, String name) throws ServerServiceException;
        public void setListesLibreDescription(String id, String description) throws ServerServiceException;
        public void setListesLibreItemProfName(String llid, String itemId, String profName) throws ServerServiceException;
        public void setListesLibreItemComment(String llid, String itemId, String description) throws ServerServiceException;
        public ListeLibreItem createListeLibreItemFromEleve(String llid, String eleveId) throws ServerServiceException;
        public void deleteListeLibreItem(String llid, String itemId);
        public void deleteAllListesLibre() throws ServerServiceException;

        public StatisticsFilter getFullStatisticsFilter() throws ServerServiceException;
        
        public Map<String,String> getInfosClassesGroupes() throws ServerServiceException;
        public void putInfosClassesGroupes(String classOrGroupName, String info) throws ServerServiceException;
        public void removeInfosClassesGroupes(String classOrGroupName) throws ServerServiceException;
        public List<Tuple<String,String>> tryInfosClassesGroupesImport(String fileUpoaded) throws ServerServiceException;
	public String commitInfosClassesGroupesImport(List<Tuple<String,String>> infos) throws ServerServiceException;
}

package ficheeps.client.service;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import ficheeps.model.BackupFile;
import ficheeps.model.CSVLine;
import ficheeps.model.Commentaire;
import ficheeps.model.Eleve;
import ficheeps.model.EleveDiff;
import ficheeps.model.EmailsDiff;
import ficheeps.model.EmailsDiffImportResult;
import ficheeps.model.EmailsImportOptions;
import ficheeps.model.Groupe;
import ficheeps.model.ImportOptions;
import ficheeps.model.ListPrettyMail;
import ficheeps.model.ListeLibre;
import ficheeps.model.ListeLibreItem;
import ficheeps.model.ListeLibreLight;
import ficheeps.model.Settings;
import ficheeps.model.StatisticsFilter;
import ficheeps.model.Tuple;
import ficheeps.model.User;
import java.util.Collection;
import java.util.Map;

public interface FicheEPSServiceAsync {

        void setEleveNumen(String eleveOrid, String newNumen, AsyncCallback<Eleve> callback);

        void setEleveEmails(String eleveOrid,String emails,AsyncCallback<String> callback);
        void retrieveAllMails(List<String> classes, List<String> groupes, List<String> listeLibresIds, AsyncCallback<ListPrettyMail> callback);
        
        void tryEmailsImport(EmailsImportOptions importOptions,AsyncCallback<EmailsDiffImportResult> callback);
	void commitEmailsImport(EmailsImportOptions importOptions,List<EmailsDiff> led,AsyncCallback<String> callback);
        
        
	void updateEleveFirstLevel(Eleve e, AsyncCallback<Eleve> callback);
        void updateEleveDeep(Eleve e, AsyncCallback<Eleve> callback);
        void createEleveLike(Eleve e,AsyncCallback<Eleve> callback);

	void getListElevesRange(int startOffset, int count,String filter, String sortBy,
			boolean sortAscending, boolean includeAllEleves, AsyncCallback< Tuple<List<Eleve>,Integer> > callback);

	void getClasses(AsyncCallback<List<String>> callback);

	void getElevesFromClasse(String classe, AsyncCallback<List<Eleve>> callback);

	void createDummyDB(AsyncCallback<Void> callback);
        
        void createDBSnapshot(AsyncCallback<Void> callback);

	void dropDB(AsyncCallback<Void> callback);

	void tryLogin(AsyncCallback<String> callback);

	void logout(AsyncCallback<String> callback);
        
	void retrieveAllUsers(AsyncCallback<List<User>> callback);

        void createUser(AsyncCallback<User> callback);
        
	void updateUserFirstLevel(User u, AsyncCallback<User> callback);

	void deleteUser(User u, AsyncCallback<Void> callback);

        void deleteAllUsers(AsyncCallback<Void> callback);
        
	void getCurrentSessionUser(AsyncCallback<User> callback);

	void deleteEleve(Eleve e, AsyncCallback<Void> callback);

	void getAllBackupFiles(AsyncCallback<List<BackupFile>> callback);

	void deleteBackupFile(BackupFile backupFile, AsyncCallback<Void> callback);

	void createBackup(AsyncCallback<BackupFile> callback);

	void restoreBackup(BackupFile backupFile, AsyncCallback<String> callback);

	void initImportFromUploadedFile(String token, AsyncCallback<ImportOptions> callback);

	void verifyImportOptions(ImportOptions importOptions, AsyncCallback<List<CSVLine>> callback);

	void tryImport(ImportOptions importOptions, AsyncCallback<List<EleveDiff>> callback);

	void commitImport(ImportOptions importOptions, List<EleveDiff> led, AsyncCallback<String> callback);

	void getGroupesLight(AsyncCallback<List<Groupe>> callback);

	void deleteGroupe(String groupeOrid, AsyncCallback<Void> callback);

	void updateGroupeFirstLevel(Groupe g, AsyncCallback<Groupe> callback);

	void getGroupeByOrid(String groupeOrid, AsyncCallback<Groupe> callback);

	void addEleveToGroupe(String groupeOrid, String eleveOrid, AsyncCallback<Void> callback);

	void removeEleveFromGroupe(String groupeOrid, String eleveOrid, AsyncCallback<Void> callback);

	void createGroupe(AsyncCallback<Groupe> callback);

	void deleteAllGroupes(AsyncCallback<Void> callback);
        
        void getAllGroupsForEleve(String eleveOrid, AsyncCallback<List<String>> callback);

	void unregisterAllElevesFromClasses(AsyncCallback<Void> callback);

	void deleteAllElevesWithoutClasses(AsyncCallback<Void> callback);
        
        void deleteAllEleveWithoutClasseBornBefore(String date,AsyncCallback<String> callback);

	void getApsaForSuggestion(AsyncCallback<List<String>> callback);

	void isTemplateSuggestionFinDeSeondeSet(AsyncCallback<Boolean> callback);

	void deleteTemplateFinDeSeconde(AsyncCallback<Void> callback);

	void getApsaSuggestion(Commentaire.Niveau niveau, String annee, String classOrGroupeName, boolean isClassElseGroupe, AsyncCallback<List<String>> callback);

        void wizardCreateCommentaireForGroupClass(Commentaire commentaireLike, String classOrGroupeName,boolean isClassElseGroupe, AsyncCallback<String> callback);
        
	//void massImportElevesInGroups(String mapFileContent, boolean testOnly, AsyncCallback<String> callback);

	void getServerTimezoneOffset(AsyncCallback<Integer> callback);
	
        //void dateRA12(AsyncCallback<String> callback);

        void getSettings(AsyncCallback<Settings> callback);
        void setConstraintApsaLabels(boolean mode, AsyncCallback<Boolean> callback);
        void addApsaLabel(String newLabel,AsyncCallback<List<String>> callback);
        void deleteApsaLabel(String label,AsyncCallback<List<String>> callback);
        
        
        void getAllListeLibreLight(AsyncCallback<Collection<ListeLibreLight>> callback);
        void getListeLibreById(String id,AsyncCallback<ListeLibre> callback);
        void createListeLibre(AsyncCallback<ListeLibre> callback);
        void deleteListeLibre(String id,AsyncCallback<Void> callback);
        void setListesLibreName(String id, String name,AsyncCallback<Void> callback);
        void setListesLibreDescription(String id, String description,AsyncCallback<Void> callback);
        void setListesLibreItemProfName(String llid, String itemId, String profName,AsyncCallback<Void> callback);
        void setListesLibreItemComment(String llid, String itemId, String description,AsyncCallback<Void> callback);
        void createListeLibreItemFromEleve(String llid, String eleveId,AsyncCallback<ListeLibreItem> callback);
        void deleteListeLibreItem(String llid, String itemId,AsyncCallback<Void> callback);
        void deleteAllListesLibre(AsyncCallback<Void> callback);

        void getFullStatisticsFilter(AsyncCallback<StatisticsFilter> callback);
        
        void getInfosClassesGroupes(AsyncCallback<Map<String,String>> callback);
        void putInfosClassesGroupes(String classOrGroupName, String info,AsyncCallback<Void> callback);
        void removeInfosClassesGroupes(String classOrGroupName,AsyncCallback<Void> callback);
        void tryInfosClassesGroupesImport(String fileUpoaded, AsyncCallback<List<Tuple<String,String>>> callback);
	void commitInfosClassesGroupesImport(List<Tuple<String,String>> infos, AsyncCallback<String> callback);       
}

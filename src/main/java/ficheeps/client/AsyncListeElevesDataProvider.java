package ficheeps.client;

import java.util.List;

import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortList.ColumnSortInfo;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.Range;
import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.HandlerRegistration;

import ficheeps.client.service.FicheEPSServiceAsync;
import ficheeps.model.Eleve;
import ficheeps.model.Tuple;


public class AsyncListeElevesDataProvider extends AsyncDataProvider<Eleve> {

	public interface SortAndFilterInfoRetriever {
		ColumnSortInfo getSortInfo();	
		String getFilter();
                boolean mustIncludeAllEleve();
	}
	
	SortAndFilterInfoRetriever sortAndFilterInfoRetriever = null;
	public void setSortInfoRetriever(SortAndFilterInfoRetriever s) {
		sortAndFilterInfoRetriever = s;
	}
	
	
        private boolean freezed = false; // if freeze: do not update
	private FicheEPSServiceAsync service;
	
	public AsyncListeElevesDataProvider(FicheEPSServiceAsync s) {
		super(Eleve.KEY_PROVIDER);
		service = s;
	}
	
        public void freeze() {
            freezed = true;
        }
        public void unfreeze() {
            freezed = false;
        }
        
	public void refresh(HasData<Eleve> display) {
		onRangeChanged(display);
	}

	@Override
	protected void onRangeChanged(HasData<Eleve> display) {
            if(freezed) {
                return;
            }
		fire(AsycListEleveDataProviderEvent.Status.Searching);
      final Range range = display.getVisibleRange();
      final int start = range.getStart();
      final int length = range.getLength();		
      
      AsyncCallback<Tuple<List<Eleve>,Integer>> callback = new AsyncCallback<Tuple<List<Eleve>,Integer>>() {
		    public void onSuccess(Tuple<List<Eleve>,Integer> result) {
		    	updateRowData(start, result.getFirst());
		    	updateRowCount(result.getSecond(), true);
		    	fire(AsycListEleveDataProviderEvent.Status.Idle);
		    }
		    public void onFailure(Throwable caught) {
		    	ErrorReportEvent.Fire("Erreur serveur: " + caught.getMessage());
		    	fire(AsycListEleveDataProviderEvent.Status.Idle);
		    }
		};
  
		String filter = "";
		String sortColumn = "";
		boolean isAscending = true;
                boolean includeAllEleves = true;
		if(sortAndFilterInfoRetriever != null) {
			filter = sortAndFilterInfoRetriever.getFilter();
			if(sortAndFilterInfoRetriever.getSortInfo() != null) {
				isAscending = sortAndFilterInfoRetriever.getSortInfo().isAscending();
				Column c = sortAndFilterInfoRetriever.getSortInfo().getColumn();
				sortColumn = c.getDataStoreName();
			}
                        includeAllEleves = sortAndFilterInfoRetriever.mustIncludeAllEleve();
		}
                
		service.getListElevesRange(start, length,filter, sortColumn, isAscending,includeAllEleves, callback);
	}

	
	
    private SimpleEventBus eventBus = new SimpleEventBus();
    public HandlerRegistration addHandler(AsycListEleveDataProviderEvent.Handler handler) {
        return eventBus.addHandler(AsycListEleveDataProviderEvent.TYPE, handler);
    }    
    public void fire(AsycListEleveDataProviderEvent event) {
    	eventBus.fireEvent(event);
    }     
    public void fire(AsycListEleveDataProviderEvent.Status status) {
    	eventBus.fireEvent(new AsycListEleveDataProviderEvent(status));
    }
    
	
	
	public static class AsycListEleveDataProviderEvent extends Event<AsycListEleveDataProviderEvent.Handler> {

	    public interface Handler {
	        void onStatusChanged(AsycListEleveDataProviderEvent event);
	    }

	    private static final Type<AsycListEleveDataProviderEvent.Handler> TYPE = new Type<AsycListEleveDataProviderEvent.Handler>();

	    
	    public enum Status {
	    	Searching,
	    	Idle
	    }
	    
	    private final Status status;

	    public AsycListEleveDataProviderEvent(Status status) {
	        this.status = status;
	    }

	    @Override
	    public Type<AsycListEleveDataProviderEvent.Handler> getAssociatedType() {
	        return TYPE;
	    }

	    public Status getStatus() {
	        return status;
	    }

	    @Override
	    protected void dispatch(Handler handler) {
	        handler.onStatusChanged(this);
	    }
	}
	
	
	
}

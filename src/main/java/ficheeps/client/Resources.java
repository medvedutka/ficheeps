package ficheeps.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;

public interface Resources extends ClientBundle {

    public static interface FicheEPSStyle extends CssResource {
        String deleteImageBackground();
        String helpImageBackgroundEleveGroupe();
        String addImageBackground();
        String boldStyle();
        String eleveGroupViewBasicEleveWidget();
        String downloadfichePDF32();
        String emailEleve32();
        String eleveNavigationDownloadfichePDF24();
        String eleveNavigationEmailEleve24();
        String noemailEleve32();
        
        String groupEleveWidgetDisable();
        
        String emailsDiffValues();
        String keepedValue();
        String removedValue();
        String addedValue();
        
        String delete16();
        
        String epsGroupBoxShadow();
        String asGroupBoxShadow();
        
        String listeEleveDownloadfichePDF32();
        String listeEleveDeleteSprite();
    }

    public static final Resources INSTANCE = GWT.create(Resources.class);

    @Source("ficheepsStyle.css")
    public FicheEPSStyle ficheepsStyle();

    @Source("logo.png")
    ImageResource logo();

    @Source("add_16x16.png")
    ImageResource add_16x16();

    @Source("save_16x16.png")
    ImageResource save_16x16();
    
    @Source("save_22x22.png")
    ImageResource save_22x22();

    @Source("save_32x32.png")
    ImageResource save_32x32();

    @Source("save_48x48.png")
    ImageResource save_48x48();

    @Source("save_64x64.png")
    ImageResource save_64x64();
    
    @Source("wait01.gif")
    ImageResource wait01();

    @Source("ajax-loader.gif")
    ImageResource ajaxLoader();

    @Source("delete_16x16.png")
    ImageResource delete_16x16();

    @Source("delete_24x24.png")
    ImageResource delete_24x24();

    @Source("delete_32x32.png")
    ImageResource delete_32x32();

    @Source("settings_16x16.png")
    ImageResource settings_16x16();

    @Source("settings_24x24.png")
    ImageResource settings_24x24();

    @Source("settings_32x32.png")
    ImageResource settings_32x32();

    @Source("excel_16x16.png")
    ImageResource excel_16x16();

    @Source("excel_24x24.png")
    ImageResource excel_24x24();
    
    @Source("excel_32x32.png")
    ImageResource excel_32x32();
    
    @Source("checkBlack_16x16.png")
    ImageResource checkBlack_16x16();

    @Source("checkBlack_32x32.png")
    ImageResource checkBlack_32x32();

    @Source("toolsWizard_16x16.png")
    ImageResource toolsWizard_16x16();

    @Source("toolsWizard_24x24.png")
    ImageResource toolsWizard_24x24();

    @Source("toolsWizard_32x32.png")
    ImageResource toolsWizard_32x32();

    @Source("help_16x16.png")
    ImageResource help_16x16();

    @Source("help_24x24.png")
    ImageResource help_24x24();

    @Source("help_32x32.png")
    ImageResource help_32x32();

    @Source("help_64x64.png")
    ImageResource help_64x64();

    @Source("health_16x16.png")
    ImageResource health_16x16();

    @Source("health_24x24.png")
    ImageResource health_24x24();

    @Source("health_32x32.png")
    ImageResource health_32x32();

    @Source("icons/theme_32x32.png")
    ImageResource theme_32x32();
    
    @Source("icons/calendar_32x32.png")
    ImageResource calendar_32x32();
    
    @Source("icons/abc_32x32.png")
    ImageResource abc_32x32();    
    
    @Source("icons/theme_16x16.png")
    ImageResource theme_16x16();
    
    @Source("icons/calendar_16x16.png")
    ImageResource calendar_16x16();
    
    @Source("icons/abc_16x16.png")
    ImageResource abc_16x16();       

    @Source("icons/nomail_24x24.png")
    ImageResource nomail_24x24();     
    
    @Source("icons/nomail_32x32.png")
    ImageResource nomail_32x32();        
    
    @Source("icons/nomail_48x48.png")
    ImageResource nomail_48x48();        

    @Source("icons/nomail_64x64.png")
    ImageResource nomail_64x64();    
    
    @Source("icons/mail_24x24.png")
    ImageResource mail_24x24();          

    @Source("icons/mail_32x32.png")
    ImageResource mail_32x32();        
    
    @Source("icons/mail_48x48.png")
    ImageResource mail_48x48();        

    @Source("icons/mail_64x64.png")
    ImageResource mail_64x64();        

    @Source("icons/pdf_24x24.png")
    ImageResource pdf_24x24();        
    
    @Source("icons/pdf_32x32.png")
    ImageResource pdf_32x32();        

    @Source("icons/pdf_48x48.png")
    ImageResource pdf_48x48();        

    @Source("icons/pdf_64x64.png")
    ImageResource pdf_64x64();        
    
    @Source("icons/rungreen_24x24.png")
    ImageResource rungreen_24x24();    

    @Source("icons/runpurple_24x24.png")
    ImageResource runpurple_24x24();    

    @Source("icons/rungold_24x24.png")
    ImageResource rungold_24x24();    
    
    @Source("icons/runblue_24x24.png")
    ImageResource runblue_24x24();        
}

package ficheeps.client;


import com.google.gwt.event.shared.SimpleEventBus;
import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;


public class ErrorReportEvent extends Event<ErrorReportEvent.Handler> {

    /**
     * Implemented by methods that handle MessageReceivedEvent events.
     */
    public interface Handler {
        /**
         * Called when an {@link MessageReceivedEvent} event is fired.
         * The name of this method is whatever you want it.
         *
         * @param event an {@link MessageReceivedEvent} instance
         */
        void onErrorReceived(ErrorReportEvent event);
    }

    private static final Type<ErrorReportEvent.Handler> TYPE = new Type<ErrorReportEvent.Handler>();

    /**
     * Register a handler for MessageReceivedEvent events on the eventbus.
     * 
     * @param eventBus the {@link EventBus}
     * @param handler an {@link MessageReceivedEvent.Handler} instance
     * @return an {@link HandlerRegistration} instance
     */
    public static HandlerRegistration register(EventBus eventBus, ErrorReportEvent.Handler handler) {
      return eventBus.addHandler(TYPE, handler);
    }    

    
    private static final SimpleEventBus ERROR_EVENT_BUS = new SimpleEventBus();
    public static HandlerRegistration Register(ErrorReportEvent.Handler handler) {
        return ERROR_EVENT_BUS.addHandler(TYPE, handler);
    }    
    public static void Fire(ErrorReportEvent event) {
        ERROR_EVENT_BUS.fireEvent(event);
    }     
    public static void Fire(String msg) {
    	ERROR_EVENT_BUS.fireEvent(new ErrorReportEvent(msg));
    }
    
    
    private final String message;

    public ErrorReportEvent(String message) {
        this.message = message;
    }

    @Override
    public Type<ErrorReportEvent.Handler> getAssociatedType() {
        return TYPE;
    }

    public String getMessage() {
        return message;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onErrorReceived(this);
    }
}

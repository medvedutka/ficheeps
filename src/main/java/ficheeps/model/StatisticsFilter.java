package ficheeps.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;


public class StatisticsFilter implements IStatisticsFilter,Serializable {

    public StatisticsFilter() {
    }

    private boolean niveau = false;
    private boolean annee = false;
    private boolean apsa = false;
    private List<String> niveaux = new LinkedList<String>();
    private List<String> annees = new LinkedList<String>();
    private List<String> apsas = new LinkedList<String>();

    public boolean isNiveau() {
        return niveau;
    }
    public void setNiveau(boolean niveau) {
        this.niveau = niveau;
    }
    public boolean isAnnee() {
        return annee;
    }
    public void setAnnee(boolean annee) {
        this.annee = annee;
    }
    public boolean isApsa() {
        return apsa;
    }
    public void setApsa(boolean apsa) {
        this.apsa = apsa;
    }
    public List<String> getNiveaux() {
        return niveaux;
    }
    public void setNiveaux(List<String> niveaux) {
        this.niveaux = niveaux;
    }
    public List<String> getAnnees() {
        return annees;
    }
    public void setAnnees(List<String> annees) {
        this.annees = annees;
    }
    public List<String> getApsas() {
        return apsas;
    }
    public void setApsas(List<String> apsas) {
        this.apsas = apsas;
    }
    
    
    public boolean isCommentaireOkToExport(Commentaire commentaire) {
        if(isNiveau()) {
            if(commentaire.getNiveau() != null) {
                return getNiveaux().contains(commentaire.getNiveau().name());
            }
        }
        if(isAnnee()){
            if(commentaire.getAnneeScolaire() != null) {
                return getAnnees().contains(commentaire.getAnneeScolaire());
            }
        }
        if(isApsa()) { //Filter by aspsa: at least one apsa must be exportable
            if(commentaire.getApsas() != null) {
                for(Apsa apsa : commentaire.getApsas()) {
                    if(isAPSAOkToExport(apsa)) {
                        return true;
                    }
                }                
            }
            return false;
        }
        return true;
    }
    
    public boolean isAPSAOkToExport(Apsa apsa) {
        if(isApsa()) {
            if(apsa.getActivite()!= null) {
                return getApsas().contains(apsa.getActivite());
            }
        }
        return true;
    }    
    
}

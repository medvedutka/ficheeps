package ficheeps.model;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Groupe implements Serializable {

	public Groupe() {
            nom = "Nouveau groupe";
            description = "";
            eleves = new LinkedList<Eleve>();
            nomProfesseur = "";
	}

	
        
	private String orid;
	public void setOrid(String orid) {
            this.orid = orid;
        }
	public String getOrid() {
            return orid;
	}
	
	private String nomProfesseur;
	private String nom;
	private String description;
	private List<Eleve> eleves;
	private GroupeType groupeType = GroupeType.EPS;
	
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getNomProfesseur() {
		return nomProfesseur;
	}
	public void setNomProfesseur(String nomProfesseur) {
		this.nomProfesseur = nomProfesseur;
	}
	public void setGroupeType(GroupeType gt) {
		groupeType = gt;
	}
	public GroupeType getGroupeType() {
		return groupeType;
	}
	public List<Eleve> getEleves() {
		return eleves;
	}
	public void setEleves(List<Eleve> eleves) {
		this.eleves = eleves;
	}

	public void addEleve(Eleve eleve) {
		for(Eleve e : getEleves()) {
			if(e.getOrid() != null && e.getOrid().equals(eleve.getOrid())) {
				return;
			}
		}
		getEleves().add(eleve);
	}
	public void removeEleve(Eleve eleve)  {
		Iterator<Eleve> iter = getEleves().iterator();
		while(iter.hasNext()) {
			Eleve e = iter.next();
			if(e != null && e.getOrid() != null && e.getOrid().equals(eleve.getOrid())) {
				iter.remove();
			}
		}
	}
        public void removeEleve(String eleveOrid)  {
            Iterator<Eleve> iter = getEleves().iterator();
            while(iter.hasNext()) {
                Eleve e = iter.next();
                if(e != null && e.getOrid() != null && e.getOrid().equals(eleveOrid)) {
                        iter.remove();
                }
            }
	}
        
        public Groupe createGroupeLight() {
            Groupe light = new Groupe();
            light.setOrid(orid);
            light.setNom(nom);
            light.setDescription(description);
            light.setGroupeType(groupeType);
            light.setNomProfesseur(nomProfesseur);
            return light;
        }
}

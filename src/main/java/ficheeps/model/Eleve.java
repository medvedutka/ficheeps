package ficheeps.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.google.gwt.view.client.ProvidesKey;


public class Eleve implements Comparable<Eleve>, Serializable {

    /**
     * The key provider that provides the unique ID of an Eleve.
     */
    public static final ProvidesKey<Eleve> KEY_PROVIDER = new ProvidesKey<Eleve>() {
      @Override
      public Object getKey(Eleve item) {
        return item == null ? null : item.getOrid();
      }
    };

    @Override
    public int compareTo(Eleve o) {
      return (o == null || o.nom == null) ? -1 : -o.nom.compareTo(nom);
    }

    @Override
    public boolean equals(Object o) {
      if (o instanceof Eleve) {
    	if(id != null) {
    		return id.equals(((Eleve) o).id);
    	}
        return ((Eleve) o).id == null;
      }
      return false;
    }
    
	@Override
	public int hashCode() {
		if(id == null) {
			return 0;
		}
		return id.hashCode();
	}


	
	public Eleve() {
	}
	

	private String orid;	
	public String getOrid() {
            return orid;
	}
        public void setOrid(String orid) {
            this.orid = orid;
        }
	
	
	private String id;
	private String nom;
	private String prenom;
	private Date dateDeNaissance;
	private String classe;
	private String suiviMedical;
	private String suggestionApresSeconde;
        private String emails;
	
	private List<Commentaire> commentaires = new LinkedList<Commentaire>();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}
	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	public String getSuggestionApresSeconde() {
		return suggestionApresSeconde;
	}
	public void setSuggestionApresSeconde(String suggestionApresSeconde) {
		this.suggestionApresSeconde = suggestionApresSeconde;
	}

	public int numberOfCommentaires() {
		if(commentaires == null) {
			return 0;
		}
		return commentaires.size();
	}
	public List<Commentaire> getCommentaires() {
		return commentaires;
	}
	public void setCommentaires(List<Commentaire> c) {
		commentaires = c;
	}	
	public void addCommentaire(Commentaire c) {
		if(commentaires == null) {
			commentaires = new LinkedList<Commentaire>();
		}
		commentaires.add(c);
	}
	public void removeCommentaire(Commentaire c) {
		if(commentaires != null) {
			commentaires.remove(c);
		}
	}
	
	public String getSuiviMedical() {
		return suiviMedical;
	}
	public void setSuiviMedical(String s) {
		suiviMedical = s;
	}
	
	public String getEmails() {
            return emails;
        }
        public void setEmails(String mails) {
            emails = mails;
        }
        
        public List<String> getEmailsAsList() {
            List<String> result = new LinkedList();
            String mailsStr = getEmails();
            if(mailsStr != null) {
                String[] mails = mailsStr.split(",");
                for(String m : mails) {
                    result.add(m);
                }
            }
            return result;
        }
        
        public List<String> getPrettyEmailsAsList() {
            List<String> result = new LinkedList();
            List<String> mails = getEmailsAsList();
            for(String mail : mails) {
                String prenomNom = getPrenom() + " "+ getNom();
                prenomNom = prenomNom.trim();
                prenomNom = prenomNom.replace("<", "").replace(">", "");
                result.add(prenomNom + " <"+mail+">");
            }
            return result;
        }
        
        
        /**
         * @param mailsList is a comma-separated list of mail adress
         * @return a comma separated list of mail adress
         * @throws java.lang.Exception
         */
        public static String cleanAndValidateMailList(String mailsList) throws Exception {
            if(mailsList == null) {
                return "";
            }
            StringBuilder errors = new StringBuilder();
            mailsList = mailsList.trim();
            String[] mails = mailsList.split(",");
            StringBuilder result = new StringBuilder();
            for(int i = 0;i<mails.length;i++) {
                String mail = mails[i];
                if(!mail.contains("@")) {
                   errors.append(mail).append(": erreur, pas de '@' !");
                }
                result.append(mail);
                if(i<mails.length-1) {
                    result.append(',');
                }
            }
            if(errors.length()>0) {
                throw new Exception(errors.toString());
            }
            return result.toString();
        }
        
	public Commentaire getCommentaireWithNiveau(Commentaire.Niveau niveau) {
		if(commentaires == null) {
			return null;
		}
		for(Commentaire c : commentaires) {
			if(c.getNiveau() == niveau) {
				return c;
			}
		}
		return null;
	}
	
	public void cleanupForSave() {
		if("<br>".equals(suiviMedical)) {
			suiviMedical = "";
		}
		
		if(commentaires != null) {
			Iterator<Commentaire> iter = commentaires.iterator();
			while(iter.hasNext()) {
				Commentaire c = iter.next();
				c.cleanupForSave();
				
				if(IsStringNullOrEmpty(c.getAnneeScolaire()) 
				&& IsStringNullOrEmpty(c.getAppreciation())
				&& IsStringNullOrEmpty(c.getAutre())
				&& IsStringNullOrEmpty(c.getProfesseur())
				&& (c.getApsas() == null || c.getApsas().isEmpty())
				&& !c.isAssociationSportive()) {
					iter.remove();
				}
			}
			
			Collections.sort(commentaires, new Commentaire.NiveauComparator(false));
		}
	}

	public boolean isSuiviMedicalEmpty() {
		return IsStringNullOrEmpty(suiviMedical);
	}

	private static boolean IsStringNullOrEmpty(String s) {
		if(s == null || s.trim().isEmpty()) {
			return true;
		}
		return false;
	}
	
	
	public static int GetAge(Date dateOfBirth) {
		Calendar c = Calendar.getInstance();
		c.setTime(dateOfBirth);
		int birthYear = c.get(Calendar.YEAR);
		int birthMonth = c.get(Calendar.MONTH);
		int birthDay = c.get(Calendar.DAY_OF_MONTH);
		c.setTime(new Date());
		int nowYear = c.get(Calendar.YEAR);
		int nowMonth = c.get(Calendar.MONTH);
		int nowDay = c.get(Calendar.DAY_OF_MONTH);
		
		int year = nowYear - birthYear;
		if((birthMonth > nowMonth)
			|| ((birthMonth == nowMonth) && (birthDay > nowDay))) {
			year--;
		}
				
		return year;
	}

        
    public String getPrettyNomPrenom() {
        StringBuilder result = new StringBuilder();
        if(getNom() != null) {
            result.append(getNom().toUpperCase());
        }
        if(getPrenom() != null) {
            if(result.length()>0) {
                result.append(" ");
            }
            result.append(getPrenom());
        }
        return result.toString();
    }
	
}

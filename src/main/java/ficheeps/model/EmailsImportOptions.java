/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficheeps.model;

import java.io.Serializable;


public class EmailsImportOptions  implements Serializable {
    /**
     * Append the new mail adress to the ones already defined
     */
    public static int APPEND = 1;  
    /**
     * Remove all defined mails adresse and add the new one
     */
    public static int REPLACE = 2;  
    
    
    private int mode = APPEND;
    private String fileUploadToken;
    
    public boolean isModeAppend() {
        return mode == APPEND;
    }
    public boolean isModeReplace() {
        return mode == REPLACE;
    }    

    public int getMode() {
        return mode;
    }
    public void setMode(int mode) {
        this.mode = mode;
    }

    public String getFileUploadToken() {
        return fileUploadToken;
    }

    public void setFileUploadToken(String fileUploadToken) {
        this.fileUploadToken = fileUploadToken;
    }
    
    public boolean isFileXLSX() {
        return fileUploadToken != null && fileUploadToken.toLowerCase().endsWith(".xlsx");
    }
    
    
}

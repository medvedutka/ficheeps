/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficheeps.model;

import java.io.Serializable;

/**
 *
 * @author cjalady
 */
public class ListPrettyMail implements Serializable  {
    
    public ListPrettyMail() {
    }

    private String maillingPrettyMails;
    private String listPrettyMails;

    public String getMaillingPrettyMails() {
        return maillingPrettyMails;
    }

    public void setMaillingPrettyMails(String maillingPrettyMails) {
        this.maillingPrettyMails = maillingPrettyMails;
    }

    public String getListPrettyMails() {
        return listPrettyMails;
    }

    public void setListPrettyMails(String listPrettyMails) {
        this.listPrettyMails = listPrettyMails;
    }
    
    
    
}

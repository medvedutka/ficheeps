package ficheeps.model;

import java.io.Serializable;

public class User implements Serializable {
	
	public User() {	
	}
        
	private String orid;
	public void setOrid(String orid) {
            this.orid = orid;
        }
	public String getOrid() {
		return orid;
	}
	
	private boolean local;
	private String login;
	private String pwd;
	private String fullName;
	private boolean admin;
	

	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public boolean isLocal() {
		return local;
	}
	public void setLocal(boolean local) {
		this.local = local;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	
	
	
	
}

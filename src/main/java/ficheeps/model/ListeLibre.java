package ficheeps.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;


public class ListeLibre implements Serializable {
    
    public ListeLibre() {
    }
    
    
    private String id;
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    private String name = "";
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    private String description = "";
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
  
    Collection<ListeLibreItem> items;
    public Collection<ListeLibreItem> getItems() {
        if(items == null ) {
            return Collections.EMPTY_LIST;
        }
        return items;
    }

    public ListeLibreLight createLight() {
        ListeLibreLight result = new ListeLibreLight();
        result.setId(getId());
        result.setDescription(getDescription());
        result.setName(getName());
        result.setNbItems(getItems().size());
        return result;
    }
    
    public ListeLibreItem getItemById(String itemId) {
        for(ListeLibreItem item :  getItems()) {
            if(itemId.equals(item.getId())) {
                return item;
            }
        }
        return null;
    }
    
    public void deleteItem(String itemId) {
        Iterator<ListeLibreItem> iter = getItems().iterator();
        while(iter.hasNext()) {
            if(itemId.equals(iter.next().getId())) {
                iter.remove();
            }
        }
    }
    
    public void deleteItemsWithEleve(String eleveOrid) {
        Iterator<ListeLibreItem> iter = getItems().iterator();
        while(iter.hasNext()) {
            Eleve eleveItem = iter.next().getEleve();
            if(eleveOrid.equals(eleveItem.getOrid())) {
                iter.remove();
            }
        }
    }
    
    public ListeLibreItem newItem() {   
        String itemId = nextListeLibreItemIdForListeLibre();
        ListeLibreItem item = new ListeLibreItem();
        item.setId(itemId);
        if(items == null) {
            items = new LinkedList<ListeLibreItem>();
        }
        items.add(item);
        return item;
    }
    
    private String nextListeLibreItemIdForListeLibre() {
        int i = getItems().size();
        String id = null;
        boolean checkAgain = true;
        while(checkAgain) {
            id = "#LLItem-" + (i++);
            checkAgain = false;
            for(ListeLibreItem lli : getItems()) {
                if(id.equals(lli.getId())) {
                    checkAgain = true;
                }
            }
        }
        return id;
    }
    
    public boolean isEleveAlreadyPresent(Eleve e) {
        for(ListeLibreItem item : getItems()) {
            if(item.getEleve() != null) {
                if(item.getEleve().getOrid().equals(e.getOrid())) {
                    return true;
                }
            }
        }
        return false;
    }
    
}

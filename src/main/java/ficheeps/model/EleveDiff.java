/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ficheeps.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author cjalady
 */
public class EleveDiff implements Serializable {

    public static EleveDiff Diff(Eleve eleve, String nom, String prenom, Date dateDeNaissance, String classe) {
        String newNom = null;
        String newPrenom = null;
        Date newDateDeNaissance = null;
        String newClasse = null;

        if (!nom.equals(eleve.getNom())) {
            newNom = nom;
        }
        if (!prenom.equals(eleve.getPrenom())) {
            newPrenom = prenom;
        }
        if (!classe.equals(eleve.getClasse())) {
            newClasse = classe;
        }

        if(eleve.getDateDeNaissance() == null) {
        	newDateDeNaissance = dateDeNaissance;
        }
        else {
	        Calendar calEleve = Calendar.getInstance();
	        calEleve.setTime(eleve.getDateDeNaissance());
	        Calendar calNew = Calendar.getInstance();
	        calNew.setTime(dateDeNaissance);
	        if ((calEleve.get(Calendar.YEAR) != calNew.get(Calendar.YEAR))
	                || (calEleve.get(Calendar.MONTH) != calNew.get(Calendar.MONTH))
	                || (calEleve.get(Calendar.DAY_OF_MONTH) != calNew.get(Calendar.DAY_OF_MONTH))) {
	            newDateDeNaissance = dateDeNaissance;
	        }
        }

        if ((newNom != null)
                || (newPrenom != null)
                || (newClasse != null)
                || (newDateDeNaissance != null)) {
            return new EleveDiff(eleve, newNom, newPrenom, newDateDeNaissance, newClasse);
        }

        return null;
    }

    public EleveDiff() {
    }
    
    protected EleveDiff(Eleve eleve,String nom, String prenom, Date dateDeNaissance, String classe) {
        this.eleve = eleve;
        this.newNom = nom;
        this.newPrenom = prenom;
        this.newDateDeNaissance = dateDeNaissance;
        this.newClasse = classe;
    }
    
    public EleveDiff(String id,String nom, String prenom, Date dateDeNaissance, String classe) {
        this.id = id;
        this.newNom = nom;
        this.newPrenom = prenom;
        this.newDateDeNaissance = dateDeNaissance;
        this.newClasse = classe;
    }    
    
    private Eleve eleve;
    private String id;
    private String newNom;
    private String newPrenom;
    private Date newDateDeNaissance;
    private String newClasse;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public Eleve getEleve() {
        return eleve;
    }
    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }
    public String getNewNom() {
        return newNom;
    }
    public void setNewNom(String newNom) {
        this.newNom = newNom;
    }
    public String getNewPrenom() {
        return newPrenom;
    }
    public void setNewPrenom(String newPrenom) {
        this.newPrenom = newPrenom;
    }
    public Date getNewDateDeNaissance() {
        return newDateDeNaissance;
    }
    public void setNewDateDeNaissance(Date newDateDeNaissance) {
        this.newDateDeNaissance = newDateDeNaissance;
    }
    public String getNewClasse() {
        return newClasse;
    }
    public void setNewClasse(String newClasse) {
        this.newClasse = newClasse;
    }
    
    
    public boolean isNewEleve() {
    	return eleve == null;
    }
    public boolean isJustClasseUpdated() {
    	return ((newNom == null)
    			&& (newPrenom ==null)
    			&& (newDateDeNaissance == null));
    }
    public boolean isLotUpdateNeeded() {
    	return ((newNom != null)
    			|| (newPrenom !=null)
    			|| (newDateDeNaissance != null));
    }
}

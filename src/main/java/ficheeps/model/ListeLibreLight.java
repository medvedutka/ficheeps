package ficheeps.model;

import java.io.Serializable;


public class ListeLibreLight implements Serializable {
    
    public ListeLibreLight() {
    }
    
    private String id;
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    private String name = "";
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    private String description;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    private int nbItems;
    public int getNbItems() {
        return nbItems;
    }
    public void setNbItems(int nbItems) {
        this.nbItems = nbItems;
    }

    
}

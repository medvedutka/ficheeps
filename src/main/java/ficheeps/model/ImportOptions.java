/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ficheeps.model;

import java.io.Serializable;


public class ImportOptions implements Serializable {
    
    public ImportOptions() {
    }
    
    private boolean skipFirstLine;
    private String fileFormat;
    private char delimiter;
    private String fileRessourceID;
    private boolean fileExcel;
    
    public boolean isSkipFirstLine() {
        return skipFirstLine;
    }

    public void setSkipFirstLine(boolean skipFirstLine) {
        this.skipFirstLine = skipFirstLine;
    }

    public String getFileFormat() {
        return fileFormat;
    }

    public void setFileFormat(String fileFormat) {
        this.fileFormat = fileFormat;
    }

    public char getDelimiter() {
        return delimiter;
    }

    public void setDelimiter(char delimiter) {
        this.delimiter = delimiter;
    }

    public String getFileRessourceID() {
        return fileRessourceID;
    }

    public void setFileRessourceID(String fileRessourceID) {
        this.fileRessourceID = fileRessourceID;
    }

	public boolean isFileExcel() {
		return fileExcel;
	}

	public void setFileExcel(boolean fileExcel) {
		this.fileExcel = fileExcel;
	}

    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficheeps.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author cjalady
 */
public class EmailsDiffImportResult implements Serializable {
    private List<EmailsDiff> emailsDiffs = new LinkedList<EmailsDiff>();
    private String infosMessage = "";
    private String errorMessage = "";

    public EmailsDiffImportResult() {
    }

    public EmailsDiffImportResult(List<EmailsDiff> emailsDiffs,String infosMessage,String errorMessage) {
        this.emailsDiffs = emailsDiffs;
        this.infosMessage = infosMessage;
        this.errorMessage = errorMessage;
    }
    
    public List<EmailsDiff> getEmailsDiffs() {
        return emailsDiffs;
    }

    public void setEmailsDiffs(List<EmailsDiff> emailsDiffs) {
        this.emailsDiffs = emailsDiffs;
    }

    public String getInfosMessage() {
        return infosMessage;
    }

    public void setInfosMessage(String infosMessage) {
        this.infosMessage = infosMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    
    
    
}

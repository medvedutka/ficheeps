package ficheeps.model;

import java.io.Serializable;


public class ListeLibreItem implements Serializable {
    
    public ListeLibreItem() {
    }
    
    private String id;
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    
    private String comment = "";
    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }

    private String profName = "";
    public String getProfName() {
        return profName;
    }
    public void setProfName(String profName) {
        this.profName = profName;
    }

    private Eleve eleve;
    public Eleve getEleve() {
        return eleve;
    }
    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }
}

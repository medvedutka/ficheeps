package ficheeps.model;

import java.io.Serializable;


@javax.persistence.Embeddable
public class Apsa implements Serializable {

	public Apsa() {
	}
	
	private String activite;
	private String resultat;
	public String getActivite() {
		return activite;
	}
	public void setActivite(String activite) {
		this.activite = activite;
	}
	public String getResultat() {
		return resultat;
	}
	public void setResultat(String resultat) {
		this.resultat = resultat;
	} 
	
	
}

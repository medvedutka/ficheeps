package ficheeps.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.google.gwt.i18n.client.NumberFormat;

@javax.persistence.Embeddable
public class Commentaire implements Serializable {

	public Commentaire() {
	}

	private Date dateCreation;
	private String appreciation;
	private Eleve eleve;

	private String anneeScolaire;
	private String professeur;

	private boolean associationSportive;

	private String autre;
	private Niveau niveau;

	//@javax.persistence.Embedded
	private List<Apsa> apsas;
	
	public List<Apsa> getApsas() {
		return apsas;
	}
	public void setApsas(List<Apsa> a) {
		apsas = a;
	}
	public void addApsa(Apsa apsa) {
		if(apsas == null) {
			apsas = new LinkedList<Apsa>();
		}
		apsas.add(apsa);
	}
	
        public Apsa getApsaForActivite(String activite) {
            if(apsas != null) {
                for(Apsa apsa : apsas) {
                    if(ficheeps.client.Utils.StringEquals(activite, apsa.getActivite())) {
                        return apsa;
                    }
                }
            }
            return null;
        }
        
	public void cleanupForSave() {
		if("<br>".equals(autre)) {
			autre = "";
		}
		if("<br>".equals(appreciation)) {
			appreciation = "";
		}
		
		if(apsas != null) {
			Iterator<Apsa> iter = apsas.iterator();
			while(iter.hasNext()) {
				Apsa apsa = iter.next();
				if(IsStringNullOrEmpty(apsa.getActivite()) && IsStringNullOrEmpty(apsa.getResultat())) {
					iter.remove();
				}
			}
		}
	}
	
	public Niveau getNiveau() {
		return niveau;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String getAppreciation() {
		return appreciation;
	}

	public void setAppreciation(String appreciation) {
		this.appreciation = appreciation;
	}

	public Eleve getEleve() {
		return eleve;
	}

	public void setEleve(Eleve eleve) {
		this.eleve = eleve;
	}

	public String getAnneeScolaire() {
		return anneeScolaire;
	}

	public void setAnneeScolaire(String anneeScolaire) {
		this.anneeScolaire = anneeScolaire;
	}

	public String getProfesseur() {
		return professeur;
	}

	public void setProfesseur(String professeur) {
		this.professeur = professeur;
	}

	public boolean isAssociationSportive() {
		return associationSportive;
	}

	public void setAssociationSportive(boolean associationSportive) {
		this.associationSportive = associationSportive;
	}

	public String getAutre() {
		return autre;
	}

	public void setAutre(String autre) {
		this.autre = autre;
	}

	public void setNiveau(Niveau niveau) {
		this.niveau = niveau;
	}

	/**
	 * Une representation de la moyenne:
	 *   - Peut afficher "erreur" ou "incomplete".
	 */
	public String getMoyenne() {
		String result = "";
		boolean incomplete = false;
		if(getApsas() != null) {
			int nbNotes = 0;
			double aggregator = 0;
			for(Apsa apsa: getApsas()) {
				String note = apsa.getResultat();
				try {
					note = note.replace(',', '.');
					double d = Double.valueOf(note);
					if(d>=0) {
						nbNotes++;
						aggregator += d;
					}
					else {
						incomplete = true;
					}
				}
				catch(Exception e) {
					incomplete = true;
				}
			}
			if(nbNotes>0) {
				NumberFormat fmt = NumberFormat.getFormat("##.##");
				result = fmt.format(aggregator / (double)nbNotes);
			}
		}
		return result + (incomplete ? " !?" : "");
	}
	
	public static String CurrentAnneeScolaire() {
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		int currentYear = c.get(Calendar.YEAR);
		int currentMonth = c.get(Calendar.MONTH);
		NumberFormat fmt = NumberFormat.getFormat("#");
		if (currentMonth < 8) {
			return "" + fmt.format(currentYear - 1) + "/" + fmt.format(currentYear);
		} else {
			return "" + fmt.format(currentYear) + "/" + fmt.format(currentYear + 1);
		}
	}

	public static enum Niveau {
		Sixieme, Cinquieme, Quatrieme, Troisieme, Seconde, Premiere, Terminale,NonSpecifie;

		public static Niveau GetNiveauForClasse(String classeName) {
			if(classeName == null || classeName.isEmpty()) {
				return NonSpecifie;
			}
			
			char c = classeName.charAt(0);
			if (c == '6') {
				return Sixieme;
			} else if (c == '5') {
				return Cinquieme;
			} else if (c == '4') {
				return Quatrieme;
			} else if (c == '3') {
				return Troisieme;
			} else if (c == '2') {
				return Seconde;
			} else if (c == '1') {
				return Premiere;
			}else if (c == 'T' || c == 't') {
				return Terminale;
			} else {
				return NonSpecifie;
			}
		}
	}
	
	
	private static boolean IsStringNullOrEmpty(String s) {
		if(s == null || s.trim().isEmpty()) {
			return true;
		}
		return false;
	}

	
	
	public static class NiveauComparator implements Comparator<Commentaire>
	{
		public NiveauComparator(boolean ascendent) {
			this.ascendent = ascendent;
		}
		
		private boolean ascendent = true;
		
		@Override
		public int compare(Commentaire o1, Commentaire o2) {
			Niveau n1 = o1.getNiveau();
			Niveau n2 = o2.getNiveau();
			if(n1 == n2) {
				//compare year "2009/2010"  "2010/2011"
				int i1 = ParseAlmostIntFromString(o1.getAnneeScolaire());
				int i2 = ParseAlmostIntFromString(o2.getAnneeScolaire());
				return ascendent ? (i1 -i2) :  (i2 - i1);
			}
			
			if(n1 == null) {
				return ascendent ? Integer.MAX_VALUE : Integer.MIN_VALUE;
			}
			if(n2 == null) {
				return ascendent ? Integer.MIN_VALUE : Integer.MAX_VALUE;
			}
			
			return ascendent ? n1.compareTo(n2) : n2.compareTo(n1);
		}
		
		
	    private static int ParseAlmostIntFromString(String s) {
	        if(s == null) {
	            return 0;
	        }
	        
	        s = s.trim();
	       
	        String subS = "";
	        for(int i=0;i<s.length();i++) {
	            char c = s.charAt(i);
	            if(Character.isDigit(c)) {
	                subS += c;
	            }
	            else {
	                break;
	            }
	        }
	        
	        if(subS.isEmpty()) {
	            return 0;
	        }
	        
	        try {
	            return Integer.parseInt(subS);
	        }
	        catch(Exception e) {
	            //discard quietly
	        }
	        return 0;
	    }
	}
	
}

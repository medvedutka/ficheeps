/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficheeps.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author cjalady
 */
public class EmailsDiff implements Serializable {
    String eleveId;
    String eleveClasse;
    String eleveName;
    
    String originalMails;
    String mailToImport;
    String newMails;
    
    List<String> keepedMails;
    List<String> addedMails;
    List<String> removedMails;
    

    public String getEleveId() {
        return eleveId;
    }
    public void setEleveId(String eleveId) {
        this.eleveId = eleveId;
    }
    public String getEleveName() {
        return eleveName;
    }
    public void setEleveName(String eleveName) {
        this.eleveName = eleveName;
    }

    public String getEleveClasse() {
        return eleveClasse;
    }

    public void setEleveClasse(String eleveClasse) {
        this.eleveClasse = eleveClasse;
    }
    

    public String getNewMails() {
        return newMails;
    }

    public void setNewMails(String newMails) {
        this.newMails = newMails;
    }
    
    public List<String> getAddedMails() {
        return addedMails;
    }
    public void setAddedMails(List<String> newMails) {
        this.addedMails = newMails;
    }

    public String getOriginalMails() {
        return originalMails;
    }

    public void setOriginalMails(String originalMails) {
        this.originalMails = originalMails;
    }

    public String getMailToImport() {
        return mailToImport;
    }

    public void setMailToImport(String mailToImport) {
        this.mailToImport = mailToImport;
    }

    public List<String> getKeepedMails() {
        return keepedMails;
    }

    public void setKeepedMails(List<String> keepedMails) {
        this.keepedMails = keepedMails;
    }

    public List<String> getRemovedMails() {
        return removedMails;
    }

    public void setRemovedMails(List<String> removedMails) {
        this.removedMails = removedMails;
    }
    
    public void appendAddedMail(String mail) {
        if(addedMails == null) {
            addedMails = new LinkedList<String>();
        }
        addedMails.add(mail);
    }    
    
    public void appendRemovedMail(String mail) {
        if(removedMails == null) {
            removedMails = new LinkedList<String>();
        }
        removedMails.add(mail);
    }
    
    public void appendKeepedMail(String mail) {
        if(keepedMails == null) {
            keepedMails = new LinkedList<String>();
        }
        keepedMails.add(mail);
    }
    
    public void appendMailToImport(String newMail) {
        if(mailToImport == null || mailToImport.isEmpty()) {
            mailToImport = newMail;
        }
        else {
            mailToImport = mailToImport + "," + newMail;
        }
    }
    
}

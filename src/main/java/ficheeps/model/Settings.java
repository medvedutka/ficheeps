package ficheeps.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Settings implements Serializable {

    public Settings() {
    }

    @javax.persistence.Id
    private String orid;

    public String getOrid() {
        return orid;
    }
    
    private boolean constraintApsaLabel = false;
    private List<String> apsaLabels = new LinkedList<String>();

    public boolean isConstraintApsaLabel() {
        return constraintApsaLabel;
    }
    public void setConstraintApsaLabel(boolean constraintApsaLabel) {
        this.constraintApsaLabel = constraintApsaLabel;
    }

    public List<String> getApsaLabels() {
        return apsaLabels;
    }
    public void setApsaLabels(List<String> apsaLabels) {
        this.apsaLabels = apsaLabels;
    }
    
    
    private Map<String,String> infosClassesGroupes = new HashMap<String, String>();
    public Map<String,String> getInfosClassesGroupes() {
        return infosClassesGroupes;
    }
    public void setInfosClassesGroupes(Map<String,String> infosClassesGroupes) {
        this.infosClassesGroupes = infosClassesGroupes;
    }
    public void putInfosClassesGroupes(String classOrGroupName, String info) {
        if(infosClassesGroupes == null) {
            infosClassesGroupes = new HashMap<String, String>();
        }
        infosClassesGroupes.put(classOrGroupName, info);
    }
    public void removeInfosClassesGroupes(String classOrGroupName) {
        if(infosClassesGroupes == null) {
            infosClassesGroupes = new HashMap<String, String>();
        }        
        infosClassesGroupes.remove(classOrGroupName);
    }
}

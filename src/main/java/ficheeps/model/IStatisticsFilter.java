package ficheeps.model;

import java.util.List;


public interface IStatisticsFilter {
    public boolean isNiveau();
    public void setNiveau(boolean niveau);
    public boolean isAnnee();
    public void setAnnee(boolean annee);
    public boolean isApsa();
    public void setApsa(boolean apsa);
    public List<String> getNiveaux();
    public void setNiveaux(List<String> niveaux);
    public List<String> getAnnees();
    public void setAnnees(List<String> annees);
    public List<String> getApsas();
    public void setApsas(List<String> apsas);
}

package ficheeps.model;

import java.io.Serializable;

public class Tuple<T,V> implements Serializable {

	public Tuple() {
	}
	
	public Tuple(T first, V second) {
		this.first = first;
		this.second = second;
	}
	
	private T first;
	private V second;
	public T getFirst() {
		return first;
	}

	public void setFirst(T first) {
		this.first = first;
	}

	public V getSecond() {
		return second;
	}

	public void setSecond(V second) {
		this.second = second;
	}
	
	
}
